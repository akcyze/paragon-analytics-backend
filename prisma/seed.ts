import {CreatureType, PrismaClient} from '../src/generated/client';
import creaturesJSON from './creatures.json';
import console from "console";

const prisma = new PrismaClient();

async function main() {
    const existed = await prisma.user.findFirst({
        where: {
            login: "bluefish"
        }
    });

    if (!existed) {
        await prisma.user.create({
            data: {
                login: "bluefish",
                email: "bluefish@gmail.com",
                password: "$2y$12$.jgxt8gdnvM.yW6ANWQTDOmgGNjWHk5Wo1vHCz1b02nn3xPqmcwbO",
                role: "User,Manager,Admin"
            }
        });

        console.log('Meta Admin Created');
    }

    const promises = [];
    creaturesJSON.creatures.forEach((creature) => {
        promises.push(new Promise<void>(async (resolve, reject) => {
            if (!await prisma.creature.findFirst({where: {name: creature}})) {

                await prisma.creature.create({
                    "data":
                        {
                            name: creature,
                            type: CreatureType.Minion,
                            buff: '{}',
                            description: '{}',
                            exp: '',
                            gold: '',
                            health: '',
                            icon: '',
                            respawnTime: '',
                            spawnTime: '',
                        }
                });

                console.log(`Creature ${creature} created`);

            }

            return resolve();
        }))
    });

    await Promise.all(promises);
}

main()
    .then(async () => {
        await prisma.$disconnect()
    })
    .catch(async (e) => {
        console.error(e)
        await prisma.$disconnect()
        process.exit(1)
    })