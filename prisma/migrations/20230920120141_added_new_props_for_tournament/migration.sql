-- CreateEnum
CREATE TYPE "BracketType" AS ENUM ('RoundRobin', 'SingleElimination');

-- AlterTable
ALTER TABLE "Tournament" ADD COLUMN     "bracketType" "BracketType" NOT NULL DEFAULT 'SingleElimination',
ADD COLUMN     "matchesCount" INTEGER NOT NULL DEFAULT 3;
