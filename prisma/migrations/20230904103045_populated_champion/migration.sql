-- AlterTable
ALTER TABLE "Champion" ADD COLUMN     "counterPicks" TEXT[],
ADD COLUMN     "role" "Role" NOT NULL DEFAULT 'Mid',
ADD COLUMN     "tier" TEXT NOT NULL DEFAULT 'B';
