-- AlterTable
ALTER TABLE "Tournament" ADD COLUMN     "maxRequests" INTEGER NOT NULL DEFAULT 40,
ADD COLUMN     "teamRequest" BOOLEAN NOT NULL DEFAULT true;
