/*
  Warnings:

  - Added the required column `role` to the `Build` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "Role" AS ENUM ('Jungle', 'Mid', 'Adc', 'Support', 'Top');

-- AlterTable
ALTER TABLE "Build" ADD COLUMN     "role" "Role" NOT NULL;
