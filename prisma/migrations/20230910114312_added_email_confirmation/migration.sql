-- AlterTable
ALTER TABLE "User" ADD COLUMN     "displayName" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "isEmailConfirmed" BOOLEAN NOT NULL DEFAULT false;
