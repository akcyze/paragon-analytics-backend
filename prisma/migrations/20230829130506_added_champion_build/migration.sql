-- CreateTable
CREATE TABLE "ChampionBuild" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "name" TEXT NOT NULL,
    "data" TEXT NOT NULL,

    CONSTRAINT "ChampionBuild_pkey" PRIMARY KEY ("id")
);
