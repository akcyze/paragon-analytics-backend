/*
  Warnings:

  - Added the required column `updatedAt` to the `Build` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Build" ADD COLUMN     "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
ADD COLUMN     "updatedAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP;

-- AlterTable
ALTER TABLE "User" ADD COLUMN     "discord" TEXT NOT NULL DEFAULT '',
ADD COLUMN     "gameRank" INTEGER NOT NULL DEFAULT 1,
ADD COLUMN     "telegram" TEXT NOT NULL DEFAULT '';
