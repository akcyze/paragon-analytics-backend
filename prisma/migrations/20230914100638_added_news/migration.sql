-- CreateTable
CREATE TABLE "News" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "icon" TEXT NOT NULL,
    "patch" TEXT NOT NULL,
    "champions" TEXT[],
    "items" TEXT[],
    "bugfixes" TEXT[],

    CONSTRAINT "News_pkey" PRIMARY KEY ("id")
);
