/*
  Warnings:

  - You are about to drop the column `skillsOrser` on the `Build` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Build" DROP COLUMN "skillsOrser",
ADD COLUMN     "skillsOrder" TEXT[];
