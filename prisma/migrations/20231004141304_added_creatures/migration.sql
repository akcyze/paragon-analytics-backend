-- CreateEnum
CREATE TYPE "CreatureType" AS ENUM ('Jungle', 'Minion', 'Tower', 'Core', 'Boss');

-- CreateTable
CREATE TABLE "Creature" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "icon" TEXT NOT NULL,
    "type" "CreatureType" NOT NULL,
    "health" INTEGER NOT NULL,
    "gold" TEXT NOT NULL,
    "exp" INTEGER NOT NULL,
    "spawnTime" INTEGER NOT NULL,
    "respawnTime" INTEGER NOT NULL,
    "buff" TEXT NOT NULL,

    CONSTRAINT "Creature_pkey" PRIMARY KEY ("id")
);
