/*
  Warnings:

  - You are about to drop the `ChampionBuild` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropTable
DROP TABLE "ChampionBuild";

-- CreateTable
CREATE TABLE "Champion" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "Champion_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Build" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,
    "championID" INTEGER NOT NULL,
    "creatorID" INTEGER NOT NULL,
    "items" TEXT[],
    "coreItems" TEXT[],
    "situationalItems" TEXT[],
    "skillsOrser" TEXT[],
    "karmas" TEXT[],

    CONSTRAINT "Build_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Build" ADD CONSTRAINT "Build_championID_fkey" FOREIGN KEY ("championID") REFERENCES "Champion"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Build" ADD CONSTRAINT "Build_creatorID_fkey" FOREIGN KEY ("creatorID") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
