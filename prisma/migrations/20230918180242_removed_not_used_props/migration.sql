/*
  Warnings:

  - You are about to drop the column `matches` on the `Tournament` table. All the data in the column will be lost.
  - You are about to drop the column `results` on the `Tournament` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Tournament" DROP COLUMN "matches",
DROP COLUMN "results";
