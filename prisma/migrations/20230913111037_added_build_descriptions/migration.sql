-- AlterTable
ALTER TABLE "Build" ADD COLUMN     "keyInsights" TEXT,
ADD COLUMN     "strengths" TEXT,
ADD COLUMN     "weaknesses" TEXT;
