/*
  Warnings:

  - The `requests` column on the `Tournament` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Tournament" DROP COLUMN "requests",
ADD COLUMN     "requests" TEXT[] DEFAULT ARRAY[]::TEXT[];
