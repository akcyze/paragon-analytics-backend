-- CreateEnum
CREATE TYPE "TournamentStatus" AS ENUM ('Pending', 'InProgress', 'Finished');

-- CreateTable
CREATE TABLE "Tournament" (
    "id" SERIAL NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "startedAt" TIMESTAMP(3) NOT NULL,
    "finishedAt" TIMESTAMP(3) NOT NULL,
    "name" TEXT NOT NULL,
    "description" TEXT NOT NULL,
    "prize" TEXT NOT NULL,
    "creatorID" INTEGER NOT NULL,
    "teams" TEXT NOT NULL,
    "bracket" TEXT NOT NULL,

    CONSTRAINT "Tournament_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "Tournament" ADD CONSTRAINT "Tournament_creatorID_fkey" FOREIGN KEY ("creatorID") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
