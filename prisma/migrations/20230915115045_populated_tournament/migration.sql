-- CreateEnum
CREATE TYPE "TournamentPickMode" AS ENUM ('Quick', 'Draft');

-- CreateEnum
CREATE TYPE "GameMode" AS ENUM ('DestroyCore', 'FirstBlood', 'FirstTower');

-- AlterTable
ALTER TABLE "Tournament" ADD COLUMN     "casters" TEXT[] DEFAULT ARRAY[]::TEXT[],
ADD COLUMN     "gameMode" "GameMode" NOT NULL DEFAULT 'DestroyCore',
ADD COLUMN     "map" TEXT,
ADD COLUMN     "matches" TEXT[] DEFAULT ARRAY[]::TEXT[],
ADD COLUMN     "pickMode" "TournamentPickMode" NOT NULL DEFAULT 'Draft',
ADD COLUMN     "results" TEXT,
ADD COLUMN     "slots" INTEGER NOT NULL DEFAULT 8,
ADD COLUMN     "teamSize" INTEGER NOT NULL DEFAULT 5,
ADD COLUMN     "videoLink" TEXT;
