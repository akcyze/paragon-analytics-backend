/*
  Warnings:

  - The `tier` column on the `Champion` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- AlterTable
ALTER TABLE "Champion" DROP COLUMN "tier",
ADD COLUMN     "tier" INTEGER NOT NULL DEFAULT 0;
