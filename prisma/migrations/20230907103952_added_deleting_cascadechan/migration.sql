-- DropForeignKey
ALTER TABLE "Build" DROP CONSTRAINT "Build_creatorID_fkey";

-- AddForeignKey
ALTER TABLE "Build" ADD CONSTRAINT "Build_creatorID_fkey" FOREIGN KEY ("creatorID") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
