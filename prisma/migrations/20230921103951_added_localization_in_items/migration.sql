-- AlterTable
ALTER TABLE "Item" ADD COLUMN     "displayName" TEXT,
ADD COLUMN     "effect" TEXT,
ADD COLUMN     "effectParams" TEXT;
