import { PrismaClient } from '../generated/client';
import { Injectable, OnModuleDestroy, OnModuleInit } from "@nestjs/common";

@Injectable()
export class PrismaService extends PrismaClient implements OnModuleInit, OnModuleDestroy {
    constructor() {
        super();
    }

    async onModuleInit(): Promise<void> {
        try {
            await this.$connect();
        } catch (error) {
            console.log(error)
        }
    }

    async onModuleDestroy(): Promise<void> {
        await this.$disconnect();
    }
}