
Object.defineProperty(exports, "__esModule", { value: true });

const {
  PrismaClientKnownRequestError,
  PrismaClientUnknownRequestError,
  PrismaClientRustPanicError,
  PrismaClientInitializationError,
  PrismaClientValidationError,
  NotFoundError,
  getPrismaClient,
  sqltag,
  empty,
  join,
  raw,
  Decimal,
  Debug,
  objectEnumValues,
  makeStrictEnum,
  Extensions,
  warnOnce,
  defineDmmfProperty,
  Public,
} = require('./runtime/library')


const Prisma = {}

exports.Prisma = Prisma
exports.$Enums = {}

/**
 * Prisma Client JS version: 5.3.1
 * Query Engine version: 61e140623197a131c2a6189271ffee05a7aa9a59
 */
Prisma.prismaVersion = {
  client: "5.3.1",
  engine: "61e140623197a131c2a6189271ffee05a7aa9a59"
}

Prisma.PrismaClientKnownRequestError = PrismaClientKnownRequestError;
Prisma.PrismaClientUnknownRequestError = PrismaClientUnknownRequestError
Prisma.PrismaClientRustPanicError = PrismaClientRustPanicError
Prisma.PrismaClientInitializationError = PrismaClientInitializationError
Prisma.PrismaClientValidationError = PrismaClientValidationError
Prisma.NotFoundError = NotFoundError
Prisma.Decimal = Decimal

/**
 * Re-export of sql-template-tag
 */
Prisma.sql = sqltag
Prisma.empty = empty
Prisma.join = join
Prisma.raw = raw
Prisma.validator = Public.validator

/**
* Extensions
*/
Prisma.getExtensionContext = Extensions.getExtensionContext
Prisma.defineExtension = Extensions.defineExtension

/**
 * Shorthand utilities for JSON filtering
 */
Prisma.DbNull = objectEnumValues.instances.DbNull
Prisma.JsonNull = objectEnumValues.instances.JsonNull
Prisma.AnyNull = objectEnumValues.instances.AnyNull

Prisma.NullTypes = {
  DbNull: objectEnumValues.classes.DbNull,
  JsonNull: objectEnumValues.classes.JsonNull,
  AnyNull: objectEnumValues.classes.AnyNull
}


  const path = require('path')

/**
 * Enums
 */
exports.Prisma.TransactionIsolationLevel = makeStrictEnum({
  ReadUncommitted: 'ReadUncommitted',
  ReadCommitted: 'ReadCommitted',
  RepeatableRead: 'RepeatableRead',
  Serializable: 'Serializable'
});

exports.Prisma.UserScalarFieldEnum = {
  id: 'id',
  login: 'login',
  displayName: 'displayName',
  discord: 'discord',
  telegram: 'telegram',
  gameRank: 'gameRank',
  email: 'email',
  isEmailConfirmed: 'isEmailConfirmed',
  password: 'password',
  icon: 'icon',
  role: 'role',
  refreshToken: 'refreshToken',
  isDisabled: 'isDisabled',
  kills: 'kills',
  deaths: 'deaths',
  assists: 'assists',
  tournamentWins: 'tournamentWins',
  gameWins: 'gameWins',
  gameLosed: 'gameLosed',
  teamID: 'teamID'
};

exports.Prisma.RegistrationTokenScalarFieldEnum = {
  id: 'id',
  token: 'token'
};

exports.Prisma.ChampionScalarFieldEnum = {
  id: 'id',
  name: 'name',
  role: 'role',
  tier: 'tier',
  counterPicks: 'counterPicks'
};

exports.Prisma.BuildScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  name: 'name',
  role: 'role',
  championID: 'championID',
  creatorID: 'creatorID',
  items: 'items',
  coreItems: 'coreItems',
  situationalItems: 'situationalItems',
  skillsOrder: 'skillsOrder',
  karmas: 'karmas',
  keyInsights: 'keyInsights',
  strengths: 'strengths',
  weaknesses: 'weaknesses'
};

exports.Prisma.LikesScalarFieldEnum = {
  userID: 'userID',
  buildID: 'buildID',
  assignedAt: 'assignedAt'
};

exports.Prisma.DisLikesScalarFieldEnum = {
  userID: 'userID',
  buildID: 'buildID',
  assignedAt: 'assignedAt'
};

exports.Prisma.TournamentScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  startedAt: 'startedAt',
  finishedAt: 'finishedAt',
  name: 'name',
  description: 'description',
  prize: 'prize',
  icon: 'icon',
  map: 'map',
  pickMode: 'pickMode',
  gameMode: 'gameMode',
  status: 'status',
  teamSize: 'teamSize',
  slots: 'slots',
  teamRequest: 'teamRequest',
  maxRequests: 'maxRequests',
  casters: 'casters',
  videoLink: 'videoLink',
  creatorID: 'creatorID',
  teams: 'teams',
  requests: 'requests',
  bracket: 'bracket',
  bracketType: 'bracketType',
  matchesCount: 'matchesCount',
  results: 'results'
};

exports.Prisma.ItemScalarFieldEnum = {
  id: 'id',
  name: 'name',
  displayName: 'displayName',
  type: 'type',
  price: 'price',
  params: 'params',
  effect: 'effect',
  effectParams: 'effectParams'
};

exports.Prisma.LogScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  type: 'type',
  message: 'message'
};

exports.Prisma.TeamScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  name: 'name',
  description: 'description',
  icon: 'icon',
  rating: 'rating',
  tournamentWins: 'tournamentWins',
  gameWins: 'gameWins',
  gameLosed: 'gameLosed',
  ownerID: 'ownerID',
  requests: 'requests'
};

exports.Prisma.NewsScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  name: 'name',
  description: 'description',
  icon: 'icon',
  patch: 'patch',
  champions: 'champions',
  items: 'items',
  bugfixes: 'bugfixes'
};

exports.Prisma.CreatureScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  name: 'name',
  description: 'description',
  icon: 'icon',
  type: 'type',
  health: 'health',
  gold: 'gold',
  exp: 'exp',
  spawnTime: 'spawnTime',
  respawnTime: 'respawnTime',
  buff: 'buff'
};

exports.Prisma.SortOrder = {
  asc: 'asc',
  desc: 'desc'
};

exports.Prisma.QueryMode = {
  default: 'default',
  insensitive: 'insensitive'
};

exports.Prisma.NullsOrder = {
  first: 'first',
  last: 'last'
};
exports.Role = exports.$Enums.Role = {
  Jungle: 'Jungle',
  Mid: 'Mid',
  Adc: 'Adc',
  Support: 'Support',
  Top: 'Top'
};

exports.TournamentPickMode = exports.$Enums.TournamentPickMode = {
  Quick: 'Quick',
  Draft: 'Draft'
};

exports.GameMode = exports.$Enums.GameMode = {
  DestroyCore: 'DestroyCore',
  FirstBlood: 'FirstBlood',
  FirstTower: 'FirstTower'
};

exports.TournamentStatus = exports.$Enums.TournamentStatus = {
  Pending: 'Pending',
  InProgress: 'InProgress',
  Finished: 'Finished'
};

exports.BracketType = exports.$Enums.BracketType = {
  RoundRobin: 'RoundRobin',
  SingleElimination: 'SingleElimination'
};

exports.ItemType = exports.$Enums.ItemType = {
  Basic: 'Basic',
  Rare: 'Rare',
  Epic: 'Epic'
};

exports.LogType = exports.$Enums.LogType = {
  Info: 'Info',
  Warning: 'Warning',
  Error: 'Error'
};

exports.CreatureType = exports.$Enums.CreatureType = {
  Jungle: 'Jungle',
  Minion: 'Minion',
  Tower: 'Tower',
  Core: 'Core',
  Boss: 'Boss'
};

exports.Prisma.ModelName = {
  User: 'User',
  RegistrationToken: 'RegistrationToken',
  Champion: 'Champion',
  Build: 'Build',
  Likes: 'Likes',
  DisLikes: 'DisLikes',
  Tournament: 'Tournament',
  Item: 'Item',
  Log: 'Log',
  Team: 'Team',
  News: 'News',
  Creature: 'Creature'
};
/**
 * Create the Client
 */
const config = {
  "generator": {
    "name": "client",
    "provider": {
      "fromEnvVar": null,
      "value": "prisma-client-js"
    },
    "output": {
      "value": "D:\\Work\\paragon-analytics-backend\\src\\generated\\client",
      "fromEnvVar": null
    },
    "config": {
      "engineType": "library"
    },
    "binaryTargets": [
      {
        "fromEnvVar": null,
        "value": "windows",
        "native": true
      }
    ],
    "previewFeatures": [],
    "isCustomOutput": true
  },
  "relativeEnvPaths": {
    "rootEnvPath": "../../../.env",
    "schemaEnvPath": "../../../.env"
  },
  "relativePath": "../../../prisma",
  "clientVersion": "5.3.1",
  "engineVersion": "61e140623197a131c2a6189271ffee05a7aa9a59",
  "datasourceNames": [
    "db"
  ],
  "activeProvider": "postgresql",
  "postinstall": false,
  "inlineDatasources": {
    "db": {
      "url": {
        "fromEnvVar": "DATABASE_URL",
        "value": null
      }
    }
  },
  "inlineSchema": "Ly8gVGhpcyBpcyB5b3VyIFByaXNtYSBzY2hlbWEgZmlsZSwNCi8vIGxlYXJuIG1vcmUgYWJvdXQgaXQgaW4gdGhlIGRvY3M6IGh0dHBzOi8vcHJpcy5seS9kL3ByaXNtYS1zY2hlbWENCg0KZ2VuZXJhdG9yIGNsaWVudCB7DQogICAgcHJvdmlkZXIgPSAicHJpc21hLWNsaWVudC1qcyINCiAgICBvdXRwdXQgICA9ICIuLi9zcmMvZ2VuZXJhdGVkL2NsaWVudCINCn0NCg0KZGF0YXNvdXJjZSBkYiB7DQogICAgcHJvdmlkZXIgPSAicG9zdGdyZXNxbCINCiAgICB1cmwgICAgICA9IGVudigiREFUQUJBU0VfVVJMIikNCn0NCg0KbW9kZWwgVXNlciB7DQogICAgaWQgICAgICAgICAgICAgICBJbnQgICAgICAgICAgQGlkIEBkZWZhdWx0KGF1dG9pbmNyZW1lbnQoKSkNCiAgICBsb2dpbiAgICAgICAgICAgIFN0cmluZyAgICAgICBAdW5pcXVlDQogICAgZGlzcGxheU5hbWUgICAgICBTdHJpbmcgICAgICAgQGRlZmF1bHQoIiIpDQogICAgZGlzY29yZCAgICAgICAgICBTdHJpbmcgICAgICAgQGRlZmF1bHQoIiIpDQogICAgdGVsZWdyYW0gICAgICAgICBTdHJpbmcgICAgICAgQGRlZmF1bHQoIiIpDQogICAgZ2FtZVJhbmsgICAgICAgICBJbnQgICAgICAgICAgQGRlZmF1bHQoMSkNCiAgICBlbWFpbCAgICAgICAgICAgIFN0cmluZyAgICAgICBAdW5pcXVlDQogICAgaXNFbWFpbENvbmZpcm1lZCBCb29sZWFuICAgICAgQGRlZmF1bHQoZmFsc2UpDQogICAgcGFzc3dvcmQgICAgICAgICBTdHJpbmcNCiAgICBpY29uICAgICAgICAgICAgIFN0cmluZyAgICAgICBAZGVmYXVsdCgibm9uZSIpDQogICAgcm9sZSAgICAgICAgICAgICBTdHJpbmcNCiAgICByZWZyZXNoVG9rZW4gICAgIFN0cmluZyAgICAgICBAZGVmYXVsdCgiIikNCiAgICBpc0Rpc2FibGVkICAgICAgIEJvb2xlYW4gICAgICBAZGVmYXVsdChmYWxzZSkNCiAgICBraWxscyAgICAgICAgICAgIEludCAgICAgICAgICBAZGVmYXVsdCgwKQ0KICAgIGRlYXRocyAgICAgICAgICAgSW50ICAgICAgICAgIEBkZWZhdWx0KDApDQogICAgYXNzaXN0cyAgICAgICAgICBJbnQgICAgICAgICAgQGRlZmF1bHQoMCkNCiAgICB0b3VybmFtZW50V2lucyAgIEludFtdICAgICAgICBAZGVmYXVsdChbXSkNCiAgICBnYW1lV2lucyAgICAgICAgIEludCAgICAgICAgICBAZGVmYXVsdCgwKQ0KICAgIGdhbWVMb3NlZCAgICAgICAgSW50ICAgICAgICAgIEBkZWZhdWx0KDApDQogICAgdGVhbUlEICAgICAgICAgICBJbnQ/DQogICAgdGVhbSAgICAgICAgICAgICBUZWFtPyAgICAgICAgQHJlbGF0aW9uKG5hbWU6ICJUZWFtTWVtYmVyIiwgZmllbGRzOiBbdGVhbUlEXSwgcmVmZXJlbmNlczogW2lkXSkNCiAgICBvd25lZFRlYW0gICAgICAgIFRlYW0/ICAgICAgICBAcmVsYXRpb24oIk93bmVkVGVhbSIpDQogICAgYnVpbGRzICAgICAgICAgICBCdWlsZFtdDQogICAgdG91cm5hbWVudHMgICAgICBUb3VybmFtZW50W10NCiAgICBsaWtlc0J1aWxkcyAgICAgIExpa2VzW10NCiAgICBkaXNsaWtlc0J1aWxkcyAgIERpc0xpa2VzW10NCn0NCg0KbW9kZWwgUmVnaXN0cmF0aW9uVG9rZW4gew0KICAgIGlkICAgIEludCAgICBAaWQgQGRlZmF1bHQoYXV0b2luY3JlbWVudCgpKQ0KICAgIHRva2VuIFN0cmluZw0KfQ0KDQptb2RlbCBDaGFtcGlvbiB7DQogICAgaWQgICAgICAgICAgIEludCAgICAgIEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpDQogICAgbmFtZSAgICAgICAgIFN0cmluZw0KICAgIHJvbGUgICAgICAgICBSb2xlICAgICBAZGVmYXVsdChNaWQpDQogICAgdGllciAgICAgICAgIEludCAgICAgIEBkZWZhdWx0KDApDQogICAgY291bnRlclBpY2tzIFN0cmluZ1tdDQogICAgYnVpbGRzICAgICAgIEJ1aWxkW10NCn0NCg0KbW9kZWwgQnVpbGQgew0KICAgIGlkICAgICAgICAgICAgICAgSW50ICAgICAgICBAaWQgQGRlZmF1bHQoYXV0b2luY3JlbWVudCgpKQ0KICAgIGNyZWF0ZWRBdCAgICAgICAgRGF0ZVRpbWUgICBAZGVmYXVsdChub3coKSkNCiAgICB1cGRhdGVkQXQgICAgICAgIERhdGVUaW1lICAgQHVwZGF0ZWRBdCgpDQogICAgbmFtZSAgICAgICAgICAgICBTdHJpbmcNCiAgICByb2xlICAgICAgICAgICAgIFJvbGUNCiAgICBjaGFtcGlvbiAgICAgICAgIENoYW1waW9uICAgQHJlbGF0aW9uKGZpZWxkczogW2NoYW1waW9uSURdLCByZWZlcmVuY2VzOiBbaWRdKQ0KICAgIGNoYW1waW9uSUQgICAgICAgSW50DQogICAgY3JlYXRvciAgICAgICAgICBVc2VyICAgICAgIEByZWxhdGlvbihmaWVsZHM6IFtjcmVhdG9ySURdLCByZWZlcmVuY2VzOiBbaWRdLCBvbkRlbGV0ZTogQ2FzY2FkZSkNCiAgICBjcmVhdG9ySUQgICAgICAgIEludA0KICAgIGl0ZW1zICAgICAgICAgICAgU3RyaW5nW10NCiAgICBjb3JlSXRlbXMgICAgICAgIFN0cmluZ1tdDQogICAgc2l0dWF0aW9uYWxJdGVtcyBTdHJpbmdbXQ0KICAgIHNraWxsc09yZGVyICAgICAgU3RyaW5nW10NCiAgICBrYXJtYXMgICAgICAgICAgIFN0cmluZ1tdDQogICAga2V5SW5zaWdodHMgICAgICBTdHJpbmc/DQogICAgc3RyZW5ndGhzICAgICAgICBTdHJpbmc/DQogICAgd2Vha25lc3NlcyAgICAgICBTdHJpbmc/DQogICAgTGlrZXMgICAgICAgICAgICBMaWtlc1tdDQogICAgRGlzTGlrZXMgICAgICAgICBEaXNMaWtlc1tdDQp9DQoNCm1vZGVsIExpa2VzIHsNCiAgICB1c2VyICAgICAgIFVzZXIgICAgIEByZWxhdGlvbihmaWVsZHM6IFt1c2VySURdLCByZWZlcmVuY2VzOiBbaWRdKQ0KICAgIHVzZXJJRCAgICAgSW50DQogICAgYnVpbGQgICAgICBCdWlsZCAgICBAcmVsYXRpb24oZmllbGRzOiBbYnVpbGRJRF0sIHJlZmVyZW5jZXM6IFtpZF0pDQogICAgYnVpbGRJRCAgICBJbnQNCiAgICBhc3NpZ25lZEF0IERhdGVUaW1lIEBkZWZhdWx0KG5vdygpKQ0KDQogICAgQEBpZChbdXNlcklELCBidWlsZElEXSkNCn0NCg0KbW9kZWwgRGlzTGlrZXMgew0KICAgIHVzZXIgICAgICAgVXNlciAgICAgQHJlbGF0aW9uKGZpZWxkczogW3VzZXJJRF0sIHJlZmVyZW5jZXM6IFtpZF0pDQogICAgdXNlcklEICAgICBJbnQNCiAgICBidWlsZCAgICAgIEJ1aWxkICAgIEByZWxhdGlvbihmaWVsZHM6IFtidWlsZElEXSwgcmVmZXJlbmNlczogW2lkXSkNCiAgICBidWlsZElEICAgIEludA0KICAgIGFzc2lnbmVkQXQgRGF0ZVRpbWUgQGRlZmF1bHQobm93KCkpDQoNCiAgICBAQGlkKFt1c2VySUQsIGJ1aWxkSURdKQ0KfQ0KDQptb2RlbCBUb3VybmFtZW50IHsNCiAgICBpZCAgICAgICAgICAgSW50ICAgICAgICAgICAgICAgIEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpDQogICAgY3JlYXRlZEF0ICAgIERhdGVUaW1lICAgICAgICAgICBAZGVmYXVsdChub3coKSkNCiAgICB1cGRhdGVkQXQgICAgRGF0ZVRpbWUgICAgICAgICAgIEB1cGRhdGVkQXQoKQ0KICAgIHN0YXJ0ZWRBdCAgICBEYXRlVGltZQ0KICAgIGZpbmlzaGVkQXQgICBEYXRlVGltZQ0KICAgIG5hbWUgICAgICAgICBTdHJpbmcNCiAgICBkZXNjcmlwdGlvbiAgU3RyaW5nDQogICAgcHJpemUgICAgICAgIFN0cmluZw0KICAgIGljb24gICAgICAgICBTdHJpbmc/DQogICAgbWFwICAgICAgICAgIFN0cmluZz8NCiAgICBwaWNrTW9kZSAgICAgVG91cm5hbWVudFBpY2tNb2RlIEBkZWZhdWx0KERyYWZ0KQ0KICAgIGdhbWVNb2RlICAgICBHYW1lTW9kZSAgICAgICAgICAgQGRlZmF1bHQoRGVzdHJveUNvcmUpDQogICAgc3RhdHVzICAgICAgIFRvdXJuYW1lbnRTdGF0dXMgICBAZGVmYXVsdChQZW5kaW5nKQ0KICAgIHRlYW1TaXplICAgICBJbnQgICAgICAgICAgICAgICAgQGRlZmF1bHQoNSkNCiAgICBzbG90cyAgICAgICAgSW50ICAgICAgICAgICAgICAgIEBkZWZhdWx0KDgpDQogICAgdGVhbVJlcXVlc3QgIEJvb2xlYW4gICAgICAgICAgICBAZGVmYXVsdCh0cnVlKQ0KICAgIG1heFJlcXVlc3RzICBJbnQgICAgICAgICAgICAgICAgQGRlZmF1bHQoNDApDQogICAgY2FzdGVycyAgICAgIFN0cmluZ1tdICAgICAgICAgICBAZGVmYXVsdChbXSkNCiAgICB2aWRlb0xpbmsgICAgU3RyaW5nPw0KICAgIGNyZWF0b3IgICAgICBVc2VyICAgICAgICAgICAgICAgQHJlbGF0aW9uKGZpZWxkczogW2NyZWF0b3JJRF0sIHJlZmVyZW5jZXM6IFtpZF0sIG9uRGVsZXRlOiBDYXNjYWRlKQ0KICAgIGNyZWF0b3JJRCAgICBJbnQNCiAgICB0ZWFtcyAgICAgICAgU3RyaW5nDQogICAgcmVxdWVzdHMgICAgIFN0cmluZ1tdICAgICAgICAgICBAZGVmYXVsdChbXSkNCiAgICBicmFja2V0ICAgICAgU3RyaW5nPw0KICAgIGJyYWNrZXRUeXBlICBCcmFja2V0VHlwZSAgICAgICAgQGRlZmF1bHQoU2luZ2xlRWxpbWluYXRpb24pDQogICAgbWF0Y2hlc0NvdW50IEludCAgICAgICAgICAgICAgICBAZGVmYXVsdCgzKQ0KICAgIHJlc3VsdHMgICAgICBTdHJpbmc/DQp9DQoNCm1vZGVsIEl0ZW0gew0KICAgIGlkICAgICAgICAgICBJbnQgICAgICBAaWQgQGRlZmF1bHQoYXV0b2luY3JlbWVudCgpKQ0KICAgIG5hbWUgICAgICAgICBTdHJpbmcNCiAgICBkaXNwbGF5TmFtZSAgU3RyaW5nPw0KICAgIHR5cGUgICAgICAgICBJdGVtVHlwZQ0KICAgIHByaWNlICAgICAgICBJbnQNCiAgICBwYXJhbXMgICAgICAgU3RyaW5nDQogICAgZWZmZWN0ICAgICAgIFN0cmluZz8NCiAgICBlZmZlY3RQYXJhbXMgU3RyaW5nPw0KfQ0KDQptb2RlbCBMb2cgew0KICAgIGlkICAgICAgICBJbnQgICAgICBAaWQgQGRlZmF1bHQoYXV0b2luY3JlbWVudCgpKQ0KICAgIGNyZWF0ZWRBdCBEYXRlVGltZSBAZGVmYXVsdChub3coKSkNCiAgICB0eXBlICAgICAgTG9nVHlwZQ0KICAgIG1lc3NhZ2UgICBTdHJpbmcNCn0NCg0KbW9kZWwgVGVhbSB7DQogICAgaWQgICAgICAgICAgICAgSW50ICAgICAgQGlkIEBkZWZhdWx0KGF1dG9pbmNyZW1lbnQoKSkNCiAgICBjcmVhdGVkQXQgICAgICBEYXRlVGltZSBAZGVmYXVsdChub3coKSkNCiAgICBuYW1lICAgICAgICAgICBTdHJpbmcNCiAgICBkZXNjcmlwdGlvbiAgICBTdHJpbmcNCiAgICBpY29uICAgICAgICAgICBTdHJpbmcNCiAgICByYXRpbmcgICAgICAgICBEZWNpbWFsICBAZGVmYXVsdCgwKQ0KICAgIHRvdXJuYW1lbnRXaW5zIEludFtdICAgIEBkZWZhdWx0KFtdKQ0KICAgIGdhbWVXaW5zICAgICAgIEludCAgICAgIEBkZWZhdWx0KDApDQogICAgZ2FtZUxvc2VkICAgICAgSW50ICAgICAgQGRlZmF1bHQoMCkNCiAgICBvd25lcklEICAgICAgICBJbnQgICAgICBAdW5pcXVlDQogICAgb3duZXIgICAgICAgICAgVXNlciAgICAgQHJlbGF0aW9uKCJPd25lZFRlYW0iLCBmaWVsZHM6IFtvd25lcklEXSwgcmVmZXJlbmNlczogW2lkXSkNCiAgICBtZW1iZXJzICAgICAgICBVc2VyW10gICBAcmVsYXRpb24oIlRlYW1NZW1iZXIiKQ0KICAgIHJlcXVlc3RzICAgICAgIFN0cmluZ1tdDQp9DQoNCm1vZGVsIE5ld3Mgew0KICAgIGlkICAgICAgICAgIEludCAgICAgIEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpDQogICAgY3JlYXRlZEF0ICAgRGF0ZVRpbWUgQGRlZmF1bHQobm93KCkpDQogICAgbmFtZSAgICAgICAgU3RyaW5nDQogICAgZGVzY3JpcHRpb24gU3RyaW5nDQogICAgaWNvbiAgICAgICAgU3RyaW5nDQogICAgcGF0Y2ggICAgICAgU3RyaW5nDQogICAgY2hhbXBpb25zICAgU3RyaW5nW10NCiAgICBpdGVtcyAgICAgICBTdHJpbmdbXQ0KICAgIGJ1Z2ZpeGVzICAgIFN0cmluZw0KfQ0KDQptb2RlbCBDcmVhdHVyZSB7DQogICAgaWQgICAgICAgICAgSW50ICAgICAgICAgIEBpZCBAZGVmYXVsdChhdXRvaW5jcmVtZW50KCkpDQogICAgY3JlYXRlZEF0ICAgRGF0ZVRpbWUgICAgIEBkZWZhdWx0KG5vdygpKQ0KICAgIG5hbWUgICAgICAgIFN0cmluZw0KICAgIGRlc2NyaXB0aW9uIFN0cmluZw0KICAgIGljb24gICAgICAgIFN0cmluZw0KICAgIHR5cGUgICAgICAgIENyZWF0dXJlVHlwZQ0KICAgIGhlYWx0aCAgICAgIFN0cmluZw0KICAgIGdvbGQgICAgICAgIFN0cmluZw0KICAgIGV4cCAgICAgICAgIFN0cmluZw0KICAgIHNwYXduVGltZSAgIFN0cmluZw0KICAgIHJlc3Bhd25UaW1lIFN0cmluZw0KICAgIGJ1ZmYgICAgICAgIFN0cmluZw0KfQ0KDQplbnVtIENyZWF0dXJlVHlwZSB7DQogICAgSnVuZ2xlDQogICAgTWluaW9uDQogICAgVG93ZXINCiAgICBDb3JlDQogICAgQm9zcw0KfQ0KDQplbnVtIFJvbGUgew0KICAgIEp1bmdsZQ0KICAgIE1pZA0KICAgIEFkYw0KICAgIFN1cHBvcnQNCiAgICBUb3ANCn0NCg0KZW51bSBJdGVtVHlwZSB7DQogICAgQmFzaWMNCiAgICBSYXJlDQogICAgRXBpYw0KfQ0KDQplbnVtIFRvdXJuYW1lbnRTdGF0dXMgew0KICAgIFBlbmRpbmcNCiAgICBJblByb2dyZXNzDQogICAgRmluaXNoZWQNCn0NCg0KZW51bSBMb2dUeXBlIHsNCiAgICBJbmZvDQogICAgV2FybmluZw0KICAgIEVycm9yDQp9DQoNCmVudW0gVG91cm5hbWVudFBpY2tNb2RlIHsNCiAgICBRdWljaw0KICAgIERyYWZ0DQp9DQoNCmVudW0gR2FtZU1vZGUgew0KICAgIERlc3Ryb3lDb3JlDQogICAgRmlyc3RCbG9vZA0KICAgIEZpcnN0VG93ZXINCn0NCg0KZW51bSBCcmFja2V0VHlwZSB7DQogICAgUm91bmRSb2Jpbg0KICAgIFNpbmdsZUVsaW1pbmF0aW9uDQp9DQo=",
  "inlineSchemaHash": "5ff0f50b65deb2941ecf7ec314c127030597bd9cada27c8263032827a885235f",
  "noEngine": false
}

const fs = require('fs')

config.dirname = __dirname
if (!fs.existsSync(path.join(__dirname, 'schema.prisma'))) {
  const alternativePaths = [
    "src/generated/client",
    "generated/client",
  ]
  
  const alternativePath = alternativePaths.find((altPath) => {
    return fs.existsSync(path.join(process.cwd(), altPath, 'schema.prisma'))
  }) ?? alternativePaths[0]

  config.dirname = path.join(process.cwd(), alternativePath)
  config.isBundled = true
}

config.runtimeDataModel = JSON.parse("{\"models\":{\"User\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"login\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":true,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"displayName\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"discord\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"telegram\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gameRank\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":1,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"email\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":true,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isEmailConfirmed\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Boolean\",\"default\":false,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"password\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"icon\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"none\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"role\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"refreshToken\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":\"\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"isDisabled\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Boolean\",\"default\":false,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"kills\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"deaths\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"assists\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"tournamentWins\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gameWins\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gameLosed\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"teamID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"team\",\"kind\":\"object\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Team\",\"relationName\":\"TeamMember\",\"relationFromFields\":[\"teamID\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"ownedTeam\",\"kind\":\"object\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Team\",\"relationName\":\"OwnedTeam\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"builds\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Build\",\"relationName\":\"BuildToUser\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"tournaments\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Tournament\",\"relationName\":\"TournamentToUser\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"likesBuilds\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Likes\",\"relationName\":\"LikesToUser\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"dislikesBuilds\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DisLikes\",\"relationName\":\"DisLikesToUser\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"RegistrationToken\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"token\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Champion\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"role\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Role\",\"default\":\"Mid\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"tier\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"counterPicks\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"builds\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Build\",\"relationName\":\"BuildToChampion\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Build\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"updatedAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DateTime\",\"isGenerated\":false,\"isUpdatedAt\":true},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"role\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Role\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"champion\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Champion\",\"relationName\":\"BuildToChampion\",\"relationFromFields\":[\"championID\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"championID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"creator\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"BuildToUser\",\"relationFromFields\":[\"creatorID\"],\"relationToFields\":[\"id\"],\"relationOnDelete\":\"Cascade\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"creatorID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"items\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"coreItems\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"situationalItems\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"skillsOrder\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"karmas\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"keyInsights\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"strengths\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"weaknesses\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"Likes\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Likes\",\"relationName\":\"BuildToLikes\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"DisLikes\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DisLikes\",\"relationName\":\"BuildToDisLikes\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Likes\":{\"dbName\":null,\"fields\":[{\"name\":\"user\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"LikesToUser\",\"relationFromFields\":[\"userID\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"userID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"build\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Build\",\"relationName\":\"BuildToLikes\",\"relationFromFields\":[\"buildID\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"buildID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"assignedAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":{\"name\":null,\"fields\":[\"userID\",\"buildID\"]},\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"DisLikes\":{\"dbName\":null,\"fields\":[{\"name\":\"user\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"DisLikesToUser\",\"relationFromFields\":[\"userID\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"userID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"build\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Build\",\"relationName\":\"BuildToDisLikes\",\"relationFromFields\":[\"buildID\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"buildID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"assignedAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":{\"name\":null,\"fields\":[\"userID\",\"buildID\"]},\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Tournament\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"updatedAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DateTime\",\"isGenerated\":false,\"isUpdatedAt\":true},{\"name\":\"startedAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DateTime\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"finishedAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"DateTime\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"description\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"prize\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"icon\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"map\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"pickMode\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"TournamentPickMode\",\"default\":\"Draft\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gameMode\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"GameMode\",\"default\":\"DestroyCore\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"status\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"TournamentStatus\",\"default\":\"Pending\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"teamSize\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":5,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"slots\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":8,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"teamRequest\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Boolean\",\"default\":true,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"maxRequests\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":40,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"casters\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"videoLink\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"creator\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"TournamentToUser\",\"relationFromFields\":[\"creatorID\"],\"relationToFields\":[\"id\"],\"relationOnDelete\":\"Cascade\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"creatorID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"teams\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"requests\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"String\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"bracket\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"bracketType\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"BracketType\",\"default\":\"SingleElimination\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"matchesCount\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":3,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"results\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Item\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"displayName\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"type\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"ItemType\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"price\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"params\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"effect\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"effectParams\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":false,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Log\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"type\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"LogType\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"message\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Team\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"description\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"icon\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"rating\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Decimal\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"tournamentWins\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gameWins\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gameLosed\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":0,\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"ownerID\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":true,\"isId\":false,\"isReadOnly\":true,\"hasDefaultValue\":false,\"type\":\"Int\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"owner\",\"kind\":\"object\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"OwnedTeam\",\"relationFromFields\":[\"ownerID\"],\"relationToFields\":[\"id\"],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"members\",\"kind\":\"object\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"User\",\"relationName\":\"TeamMember\",\"relationFromFields\":[],\"relationToFields\":[],\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"requests\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"News\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"description\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"icon\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"patch\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"champions\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"items\",\"kind\":\"scalar\",\"isList\":true,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"bugfixes\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false},\"Creature\":{\"dbName\":null,\"fields\":[{\"name\":\"id\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":true,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"Int\",\"default\":{\"name\":\"autoincrement\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"createdAt\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":true,\"type\":\"DateTime\",\"default\":{\"name\":\"now\",\"args\":[]},\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"name\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"description\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"icon\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"type\",\"kind\":\"enum\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"CreatureType\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"health\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"gold\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"exp\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"spawnTime\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"respawnTime\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false},{\"name\":\"buff\",\"kind\":\"scalar\",\"isList\":false,\"isRequired\":true,\"isUnique\":false,\"isId\":false,\"isReadOnly\":false,\"hasDefaultValue\":false,\"type\":\"String\",\"isGenerated\":false,\"isUpdatedAt\":false}],\"primaryKey\":null,\"uniqueFields\":[],\"uniqueIndexes\":[],\"isGenerated\":false}},\"enums\":{\"CreatureType\":{\"values\":[{\"name\":\"Jungle\",\"dbName\":null},{\"name\":\"Minion\",\"dbName\":null},{\"name\":\"Tower\",\"dbName\":null},{\"name\":\"Core\",\"dbName\":null},{\"name\":\"Boss\",\"dbName\":null}],\"dbName\":null},\"Role\":{\"values\":[{\"name\":\"Jungle\",\"dbName\":null},{\"name\":\"Mid\",\"dbName\":null},{\"name\":\"Adc\",\"dbName\":null},{\"name\":\"Support\",\"dbName\":null},{\"name\":\"Top\",\"dbName\":null}],\"dbName\":null},\"ItemType\":{\"values\":[{\"name\":\"Basic\",\"dbName\":null},{\"name\":\"Rare\",\"dbName\":null},{\"name\":\"Epic\",\"dbName\":null}],\"dbName\":null},\"TournamentStatus\":{\"values\":[{\"name\":\"Pending\",\"dbName\":null},{\"name\":\"InProgress\",\"dbName\":null},{\"name\":\"Finished\",\"dbName\":null}],\"dbName\":null},\"LogType\":{\"values\":[{\"name\":\"Info\",\"dbName\":null},{\"name\":\"Warning\",\"dbName\":null},{\"name\":\"Error\",\"dbName\":null}],\"dbName\":null},\"TournamentPickMode\":{\"values\":[{\"name\":\"Quick\",\"dbName\":null},{\"name\":\"Draft\",\"dbName\":null}],\"dbName\":null},\"GameMode\":{\"values\":[{\"name\":\"DestroyCore\",\"dbName\":null},{\"name\":\"FirstBlood\",\"dbName\":null},{\"name\":\"FirstTower\",\"dbName\":null}],\"dbName\":null},\"BracketType\":{\"values\":[{\"name\":\"RoundRobin\",\"dbName\":null},{\"name\":\"SingleElimination\",\"dbName\":null}],\"dbName\":null}},\"types\":{}}")
defineDmmfProperty(exports.Prisma, config.runtimeDataModel)



const { warnEnvConflicts } = require('./runtime/library')

warnEnvConflicts({
    rootEnvPath: config.relativeEnvPaths.rootEnvPath && path.resolve(config.dirname, config.relativeEnvPaths.rootEnvPath),
    schemaEnvPath: config.relativeEnvPaths.schemaEnvPath && path.resolve(config.dirname, config.relativeEnvPaths.schemaEnvPath)
})

const PrismaClient = getPrismaClient(config)
exports.PrismaClient = PrismaClient
Object.assign(exports, Prisma)

// file annotations for bundling tools to include these files
path.join(__dirname, "query_engine-windows.dll.node");
path.join(process.cwd(), "src/generated/client/query_engine-windows.dll.node")
// file annotations for bundling tools to include these files
path.join(__dirname, "schema.prisma");
path.join(process.cwd(), "src/generated/client/schema.prisma")
