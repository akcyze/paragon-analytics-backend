
/**
 * Client
**/

import * as runtime from './runtime/library';
import $Types = runtime.Types // general types
import $Public = runtime.Types.Public
import $Utils = runtime.Types.Utils
import $Extensions = runtime.Types.Extensions
import $Result = runtime.Types.Result

export type PrismaPromise<T> = $Public.PrismaPromise<T>


/**
 * Model User
 * 
 */
export type User = $Result.DefaultSelection<Prisma.$UserPayload>
/**
 * Model RegistrationToken
 * 
 */
export type RegistrationToken = $Result.DefaultSelection<Prisma.$RegistrationTokenPayload>
/**
 * Model Champion
 * 
 */
export type Champion = $Result.DefaultSelection<Prisma.$ChampionPayload>
/**
 * Model Build
 * 
 */
export type Build = $Result.DefaultSelection<Prisma.$BuildPayload>
/**
 * Model Likes
 * 
 */
export type Likes = $Result.DefaultSelection<Prisma.$LikesPayload>
/**
 * Model DisLikes
 * 
 */
export type DisLikes = $Result.DefaultSelection<Prisma.$DisLikesPayload>
/**
 * Model Tournament
 * 
 */
export type Tournament = $Result.DefaultSelection<Prisma.$TournamentPayload>
/**
 * Model Item
 * 
 */
export type Item = $Result.DefaultSelection<Prisma.$ItemPayload>
/**
 * Model Log
 * 
 */
export type Log = $Result.DefaultSelection<Prisma.$LogPayload>
/**
 * Model Team
 * 
 */
export type Team = $Result.DefaultSelection<Prisma.$TeamPayload>
/**
 * Model News
 * 
 */
export type News = $Result.DefaultSelection<Prisma.$NewsPayload>
/**
 * Model Creature
 * 
 */
export type Creature = $Result.DefaultSelection<Prisma.$CreaturePayload>

/**
 * Enums
 */
export namespace $Enums {
  export const Role: {
  Jungle: 'Jungle',
  Mid: 'Mid',
  Adc: 'Adc',
  Support: 'Support',
  Top: 'Top'
};

export type Role = (typeof Role)[keyof typeof Role]


export const TournamentPickMode: {
  Quick: 'Quick',
  Draft: 'Draft'
};

export type TournamentPickMode = (typeof TournamentPickMode)[keyof typeof TournamentPickMode]


export const GameMode: {
  DestroyCore: 'DestroyCore',
  FirstBlood: 'FirstBlood',
  FirstTower: 'FirstTower'
};

export type GameMode = (typeof GameMode)[keyof typeof GameMode]


export const TournamentStatus: {
  Pending: 'Pending',
  InProgress: 'InProgress',
  Finished: 'Finished'
};

export type TournamentStatus = (typeof TournamentStatus)[keyof typeof TournamentStatus]


export const BracketType: {
  RoundRobin: 'RoundRobin',
  SingleElimination: 'SingleElimination'
};

export type BracketType = (typeof BracketType)[keyof typeof BracketType]


export const ItemType: {
  Basic: 'Basic',
  Rare: 'Rare',
  Epic: 'Epic'
};

export type ItemType = (typeof ItemType)[keyof typeof ItemType]


export const LogType: {
  Info: 'Info',
  Warning: 'Warning',
  Error: 'Error'
};

export type LogType = (typeof LogType)[keyof typeof LogType]


export const CreatureType: {
  Jungle: 'Jungle',
  Minion: 'Minion',
  Tower: 'Tower',
  Core: 'Core',
  Boss: 'Boss'
};

export type CreatureType = (typeof CreatureType)[keyof typeof CreatureType]

}

export type Role = $Enums.Role

export const Role: typeof $Enums.Role

export type TournamentPickMode = $Enums.TournamentPickMode

export const TournamentPickMode: typeof $Enums.TournamentPickMode

export type GameMode = $Enums.GameMode

export const GameMode: typeof $Enums.GameMode

export type TournamentStatus = $Enums.TournamentStatus

export const TournamentStatus: typeof $Enums.TournamentStatus

export type BracketType = $Enums.BracketType

export const BracketType: typeof $Enums.BracketType

export type ItemType = $Enums.ItemType

export const ItemType: typeof $Enums.ItemType

export type LogType = $Enums.LogType

export const LogType: typeof $Enums.LogType

export type CreatureType = $Enums.CreatureType

export const CreatureType: typeof $Enums.CreatureType

/**
 * ##  Prisma Client ʲˢ
 * 
 * Type-safe database client for TypeScript & Node.js
 * @example
 * ```
 * const prisma = new PrismaClient()
 * // Fetch zero or more Users
 * const users = await prisma.user.findMany()
 * ```
 *
 * 
 * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
 */
export class PrismaClient<
  T extends Prisma.PrismaClientOptions = Prisma.PrismaClientOptions,
  U = 'log' extends keyof T ? T['log'] extends Array<Prisma.LogLevel | Prisma.LogDefinition> ? Prisma.GetEvents<T['log']> : never : never,
  ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs
> {
  [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['other'] }

    /**
   * ##  Prisma Client ʲˢ
   * 
   * Type-safe database client for TypeScript & Node.js
   * @example
   * ```
   * const prisma = new PrismaClient()
   * // Fetch zero or more Users
   * const users = await prisma.user.findMany()
   * ```
   *
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client).
   */

  constructor(optionsArg ?: Prisma.Subset<T, Prisma.PrismaClientOptions>);
  $on<V extends U>(eventType: V, callback: (event: V extends 'query' ? Prisma.QueryEvent : Prisma.LogEvent) => void): void;

  /**
   * Connect with the database
   */
  $connect(): $Utils.JsPromise<void>;

  /**
   * Disconnect from the database
   */
  $disconnect(): $Utils.JsPromise<void>;

  /**
   * Add a middleware
   * @deprecated since 4.16.0. For new code, prefer client extensions instead.
   * @see https://pris.ly/d/extensions
   */
  $use(cb: Prisma.Middleware): void

/**
   * Executes a prepared raw query and returns the number of affected rows.
   * @example
   * ```
   * const result = await prisma.$executeRaw`UPDATE User SET cool = ${true} WHERE email = ${'user@email.com'};`
   * ```
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
   */
  $executeRaw<T = unknown>(query: TemplateStringsArray | Prisma.Sql, ...values: any[]): Prisma.PrismaPromise<number>;

  /**
   * Executes a raw query and returns the number of affected rows.
   * Susceptible to SQL injections, see documentation.
   * @example
   * ```
   * const result = await prisma.$executeRawUnsafe('UPDATE User SET cool = $1 WHERE email = $2 ;', true, 'user@email.com')
   * ```
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
   */
  $executeRawUnsafe<T = unknown>(query: string, ...values: any[]): Prisma.PrismaPromise<number>;

  /**
   * Performs a prepared raw query and returns the `SELECT` data.
   * @example
   * ```
   * const result = await prisma.$queryRaw`SELECT * FROM User WHERE id = ${1} OR email = ${'user@email.com'};`
   * ```
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
   */
  $queryRaw<T = unknown>(query: TemplateStringsArray | Prisma.Sql, ...values: any[]): Prisma.PrismaPromise<T>;

  /**
   * Performs a raw query and returns the `SELECT` data.
   * Susceptible to SQL injections, see documentation.
   * @example
   * ```
   * const result = await prisma.$queryRawUnsafe('SELECT * FROM User WHERE id = $1 OR email = $2;', 1, 'user@email.com')
   * ```
   * 
   * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/raw-database-access).
   */
  $queryRawUnsafe<T = unknown>(query: string, ...values: any[]): Prisma.PrismaPromise<T>;

  /**
   * Allows the running of a sequence of read/write operations that are guaranteed to either succeed or fail as a whole.
   * @example
   * ```
   * const [george, bob, alice] = await prisma.$transaction([
   *   prisma.user.create({ data: { name: 'George' } }),
   *   prisma.user.create({ data: { name: 'Bob' } }),
   *   prisma.user.create({ data: { name: 'Alice' } }),
   * ])
   * ```
   * 
   * Read more in our [docs](https://www.prisma.io/docs/concepts/components/prisma-client/transactions).
   */
  $transaction<P extends Prisma.PrismaPromise<any>[]>(arg: [...P], options?: { isolationLevel?: Prisma.TransactionIsolationLevel }): $Utils.JsPromise<runtime.Types.Utils.UnwrapTuple<P>>

  $transaction<R>(fn: (prisma: Omit<PrismaClient, runtime.ITXClientDenyList>) => $Utils.JsPromise<R>, options?: { maxWait?: number, timeout?: number, isolationLevel?: Prisma.TransactionIsolationLevel }): $Utils.JsPromise<R>


  $extends: $Extensions.ExtendsHook<'extends', Prisma.TypeMapCb, ExtArgs>

      /**
   * `prisma.user`: Exposes CRUD operations for the **User** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Users
    * const users = await prisma.user.findMany()
    * ```
    */
  get user(): Prisma.UserDelegate<ExtArgs>;

  /**
   * `prisma.registrationToken`: Exposes CRUD operations for the **RegistrationToken** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more RegistrationTokens
    * const registrationTokens = await prisma.registrationToken.findMany()
    * ```
    */
  get registrationToken(): Prisma.RegistrationTokenDelegate<ExtArgs>;

  /**
   * `prisma.champion`: Exposes CRUD operations for the **Champion** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Champions
    * const champions = await prisma.champion.findMany()
    * ```
    */
  get champion(): Prisma.ChampionDelegate<ExtArgs>;

  /**
   * `prisma.build`: Exposes CRUD operations for the **Build** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Builds
    * const builds = await prisma.build.findMany()
    * ```
    */
  get build(): Prisma.BuildDelegate<ExtArgs>;

  /**
   * `prisma.likes`: Exposes CRUD operations for the **Likes** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Likes
    * const likes = await prisma.likes.findMany()
    * ```
    */
  get likes(): Prisma.LikesDelegate<ExtArgs>;

  /**
   * `prisma.disLikes`: Exposes CRUD operations for the **DisLikes** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more DisLikes
    * const disLikes = await prisma.disLikes.findMany()
    * ```
    */
  get disLikes(): Prisma.DisLikesDelegate<ExtArgs>;

  /**
   * `prisma.tournament`: Exposes CRUD operations for the **Tournament** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Tournaments
    * const tournaments = await prisma.tournament.findMany()
    * ```
    */
  get tournament(): Prisma.TournamentDelegate<ExtArgs>;

  /**
   * `prisma.item`: Exposes CRUD operations for the **Item** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Items
    * const items = await prisma.item.findMany()
    * ```
    */
  get item(): Prisma.ItemDelegate<ExtArgs>;

  /**
   * `prisma.log`: Exposes CRUD operations for the **Log** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Logs
    * const logs = await prisma.log.findMany()
    * ```
    */
  get log(): Prisma.LogDelegate<ExtArgs>;

  /**
   * `prisma.team`: Exposes CRUD operations for the **Team** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Teams
    * const teams = await prisma.team.findMany()
    * ```
    */
  get team(): Prisma.TeamDelegate<ExtArgs>;

  /**
   * `prisma.news`: Exposes CRUD operations for the **News** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more News
    * const news = await prisma.news.findMany()
    * ```
    */
  get news(): Prisma.NewsDelegate<ExtArgs>;

  /**
   * `prisma.creature`: Exposes CRUD operations for the **Creature** model.
    * Example usage:
    * ```ts
    * // Fetch zero or more Creatures
    * const creatures = await prisma.creature.findMany()
    * ```
    */
  get creature(): Prisma.CreatureDelegate<ExtArgs>;
}

export namespace Prisma {
  export import DMMF = runtime.DMMF

  export type PrismaPromise<T> = $Public.PrismaPromise<T>

  /**
   * Validator
   */
  export import validator = runtime.Public.validator

  /**
   * Prisma Errors
   */
  export import PrismaClientKnownRequestError = runtime.PrismaClientKnownRequestError
  export import PrismaClientUnknownRequestError = runtime.PrismaClientUnknownRequestError
  export import PrismaClientRustPanicError = runtime.PrismaClientRustPanicError
  export import PrismaClientInitializationError = runtime.PrismaClientInitializationError
  export import PrismaClientValidationError = runtime.PrismaClientValidationError
  export import NotFoundError = runtime.NotFoundError

  /**
   * Re-export of sql-template-tag
   */
  export import sql = runtime.sqltag
  export import empty = runtime.empty
  export import join = runtime.join
  export import raw = runtime.raw
  export import Sql = runtime.Sql

  /**
   * Decimal.js
   */
  export import Decimal = runtime.Decimal

  export type DecimalJsLike = runtime.DecimalJsLike

  /**
   * Metrics 
   */
  export type Metrics = runtime.Metrics
  export type Metric<T> = runtime.Metric<T>
  export type MetricHistogram = runtime.MetricHistogram
  export type MetricHistogramBucket = runtime.MetricHistogramBucket

  /**
  * Extensions
  */
  export import Extension = $Extensions.UserArgs
  export import getExtensionContext = runtime.Extensions.getExtensionContext
  export import Args = $Public.Args
  export import Payload = $Public.Payload
  export import Result = $Public.Result
  export import Exact = $Public.Exact

  /**
   * Prisma Client JS version: 5.3.1
   * Query Engine version: 61e140623197a131c2a6189271ffee05a7aa9a59
   */
  export type PrismaVersion = {
    client: string
  }

  export const prismaVersion: PrismaVersion 

  /**
   * Utility Types
   */

  /**
   * From https://github.com/sindresorhus/type-fest/
   * Matches a JSON object.
   * This type can be useful to enforce some input to be JSON-compatible or as a super-type to be extended from. 
   */
  export type JsonObject = {[Key in string]?: JsonValue}

  /**
   * From https://github.com/sindresorhus/type-fest/
   * Matches a JSON array.
   */
  export interface JsonArray extends Array<JsonValue> {}

  /**
   * From https://github.com/sindresorhus/type-fest/
   * Matches any valid JSON value.
   */
  export type JsonValue = string | number | boolean | JsonObject | JsonArray | null

  /**
   * Matches a JSON object.
   * Unlike `JsonObject`, this type allows undefined and read-only properties.
   */
  export type InputJsonObject = {readonly [Key in string]?: InputJsonValue | null}

  /**
   * Matches a JSON array.
   * Unlike `JsonArray`, readonly arrays are assignable to this type.
   */
  export interface InputJsonArray extends ReadonlyArray<InputJsonValue | null> {}

  /**
   * Matches any valid value that can be used as an input for operations like
   * create and update as the value of a JSON field. Unlike `JsonValue`, this
   * type allows read-only arrays and read-only object properties and disallows
   * `null` at the top level.
   *
   * `null` cannot be used as the value of a JSON field because its meaning
   * would be ambiguous. Use `Prisma.JsonNull` to store the JSON null value or
   * `Prisma.DbNull` to clear the JSON value and set the field to the database
   * NULL value instead.
   *
   * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-by-null-values
   */
  export type InputJsonValue = string | number | boolean | InputJsonObject | InputJsonArray | { toJSON(): unknown }

  /**
   * Types of the values used to represent different kinds of `null` values when working with JSON fields.
   * 
   * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-on-a-json-field
   */
  namespace NullTypes {
    /**
    * Type of `Prisma.DbNull`.
    * 
    * You cannot use other instances of this class. Please use the `Prisma.DbNull` value.
    * 
    * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-on-a-json-field
    */
    class DbNull {
      private DbNull: never
      private constructor()
    }

    /**
    * Type of `Prisma.JsonNull`.
    * 
    * You cannot use other instances of this class. Please use the `Prisma.JsonNull` value.
    * 
    * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-on-a-json-field
    */
    class JsonNull {
      private JsonNull: never
      private constructor()
    }

    /**
    * Type of `Prisma.AnyNull`.
    * 
    * You cannot use other instances of this class. Please use the `Prisma.AnyNull` value.
    * 
    * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-on-a-json-field
    */
    class AnyNull {
      private AnyNull: never
      private constructor()
    }
  }

  /**
   * Helper for filtering JSON entries that have `null` on the database (empty on the db)
   * 
   * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-on-a-json-field
   */
  export const DbNull: NullTypes.DbNull

  /**
   * Helper for filtering JSON entries that have JSON `null` values (not empty on the db)
   * 
   * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-on-a-json-field
   */
  export const JsonNull: NullTypes.JsonNull

  /**
   * Helper for filtering JSON entries that are `Prisma.DbNull` or `Prisma.JsonNull`
   * 
   * @see https://www.prisma.io/docs/concepts/components/prisma-client/working-with-fields/working-with-json-fields#filtering-on-a-json-field
   */
  export const AnyNull: NullTypes.AnyNull

  type SelectAndInclude = {
    select: any
    include: any
  }

  /**
   * Get the type of the value, that the Promise holds.
   */
  export type PromiseType<T extends PromiseLike<any>> = T extends PromiseLike<infer U> ? U : T;

  /**
   * Get the return type of a function which returns a Promise.
   */
  export type PromiseReturnType<T extends (...args: any) => $Utils.JsPromise<any>> = PromiseType<ReturnType<T>>

  /**
   * From T, pick a set of properties whose keys are in the union K
   */
  type Prisma__Pick<T, K extends keyof T> = {
      [P in K]: T[P];
  };


  export type Enumerable<T> = T | Array<T>;

  export type RequiredKeys<T> = {
    [K in keyof T]-?: {} extends Prisma__Pick<T, K> ? never : K
  }[keyof T]

  export type TruthyKeys<T> = keyof {
    [K in keyof T as T[K] extends false | undefined | null ? never : K]: K
  }

  export type TrueKeys<T> = TruthyKeys<Prisma__Pick<T, RequiredKeys<T>>>

  /**
   * Subset
   * @desc From `T` pick properties that exist in `U`. Simple version of Intersection
   */
  export type Subset<T, U> = {
    [key in keyof T]: key extends keyof U ? T[key] : never;
  };

  /**
   * SelectSubset
   * @desc From `T` pick properties that exist in `U`. Simple version of Intersection.
   * Additionally, it validates, if both select and include are present. If the case, it errors.
   */
  export type SelectSubset<T, U> = {
    [key in keyof T]: key extends keyof U ? T[key] : never
  } &
    (T extends SelectAndInclude
      ? 'Please either choose `select` or `include`.'
      : {})

  /**
   * Subset + Intersection
   * @desc From `T` pick properties that exist in `U` and intersect `K`
   */
  export type SubsetIntersection<T, U, K> = {
    [key in keyof T]: key extends keyof U ? T[key] : never
  } &
    K

  type Without<T, U> = { [P in Exclude<keyof T, keyof U>]?: never };

  /**
   * XOR is needed to have a real mutually exclusive union type
   * https://stackoverflow.com/questions/42123407/does-typescript-support-mutually-exclusive-types
   */
  type XOR<T, U> =
    T extends object ?
    U extends object ?
      (Without<T, U> & U) | (Without<U, T> & T)
    : U : T


  /**
   * Is T a Record?
   */
  type IsObject<T extends any> = T extends Array<any>
  ? False
  : T extends Date
  ? False
  : T extends Uint8Array
  ? False
  : T extends BigInt
  ? False
  : T extends object
  ? True
  : False


  /**
   * If it's T[], return T
   */
  export type UnEnumerate<T extends unknown> = T extends Array<infer U> ? U : T

  /**
   * From ts-toolbelt
   */

  type __Either<O extends object, K extends Key> = Omit<O, K> &
    {
      // Merge all but K
      [P in K]: Prisma__Pick<O, P & keyof O> // With K possibilities
    }[K]

  type EitherStrict<O extends object, K extends Key> = Strict<__Either<O, K>>

  type EitherLoose<O extends object, K extends Key> = ComputeRaw<__Either<O, K>>

  type _Either<
    O extends object,
    K extends Key,
    strict extends Boolean
  > = {
    1: EitherStrict<O, K>
    0: EitherLoose<O, K>
  }[strict]

  type Either<
    O extends object,
    K extends Key,
    strict extends Boolean = 1
  > = O extends unknown ? _Either<O, K, strict> : never

  export type Union = any

  type PatchUndefined<O extends object, O1 extends object> = {
    [K in keyof O]: O[K] extends undefined ? At<O1, K> : O[K]
  } & {}

  /** Helper Types for "Merge" **/
  export type IntersectOf<U extends Union> = (
    U extends unknown ? (k: U) => void : never
  ) extends (k: infer I) => void
    ? I
    : never

  export type Overwrite<O extends object, O1 extends object> = {
      [K in keyof O]: K extends keyof O1 ? O1[K] : O[K];
  } & {};

  type _Merge<U extends object> = IntersectOf<Overwrite<U, {
      [K in keyof U]-?: At<U, K>;
  }>>;

  type Key = string | number | symbol;
  type AtBasic<O extends object, K extends Key> = K extends keyof O ? O[K] : never;
  type AtStrict<O extends object, K extends Key> = O[K & keyof O];
  type AtLoose<O extends object, K extends Key> = O extends unknown ? AtStrict<O, K> : never;
  export type At<O extends object, K extends Key, strict extends Boolean = 1> = {
      1: AtStrict<O, K>;
      0: AtLoose<O, K>;
  }[strict];

  export type ComputeRaw<A extends any> = A extends Function ? A : {
    [K in keyof A]: A[K];
  } & {};

  export type OptionalFlat<O> = {
    [K in keyof O]?: O[K];
  } & {};

  type _Record<K extends keyof any, T> = {
    [P in K]: T;
  };

  // cause typescript not to expand types and preserve names
  type NoExpand<T> = T extends unknown ? T : never;

  // this type assumes the passed object is entirely optional
  type AtLeast<O extends object, K extends string> = NoExpand<
    O extends unknown
    ? | (K extends keyof O ? { [P in K]: O[P] } & O : O)
      | {[P in keyof O as P extends K ? K : never]-?: O[P]} & O
    : never>;

  type _Strict<U, _U = U> = U extends unknown ? U & OptionalFlat<_Record<Exclude<Keys<_U>, keyof U>, never>> : never;

  export type Strict<U extends object> = ComputeRaw<_Strict<U>>;
  /** End Helper Types for "Merge" **/

  export type Merge<U extends object> = ComputeRaw<_Merge<Strict<U>>>;

  /**
  A [[Boolean]]
  */
  export type Boolean = True | False

  // /**
  // 1
  // */
  export type True = 1

  /**
  0
  */
  export type False = 0

  export type Not<B extends Boolean> = {
    0: 1
    1: 0
  }[B]

  export type Extends<A1 extends any, A2 extends any> = [A1] extends [never]
    ? 0 // anything `never` is false
    : A1 extends A2
    ? 1
    : 0

  export type Has<U extends Union, U1 extends Union> = Not<
    Extends<Exclude<U1, U>, U1>
  >

  export type Or<B1 extends Boolean, B2 extends Boolean> = {
    0: {
      0: 0
      1: 1
    }
    1: {
      0: 1
      1: 1
    }
  }[B1][B2]

  export type Keys<U extends Union> = U extends unknown ? keyof U : never

  type Cast<A, B> = A extends B ? A : B;

  export const type: unique symbol;



  /**
   * Used by group by
   */

  export type GetScalarType<T, O> = O extends object ? {
    [P in keyof T]: P extends keyof O
      ? O[P]
      : never
  } : never

  type FieldPaths<
    T,
    U = Omit<T, '_avg' | '_sum' | '_count' | '_min' | '_max'>
  > = IsObject<T> extends True ? U : T

  type GetHavingFields<T> = {
    [K in keyof T]: Or<
      Or<Extends<'OR', K>, Extends<'AND', K>>,
      Extends<'NOT', K>
    > extends True
      ? // infer is only needed to not hit TS limit
        // based on the brilliant idea of Pierre-Antoine Mills
        // https://github.com/microsoft/TypeScript/issues/30188#issuecomment-478938437
        T[K] extends infer TK
        ? GetHavingFields<UnEnumerate<TK> extends object ? Merge<UnEnumerate<TK>> : never>
        : never
      : {} extends FieldPaths<T[K]>
      ? never
      : K
  }[keyof T]

  /**
   * Convert tuple to union
   */
  type _TupleToUnion<T> = T extends (infer E)[] ? E : never
  type TupleToUnion<K extends readonly any[]> = _TupleToUnion<K>
  type MaybeTupleToUnion<T> = T extends any[] ? TupleToUnion<T> : T

  /**
   * Like `Pick`, but additionally can also accept an array of keys
   */
  type PickEnumerable<T, K extends Enumerable<keyof T> | keyof T> = Prisma__Pick<T, MaybeTupleToUnion<K>>

  /**
   * Exclude all keys with underscores
   */
  type ExcludeUnderscoreKeys<T extends string> = T extends `_${string}` ? never : T


  export type FieldRef<Model, FieldType> = runtime.FieldRef<Model, FieldType>

  type FieldRefInputType<Model, FieldType> = Model extends never ? never : FieldRef<Model, FieldType>


  export const ModelName: {
    User: 'User',
    RegistrationToken: 'RegistrationToken',
    Champion: 'Champion',
    Build: 'Build',
    Likes: 'Likes',
    DisLikes: 'DisLikes',
    Tournament: 'Tournament',
    Item: 'Item',
    Log: 'Log',
    Team: 'Team',
    News: 'News',
    Creature: 'Creature'
  };

  export type ModelName = (typeof ModelName)[keyof typeof ModelName]


  export type Datasources = {
    db?: Datasource
  }


  interface TypeMapCb extends $Utils.Fn<{extArgs: $Extensions.Args}, $Utils.Record<string, any>> {
    returns: Prisma.TypeMap<this['params']['extArgs']>
  }

  export type TypeMap<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    meta: {
      modelProps: 'user' | 'registrationToken' | 'champion' | 'build' | 'likes' | 'disLikes' | 'tournament' | 'item' | 'log' | 'team' | 'news' | 'creature'
      txIsolationLevel: Prisma.TransactionIsolationLevel
    },
    model: {
      User: {
        payload: Prisma.$UserPayload<ExtArgs>
        fields: Prisma.UserFieldRefs
        operations: {
          findUnique: {
            args: Prisma.UserFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.UserFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload>
          }
          findFirst: {
            args: Prisma.UserFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.UserFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload>
          }
          findMany: {
            args: Prisma.UserFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload>[]
          }
          create: {
            args: Prisma.UserCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload>
          }
          createMany: {
            args: Prisma.UserCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.UserDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload>
          }
          update: {
            args: Prisma.UserUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload>
          }
          deleteMany: {
            args: Prisma.UserDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.UserUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.UserUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$UserPayload>
          }
          aggregate: {
            args: Prisma.UserAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateUser>
          }
          groupBy: {
            args: Prisma.UserGroupByArgs<ExtArgs>,
            result: $Utils.Optional<UserGroupByOutputType>[]
          }
          count: {
            args: Prisma.UserCountArgs<ExtArgs>,
            result: $Utils.Optional<UserCountAggregateOutputType> | number
          }
        }
      }
      RegistrationToken: {
        payload: Prisma.$RegistrationTokenPayload<ExtArgs>
        fields: Prisma.RegistrationTokenFieldRefs
        operations: {
          findUnique: {
            args: Prisma.RegistrationTokenFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.RegistrationTokenFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload>
          }
          findFirst: {
            args: Prisma.RegistrationTokenFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.RegistrationTokenFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload>
          }
          findMany: {
            args: Prisma.RegistrationTokenFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload>[]
          }
          create: {
            args: Prisma.RegistrationTokenCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload>
          }
          createMany: {
            args: Prisma.RegistrationTokenCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.RegistrationTokenDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload>
          }
          update: {
            args: Prisma.RegistrationTokenUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload>
          }
          deleteMany: {
            args: Prisma.RegistrationTokenDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.RegistrationTokenUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.RegistrationTokenUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$RegistrationTokenPayload>
          }
          aggregate: {
            args: Prisma.RegistrationTokenAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateRegistrationToken>
          }
          groupBy: {
            args: Prisma.RegistrationTokenGroupByArgs<ExtArgs>,
            result: $Utils.Optional<RegistrationTokenGroupByOutputType>[]
          }
          count: {
            args: Prisma.RegistrationTokenCountArgs<ExtArgs>,
            result: $Utils.Optional<RegistrationTokenCountAggregateOutputType> | number
          }
        }
      }
      Champion: {
        payload: Prisma.$ChampionPayload<ExtArgs>
        fields: Prisma.ChampionFieldRefs
        operations: {
          findUnique: {
            args: Prisma.ChampionFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.ChampionFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload>
          }
          findFirst: {
            args: Prisma.ChampionFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.ChampionFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload>
          }
          findMany: {
            args: Prisma.ChampionFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload>[]
          }
          create: {
            args: Prisma.ChampionCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload>
          }
          createMany: {
            args: Prisma.ChampionCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.ChampionDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload>
          }
          update: {
            args: Prisma.ChampionUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload>
          }
          deleteMany: {
            args: Prisma.ChampionDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.ChampionUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.ChampionUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ChampionPayload>
          }
          aggregate: {
            args: Prisma.ChampionAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateChampion>
          }
          groupBy: {
            args: Prisma.ChampionGroupByArgs<ExtArgs>,
            result: $Utils.Optional<ChampionGroupByOutputType>[]
          }
          count: {
            args: Prisma.ChampionCountArgs<ExtArgs>,
            result: $Utils.Optional<ChampionCountAggregateOutputType> | number
          }
        }
      }
      Build: {
        payload: Prisma.$BuildPayload<ExtArgs>
        fields: Prisma.BuildFieldRefs
        operations: {
          findUnique: {
            args: Prisma.BuildFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.BuildFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload>
          }
          findFirst: {
            args: Prisma.BuildFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.BuildFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload>
          }
          findMany: {
            args: Prisma.BuildFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload>[]
          }
          create: {
            args: Prisma.BuildCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload>
          }
          createMany: {
            args: Prisma.BuildCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.BuildDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload>
          }
          update: {
            args: Prisma.BuildUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload>
          }
          deleteMany: {
            args: Prisma.BuildDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.BuildUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.BuildUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$BuildPayload>
          }
          aggregate: {
            args: Prisma.BuildAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateBuild>
          }
          groupBy: {
            args: Prisma.BuildGroupByArgs<ExtArgs>,
            result: $Utils.Optional<BuildGroupByOutputType>[]
          }
          count: {
            args: Prisma.BuildCountArgs<ExtArgs>,
            result: $Utils.Optional<BuildCountAggregateOutputType> | number
          }
        }
      }
      Likes: {
        payload: Prisma.$LikesPayload<ExtArgs>
        fields: Prisma.LikesFieldRefs
        operations: {
          findUnique: {
            args: Prisma.LikesFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.LikesFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload>
          }
          findFirst: {
            args: Prisma.LikesFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.LikesFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload>
          }
          findMany: {
            args: Prisma.LikesFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload>[]
          }
          create: {
            args: Prisma.LikesCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload>
          }
          createMany: {
            args: Prisma.LikesCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.LikesDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload>
          }
          update: {
            args: Prisma.LikesUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload>
          }
          deleteMany: {
            args: Prisma.LikesDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.LikesUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.LikesUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LikesPayload>
          }
          aggregate: {
            args: Prisma.LikesAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateLikes>
          }
          groupBy: {
            args: Prisma.LikesGroupByArgs<ExtArgs>,
            result: $Utils.Optional<LikesGroupByOutputType>[]
          }
          count: {
            args: Prisma.LikesCountArgs<ExtArgs>,
            result: $Utils.Optional<LikesCountAggregateOutputType> | number
          }
        }
      }
      DisLikes: {
        payload: Prisma.$DisLikesPayload<ExtArgs>
        fields: Prisma.DisLikesFieldRefs
        operations: {
          findUnique: {
            args: Prisma.DisLikesFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.DisLikesFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload>
          }
          findFirst: {
            args: Prisma.DisLikesFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.DisLikesFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload>
          }
          findMany: {
            args: Prisma.DisLikesFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload>[]
          }
          create: {
            args: Prisma.DisLikesCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload>
          }
          createMany: {
            args: Prisma.DisLikesCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.DisLikesDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload>
          }
          update: {
            args: Prisma.DisLikesUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload>
          }
          deleteMany: {
            args: Prisma.DisLikesDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.DisLikesUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.DisLikesUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$DisLikesPayload>
          }
          aggregate: {
            args: Prisma.DisLikesAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateDisLikes>
          }
          groupBy: {
            args: Prisma.DisLikesGroupByArgs<ExtArgs>,
            result: $Utils.Optional<DisLikesGroupByOutputType>[]
          }
          count: {
            args: Prisma.DisLikesCountArgs<ExtArgs>,
            result: $Utils.Optional<DisLikesCountAggregateOutputType> | number
          }
        }
      }
      Tournament: {
        payload: Prisma.$TournamentPayload<ExtArgs>
        fields: Prisma.TournamentFieldRefs
        operations: {
          findUnique: {
            args: Prisma.TournamentFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.TournamentFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload>
          }
          findFirst: {
            args: Prisma.TournamentFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.TournamentFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload>
          }
          findMany: {
            args: Prisma.TournamentFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload>[]
          }
          create: {
            args: Prisma.TournamentCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload>
          }
          createMany: {
            args: Prisma.TournamentCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.TournamentDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload>
          }
          update: {
            args: Prisma.TournamentUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload>
          }
          deleteMany: {
            args: Prisma.TournamentDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.TournamentUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.TournamentUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TournamentPayload>
          }
          aggregate: {
            args: Prisma.TournamentAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateTournament>
          }
          groupBy: {
            args: Prisma.TournamentGroupByArgs<ExtArgs>,
            result: $Utils.Optional<TournamentGroupByOutputType>[]
          }
          count: {
            args: Prisma.TournamentCountArgs<ExtArgs>,
            result: $Utils.Optional<TournamentCountAggregateOutputType> | number
          }
        }
      }
      Item: {
        payload: Prisma.$ItemPayload<ExtArgs>
        fields: Prisma.ItemFieldRefs
        operations: {
          findUnique: {
            args: Prisma.ItemFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.ItemFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload>
          }
          findFirst: {
            args: Prisma.ItemFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.ItemFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload>
          }
          findMany: {
            args: Prisma.ItemFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload>[]
          }
          create: {
            args: Prisma.ItemCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload>
          }
          createMany: {
            args: Prisma.ItemCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.ItemDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload>
          }
          update: {
            args: Prisma.ItemUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload>
          }
          deleteMany: {
            args: Prisma.ItemDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.ItemUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.ItemUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$ItemPayload>
          }
          aggregate: {
            args: Prisma.ItemAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateItem>
          }
          groupBy: {
            args: Prisma.ItemGroupByArgs<ExtArgs>,
            result: $Utils.Optional<ItemGroupByOutputType>[]
          }
          count: {
            args: Prisma.ItemCountArgs<ExtArgs>,
            result: $Utils.Optional<ItemCountAggregateOutputType> | number
          }
        }
      }
      Log: {
        payload: Prisma.$LogPayload<ExtArgs>
        fields: Prisma.LogFieldRefs
        operations: {
          findUnique: {
            args: Prisma.LogFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.LogFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload>
          }
          findFirst: {
            args: Prisma.LogFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.LogFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload>
          }
          findMany: {
            args: Prisma.LogFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload>[]
          }
          create: {
            args: Prisma.LogCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload>
          }
          createMany: {
            args: Prisma.LogCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.LogDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload>
          }
          update: {
            args: Prisma.LogUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload>
          }
          deleteMany: {
            args: Prisma.LogDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.LogUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.LogUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$LogPayload>
          }
          aggregate: {
            args: Prisma.LogAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateLog>
          }
          groupBy: {
            args: Prisma.LogGroupByArgs<ExtArgs>,
            result: $Utils.Optional<LogGroupByOutputType>[]
          }
          count: {
            args: Prisma.LogCountArgs<ExtArgs>,
            result: $Utils.Optional<LogCountAggregateOutputType> | number
          }
        }
      }
      Team: {
        payload: Prisma.$TeamPayload<ExtArgs>
        fields: Prisma.TeamFieldRefs
        operations: {
          findUnique: {
            args: Prisma.TeamFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.TeamFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload>
          }
          findFirst: {
            args: Prisma.TeamFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.TeamFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload>
          }
          findMany: {
            args: Prisma.TeamFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload>[]
          }
          create: {
            args: Prisma.TeamCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload>
          }
          createMany: {
            args: Prisma.TeamCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.TeamDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload>
          }
          update: {
            args: Prisma.TeamUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload>
          }
          deleteMany: {
            args: Prisma.TeamDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.TeamUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.TeamUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$TeamPayload>
          }
          aggregate: {
            args: Prisma.TeamAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateTeam>
          }
          groupBy: {
            args: Prisma.TeamGroupByArgs<ExtArgs>,
            result: $Utils.Optional<TeamGroupByOutputType>[]
          }
          count: {
            args: Prisma.TeamCountArgs<ExtArgs>,
            result: $Utils.Optional<TeamCountAggregateOutputType> | number
          }
        }
      }
      News: {
        payload: Prisma.$NewsPayload<ExtArgs>
        fields: Prisma.NewsFieldRefs
        operations: {
          findUnique: {
            args: Prisma.NewsFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.NewsFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload>
          }
          findFirst: {
            args: Prisma.NewsFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.NewsFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload>
          }
          findMany: {
            args: Prisma.NewsFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload>[]
          }
          create: {
            args: Prisma.NewsCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload>
          }
          createMany: {
            args: Prisma.NewsCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.NewsDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload>
          }
          update: {
            args: Prisma.NewsUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload>
          }
          deleteMany: {
            args: Prisma.NewsDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.NewsUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.NewsUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$NewsPayload>
          }
          aggregate: {
            args: Prisma.NewsAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateNews>
          }
          groupBy: {
            args: Prisma.NewsGroupByArgs<ExtArgs>,
            result: $Utils.Optional<NewsGroupByOutputType>[]
          }
          count: {
            args: Prisma.NewsCountArgs<ExtArgs>,
            result: $Utils.Optional<NewsCountAggregateOutputType> | number
          }
        }
      }
      Creature: {
        payload: Prisma.$CreaturePayload<ExtArgs>
        fields: Prisma.CreatureFieldRefs
        operations: {
          findUnique: {
            args: Prisma.CreatureFindUniqueArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload> | null
          }
          findUniqueOrThrow: {
            args: Prisma.CreatureFindUniqueOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload>
          }
          findFirst: {
            args: Prisma.CreatureFindFirstArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload> | null
          }
          findFirstOrThrow: {
            args: Prisma.CreatureFindFirstOrThrowArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload>
          }
          findMany: {
            args: Prisma.CreatureFindManyArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload>[]
          }
          create: {
            args: Prisma.CreatureCreateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload>
          }
          createMany: {
            args: Prisma.CreatureCreateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          delete: {
            args: Prisma.CreatureDeleteArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload>
          }
          update: {
            args: Prisma.CreatureUpdateArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload>
          }
          deleteMany: {
            args: Prisma.CreatureDeleteManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          updateMany: {
            args: Prisma.CreatureUpdateManyArgs<ExtArgs>,
            result: Prisma.BatchPayload
          }
          upsert: {
            args: Prisma.CreatureUpsertArgs<ExtArgs>,
            result: $Utils.PayloadToResult<Prisma.$CreaturePayload>
          }
          aggregate: {
            args: Prisma.CreatureAggregateArgs<ExtArgs>,
            result: $Utils.Optional<AggregateCreature>
          }
          groupBy: {
            args: Prisma.CreatureGroupByArgs<ExtArgs>,
            result: $Utils.Optional<CreatureGroupByOutputType>[]
          }
          count: {
            args: Prisma.CreatureCountArgs<ExtArgs>,
            result: $Utils.Optional<CreatureCountAggregateOutputType> | number
          }
        }
      }
    }
  } & {
    other: {
      payload: any
      operations: {
        $executeRawUnsafe: {
          args: [query: string, ...values: any[]],
          result: any
        }
        $executeRaw: {
          args: [query: TemplateStringsArray | Prisma.Sql, ...values: any[]],
          result: any
        }
        $queryRawUnsafe: {
          args: [query: string, ...values: any[]],
          result: any
        }
        $queryRaw: {
          args: [query: TemplateStringsArray | Prisma.Sql, ...values: any[]],
          result: any
        }
      }
    }
  }
  export const defineExtension: $Extensions.ExtendsHook<'define', Prisma.TypeMapCb, $Extensions.DefaultArgs>
  export type DefaultPrismaClient = PrismaClient
  export type ErrorFormat = 'pretty' | 'colorless' | 'minimal'

  export interface PrismaClientOptions {
    /**
     * Overwrites the datasource url from your schema.prisma file
     */
    datasources?: Datasources

    /**
     * Overwrites the datasource url from your schema.prisma file
     */
    datasourceUrl?: string

    /**
     * @default "colorless"
     */
    errorFormat?: ErrorFormat

    /**
     * @example
     * ```
     * // Defaults to stdout
     * log: ['query', 'info', 'warn', 'error']
     * 
     * // Emit as events
     * log: [
     *  { emit: 'stdout', level: 'query' },
     *  { emit: 'stdout', level: 'info' },
     *  { emit: 'stdout', level: 'warn' }
     *  { emit: 'stdout', level: 'error' }
     * ]
     * ```
     * Read more in our [docs](https://www.prisma.io/docs/reference/tools-and-interfaces/prisma-client/logging#the-log-option).
     */
    log?: Array<LogLevel | LogDefinition>
  }

  /* Types for Logging */
  export type LogLevel = 'info' | 'query' | 'warn' | 'error'
  export type LogDefinition = {
    level: LogLevel
    emit: 'stdout' | 'event'
  }

  export type GetLogType<T extends LogLevel | LogDefinition> = T extends LogDefinition ? T['emit'] extends 'event' ? T['level'] : never : never
  export type GetEvents<T extends any> = T extends Array<LogLevel | LogDefinition> ?
    GetLogType<T[0]> | GetLogType<T[1]> | GetLogType<T[2]> | GetLogType<T[3]>
    : never

  export type QueryEvent = {
    timestamp: Date
    query: string
    params: string
    duration: number
    target: string
  }

  export type LogEvent = {
    timestamp: Date
    message: string
    target: string
  }
  /* End Types for Logging */


  export type PrismaAction =
    | 'findUnique'
    | 'findUniqueOrThrow'
    | 'findMany'
    | 'findFirst'
    | 'findFirstOrThrow'
    | 'create'
    | 'createMany'
    | 'update'
    | 'updateMany'
    | 'upsert'
    | 'delete'
    | 'deleteMany'
    | 'executeRaw'
    | 'queryRaw'
    | 'aggregate'
    | 'count'
    | 'runCommandRaw'
    | 'findRaw'
    | 'groupBy'

  /**
   * These options are being passed into the middleware as "params"
   */
  export type MiddlewareParams = {
    model?: ModelName
    action: PrismaAction
    args: any
    dataPath: string[]
    runInTransaction: boolean
  }

  /**
   * The `T` type makes sure, that the `return proceed` is not forgotten in the middleware implementation
   */
  export type Middleware<T = any> = (
    params: MiddlewareParams,
    next: (params: MiddlewareParams) => $Utils.JsPromise<T>,
  ) => $Utils.JsPromise<T>

  // tested in getLogLevel.test.ts
  export function getLogLevel(log: Array<LogLevel | LogDefinition>): LogLevel | undefined;

  /**
   * `PrismaClient` proxy available in interactive transactions.
   */
  export type TransactionClient = Omit<Prisma.DefaultPrismaClient, runtime.ITXClientDenyList>

  export type Datasource = {
    url?: string
  }

  /**
   * Count Types
   */


  /**
   * Count Type UserCountOutputType
   */

  export type UserCountOutputType = {
    builds: number
    tournaments: number
    likesBuilds: number
    dislikesBuilds: number
  }

  export type UserCountOutputTypeSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    builds?: boolean | UserCountOutputTypeCountBuildsArgs
    tournaments?: boolean | UserCountOutputTypeCountTournamentsArgs
    likesBuilds?: boolean | UserCountOutputTypeCountLikesBuildsArgs
    dislikesBuilds?: boolean | UserCountOutputTypeCountDislikesBuildsArgs
  }

  // Custom InputTypes

  /**
   * UserCountOutputType without action
   */
  export type UserCountOutputTypeDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the UserCountOutputType
     */
    select?: UserCountOutputTypeSelect<ExtArgs> | null
  }


  /**
   * UserCountOutputType without action
   */
  export type UserCountOutputTypeCountBuildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: BuildWhereInput
  }


  /**
   * UserCountOutputType without action
   */
  export type UserCountOutputTypeCountTournamentsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: TournamentWhereInput
  }


  /**
   * UserCountOutputType without action
   */
  export type UserCountOutputTypeCountLikesBuildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: LikesWhereInput
  }


  /**
   * UserCountOutputType without action
   */
  export type UserCountOutputTypeCountDislikesBuildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: DisLikesWhereInput
  }



  /**
   * Count Type ChampionCountOutputType
   */

  export type ChampionCountOutputType = {
    builds: number
  }

  export type ChampionCountOutputTypeSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    builds?: boolean | ChampionCountOutputTypeCountBuildsArgs
  }

  // Custom InputTypes

  /**
   * ChampionCountOutputType without action
   */
  export type ChampionCountOutputTypeDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the ChampionCountOutputType
     */
    select?: ChampionCountOutputTypeSelect<ExtArgs> | null
  }


  /**
   * ChampionCountOutputType without action
   */
  export type ChampionCountOutputTypeCountBuildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: BuildWhereInput
  }



  /**
   * Count Type BuildCountOutputType
   */

  export type BuildCountOutputType = {
    Likes: number
    DisLikes: number
  }

  export type BuildCountOutputTypeSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    Likes?: boolean | BuildCountOutputTypeCountLikesArgs
    DisLikes?: boolean | BuildCountOutputTypeCountDisLikesArgs
  }

  // Custom InputTypes

  /**
   * BuildCountOutputType without action
   */
  export type BuildCountOutputTypeDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the BuildCountOutputType
     */
    select?: BuildCountOutputTypeSelect<ExtArgs> | null
  }


  /**
   * BuildCountOutputType without action
   */
  export type BuildCountOutputTypeCountLikesArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: LikesWhereInput
  }


  /**
   * BuildCountOutputType without action
   */
  export type BuildCountOutputTypeCountDisLikesArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: DisLikesWhereInput
  }



  /**
   * Count Type TeamCountOutputType
   */

  export type TeamCountOutputType = {
    members: number
  }

  export type TeamCountOutputTypeSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    members?: boolean | TeamCountOutputTypeCountMembersArgs
  }

  // Custom InputTypes

  /**
   * TeamCountOutputType without action
   */
  export type TeamCountOutputTypeDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the TeamCountOutputType
     */
    select?: TeamCountOutputTypeSelect<ExtArgs> | null
  }


  /**
   * TeamCountOutputType without action
   */
  export type TeamCountOutputTypeCountMembersArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: UserWhereInput
  }



  /**
   * Models
   */

  /**
   * Model User
   */

  export type AggregateUser = {
    _count: UserCountAggregateOutputType | null
    _avg: UserAvgAggregateOutputType | null
    _sum: UserSumAggregateOutputType | null
    _min: UserMinAggregateOutputType | null
    _max: UserMaxAggregateOutputType | null
  }

  export type UserAvgAggregateOutputType = {
    id: number | null
    gameRank: number | null
    kills: number | null
    deaths: number | null
    assists: number | null
    tournamentWins: number | null
    gameWins: number | null
    gameLosed: number | null
    teamID: number | null
  }

  export type UserSumAggregateOutputType = {
    id: number | null
    gameRank: number | null
    kills: number | null
    deaths: number | null
    assists: number | null
    tournamentWins: number[]
    gameWins: number | null
    gameLosed: number | null
    teamID: number | null
  }

  export type UserMinAggregateOutputType = {
    id: number | null
    login: string | null
    displayName: string | null
    discord: string | null
    telegram: string | null
    gameRank: number | null
    email: string | null
    isEmailConfirmed: boolean | null
    password: string | null
    icon: string | null
    role: string | null
    refreshToken: string | null
    isDisabled: boolean | null
    kills: number | null
    deaths: number | null
    assists: number | null
    gameWins: number | null
    gameLosed: number | null
    teamID: number | null
  }

  export type UserMaxAggregateOutputType = {
    id: number | null
    login: string | null
    displayName: string | null
    discord: string | null
    telegram: string | null
    gameRank: number | null
    email: string | null
    isEmailConfirmed: boolean | null
    password: string | null
    icon: string | null
    role: string | null
    refreshToken: string | null
    isDisabled: boolean | null
    kills: number | null
    deaths: number | null
    assists: number | null
    gameWins: number | null
    gameLosed: number | null
    teamID: number | null
  }

  export type UserCountAggregateOutputType = {
    id: number
    login: number
    displayName: number
    discord: number
    telegram: number
    gameRank: number
    email: number
    isEmailConfirmed: number
    password: number
    icon: number
    role: number
    refreshToken: number
    isDisabled: number
    kills: number
    deaths: number
    assists: number
    tournamentWins: number
    gameWins: number
    gameLosed: number
    teamID: number
    _all: number
  }


  export type UserAvgAggregateInputType = {
    id?: true
    gameRank?: true
    kills?: true
    deaths?: true
    assists?: true
    tournamentWins?: true
    gameWins?: true
    gameLosed?: true
    teamID?: true
  }

  export type UserSumAggregateInputType = {
    id?: true
    gameRank?: true
    kills?: true
    deaths?: true
    assists?: true
    tournamentWins?: true
    gameWins?: true
    gameLosed?: true
    teamID?: true
  }

  export type UserMinAggregateInputType = {
    id?: true
    login?: true
    displayName?: true
    discord?: true
    telegram?: true
    gameRank?: true
    email?: true
    isEmailConfirmed?: true
    password?: true
    icon?: true
    role?: true
    refreshToken?: true
    isDisabled?: true
    kills?: true
    deaths?: true
    assists?: true
    gameWins?: true
    gameLosed?: true
    teamID?: true
  }

  export type UserMaxAggregateInputType = {
    id?: true
    login?: true
    displayName?: true
    discord?: true
    telegram?: true
    gameRank?: true
    email?: true
    isEmailConfirmed?: true
    password?: true
    icon?: true
    role?: true
    refreshToken?: true
    isDisabled?: true
    kills?: true
    deaths?: true
    assists?: true
    gameWins?: true
    gameLosed?: true
    teamID?: true
  }

  export type UserCountAggregateInputType = {
    id?: true
    login?: true
    displayName?: true
    discord?: true
    telegram?: true
    gameRank?: true
    email?: true
    isEmailConfirmed?: true
    password?: true
    icon?: true
    role?: true
    refreshToken?: true
    isDisabled?: true
    kills?: true
    deaths?: true
    assists?: true
    tournamentWins?: true
    gameWins?: true
    gameLosed?: true
    teamID?: true
    _all?: true
  }

  export type UserAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which User to aggregate.
     */
    where?: UserWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Users to fetch.
     */
    orderBy?: UserOrderByWithRelationInput | UserOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: UserWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Users from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Users.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Users
    **/
    _count?: true | UserCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: UserAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: UserSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: UserMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: UserMaxAggregateInputType
  }

  export type GetUserAggregateType<T extends UserAggregateArgs> = {
        [P in keyof T & keyof AggregateUser]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateUser[P]>
      : GetScalarType<T[P], AggregateUser[P]>
  }




  export type UserGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: UserWhereInput
    orderBy?: UserOrderByWithAggregationInput | UserOrderByWithAggregationInput[]
    by: UserScalarFieldEnum[] | UserScalarFieldEnum
    having?: UserScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: UserCountAggregateInputType | true
    _avg?: UserAvgAggregateInputType
    _sum?: UserSumAggregateInputType
    _min?: UserMinAggregateInputType
    _max?: UserMaxAggregateInputType
  }

  export type UserGroupByOutputType = {
    id: number
    login: string
    displayName: string
    discord: string
    telegram: string
    gameRank: number
    email: string
    isEmailConfirmed: boolean
    password: string
    icon: string
    role: string
    refreshToken: string
    isDisabled: boolean
    kills: number
    deaths: number
    assists: number
    tournamentWins: number[]
    gameWins: number
    gameLosed: number
    teamID: number | null
    _count: UserCountAggregateOutputType | null
    _avg: UserAvgAggregateOutputType | null
    _sum: UserSumAggregateOutputType | null
    _min: UserMinAggregateOutputType | null
    _max: UserMaxAggregateOutputType | null
  }

  type GetUserGroupByPayload<T extends UserGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<UserGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof UserGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], UserGroupByOutputType[P]>
            : GetScalarType<T[P], UserGroupByOutputType[P]>
        }
      >
    >


  export type UserSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    login?: boolean
    displayName?: boolean
    discord?: boolean
    telegram?: boolean
    gameRank?: boolean
    email?: boolean
    isEmailConfirmed?: boolean
    password?: boolean
    icon?: boolean
    role?: boolean
    refreshToken?: boolean
    isDisabled?: boolean
    kills?: boolean
    deaths?: boolean
    assists?: boolean
    tournamentWins?: boolean
    gameWins?: boolean
    gameLosed?: boolean
    teamID?: boolean
    team?: boolean | User$teamArgs<ExtArgs>
    ownedTeam?: boolean | User$ownedTeamArgs<ExtArgs>
    builds?: boolean | User$buildsArgs<ExtArgs>
    tournaments?: boolean | User$tournamentsArgs<ExtArgs>
    likesBuilds?: boolean | User$likesBuildsArgs<ExtArgs>
    dislikesBuilds?: boolean | User$dislikesBuildsArgs<ExtArgs>
    _count?: boolean | UserCountOutputTypeDefaultArgs<ExtArgs>
  }, ExtArgs["result"]["user"]>

  export type UserSelectScalar = {
    id?: boolean
    login?: boolean
    displayName?: boolean
    discord?: boolean
    telegram?: boolean
    gameRank?: boolean
    email?: boolean
    isEmailConfirmed?: boolean
    password?: boolean
    icon?: boolean
    role?: boolean
    refreshToken?: boolean
    isDisabled?: boolean
    kills?: boolean
    deaths?: boolean
    assists?: boolean
    tournamentWins?: boolean
    gameWins?: boolean
    gameLosed?: boolean
    teamID?: boolean
  }

  export type UserInclude<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    team?: boolean | User$teamArgs<ExtArgs>
    ownedTeam?: boolean | User$ownedTeamArgs<ExtArgs>
    builds?: boolean | User$buildsArgs<ExtArgs>
    tournaments?: boolean | User$tournamentsArgs<ExtArgs>
    likesBuilds?: boolean | User$likesBuildsArgs<ExtArgs>
    dislikesBuilds?: boolean | User$dislikesBuildsArgs<ExtArgs>
    _count?: boolean | UserCountOutputTypeDefaultArgs<ExtArgs>
  }


  export type $UserPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "User"
    objects: {
      team: Prisma.$TeamPayload<ExtArgs> | null
      ownedTeam: Prisma.$TeamPayload<ExtArgs> | null
      builds: Prisma.$BuildPayload<ExtArgs>[]
      tournaments: Prisma.$TournamentPayload<ExtArgs>[]
      likesBuilds: Prisma.$LikesPayload<ExtArgs>[]
      dislikesBuilds: Prisma.$DisLikesPayload<ExtArgs>[]
    }
    scalars: $Extensions.GetResult<{
      id: number
      login: string
      displayName: string
      discord: string
      telegram: string
      gameRank: number
      email: string
      isEmailConfirmed: boolean
      password: string
      icon: string
      role: string
      refreshToken: string
      isDisabled: boolean
      kills: number
      deaths: number
      assists: number
      tournamentWins: number[]
      gameWins: number
      gameLosed: number
      teamID: number | null
    }, ExtArgs["result"]["user"]>
    composites: {}
  }


  type UserGetPayload<S extends boolean | null | undefined | UserDefaultArgs> = $Result.GetResult<Prisma.$UserPayload, S>

  type UserCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<UserFindManyArgs, 'select' | 'include'> & {
      select?: UserCountAggregateInputType | true
    }

  export interface UserDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['User'], meta: { name: 'User' } }
    /**
     * Find zero or one User that matches the filter.
     * @param {UserFindUniqueArgs} args - Arguments to find a User
     * @example
     * // Get one User
     * const user = await prisma.user.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends UserFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, UserFindUniqueArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one User that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {UserFindUniqueOrThrowArgs} args - Arguments to find a User
     * @example
     * // Get one User
     * const user = await prisma.user.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends UserFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, UserFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first User that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {UserFindFirstArgs} args - Arguments to find a User
     * @example
     * // Get one User
     * const user = await prisma.user.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends UserFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, UserFindFirstArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first User that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {UserFindFirstOrThrowArgs} args - Arguments to find a User
     * @example
     * // Get one User
     * const user = await prisma.user.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends UserFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, UserFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Users that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {UserFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Users
     * const users = await prisma.user.findMany()
     * 
     * // Get first 10 Users
     * const users = await prisma.user.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const userWithIdOnly = await prisma.user.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends UserFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, UserFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a User.
     * @param {UserCreateArgs} args - Arguments to create a User.
     * @example
     * // Create one User
     * const User = await prisma.user.create({
     *   data: {
     *     // ... data to create a User
     *   }
     * })
     * 
    **/
    create<T extends UserCreateArgs<ExtArgs>>(
      args: SelectSubset<T, UserCreateArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Users.
     *     @param {UserCreateManyArgs} args - Arguments to create many Users.
     *     @example
     *     // Create many Users
     *     const user = await prisma.user.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends UserCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, UserCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a User.
     * @param {UserDeleteArgs} args - Arguments to delete one User.
     * @example
     * // Delete one User
     * const User = await prisma.user.delete({
     *   where: {
     *     // ... filter to delete one User
     *   }
     * })
     * 
    **/
    delete<T extends UserDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, UserDeleteArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one User.
     * @param {UserUpdateArgs} args - Arguments to update one User.
     * @example
     * // Update one User
     * const user = await prisma.user.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends UserUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, UserUpdateArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Users.
     * @param {UserDeleteManyArgs} args - Arguments to filter Users to delete.
     * @example
     * // Delete a few Users
     * const { count } = await prisma.user.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends UserDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, UserDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Users.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {UserUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Users
     * const user = await prisma.user.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends UserUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, UserUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one User.
     * @param {UserUpsertArgs} args - Arguments to update or create a User.
     * @example
     * // Update or create a User
     * const user = await prisma.user.upsert({
     *   create: {
     *     // ... data to create a User
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the User we want to update
     *   }
     * })
    **/
    upsert<T extends UserUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, UserUpsertArgs<ExtArgs>>
    ): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Users.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {UserCountArgs} args - Arguments to filter Users to count.
     * @example
     * // Count the number of Users
     * const count = await prisma.user.count({
     *   where: {
     *     // ... the filter for the Users we want to count
     *   }
     * })
    **/
    count<T extends UserCountArgs>(
      args?: Subset<T, UserCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], UserCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a User.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {UserAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends UserAggregateArgs>(args: Subset<T, UserAggregateArgs>): Prisma.PrismaPromise<GetUserAggregateType<T>>

    /**
     * Group by User.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {UserGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends UserGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: UserGroupByArgs['orderBy'] }
        : { orderBy?: UserGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, UserGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetUserGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the User model
   */
  readonly fields: UserFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for User.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__UserClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';

    team<T extends User$teamArgs<ExtArgs> = {}>(args?: Subset<T, User$teamArgs<ExtArgs>>): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'findUniqueOrThrow'> | null, null, ExtArgs>;

    ownedTeam<T extends User$ownedTeamArgs<ExtArgs> = {}>(args?: Subset<T, User$ownedTeamArgs<ExtArgs>>): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'findUniqueOrThrow'> | null, null, ExtArgs>;

    builds<T extends User$buildsArgs<ExtArgs> = {}>(args?: Subset<T, User$buildsArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findMany'> | Null>;

    tournaments<T extends User$tournamentsArgs<ExtArgs> = {}>(args?: Subset<T, User$tournamentsArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'findMany'> | Null>;

    likesBuilds<T extends User$likesBuildsArgs<ExtArgs> = {}>(args?: Subset<T, User$likesBuildsArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'findMany'> | Null>;

    dislikesBuilds<T extends User$dislikesBuildsArgs<ExtArgs> = {}>(args?: Subset<T, User$dislikesBuildsArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'findMany'> | Null>;

    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the User model
   */ 
  interface UserFieldRefs {
    readonly id: FieldRef<"User", 'Int'>
    readonly login: FieldRef<"User", 'String'>
    readonly displayName: FieldRef<"User", 'String'>
    readonly discord: FieldRef<"User", 'String'>
    readonly telegram: FieldRef<"User", 'String'>
    readonly gameRank: FieldRef<"User", 'Int'>
    readonly email: FieldRef<"User", 'String'>
    readonly isEmailConfirmed: FieldRef<"User", 'Boolean'>
    readonly password: FieldRef<"User", 'String'>
    readonly icon: FieldRef<"User", 'String'>
    readonly role: FieldRef<"User", 'String'>
    readonly refreshToken: FieldRef<"User", 'String'>
    readonly isDisabled: FieldRef<"User", 'Boolean'>
    readonly kills: FieldRef<"User", 'Int'>
    readonly deaths: FieldRef<"User", 'Int'>
    readonly assists: FieldRef<"User", 'Int'>
    readonly tournamentWins: FieldRef<"User", 'Int[]'>
    readonly gameWins: FieldRef<"User", 'Int'>
    readonly gameLosed: FieldRef<"User", 'Int'>
    readonly teamID: FieldRef<"User", 'Int'>
  }
    

  // Custom InputTypes

  /**
   * User findUnique
   */
  export type UserFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * Filter, which User to fetch.
     */
    where: UserWhereUniqueInput
  }


  /**
   * User findUniqueOrThrow
   */
  export type UserFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * Filter, which User to fetch.
     */
    where: UserWhereUniqueInput
  }


  /**
   * User findFirst
   */
  export type UserFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * Filter, which User to fetch.
     */
    where?: UserWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Users to fetch.
     */
    orderBy?: UserOrderByWithRelationInput | UserOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Users.
     */
    cursor?: UserWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Users from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Users.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Users.
     */
    distinct?: UserScalarFieldEnum | UserScalarFieldEnum[]
  }


  /**
   * User findFirstOrThrow
   */
  export type UserFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * Filter, which User to fetch.
     */
    where?: UserWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Users to fetch.
     */
    orderBy?: UserOrderByWithRelationInput | UserOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Users.
     */
    cursor?: UserWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Users from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Users.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Users.
     */
    distinct?: UserScalarFieldEnum | UserScalarFieldEnum[]
  }


  /**
   * User findMany
   */
  export type UserFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * Filter, which Users to fetch.
     */
    where?: UserWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Users to fetch.
     */
    orderBy?: UserOrderByWithRelationInput | UserOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Users.
     */
    cursor?: UserWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Users from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Users.
     */
    skip?: number
    distinct?: UserScalarFieldEnum | UserScalarFieldEnum[]
  }


  /**
   * User create
   */
  export type UserCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * The data needed to create a User.
     */
    data: XOR<UserCreateInput, UserUncheckedCreateInput>
  }


  /**
   * User createMany
   */
  export type UserCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Users.
     */
    data: UserCreateManyInput | UserCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * User update
   */
  export type UserUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * The data needed to update a User.
     */
    data: XOR<UserUpdateInput, UserUncheckedUpdateInput>
    /**
     * Choose, which User to update.
     */
    where: UserWhereUniqueInput
  }


  /**
   * User updateMany
   */
  export type UserUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Users.
     */
    data: XOR<UserUpdateManyMutationInput, UserUncheckedUpdateManyInput>
    /**
     * Filter which Users to update
     */
    where?: UserWhereInput
  }


  /**
   * User upsert
   */
  export type UserUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * The filter to search for the User to update in case it exists.
     */
    where: UserWhereUniqueInput
    /**
     * In case the User found by the `where` argument doesn't exist, create a new User with this data.
     */
    create: XOR<UserCreateInput, UserUncheckedCreateInput>
    /**
     * In case the User was found with the provided `where` argument, update it with this data.
     */
    update: XOR<UserUpdateInput, UserUncheckedUpdateInput>
  }


  /**
   * User delete
   */
  export type UserDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    /**
     * Filter which User to delete.
     */
    where: UserWhereUniqueInput
  }


  /**
   * User deleteMany
   */
  export type UserDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Users to delete
     */
    where?: UserWhereInput
  }


  /**
   * User.team
   */
  export type User$teamArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    where?: TeamWhereInput
  }


  /**
   * User.ownedTeam
   */
  export type User$ownedTeamArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    where?: TeamWhereInput
  }


  /**
   * User.builds
   */
  export type User$buildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    where?: BuildWhereInput
    orderBy?: BuildOrderByWithRelationInput | BuildOrderByWithRelationInput[]
    cursor?: BuildWhereUniqueInput
    take?: number
    skip?: number
    distinct?: BuildScalarFieldEnum | BuildScalarFieldEnum[]
  }


  /**
   * User.tournaments
   */
  export type User$tournamentsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    where?: TournamentWhereInput
    orderBy?: TournamentOrderByWithRelationInput | TournamentOrderByWithRelationInput[]
    cursor?: TournamentWhereUniqueInput
    take?: number
    skip?: number
    distinct?: TournamentScalarFieldEnum | TournamentScalarFieldEnum[]
  }


  /**
   * User.likesBuilds
   */
  export type User$likesBuildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    where?: LikesWhereInput
    orderBy?: LikesOrderByWithRelationInput | LikesOrderByWithRelationInput[]
    cursor?: LikesWhereUniqueInput
    take?: number
    skip?: number
    distinct?: LikesScalarFieldEnum | LikesScalarFieldEnum[]
  }


  /**
   * User.dislikesBuilds
   */
  export type User$dislikesBuildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    where?: DisLikesWhereInput
    orderBy?: DisLikesOrderByWithRelationInput | DisLikesOrderByWithRelationInput[]
    cursor?: DisLikesWhereUniqueInput
    take?: number
    skip?: number
    distinct?: DisLikesScalarFieldEnum | DisLikesScalarFieldEnum[]
  }


  /**
   * User without action
   */
  export type UserDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
  }



  /**
   * Model RegistrationToken
   */

  export type AggregateRegistrationToken = {
    _count: RegistrationTokenCountAggregateOutputType | null
    _avg: RegistrationTokenAvgAggregateOutputType | null
    _sum: RegistrationTokenSumAggregateOutputType | null
    _min: RegistrationTokenMinAggregateOutputType | null
    _max: RegistrationTokenMaxAggregateOutputType | null
  }

  export type RegistrationTokenAvgAggregateOutputType = {
    id: number | null
  }

  export type RegistrationTokenSumAggregateOutputType = {
    id: number | null
  }

  export type RegistrationTokenMinAggregateOutputType = {
    id: number | null
    token: string | null
  }

  export type RegistrationTokenMaxAggregateOutputType = {
    id: number | null
    token: string | null
  }

  export type RegistrationTokenCountAggregateOutputType = {
    id: number
    token: number
    _all: number
  }


  export type RegistrationTokenAvgAggregateInputType = {
    id?: true
  }

  export type RegistrationTokenSumAggregateInputType = {
    id?: true
  }

  export type RegistrationTokenMinAggregateInputType = {
    id?: true
    token?: true
  }

  export type RegistrationTokenMaxAggregateInputType = {
    id?: true
    token?: true
  }

  export type RegistrationTokenCountAggregateInputType = {
    id?: true
    token?: true
    _all?: true
  }

  export type RegistrationTokenAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which RegistrationToken to aggregate.
     */
    where?: RegistrationTokenWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of RegistrationTokens to fetch.
     */
    orderBy?: RegistrationTokenOrderByWithRelationInput | RegistrationTokenOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: RegistrationTokenWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` RegistrationTokens from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` RegistrationTokens.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned RegistrationTokens
    **/
    _count?: true | RegistrationTokenCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: RegistrationTokenAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: RegistrationTokenSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: RegistrationTokenMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: RegistrationTokenMaxAggregateInputType
  }

  export type GetRegistrationTokenAggregateType<T extends RegistrationTokenAggregateArgs> = {
        [P in keyof T & keyof AggregateRegistrationToken]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateRegistrationToken[P]>
      : GetScalarType<T[P], AggregateRegistrationToken[P]>
  }




  export type RegistrationTokenGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: RegistrationTokenWhereInput
    orderBy?: RegistrationTokenOrderByWithAggregationInput | RegistrationTokenOrderByWithAggregationInput[]
    by: RegistrationTokenScalarFieldEnum[] | RegistrationTokenScalarFieldEnum
    having?: RegistrationTokenScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: RegistrationTokenCountAggregateInputType | true
    _avg?: RegistrationTokenAvgAggregateInputType
    _sum?: RegistrationTokenSumAggregateInputType
    _min?: RegistrationTokenMinAggregateInputType
    _max?: RegistrationTokenMaxAggregateInputType
  }

  export type RegistrationTokenGroupByOutputType = {
    id: number
    token: string
    _count: RegistrationTokenCountAggregateOutputType | null
    _avg: RegistrationTokenAvgAggregateOutputType | null
    _sum: RegistrationTokenSumAggregateOutputType | null
    _min: RegistrationTokenMinAggregateOutputType | null
    _max: RegistrationTokenMaxAggregateOutputType | null
  }

  type GetRegistrationTokenGroupByPayload<T extends RegistrationTokenGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<RegistrationTokenGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof RegistrationTokenGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], RegistrationTokenGroupByOutputType[P]>
            : GetScalarType<T[P], RegistrationTokenGroupByOutputType[P]>
        }
      >
    >


  export type RegistrationTokenSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    token?: boolean
  }, ExtArgs["result"]["registrationToken"]>

  export type RegistrationTokenSelectScalar = {
    id?: boolean
    token?: boolean
  }


  export type $RegistrationTokenPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "RegistrationToken"
    objects: {}
    scalars: $Extensions.GetResult<{
      id: number
      token: string
    }, ExtArgs["result"]["registrationToken"]>
    composites: {}
  }


  type RegistrationTokenGetPayload<S extends boolean | null | undefined | RegistrationTokenDefaultArgs> = $Result.GetResult<Prisma.$RegistrationTokenPayload, S>

  type RegistrationTokenCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<RegistrationTokenFindManyArgs, 'select' | 'include'> & {
      select?: RegistrationTokenCountAggregateInputType | true
    }

  export interface RegistrationTokenDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['RegistrationToken'], meta: { name: 'RegistrationToken' } }
    /**
     * Find zero or one RegistrationToken that matches the filter.
     * @param {RegistrationTokenFindUniqueArgs} args - Arguments to find a RegistrationToken
     * @example
     * // Get one RegistrationToken
     * const registrationToken = await prisma.registrationToken.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends RegistrationTokenFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, RegistrationTokenFindUniqueArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one RegistrationToken that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {RegistrationTokenFindUniqueOrThrowArgs} args - Arguments to find a RegistrationToken
     * @example
     * // Get one RegistrationToken
     * const registrationToken = await prisma.registrationToken.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends RegistrationTokenFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, RegistrationTokenFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first RegistrationToken that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {RegistrationTokenFindFirstArgs} args - Arguments to find a RegistrationToken
     * @example
     * // Get one RegistrationToken
     * const registrationToken = await prisma.registrationToken.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends RegistrationTokenFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, RegistrationTokenFindFirstArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first RegistrationToken that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {RegistrationTokenFindFirstOrThrowArgs} args - Arguments to find a RegistrationToken
     * @example
     * // Get one RegistrationToken
     * const registrationToken = await prisma.registrationToken.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends RegistrationTokenFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, RegistrationTokenFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more RegistrationTokens that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {RegistrationTokenFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all RegistrationTokens
     * const registrationTokens = await prisma.registrationToken.findMany()
     * 
     * // Get first 10 RegistrationTokens
     * const registrationTokens = await prisma.registrationToken.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const registrationTokenWithIdOnly = await prisma.registrationToken.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends RegistrationTokenFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, RegistrationTokenFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a RegistrationToken.
     * @param {RegistrationTokenCreateArgs} args - Arguments to create a RegistrationToken.
     * @example
     * // Create one RegistrationToken
     * const RegistrationToken = await prisma.registrationToken.create({
     *   data: {
     *     // ... data to create a RegistrationToken
     *   }
     * })
     * 
    **/
    create<T extends RegistrationTokenCreateArgs<ExtArgs>>(
      args: SelectSubset<T, RegistrationTokenCreateArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many RegistrationTokens.
     *     @param {RegistrationTokenCreateManyArgs} args - Arguments to create many RegistrationTokens.
     *     @example
     *     // Create many RegistrationTokens
     *     const registrationToken = await prisma.registrationToken.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends RegistrationTokenCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, RegistrationTokenCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a RegistrationToken.
     * @param {RegistrationTokenDeleteArgs} args - Arguments to delete one RegistrationToken.
     * @example
     * // Delete one RegistrationToken
     * const RegistrationToken = await prisma.registrationToken.delete({
     *   where: {
     *     // ... filter to delete one RegistrationToken
     *   }
     * })
     * 
    **/
    delete<T extends RegistrationTokenDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, RegistrationTokenDeleteArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one RegistrationToken.
     * @param {RegistrationTokenUpdateArgs} args - Arguments to update one RegistrationToken.
     * @example
     * // Update one RegistrationToken
     * const registrationToken = await prisma.registrationToken.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends RegistrationTokenUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, RegistrationTokenUpdateArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more RegistrationTokens.
     * @param {RegistrationTokenDeleteManyArgs} args - Arguments to filter RegistrationTokens to delete.
     * @example
     * // Delete a few RegistrationTokens
     * const { count } = await prisma.registrationToken.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends RegistrationTokenDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, RegistrationTokenDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more RegistrationTokens.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {RegistrationTokenUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many RegistrationTokens
     * const registrationToken = await prisma.registrationToken.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends RegistrationTokenUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, RegistrationTokenUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one RegistrationToken.
     * @param {RegistrationTokenUpsertArgs} args - Arguments to update or create a RegistrationToken.
     * @example
     * // Update or create a RegistrationToken
     * const registrationToken = await prisma.registrationToken.upsert({
     *   create: {
     *     // ... data to create a RegistrationToken
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the RegistrationToken we want to update
     *   }
     * })
    **/
    upsert<T extends RegistrationTokenUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, RegistrationTokenUpsertArgs<ExtArgs>>
    ): Prisma__RegistrationTokenClient<$Result.GetResult<Prisma.$RegistrationTokenPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of RegistrationTokens.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {RegistrationTokenCountArgs} args - Arguments to filter RegistrationTokens to count.
     * @example
     * // Count the number of RegistrationTokens
     * const count = await prisma.registrationToken.count({
     *   where: {
     *     // ... the filter for the RegistrationTokens we want to count
     *   }
     * })
    **/
    count<T extends RegistrationTokenCountArgs>(
      args?: Subset<T, RegistrationTokenCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], RegistrationTokenCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a RegistrationToken.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {RegistrationTokenAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends RegistrationTokenAggregateArgs>(args: Subset<T, RegistrationTokenAggregateArgs>): Prisma.PrismaPromise<GetRegistrationTokenAggregateType<T>>

    /**
     * Group by RegistrationToken.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {RegistrationTokenGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends RegistrationTokenGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: RegistrationTokenGroupByArgs['orderBy'] }
        : { orderBy?: RegistrationTokenGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, RegistrationTokenGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetRegistrationTokenGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the RegistrationToken model
   */
  readonly fields: RegistrationTokenFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for RegistrationToken.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__RegistrationTokenClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';


    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the RegistrationToken model
   */ 
  interface RegistrationTokenFieldRefs {
    readonly id: FieldRef<"RegistrationToken", 'Int'>
    readonly token: FieldRef<"RegistrationToken", 'String'>
  }
    

  // Custom InputTypes

  /**
   * RegistrationToken findUnique
   */
  export type RegistrationTokenFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * Filter, which RegistrationToken to fetch.
     */
    where: RegistrationTokenWhereUniqueInput
  }


  /**
   * RegistrationToken findUniqueOrThrow
   */
  export type RegistrationTokenFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * Filter, which RegistrationToken to fetch.
     */
    where: RegistrationTokenWhereUniqueInput
  }


  /**
   * RegistrationToken findFirst
   */
  export type RegistrationTokenFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * Filter, which RegistrationToken to fetch.
     */
    where?: RegistrationTokenWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of RegistrationTokens to fetch.
     */
    orderBy?: RegistrationTokenOrderByWithRelationInput | RegistrationTokenOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for RegistrationTokens.
     */
    cursor?: RegistrationTokenWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` RegistrationTokens from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` RegistrationTokens.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of RegistrationTokens.
     */
    distinct?: RegistrationTokenScalarFieldEnum | RegistrationTokenScalarFieldEnum[]
  }


  /**
   * RegistrationToken findFirstOrThrow
   */
  export type RegistrationTokenFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * Filter, which RegistrationToken to fetch.
     */
    where?: RegistrationTokenWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of RegistrationTokens to fetch.
     */
    orderBy?: RegistrationTokenOrderByWithRelationInput | RegistrationTokenOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for RegistrationTokens.
     */
    cursor?: RegistrationTokenWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` RegistrationTokens from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` RegistrationTokens.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of RegistrationTokens.
     */
    distinct?: RegistrationTokenScalarFieldEnum | RegistrationTokenScalarFieldEnum[]
  }


  /**
   * RegistrationToken findMany
   */
  export type RegistrationTokenFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * Filter, which RegistrationTokens to fetch.
     */
    where?: RegistrationTokenWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of RegistrationTokens to fetch.
     */
    orderBy?: RegistrationTokenOrderByWithRelationInput | RegistrationTokenOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing RegistrationTokens.
     */
    cursor?: RegistrationTokenWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` RegistrationTokens from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` RegistrationTokens.
     */
    skip?: number
    distinct?: RegistrationTokenScalarFieldEnum | RegistrationTokenScalarFieldEnum[]
  }


  /**
   * RegistrationToken create
   */
  export type RegistrationTokenCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * The data needed to create a RegistrationToken.
     */
    data: XOR<RegistrationTokenCreateInput, RegistrationTokenUncheckedCreateInput>
  }


  /**
   * RegistrationToken createMany
   */
  export type RegistrationTokenCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many RegistrationTokens.
     */
    data: RegistrationTokenCreateManyInput | RegistrationTokenCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * RegistrationToken update
   */
  export type RegistrationTokenUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * The data needed to update a RegistrationToken.
     */
    data: XOR<RegistrationTokenUpdateInput, RegistrationTokenUncheckedUpdateInput>
    /**
     * Choose, which RegistrationToken to update.
     */
    where: RegistrationTokenWhereUniqueInput
  }


  /**
   * RegistrationToken updateMany
   */
  export type RegistrationTokenUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update RegistrationTokens.
     */
    data: XOR<RegistrationTokenUpdateManyMutationInput, RegistrationTokenUncheckedUpdateManyInput>
    /**
     * Filter which RegistrationTokens to update
     */
    where?: RegistrationTokenWhereInput
  }


  /**
   * RegistrationToken upsert
   */
  export type RegistrationTokenUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * The filter to search for the RegistrationToken to update in case it exists.
     */
    where: RegistrationTokenWhereUniqueInput
    /**
     * In case the RegistrationToken found by the `where` argument doesn't exist, create a new RegistrationToken with this data.
     */
    create: XOR<RegistrationTokenCreateInput, RegistrationTokenUncheckedCreateInput>
    /**
     * In case the RegistrationToken was found with the provided `where` argument, update it with this data.
     */
    update: XOR<RegistrationTokenUpdateInput, RegistrationTokenUncheckedUpdateInput>
  }


  /**
   * RegistrationToken delete
   */
  export type RegistrationTokenDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
    /**
     * Filter which RegistrationToken to delete.
     */
    where: RegistrationTokenWhereUniqueInput
  }


  /**
   * RegistrationToken deleteMany
   */
  export type RegistrationTokenDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which RegistrationTokens to delete
     */
    where?: RegistrationTokenWhereInput
  }


  /**
   * RegistrationToken without action
   */
  export type RegistrationTokenDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the RegistrationToken
     */
    select?: RegistrationTokenSelect<ExtArgs> | null
  }



  /**
   * Model Champion
   */

  export type AggregateChampion = {
    _count: ChampionCountAggregateOutputType | null
    _avg: ChampionAvgAggregateOutputType | null
    _sum: ChampionSumAggregateOutputType | null
    _min: ChampionMinAggregateOutputType | null
    _max: ChampionMaxAggregateOutputType | null
  }

  export type ChampionAvgAggregateOutputType = {
    id: number | null
    tier: number | null
  }

  export type ChampionSumAggregateOutputType = {
    id: number | null
    tier: number | null
  }

  export type ChampionMinAggregateOutputType = {
    id: number | null
    name: string | null
    role: $Enums.Role | null
    tier: number | null
  }

  export type ChampionMaxAggregateOutputType = {
    id: number | null
    name: string | null
    role: $Enums.Role | null
    tier: number | null
  }

  export type ChampionCountAggregateOutputType = {
    id: number
    name: number
    role: number
    tier: number
    counterPicks: number
    _all: number
  }


  export type ChampionAvgAggregateInputType = {
    id?: true
    tier?: true
  }

  export type ChampionSumAggregateInputType = {
    id?: true
    tier?: true
  }

  export type ChampionMinAggregateInputType = {
    id?: true
    name?: true
    role?: true
    tier?: true
  }

  export type ChampionMaxAggregateInputType = {
    id?: true
    name?: true
    role?: true
    tier?: true
  }

  export type ChampionCountAggregateInputType = {
    id?: true
    name?: true
    role?: true
    tier?: true
    counterPicks?: true
    _all?: true
  }

  export type ChampionAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Champion to aggregate.
     */
    where?: ChampionWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Champions to fetch.
     */
    orderBy?: ChampionOrderByWithRelationInput | ChampionOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: ChampionWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Champions from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Champions.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Champions
    **/
    _count?: true | ChampionCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: ChampionAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: ChampionSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: ChampionMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: ChampionMaxAggregateInputType
  }

  export type GetChampionAggregateType<T extends ChampionAggregateArgs> = {
        [P in keyof T & keyof AggregateChampion]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateChampion[P]>
      : GetScalarType<T[P], AggregateChampion[P]>
  }




  export type ChampionGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: ChampionWhereInput
    orderBy?: ChampionOrderByWithAggregationInput | ChampionOrderByWithAggregationInput[]
    by: ChampionScalarFieldEnum[] | ChampionScalarFieldEnum
    having?: ChampionScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: ChampionCountAggregateInputType | true
    _avg?: ChampionAvgAggregateInputType
    _sum?: ChampionSumAggregateInputType
    _min?: ChampionMinAggregateInputType
    _max?: ChampionMaxAggregateInputType
  }

  export type ChampionGroupByOutputType = {
    id: number
    name: string
    role: $Enums.Role
    tier: number
    counterPicks: string[]
    _count: ChampionCountAggregateOutputType | null
    _avg: ChampionAvgAggregateOutputType | null
    _sum: ChampionSumAggregateOutputType | null
    _min: ChampionMinAggregateOutputType | null
    _max: ChampionMaxAggregateOutputType | null
  }

  type GetChampionGroupByPayload<T extends ChampionGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<ChampionGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof ChampionGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], ChampionGroupByOutputType[P]>
            : GetScalarType<T[P], ChampionGroupByOutputType[P]>
        }
      >
    >


  export type ChampionSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    name?: boolean
    role?: boolean
    tier?: boolean
    counterPicks?: boolean
    builds?: boolean | Champion$buildsArgs<ExtArgs>
    _count?: boolean | ChampionCountOutputTypeDefaultArgs<ExtArgs>
  }, ExtArgs["result"]["champion"]>

  export type ChampionSelectScalar = {
    id?: boolean
    name?: boolean
    role?: boolean
    tier?: boolean
    counterPicks?: boolean
  }

  export type ChampionInclude<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    builds?: boolean | Champion$buildsArgs<ExtArgs>
    _count?: boolean | ChampionCountOutputTypeDefaultArgs<ExtArgs>
  }


  export type $ChampionPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Champion"
    objects: {
      builds: Prisma.$BuildPayload<ExtArgs>[]
    }
    scalars: $Extensions.GetResult<{
      id: number
      name: string
      role: $Enums.Role
      tier: number
      counterPicks: string[]
    }, ExtArgs["result"]["champion"]>
    composites: {}
  }


  type ChampionGetPayload<S extends boolean | null | undefined | ChampionDefaultArgs> = $Result.GetResult<Prisma.$ChampionPayload, S>

  type ChampionCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<ChampionFindManyArgs, 'select' | 'include'> & {
      select?: ChampionCountAggregateInputType | true
    }

  export interface ChampionDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Champion'], meta: { name: 'Champion' } }
    /**
     * Find zero or one Champion that matches the filter.
     * @param {ChampionFindUniqueArgs} args - Arguments to find a Champion
     * @example
     * // Get one Champion
     * const champion = await prisma.champion.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends ChampionFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, ChampionFindUniqueArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Champion that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {ChampionFindUniqueOrThrowArgs} args - Arguments to find a Champion
     * @example
     * // Get one Champion
     * const champion = await prisma.champion.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends ChampionFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, ChampionFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Champion that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ChampionFindFirstArgs} args - Arguments to find a Champion
     * @example
     * // Get one Champion
     * const champion = await prisma.champion.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends ChampionFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, ChampionFindFirstArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Champion that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ChampionFindFirstOrThrowArgs} args - Arguments to find a Champion
     * @example
     * // Get one Champion
     * const champion = await prisma.champion.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends ChampionFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, ChampionFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Champions that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ChampionFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Champions
     * const champions = await prisma.champion.findMany()
     * 
     * // Get first 10 Champions
     * const champions = await prisma.champion.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const championWithIdOnly = await prisma.champion.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends ChampionFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, ChampionFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Champion.
     * @param {ChampionCreateArgs} args - Arguments to create a Champion.
     * @example
     * // Create one Champion
     * const Champion = await prisma.champion.create({
     *   data: {
     *     // ... data to create a Champion
     *   }
     * })
     * 
    **/
    create<T extends ChampionCreateArgs<ExtArgs>>(
      args: SelectSubset<T, ChampionCreateArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Champions.
     *     @param {ChampionCreateManyArgs} args - Arguments to create many Champions.
     *     @example
     *     // Create many Champions
     *     const champion = await prisma.champion.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends ChampionCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, ChampionCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Champion.
     * @param {ChampionDeleteArgs} args - Arguments to delete one Champion.
     * @example
     * // Delete one Champion
     * const Champion = await prisma.champion.delete({
     *   where: {
     *     // ... filter to delete one Champion
     *   }
     * })
     * 
    **/
    delete<T extends ChampionDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, ChampionDeleteArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Champion.
     * @param {ChampionUpdateArgs} args - Arguments to update one Champion.
     * @example
     * // Update one Champion
     * const champion = await prisma.champion.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends ChampionUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, ChampionUpdateArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Champions.
     * @param {ChampionDeleteManyArgs} args - Arguments to filter Champions to delete.
     * @example
     * // Delete a few Champions
     * const { count } = await prisma.champion.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends ChampionDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, ChampionDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Champions.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ChampionUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Champions
     * const champion = await prisma.champion.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends ChampionUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, ChampionUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Champion.
     * @param {ChampionUpsertArgs} args - Arguments to update or create a Champion.
     * @example
     * // Update or create a Champion
     * const champion = await prisma.champion.upsert({
     *   create: {
     *     // ... data to create a Champion
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Champion we want to update
     *   }
     * })
    **/
    upsert<T extends ChampionUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, ChampionUpsertArgs<ExtArgs>>
    ): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Champions.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ChampionCountArgs} args - Arguments to filter Champions to count.
     * @example
     * // Count the number of Champions
     * const count = await prisma.champion.count({
     *   where: {
     *     // ... the filter for the Champions we want to count
     *   }
     * })
    **/
    count<T extends ChampionCountArgs>(
      args?: Subset<T, ChampionCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], ChampionCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Champion.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ChampionAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends ChampionAggregateArgs>(args: Subset<T, ChampionAggregateArgs>): Prisma.PrismaPromise<GetChampionAggregateType<T>>

    /**
     * Group by Champion.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ChampionGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends ChampionGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: ChampionGroupByArgs['orderBy'] }
        : { orderBy?: ChampionGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, ChampionGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetChampionGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Champion model
   */
  readonly fields: ChampionFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Champion.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__ChampionClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';

    builds<T extends Champion$buildsArgs<ExtArgs> = {}>(args?: Subset<T, Champion$buildsArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findMany'> | Null>;

    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Champion model
   */ 
  interface ChampionFieldRefs {
    readonly id: FieldRef<"Champion", 'Int'>
    readonly name: FieldRef<"Champion", 'String'>
    readonly role: FieldRef<"Champion", 'Role'>
    readonly tier: FieldRef<"Champion", 'Int'>
    readonly counterPicks: FieldRef<"Champion", 'String[]'>
  }
    

  // Custom InputTypes

  /**
   * Champion findUnique
   */
  export type ChampionFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * Filter, which Champion to fetch.
     */
    where: ChampionWhereUniqueInput
  }


  /**
   * Champion findUniqueOrThrow
   */
  export type ChampionFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * Filter, which Champion to fetch.
     */
    where: ChampionWhereUniqueInput
  }


  /**
   * Champion findFirst
   */
  export type ChampionFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * Filter, which Champion to fetch.
     */
    where?: ChampionWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Champions to fetch.
     */
    orderBy?: ChampionOrderByWithRelationInput | ChampionOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Champions.
     */
    cursor?: ChampionWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Champions from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Champions.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Champions.
     */
    distinct?: ChampionScalarFieldEnum | ChampionScalarFieldEnum[]
  }


  /**
   * Champion findFirstOrThrow
   */
  export type ChampionFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * Filter, which Champion to fetch.
     */
    where?: ChampionWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Champions to fetch.
     */
    orderBy?: ChampionOrderByWithRelationInput | ChampionOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Champions.
     */
    cursor?: ChampionWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Champions from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Champions.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Champions.
     */
    distinct?: ChampionScalarFieldEnum | ChampionScalarFieldEnum[]
  }


  /**
   * Champion findMany
   */
  export type ChampionFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * Filter, which Champions to fetch.
     */
    where?: ChampionWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Champions to fetch.
     */
    orderBy?: ChampionOrderByWithRelationInput | ChampionOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Champions.
     */
    cursor?: ChampionWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Champions from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Champions.
     */
    skip?: number
    distinct?: ChampionScalarFieldEnum | ChampionScalarFieldEnum[]
  }


  /**
   * Champion create
   */
  export type ChampionCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * The data needed to create a Champion.
     */
    data: XOR<ChampionCreateInput, ChampionUncheckedCreateInput>
  }


  /**
   * Champion createMany
   */
  export type ChampionCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Champions.
     */
    data: ChampionCreateManyInput | ChampionCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Champion update
   */
  export type ChampionUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * The data needed to update a Champion.
     */
    data: XOR<ChampionUpdateInput, ChampionUncheckedUpdateInput>
    /**
     * Choose, which Champion to update.
     */
    where: ChampionWhereUniqueInput
  }


  /**
   * Champion updateMany
   */
  export type ChampionUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Champions.
     */
    data: XOR<ChampionUpdateManyMutationInput, ChampionUncheckedUpdateManyInput>
    /**
     * Filter which Champions to update
     */
    where?: ChampionWhereInput
  }


  /**
   * Champion upsert
   */
  export type ChampionUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * The filter to search for the Champion to update in case it exists.
     */
    where: ChampionWhereUniqueInput
    /**
     * In case the Champion found by the `where` argument doesn't exist, create a new Champion with this data.
     */
    create: XOR<ChampionCreateInput, ChampionUncheckedCreateInput>
    /**
     * In case the Champion was found with the provided `where` argument, update it with this data.
     */
    update: XOR<ChampionUpdateInput, ChampionUncheckedUpdateInput>
  }


  /**
   * Champion delete
   */
  export type ChampionDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
    /**
     * Filter which Champion to delete.
     */
    where: ChampionWhereUniqueInput
  }


  /**
   * Champion deleteMany
   */
  export type ChampionDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Champions to delete
     */
    where?: ChampionWhereInput
  }


  /**
   * Champion.builds
   */
  export type Champion$buildsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    where?: BuildWhereInput
    orderBy?: BuildOrderByWithRelationInput | BuildOrderByWithRelationInput[]
    cursor?: BuildWhereUniqueInput
    take?: number
    skip?: number
    distinct?: BuildScalarFieldEnum | BuildScalarFieldEnum[]
  }


  /**
   * Champion without action
   */
  export type ChampionDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Champion
     */
    select?: ChampionSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: ChampionInclude<ExtArgs> | null
  }



  /**
   * Model Build
   */

  export type AggregateBuild = {
    _count: BuildCountAggregateOutputType | null
    _avg: BuildAvgAggregateOutputType | null
    _sum: BuildSumAggregateOutputType | null
    _min: BuildMinAggregateOutputType | null
    _max: BuildMaxAggregateOutputType | null
  }

  export type BuildAvgAggregateOutputType = {
    id: number | null
    championID: number | null
    creatorID: number | null
  }

  export type BuildSumAggregateOutputType = {
    id: number | null
    championID: number | null
    creatorID: number | null
  }

  export type BuildMinAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    updatedAt: Date | null
    name: string | null
    role: $Enums.Role | null
    championID: number | null
    creatorID: number | null
    keyInsights: string | null
    strengths: string | null
    weaknesses: string | null
  }

  export type BuildMaxAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    updatedAt: Date | null
    name: string | null
    role: $Enums.Role | null
    championID: number | null
    creatorID: number | null
    keyInsights: string | null
    strengths: string | null
    weaknesses: string | null
  }

  export type BuildCountAggregateOutputType = {
    id: number
    createdAt: number
    updatedAt: number
    name: number
    role: number
    championID: number
    creatorID: number
    items: number
    coreItems: number
    situationalItems: number
    skillsOrder: number
    karmas: number
    keyInsights: number
    strengths: number
    weaknesses: number
    _all: number
  }


  export type BuildAvgAggregateInputType = {
    id?: true
    championID?: true
    creatorID?: true
  }

  export type BuildSumAggregateInputType = {
    id?: true
    championID?: true
    creatorID?: true
  }

  export type BuildMinAggregateInputType = {
    id?: true
    createdAt?: true
    updatedAt?: true
    name?: true
    role?: true
    championID?: true
    creatorID?: true
    keyInsights?: true
    strengths?: true
    weaknesses?: true
  }

  export type BuildMaxAggregateInputType = {
    id?: true
    createdAt?: true
    updatedAt?: true
    name?: true
    role?: true
    championID?: true
    creatorID?: true
    keyInsights?: true
    strengths?: true
    weaknesses?: true
  }

  export type BuildCountAggregateInputType = {
    id?: true
    createdAt?: true
    updatedAt?: true
    name?: true
    role?: true
    championID?: true
    creatorID?: true
    items?: true
    coreItems?: true
    situationalItems?: true
    skillsOrder?: true
    karmas?: true
    keyInsights?: true
    strengths?: true
    weaknesses?: true
    _all?: true
  }

  export type BuildAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Build to aggregate.
     */
    where?: BuildWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Builds to fetch.
     */
    orderBy?: BuildOrderByWithRelationInput | BuildOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: BuildWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Builds from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Builds.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Builds
    **/
    _count?: true | BuildCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: BuildAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: BuildSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: BuildMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: BuildMaxAggregateInputType
  }

  export type GetBuildAggregateType<T extends BuildAggregateArgs> = {
        [P in keyof T & keyof AggregateBuild]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateBuild[P]>
      : GetScalarType<T[P], AggregateBuild[P]>
  }




  export type BuildGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: BuildWhereInput
    orderBy?: BuildOrderByWithAggregationInput | BuildOrderByWithAggregationInput[]
    by: BuildScalarFieldEnum[] | BuildScalarFieldEnum
    having?: BuildScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: BuildCountAggregateInputType | true
    _avg?: BuildAvgAggregateInputType
    _sum?: BuildSumAggregateInputType
    _min?: BuildMinAggregateInputType
    _max?: BuildMaxAggregateInputType
  }

  export type BuildGroupByOutputType = {
    id: number
    createdAt: Date
    updatedAt: Date
    name: string
    role: $Enums.Role
    championID: number
    creatorID: number
    items: string[]
    coreItems: string[]
    situationalItems: string[]
    skillsOrder: string[]
    karmas: string[]
    keyInsights: string | null
    strengths: string | null
    weaknesses: string | null
    _count: BuildCountAggregateOutputType | null
    _avg: BuildAvgAggregateOutputType | null
    _sum: BuildSumAggregateOutputType | null
    _min: BuildMinAggregateOutputType | null
    _max: BuildMaxAggregateOutputType | null
  }

  type GetBuildGroupByPayload<T extends BuildGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<BuildGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof BuildGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], BuildGroupByOutputType[P]>
            : GetScalarType<T[P], BuildGroupByOutputType[P]>
        }
      >
    >


  export type BuildSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    createdAt?: boolean
    updatedAt?: boolean
    name?: boolean
    role?: boolean
    championID?: boolean
    creatorID?: boolean
    items?: boolean
    coreItems?: boolean
    situationalItems?: boolean
    skillsOrder?: boolean
    karmas?: boolean
    keyInsights?: boolean
    strengths?: boolean
    weaknesses?: boolean
    champion?: boolean | ChampionDefaultArgs<ExtArgs>
    creator?: boolean | UserDefaultArgs<ExtArgs>
    Likes?: boolean | Build$LikesArgs<ExtArgs>
    DisLikes?: boolean | Build$DisLikesArgs<ExtArgs>
    _count?: boolean | BuildCountOutputTypeDefaultArgs<ExtArgs>
  }, ExtArgs["result"]["build"]>

  export type BuildSelectScalar = {
    id?: boolean
    createdAt?: boolean
    updatedAt?: boolean
    name?: boolean
    role?: boolean
    championID?: boolean
    creatorID?: boolean
    items?: boolean
    coreItems?: boolean
    situationalItems?: boolean
    skillsOrder?: boolean
    karmas?: boolean
    keyInsights?: boolean
    strengths?: boolean
    weaknesses?: boolean
  }

  export type BuildInclude<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    champion?: boolean | ChampionDefaultArgs<ExtArgs>
    creator?: boolean | UserDefaultArgs<ExtArgs>
    Likes?: boolean | Build$LikesArgs<ExtArgs>
    DisLikes?: boolean | Build$DisLikesArgs<ExtArgs>
    _count?: boolean | BuildCountOutputTypeDefaultArgs<ExtArgs>
  }


  export type $BuildPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Build"
    objects: {
      champion: Prisma.$ChampionPayload<ExtArgs>
      creator: Prisma.$UserPayload<ExtArgs>
      Likes: Prisma.$LikesPayload<ExtArgs>[]
      DisLikes: Prisma.$DisLikesPayload<ExtArgs>[]
    }
    scalars: $Extensions.GetResult<{
      id: number
      createdAt: Date
      updatedAt: Date
      name: string
      role: $Enums.Role
      championID: number
      creatorID: number
      items: string[]
      coreItems: string[]
      situationalItems: string[]
      skillsOrder: string[]
      karmas: string[]
      keyInsights: string | null
      strengths: string | null
      weaknesses: string | null
    }, ExtArgs["result"]["build"]>
    composites: {}
  }


  type BuildGetPayload<S extends boolean | null | undefined | BuildDefaultArgs> = $Result.GetResult<Prisma.$BuildPayload, S>

  type BuildCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<BuildFindManyArgs, 'select' | 'include'> & {
      select?: BuildCountAggregateInputType | true
    }

  export interface BuildDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Build'], meta: { name: 'Build' } }
    /**
     * Find zero or one Build that matches the filter.
     * @param {BuildFindUniqueArgs} args - Arguments to find a Build
     * @example
     * // Get one Build
     * const build = await prisma.build.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends BuildFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, BuildFindUniqueArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Build that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {BuildFindUniqueOrThrowArgs} args - Arguments to find a Build
     * @example
     * // Get one Build
     * const build = await prisma.build.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends BuildFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, BuildFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Build that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {BuildFindFirstArgs} args - Arguments to find a Build
     * @example
     * // Get one Build
     * const build = await prisma.build.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends BuildFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, BuildFindFirstArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Build that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {BuildFindFirstOrThrowArgs} args - Arguments to find a Build
     * @example
     * // Get one Build
     * const build = await prisma.build.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends BuildFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, BuildFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Builds that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {BuildFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Builds
     * const builds = await prisma.build.findMany()
     * 
     * // Get first 10 Builds
     * const builds = await prisma.build.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const buildWithIdOnly = await prisma.build.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends BuildFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, BuildFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Build.
     * @param {BuildCreateArgs} args - Arguments to create a Build.
     * @example
     * // Create one Build
     * const Build = await prisma.build.create({
     *   data: {
     *     // ... data to create a Build
     *   }
     * })
     * 
    **/
    create<T extends BuildCreateArgs<ExtArgs>>(
      args: SelectSubset<T, BuildCreateArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Builds.
     *     @param {BuildCreateManyArgs} args - Arguments to create many Builds.
     *     @example
     *     // Create many Builds
     *     const build = await prisma.build.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends BuildCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, BuildCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Build.
     * @param {BuildDeleteArgs} args - Arguments to delete one Build.
     * @example
     * // Delete one Build
     * const Build = await prisma.build.delete({
     *   where: {
     *     // ... filter to delete one Build
     *   }
     * })
     * 
    **/
    delete<T extends BuildDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, BuildDeleteArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Build.
     * @param {BuildUpdateArgs} args - Arguments to update one Build.
     * @example
     * // Update one Build
     * const build = await prisma.build.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends BuildUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, BuildUpdateArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Builds.
     * @param {BuildDeleteManyArgs} args - Arguments to filter Builds to delete.
     * @example
     * // Delete a few Builds
     * const { count } = await prisma.build.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends BuildDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, BuildDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Builds.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {BuildUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Builds
     * const build = await prisma.build.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends BuildUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, BuildUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Build.
     * @param {BuildUpsertArgs} args - Arguments to update or create a Build.
     * @example
     * // Update or create a Build
     * const build = await prisma.build.upsert({
     *   create: {
     *     // ... data to create a Build
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Build we want to update
     *   }
     * })
    **/
    upsert<T extends BuildUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, BuildUpsertArgs<ExtArgs>>
    ): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Builds.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {BuildCountArgs} args - Arguments to filter Builds to count.
     * @example
     * // Count the number of Builds
     * const count = await prisma.build.count({
     *   where: {
     *     // ... the filter for the Builds we want to count
     *   }
     * })
    **/
    count<T extends BuildCountArgs>(
      args?: Subset<T, BuildCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], BuildCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Build.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {BuildAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends BuildAggregateArgs>(args: Subset<T, BuildAggregateArgs>): Prisma.PrismaPromise<GetBuildAggregateType<T>>

    /**
     * Group by Build.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {BuildGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends BuildGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: BuildGroupByArgs['orderBy'] }
        : { orderBy?: BuildGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, BuildGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetBuildGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Build model
   */
  readonly fields: BuildFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Build.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__BuildClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';

    champion<T extends ChampionDefaultArgs<ExtArgs> = {}>(args?: Subset<T, ChampionDefaultArgs<ExtArgs>>): Prisma__ChampionClient<$Result.GetResult<Prisma.$ChampionPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    creator<T extends UserDefaultArgs<ExtArgs> = {}>(args?: Subset<T, UserDefaultArgs<ExtArgs>>): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    Likes<T extends Build$LikesArgs<ExtArgs> = {}>(args?: Subset<T, Build$LikesArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'findMany'> | Null>;

    DisLikes<T extends Build$DisLikesArgs<ExtArgs> = {}>(args?: Subset<T, Build$DisLikesArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'findMany'> | Null>;

    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Build model
   */ 
  interface BuildFieldRefs {
    readonly id: FieldRef<"Build", 'Int'>
    readonly createdAt: FieldRef<"Build", 'DateTime'>
    readonly updatedAt: FieldRef<"Build", 'DateTime'>
    readonly name: FieldRef<"Build", 'String'>
    readonly role: FieldRef<"Build", 'Role'>
    readonly championID: FieldRef<"Build", 'Int'>
    readonly creatorID: FieldRef<"Build", 'Int'>
    readonly items: FieldRef<"Build", 'String[]'>
    readonly coreItems: FieldRef<"Build", 'String[]'>
    readonly situationalItems: FieldRef<"Build", 'String[]'>
    readonly skillsOrder: FieldRef<"Build", 'String[]'>
    readonly karmas: FieldRef<"Build", 'String[]'>
    readonly keyInsights: FieldRef<"Build", 'String'>
    readonly strengths: FieldRef<"Build", 'String'>
    readonly weaknesses: FieldRef<"Build", 'String'>
  }
    

  // Custom InputTypes

  /**
   * Build findUnique
   */
  export type BuildFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * Filter, which Build to fetch.
     */
    where: BuildWhereUniqueInput
  }


  /**
   * Build findUniqueOrThrow
   */
  export type BuildFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * Filter, which Build to fetch.
     */
    where: BuildWhereUniqueInput
  }


  /**
   * Build findFirst
   */
  export type BuildFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * Filter, which Build to fetch.
     */
    where?: BuildWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Builds to fetch.
     */
    orderBy?: BuildOrderByWithRelationInput | BuildOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Builds.
     */
    cursor?: BuildWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Builds from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Builds.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Builds.
     */
    distinct?: BuildScalarFieldEnum | BuildScalarFieldEnum[]
  }


  /**
   * Build findFirstOrThrow
   */
  export type BuildFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * Filter, which Build to fetch.
     */
    where?: BuildWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Builds to fetch.
     */
    orderBy?: BuildOrderByWithRelationInput | BuildOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Builds.
     */
    cursor?: BuildWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Builds from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Builds.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Builds.
     */
    distinct?: BuildScalarFieldEnum | BuildScalarFieldEnum[]
  }


  /**
   * Build findMany
   */
  export type BuildFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * Filter, which Builds to fetch.
     */
    where?: BuildWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Builds to fetch.
     */
    orderBy?: BuildOrderByWithRelationInput | BuildOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Builds.
     */
    cursor?: BuildWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Builds from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Builds.
     */
    skip?: number
    distinct?: BuildScalarFieldEnum | BuildScalarFieldEnum[]
  }


  /**
   * Build create
   */
  export type BuildCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * The data needed to create a Build.
     */
    data: XOR<BuildCreateInput, BuildUncheckedCreateInput>
  }


  /**
   * Build createMany
   */
  export type BuildCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Builds.
     */
    data: BuildCreateManyInput | BuildCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Build update
   */
  export type BuildUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * The data needed to update a Build.
     */
    data: XOR<BuildUpdateInput, BuildUncheckedUpdateInput>
    /**
     * Choose, which Build to update.
     */
    where: BuildWhereUniqueInput
  }


  /**
   * Build updateMany
   */
  export type BuildUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Builds.
     */
    data: XOR<BuildUpdateManyMutationInput, BuildUncheckedUpdateManyInput>
    /**
     * Filter which Builds to update
     */
    where?: BuildWhereInput
  }


  /**
   * Build upsert
   */
  export type BuildUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * The filter to search for the Build to update in case it exists.
     */
    where: BuildWhereUniqueInput
    /**
     * In case the Build found by the `where` argument doesn't exist, create a new Build with this data.
     */
    create: XOR<BuildCreateInput, BuildUncheckedCreateInput>
    /**
     * In case the Build was found with the provided `where` argument, update it with this data.
     */
    update: XOR<BuildUpdateInput, BuildUncheckedUpdateInput>
  }


  /**
   * Build delete
   */
  export type BuildDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
    /**
     * Filter which Build to delete.
     */
    where: BuildWhereUniqueInput
  }


  /**
   * Build deleteMany
   */
  export type BuildDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Builds to delete
     */
    where?: BuildWhereInput
  }


  /**
   * Build.Likes
   */
  export type Build$LikesArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    where?: LikesWhereInput
    orderBy?: LikesOrderByWithRelationInput | LikesOrderByWithRelationInput[]
    cursor?: LikesWhereUniqueInput
    take?: number
    skip?: number
    distinct?: LikesScalarFieldEnum | LikesScalarFieldEnum[]
  }


  /**
   * Build.DisLikes
   */
  export type Build$DisLikesArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    where?: DisLikesWhereInput
    orderBy?: DisLikesOrderByWithRelationInput | DisLikesOrderByWithRelationInput[]
    cursor?: DisLikesWhereUniqueInput
    take?: number
    skip?: number
    distinct?: DisLikesScalarFieldEnum | DisLikesScalarFieldEnum[]
  }


  /**
   * Build without action
   */
  export type BuildDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Build
     */
    select?: BuildSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: BuildInclude<ExtArgs> | null
  }



  /**
   * Model Likes
   */

  export type AggregateLikes = {
    _count: LikesCountAggregateOutputType | null
    _avg: LikesAvgAggregateOutputType | null
    _sum: LikesSumAggregateOutputType | null
    _min: LikesMinAggregateOutputType | null
    _max: LikesMaxAggregateOutputType | null
  }

  export type LikesAvgAggregateOutputType = {
    userID: number | null
    buildID: number | null
  }

  export type LikesSumAggregateOutputType = {
    userID: number | null
    buildID: number | null
  }

  export type LikesMinAggregateOutputType = {
    userID: number | null
    buildID: number | null
    assignedAt: Date | null
  }

  export type LikesMaxAggregateOutputType = {
    userID: number | null
    buildID: number | null
    assignedAt: Date | null
  }

  export type LikesCountAggregateOutputType = {
    userID: number
    buildID: number
    assignedAt: number
    _all: number
  }


  export type LikesAvgAggregateInputType = {
    userID?: true
    buildID?: true
  }

  export type LikesSumAggregateInputType = {
    userID?: true
    buildID?: true
  }

  export type LikesMinAggregateInputType = {
    userID?: true
    buildID?: true
    assignedAt?: true
  }

  export type LikesMaxAggregateInputType = {
    userID?: true
    buildID?: true
    assignedAt?: true
  }

  export type LikesCountAggregateInputType = {
    userID?: true
    buildID?: true
    assignedAt?: true
    _all?: true
  }

  export type LikesAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Likes to aggregate.
     */
    where?: LikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Likes to fetch.
     */
    orderBy?: LikesOrderByWithRelationInput | LikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: LikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Likes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Likes.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Likes
    **/
    _count?: true | LikesCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: LikesAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: LikesSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: LikesMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: LikesMaxAggregateInputType
  }

  export type GetLikesAggregateType<T extends LikesAggregateArgs> = {
        [P in keyof T & keyof AggregateLikes]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateLikes[P]>
      : GetScalarType<T[P], AggregateLikes[P]>
  }




  export type LikesGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: LikesWhereInput
    orderBy?: LikesOrderByWithAggregationInput | LikesOrderByWithAggregationInput[]
    by: LikesScalarFieldEnum[] | LikesScalarFieldEnum
    having?: LikesScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: LikesCountAggregateInputType | true
    _avg?: LikesAvgAggregateInputType
    _sum?: LikesSumAggregateInputType
    _min?: LikesMinAggregateInputType
    _max?: LikesMaxAggregateInputType
  }

  export type LikesGroupByOutputType = {
    userID: number
    buildID: number
    assignedAt: Date
    _count: LikesCountAggregateOutputType | null
    _avg: LikesAvgAggregateOutputType | null
    _sum: LikesSumAggregateOutputType | null
    _min: LikesMinAggregateOutputType | null
    _max: LikesMaxAggregateOutputType | null
  }

  type GetLikesGroupByPayload<T extends LikesGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<LikesGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof LikesGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], LikesGroupByOutputType[P]>
            : GetScalarType<T[P], LikesGroupByOutputType[P]>
        }
      >
    >


  export type LikesSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    userID?: boolean
    buildID?: boolean
    assignedAt?: boolean
    user?: boolean | UserDefaultArgs<ExtArgs>
    build?: boolean | BuildDefaultArgs<ExtArgs>
  }, ExtArgs["result"]["likes"]>

  export type LikesSelectScalar = {
    userID?: boolean
    buildID?: boolean
    assignedAt?: boolean
  }

  export type LikesInclude<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    user?: boolean | UserDefaultArgs<ExtArgs>
    build?: boolean | BuildDefaultArgs<ExtArgs>
  }


  export type $LikesPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Likes"
    objects: {
      user: Prisma.$UserPayload<ExtArgs>
      build: Prisma.$BuildPayload<ExtArgs>
    }
    scalars: $Extensions.GetResult<{
      userID: number
      buildID: number
      assignedAt: Date
    }, ExtArgs["result"]["likes"]>
    composites: {}
  }


  type LikesGetPayload<S extends boolean | null | undefined | LikesDefaultArgs> = $Result.GetResult<Prisma.$LikesPayload, S>

  type LikesCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<LikesFindManyArgs, 'select' | 'include'> & {
      select?: LikesCountAggregateInputType | true
    }

  export interface LikesDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Likes'], meta: { name: 'Likes' } }
    /**
     * Find zero or one Likes that matches the filter.
     * @param {LikesFindUniqueArgs} args - Arguments to find a Likes
     * @example
     * // Get one Likes
     * const likes = await prisma.likes.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends LikesFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, LikesFindUniqueArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Likes that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {LikesFindUniqueOrThrowArgs} args - Arguments to find a Likes
     * @example
     * // Get one Likes
     * const likes = await prisma.likes.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends LikesFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, LikesFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Likes that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LikesFindFirstArgs} args - Arguments to find a Likes
     * @example
     * // Get one Likes
     * const likes = await prisma.likes.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends LikesFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, LikesFindFirstArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Likes that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LikesFindFirstOrThrowArgs} args - Arguments to find a Likes
     * @example
     * // Get one Likes
     * const likes = await prisma.likes.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends LikesFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, LikesFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Likes that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LikesFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Likes
     * const likes = await prisma.likes.findMany()
     * 
     * // Get first 10 Likes
     * const likes = await prisma.likes.findMany({ take: 10 })
     * 
     * // Only select the `userID`
     * const likesWithUserIDOnly = await prisma.likes.findMany({ select: { userID: true } })
     * 
    **/
    findMany<T extends LikesFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, LikesFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Likes.
     * @param {LikesCreateArgs} args - Arguments to create a Likes.
     * @example
     * // Create one Likes
     * const Likes = await prisma.likes.create({
     *   data: {
     *     // ... data to create a Likes
     *   }
     * })
     * 
    **/
    create<T extends LikesCreateArgs<ExtArgs>>(
      args: SelectSubset<T, LikesCreateArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Likes.
     *     @param {LikesCreateManyArgs} args - Arguments to create many Likes.
     *     @example
     *     // Create many Likes
     *     const likes = await prisma.likes.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends LikesCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, LikesCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Likes.
     * @param {LikesDeleteArgs} args - Arguments to delete one Likes.
     * @example
     * // Delete one Likes
     * const Likes = await prisma.likes.delete({
     *   where: {
     *     // ... filter to delete one Likes
     *   }
     * })
     * 
    **/
    delete<T extends LikesDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, LikesDeleteArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Likes.
     * @param {LikesUpdateArgs} args - Arguments to update one Likes.
     * @example
     * // Update one Likes
     * const likes = await prisma.likes.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends LikesUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, LikesUpdateArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Likes.
     * @param {LikesDeleteManyArgs} args - Arguments to filter Likes to delete.
     * @example
     * // Delete a few Likes
     * const { count } = await prisma.likes.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends LikesDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, LikesDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Likes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LikesUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Likes
     * const likes = await prisma.likes.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends LikesUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, LikesUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Likes.
     * @param {LikesUpsertArgs} args - Arguments to update or create a Likes.
     * @example
     * // Update or create a Likes
     * const likes = await prisma.likes.upsert({
     *   create: {
     *     // ... data to create a Likes
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Likes we want to update
     *   }
     * })
    **/
    upsert<T extends LikesUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, LikesUpsertArgs<ExtArgs>>
    ): Prisma__LikesClient<$Result.GetResult<Prisma.$LikesPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Likes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LikesCountArgs} args - Arguments to filter Likes to count.
     * @example
     * // Count the number of Likes
     * const count = await prisma.likes.count({
     *   where: {
     *     // ... the filter for the Likes we want to count
     *   }
     * })
    **/
    count<T extends LikesCountArgs>(
      args?: Subset<T, LikesCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], LikesCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Likes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LikesAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends LikesAggregateArgs>(args: Subset<T, LikesAggregateArgs>): Prisma.PrismaPromise<GetLikesAggregateType<T>>

    /**
     * Group by Likes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LikesGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends LikesGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: LikesGroupByArgs['orderBy'] }
        : { orderBy?: LikesGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, LikesGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetLikesGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Likes model
   */
  readonly fields: LikesFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Likes.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__LikesClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';

    user<T extends UserDefaultArgs<ExtArgs> = {}>(args?: Subset<T, UserDefaultArgs<ExtArgs>>): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    build<T extends BuildDefaultArgs<ExtArgs> = {}>(args?: Subset<T, BuildDefaultArgs<ExtArgs>>): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Likes model
   */ 
  interface LikesFieldRefs {
    readonly userID: FieldRef<"Likes", 'Int'>
    readonly buildID: FieldRef<"Likes", 'Int'>
    readonly assignedAt: FieldRef<"Likes", 'DateTime'>
  }
    

  // Custom InputTypes

  /**
   * Likes findUnique
   */
  export type LikesFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * Filter, which Likes to fetch.
     */
    where: LikesWhereUniqueInput
  }


  /**
   * Likes findUniqueOrThrow
   */
  export type LikesFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * Filter, which Likes to fetch.
     */
    where: LikesWhereUniqueInput
  }


  /**
   * Likes findFirst
   */
  export type LikesFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * Filter, which Likes to fetch.
     */
    where?: LikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Likes to fetch.
     */
    orderBy?: LikesOrderByWithRelationInput | LikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Likes.
     */
    cursor?: LikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Likes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Likes.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Likes.
     */
    distinct?: LikesScalarFieldEnum | LikesScalarFieldEnum[]
  }


  /**
   * Likes findFirstOrThrow
   */
  export type LikesFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * Filter, which Likes to fetch.
     */
    where?: LikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Likes to fetch.
     */
    orderBy?: LikesOrderByWithRelationInput | LikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Likes.
     */
    cursor?: LikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Likes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Likes.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Likes.
     */
    distinct?: LikesScalarFieldEnum | LikesScalarFieldEnum[]
  }


  /**
   * Likes findMany
   */
  export type LikesFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * Filter, which Likes to fetch.
     */
    where?: LikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Likes to fetch.
     */
    orderBy?: LikesOrderByWithRelationInput | LikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Likes.
     */
    cursor?: LikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Likes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Likes.
     */
    skip?: number
    distinct?: LikesScalarFieldEnum | LikesScalarFieldEnum[]
  }


  /**
   * Likes create
   */
  export type LikesCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * The data needed to create a Likes.
     */
    data: XOR<LikesCreateInput, LikesUncheckedCreateInput>
  }


  /**
   * Likes createMany
   */
  export type LikesCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Likes.
     */
    data: LikesCreateManyInput | LikesCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Likes update
   */
  export type LikesUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * The data needed to update a Likes.
     */
    data: XOR<LikesUpdateInput, LikesUncheckedUpdateInput>
    /**
     * Choose, which Likes to update.
     */
    where: LikesWhereUniqueInput
  }


  /**
   * Likes updateMany
   */
  export type LikesUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Likes.
     */
    data: XOR<LikesUpdateManyMutationInput, LikesUncheckedUpdateManyInput>
    /**
     * Filter which Likes to update
     */
    where?: LikesWhereInput
  }


  /**
   * Likes upsert
   */
  export type LikesUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * The filter to search for the Likes to update in case it exists.
     */
    where: LikesWhereUniqueInput
    /**
     * In case the Likes found by the `where` argument doesn't exist, create a new Likes with this data.
     */
    create: XOR<LikesCreateInput, LikesUncheckedCreateInput>
    /**
     * In case the Likes was found with the provided `where` argument, update it with this data.
     */
    update: XOR<LikesUpdateInput, LikesUncheckedUpdateInput>
  }


  /**
   * Likes delete
   */
  export type LikesDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
    /**
     * Filter which Likes to delete.
     */
    where: LikesWhereUniqueInput
  }


  /**
   * Likes deleteMany
   */
  export type LikesDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Likes to delete
     */
    where?: LikesWhereInput
  }


  /**
   * Likes without action
   */
  export type LikesDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Likes
     */
    select?: LikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: LikesInclude<ExtArgs> | null
  }



  /**
   * Model DisLikes
   */

  export type AggregateDisLikes = {
    _count: DisLikesCountAggregateOutputType | null
    _avg: DisLikesAvgAggregateOutputType | null
    _sum: DisLikesSumAggregateOutputType | null
    _min: DisLikesMinAggregateOutputType | null
    _max: DisLikesMaxAggregateOutputType | null
  }

  export type DisLikesAvgAggregateOutputType = {
    userID: number | null
    buildID: number | null
  }

  export type DisLikesSumAggregateOutputType = {
    userID: number | null
    buildID: number | null
  }

  export type DisLikesMinAggregateOutputType = {
    userID: number | null
    buildID: number | null
    assignedAt: Date | null
  }

  export type DisLikesMaxAggregateOutputType = {
    userID: number | null
    buildID: number | null
    assignedAt: Date | null
  }

  export type DisLikesCountAggregateOutputType = {
    userID: number
    buildID: number
    assignedAt: number
    _all: number
  }


  export type DisLikesAvgAggregateInputType = {
    userID?: true
    buildID?: true
  }

  export type DisLikesSumAggregateInputType = {
    userID?: true
    buildID?: true
  }

  export type DisLikesMinAggregateInputType = {
    userID?: true
    buildID?: true
    assignedAt?: true
  }

  export type DisLikesMaxAggregateInputType = {
    userID?: true
    buildID?: true
    assignedAt?: true
  }

  export type DisLikesCountAggregateInputType = {
    userID?: true
    buildID?: true
    assignedAt?: true
    _all?: true
  }

  export type DisLikesAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which DisLikes to aggregate.
     */
    where?: DisLikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of DisLikes to fetch.
     */
    orderBy?: DisLikesOrderByWithRelationInput | DisLikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: DisLikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` DisLikes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` DisLikes.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned DisLikes
    **/
    _count?: true | DisLikesCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: DisLikesAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: DisLikesSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: DisLikesMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: DisLikesMaxAggregateInputType
  }

  export type GetDisLikesAggregateType<T extends DisLikesAggregateArgs> = {
        [P in keyof T & keyof AggregateDisLikes]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateDisLikes[P]>
      : GetScalarType<T[P], AggregateDisLikes[P]>
  }




  export type DisLikesGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: DisLikesWhereInput
    orderBy?: DisLikesOrderByWithAggregationInput | DisLikesOrderByWithAggregationInput[]
    by: DisLikesScalarFieldEnum[] | DisLikesScalarFieldEnum
    having?: DisLikesScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: DisLikesCountAggregateInputType | true
    _avg?: DisLikesAvgAggregateInputType
    _sum?: DisLikesSumAggregateInputType
    _min?: DisLikesMinAggregateInputType
    _max?: DisLikesMaxAggregateInputType
  }

  export type DisLikesGroupByOutputType = {
    userID: number
    buildID: number
    assignedAt: Date
    _count: DisLikesCountAggregateOutputType | null
    _avg: DisLikesAvgAggregateOutputType | null
    _sum: DisLikesSumAggregateOutputType | null
    _min: DisLikesMinAggregateOutputType | null
    _max: DisLikesMaxAggregateOutputType | null
  }

  type GetDisLikesGroupByPayload<T extends DisLikesGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<DisLikesGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof DisLikesGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], DisLikesGroupByOutputType[P]>
            : GetScalarType<T[P], DisLikesGroupByOutputType[P]>
        }
      >
    >


  export type DisLikesSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    userID?: boolean
    buildID?: boolean
    assignedAt?: boolean
    user?: boolean | UserDefaultArgs<ExtArgs>
    build?: boolean | BuildDefaultArgs<ExtArgs>
  }, ExtArgs["result"]["disLikes"]>

  export type DisLikesSelectScalar = {
    userID?: boolean
    buildID?: boolean
    assignedAt?: boolean
  }

  export type DisLikesInclude<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    user?: boolean | UserDefaultArgs<ExtArgs>
    build?: boolean | BuildDefaultArgs<ExtArgs>
  }


  export type $DisLikesPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "DisLikes"
    objects: {
      user: Prisma.$UserPayload<ExtArgs>
      build: Prisma.$BuildPayload<ExtArgs>
    }
    scalars: $Extensions.GetResult<{
      userID: number
      buildID: number
      assignedAt: Date
    }, ExtArgs["result"]["disLikes"]>
    composites: {}
  }


  type DisLikesGetPayload<S extends boolean | null | undefined | DisLikesDefaultArgs> = $Result.GetResult<Prisma.$DisLikesPayload, S>

  type DisLikesCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<DisLikesFindManyArgs, 'select' | 'include'> & {
      select?: DisLikesCountAggregateInputType | true
    }

  export interface DisLikesDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['DisLikes'], meta: { name: 'DisLikes' } }
    /**
     * Find zero or one DisLikes that matches the filter.
     * @param {DisLikesFindUniqueArgs} args - Arguments to find a DisLikes
     * @example
     * // Get one DisLikes
     * const disLikes = await prisma.disLikes.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends DisLikesFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, DisLikesFindUniqueArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one DisLikes that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {DisLikesFindUniqueOrThrowArgs} args - Arguments to find a DisLikes
     * @example
     * // Get one DisLikes
     * const disLikes = await prisma.disLikes.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends DisLikesFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, DisLikesFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first DisLikes that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {DisLikesFindFirstArgs} args - Arguments to find a DisLikes
     * @example
     * // Get one DisLikes
     * const disLikes = await prisma.disLikes.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends DisLikesFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, DisLikesFindFirstArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first DisLikes that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {DisLikesFindFirstOrThrowArgs} args - Arguments to find a DisLikes
     * @example
     * // Get one DisLikes
     * const disLikes = await prisma.disLikes.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends DisLikesFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, DisLikesFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more DisLikes that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {DisLikesFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all DisLikes
     * const disLikes = await prisma.disLikes.findMany()
     * 
     * // Get first 10 DisLikes
     * const disLikes = await prisma.disLikes.findMany({ take: 10 })
     * 
     * // Only select the `userID`
     * const disLikesWithUserIDOnly = await prisma.disLikes.findMany({ select: { userID: true } })
     * 
    **/
    findMany<T extends DisLikesFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, DisLikesFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a DisLikes.
     * @param {DisLikesCreateArgs} args - Arguments to create a DisLikes.
     * @example
     * // Create one DisLikes
     * const DisLikes = await prisma.disLikes.create({
     *   data: {
     *     // ... data to create a DisLikes
     *   }
     * })
     * 
    **/
    create<T extends DisLikesCreateArgs<ExtArgs>>(
      args: SelectSubset<T, DisLikesCreateArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many DisLikes.
     *     @param {DisLikesCreateManyArgs} args - Arguments to create many DisLikes.
     *     @example
     *     // Create many DisLikes
     *     const disLikes = await prisma.disLikes.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends DisLikesCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, DisLikesCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a DisLikes.
     * @param {DisLikesDeleteArgs} args - Arguments to delete one DisLikes.
     * @example
     * // Delete one DisLikes
     * const DisLikes = await prisma.disLikes.delete({
     *   where: {
     *     // ... filter to delete one DisLikes
     *   }
     * })
     * 
    **/
    delete<T extends DisLikesDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, DisLikesDeleteArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one DisLikes.
     * @param {DisLikesUpdateArgs} args - Arguments to update one DisLikes.
     * @example
     * // Update one DisLikes
     * const disLikes = await prisma.disLikes.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends DisLikesUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, DisLikesUpdateArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more DisLikes.
     * @param {DisLikesDeleteManyArgs} args - Arguments to filter DisLikes to delete.
     * @example
     * // Delete a few DisLikes
     * const { count } = await prisma.disLikes.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends DisLikesDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, DisLikesDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more DisLikes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {DisLikesUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many DisLikes
     * const disLikes = await prisma.disLikes.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends DisLikesUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, DisLikesUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one DisLikes.
     * @param {DisLikesUpsertArgs} args - Arguments to update or create a DisLikes.
     * @example
     * // Update or create a DisLikes
     * const disLikes = await prisma.disLikes.upsert({
     *   create: {
     *     // ... data to create a DisLikes
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the DisLikes we want to update
     *   }
     * })
    **/
    upsert<T extends DisLikesUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, DisLikesUpsertArgs<ExtArgs>>
    ): Prisma__DisLikesClient<$Result.GetResult<Prisma.$DisLikesPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of DisLikes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {DisLikesCountArgs} args - Arguments to filter DisLikes to count.
     * @example
     * // Count the number of DisLikes
     * const count = await prisma.disLikes.count({
     *   where: {
     *     // ... the filter for the DisLikes we want to count
     *   }
     * })
    **/
    count<T extends DisLikesCountArgs>(
      args?: Subset<T, DisLikesCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], DisLikesCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a DisLikes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {DisLikesAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends DisLikesAggregateArgs>(args: Subset<T, DisLikesAggregateArgs>): Prisma.PrismaPromise<GetDisLikesAggregateType<T>>

    /**
     * Group by DisLikes.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {DisLikesGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends DisLikesGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: DisLikesGroupByArgs['orderBy'] }
        : { orderBy?: DisLikesGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, DisLikesGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetDisLikesGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the DisLikes model
   */
  readonly fields: DisLikesFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for DisLikes.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__DisLikesClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';

    user<T extends UserDefaultArgs<ExtArgs> = {}>(args?: Subset<T, UserDefaultArgs<ExtArgs>>): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    build<T extends BuildDefaultArgs<ExtArgs> = {}>(args?: Subset<T, BuildDefaultArgs<ExtArgs>>): Prisma__BuildClient<$Result.GetResult<Prisma.$BuildPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the DisLikes model
   */ 
  interface DisLikesFieldRefs {
    readonly userID: FieldRef<"DisLikes", 'Int'>
    readonly buildID: FieldRef<"DisLikes", 'Int'>
    readonly assignedAt: FieldRef<"DisLikes", 'DateTime'>
  }
    

  // Custom InputTypes

  /**
   * DisLikes findUnique
   */
  export type DisLikesFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * Filter, which DisLikes to fetch.
     */
    where: DisLikesWhereUniqueInput
  }


  /**
   * DisLikes findUniqueOrThrow
   */
  export type DisLikesFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * Filter, which DisLikes to fetch.
     */
    where: DisLikesWhereUniqueInput
  }


  /**
   * DisLikes findFirst
   */
  export type DisLikesFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * Filter, which DisLikes to fetch.
     */
    where?: DisLikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of DisLikes to fetch.
     */
    orderBy?: DisLikesOrderByWithRelationInput | DisLikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for DisLikes.
     */
    cursor?: DisLikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` DisLikes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` DisLikes.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of DisLikes.
     */
    distinct?: DisLikesScalarFieldEnum | DisLikesScalarFieldEnum[]
  }


  /**
   * DisLikes findFirstOrThrow
   */
  export type DisLikesFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * Filter, which DisLikes to fetch.
     */
    where?: DisLikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of DisLikes to fetch.
     */
    orderBy?: DisLikesOrderByWithRelationInput | DisLikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for DisLikes.
     */
    cursor?: DisLikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` DisLikes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` DisLikes.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of DisLikes.
     */
    distinct?: DisLikesScalarFieldEnum | DisLikesScalarFieldEnum[]
  }


  /**
   * DisLikes findMany
   */
  export type DisLikesFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * Filter, which DisLikes to fetch.
     */
    where?: DisLikesWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of DisLikes to fetch.
     */
    orderBy?: DisLikesOrderByWithRelationInput | DisLikesOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing DisLikes.
     */
    cursor?: DisLikesWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` DisLikes from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` DisLikes.
     */
    skip?: number
    distinct?: DisLikesScalarFieldEnum | DisLikesScalarFieldEnum[]
  }


  /**
   * DisLikes create
   */
  export type DisLikesCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * The data needed to create a DisLikes.
     */
    data: XOR<DisLikesCreateInput, DisLikesUncheckedCreateInput>
  }


  /**
   * DisLikes createMany
   */
  export type DisLikesCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many DisLikes.
     */
    data: DisLikesCreateManyInput | DisLikesCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * DisLikes update
   */
  export type DisLikesUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * The data needed to update a DisLikes.
     */
    data: XOR<DisLikesUpdateInput, DisLikesUncheckedUpdateInput>
    /**
     * Choose, which DisLikes to update.
     */
    where: DisLikesWhereUniqueInput
  }


  /**
   * DisLikes updateMany
   */
  export type DisLikesUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update DisLikes.
     */
    data: XOR<DisLikesUpdateManyMutationInput, DisLikesUncheckedUpdateManyInput>
    /**
     * Filter which DisLikes to update
     */
    where?: DisLikesWhereInput
  }


  /**
   * DisLikes upsert
   */
  export type DisLikesUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * The filter to search for the DisLikes to update in case it exists.
     */
    where: DisLikesWhereUniqueInput
    /**
     * In case the DisLikes found by the `where` argument doesn't exist, create a new DisLikes with this data.
     */
    create: XOR<DisLikesCreateInput, DisLikesUncheckedCreateInput>
    /**
     * In case the DisLikes was found with the provided `where` argument, update it with this data.
     */
    update: XOR<DisLikesUpdateInput, DisLikesUncheckedUpdateInput>
  }


  /**
   * DisLikes delete
   */
  export type DisLikesDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
    /**
     * Filter which DisLikes to delete.
     */
    where: DisLikesWhereUniqueInput
  }


  /**
   * DisLikes deleteMany
   */
  export type DisLikesDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which DisLikes to delete
     */
    where?: DisLikesWhereInput
  }


  /**
   * DisLikes without action
   */
  export type DisLikesDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the DisLikes
     */
    select?: DisLikesSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: DisLikesInclude<ExtArgs> | null
  }



  /**
   * Model Tournament
   */

  export type AggregateTournament = {
    _count: TournamentCountAggregateOutputType | null
    _avg: TournamentAvgAggregateOutputType | null
    _sum: TournamentSumAggregateOutputType | null
    _min: TournamentMinAggregateOutputType | null
    _max: TournamentMaxAggregateOutputType | null
  }

  export type TournamentAvgAggregateOutputType = {
    id: number | null
    teamSize: number | null
    slots: number | null
    maxRequests: number | null
    creatorID: number | null
    matchesCount: number | null
  }

  export type TournamentSumAggregateOutputType = {
    id: number | null
    teamSize: number | null
    slots: number | null
    maxRequests: number | null
    creatorID: number | null
    matchesCount: number | null
  }

  export type TournamentMinAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    updatedAt: Date | null
    startedAt: Date | null
    finishedAt: Date | null
    name: string | null
    description: string | null
    prize: string | null
    icon: string | null
    map: string | null
    pickMode: $Enums.TournamentPickMode | null
    gameMode: $Enums.GameMode | null
    status: $Enums.TournamentStatus | null
    teamSize: number | null
    slots: number | null
    teamRequest: boolean | null
    maxRequests: number | null
    videoLink: string | null
    creatorID: number | null
    teams: string | null
    bracket: string | null
    bracketType: $Enums.BracketType | null
    matchesCount: number | null
    results: string | null
  }

  export type TournamentMaxAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    updatedAt: Date | null
    startedAt: Date | null
    finishedAt: Date | null
    name: string | null
    description: string | null
    prize: string | null
    icon: string | null
    map: string | null
    pickMode: $Enums.TournamentPickMode | null
    gameMode: $Enums.GameMode | null
    status: $Enums.TournamentStatus | null
    teamSize: number | null
    slots: number | null
    teamRequest: boolean | null
    maxRequests: number | null
    videoLink: string | null
    creatorID: number | null
    teams: string | null
    bracket: string | null
    bracketType: $Enums.BracketType | null
    matchesCount: number | null
    results: string | null
  }

  export type TournamentCountAggregateOutputType = {
    id: number
    createdAt: number
    updatedAt: number
    startedAt: number
    finishedAt: number
    name: number
    description: number
    prize: number
    icon: number
    map: number
    pickMode: number
    gameMode: number
    status: number
    teamSize: number
    slots: number
    teamRequest: number
    maxRequests: number
    casters: number
    videoLink: number
    creatorID: number
    teams: number
    requests: number
    bracket: number
    bracketType: number
    matchesCount: number
    results: number
    _all: number
  }


  export type TournamentAvgAggregateInputType = {
    id?: true
    teamSize?: true
    slots?: true
    maxRequests?: true
    creatorID?: true
    matchesCount?: true
  }

  export type TournamentSumAggregateInputType = {
    id?: true
    teamSize?: true
    slots?: true
    maxRequests?: true
    creatorID?: true
    matchesCount?: true
  }

  export type TournamentMinAggregateInputType = {
    id?: true
    createdAt?: true
    updatedAt?: true
    startedAt?: true
    finishedAt?: true
    name?: true
    description?: true
    prize?: true
    icon?: true
    map?: true
    pickMode?: true
    gameMode?: true
    status?: true
    teamSize?: true
    slots?: true
    teamRequest?: true
    maxRequests?: true
    videoLink?: true
    creatorID?: true
    teams?: true
    bracket?: true
    bracketType?: true
    matchesCount?: true
    results?: true
  }

  export type TournamentMaxAggregateInputType = {
    id?: true
    createdAt?: true
    updatedAt?: true
    startedAt?: true
    finishedAt?: true
    name?: true
    description?: true
    prize?: true
    icon?: true
    map?: true
    pickMode?: true
    gameMode?: true
    status?: true
    teamSize?: true
    slots?: true
    teamRequest?: true
    maxRequests?: true
    videoLink?: true
    creatorID?: true
    teams?: true
    bracket?: true
    bracketType?: true
    matchesCount?: true
    results?: true
  }

  export type TournamentCountAggregateInputType = {
    id?: true
    createdAt?: true
    updatedAt?: true
    startedAt?: true
    finishedAt?: true
    name?: true
    description?: true
    prize?: true
    icon?: true
    map?: true
    pickMode?: true
    gameMode?: true
    status?: true
    teamSize?: true
    slots?: true
    teamRequest?: true
    maxRequests?: true
    casters?: true
    videoLink?: true
    creatorID?: true
    teams?: true
    requests?: true
    bracket?: true
    bracketType?: true
    matchesCount?: true
    results?: true
    _all?: true
  }

  export type TournamentAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Tournament to aggregate.
     */
    where?: TournamentWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Tournaments to fetch.
     */
    orderBy?: TournamentOrderByWithRelationInput | TournamentOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: TournamentWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Tournaments from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Tournaments.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Tournaments
    **/
    _count?: true | TournamentCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: TournamentAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: TournamentSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: TournamentMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: TournamentMaxAggregateInputType
  }

  export type GetTournamentAggregateType<T extends TournamentAggregateArgs> = {
        [P in keyof T & keyof AggregateTournament]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateTournament[P]>
      : GetScalarType<T[P], AggregateTournament[P]>
  }




  export type TournamentGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: TournamentWhereInput
    orderBy?: TournamentOrderByWithAggregationInput | TournamentOrderByWithAggregationInput[]
    by: TournamentScalarFieldEnum[] | TournamentScalarFieldEnum
    having?: TournamentScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: TournamentCountAggregateInputType | true
    _avg?: TournamentAvgAggregateInputType
    _sum?: TournamentSumAggregateInputType
    _min?: TournamentMinAggregateInputType
    _max?: TournamentMaxAggregateInputType
  }

  export type TournamentGroupByOutputType = {
    id: number
    createdAt: Date
    updatedAt: Date
    startedAt: Date
    finishedAt: Date
    name: string
    description: string
    prize: string
    icon: string | null
    map: string | null
    pickMode: $Enums.TournamentPickMode
    gameMode: $Enums.GameMode
    status: $Enums.TournamentStatus
    teamSize: number
    slots: number
    teamRequest: boolean
    maxRequests: number
    casters: string[]
    videoLink: string | null
    creatorID: number
    teams: string
    requests: string[]
    bracket: string | null
    bracketType: $Enums.BracketType
    matchesCount: number
    results: string | null
    _count: TournamentCountAggregateOutputType | null
    _avg: TournamentAvgAggregateOutputType | null
    _sum: TournamentSumAggregateOutputType | null
    _min: TournamentMinAggregateOutputType | null
    _max: TournamentMaxAggregateOutputType | null
  }

  type GetTournamentGroupByPayload<T extends TournamentGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<TournamentGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof TournamentGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], TournamentGroupByOutputType[P]>
            : GetScalarType<T[P], TournamentGroupByOutputType[P]>
        }
      >
    >


  export type TournamentSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    createdAt?: boolean
    updatedAt?: boolean
    startedAt?: boolean
    finishedAt?: boolean
    name?: boolean
    description?: boolean
    prize?: boolean
    icon?: boolean
    map?: boolean
    pickMode?: boolean
    gameMode?: boolean
    status?: boolean
    teamSize?: boolean
    slots?: boolean
    teamRequest?: boolean
    maxRequests?: boolean
    casters?: boolean
    videoLink?: boolean
    creatorID?: boolean
    teams?: boolean
    requests?: boolean
    bracket?: boolean
    bracketType?: boolean
    matchesCount?: boolean
    results?: boolean
    creator?: boolean | UserDefaultArgs<ExtArgs>
  }, ExtArgs["result"]["tournament"]>

  export type TournamentSelectScalar = {
    id?: boolean
    createdAt?: boolean
    updatedAt?: boolean
    startedAt?: boolean
    finishedAt?: boolean
    name?: boolean
    description?: boolean
    prize?: boolean
    icon?: boolean
    map?: boolean
    pickMode?: boolean
    gameMode?: boolean
    status?: boolean
    teamSize?: boolean
    slots?: boolean
    teamRequest?: boolean
    maxRequests?: boolean
    casters?: boolean
    videoLink?: boolean
    creatorID?: boolean
    teams?: boolean
    requests?: boolean
    bracket?: boolean
    bracketType?: boolean
    matchesCount?: boolean
    results?: boolean
  }

  export type TournamentInclude<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    creator?: boolean | UserDefaultArgs<ExtArgs>
  }


  export type $TournamentPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Tournament"
    objects: {
      creator: Prisma.$UserPayload<ExtArgs>
    }
    scalars: $Extensions.GetResult<{
      id: number
      createdAt: Date
      updatedAt: Date
      startedAt: Date
      finishedAt: Date
      name: string
      description: string
      prize: string
      icon: string | null
      map: string | null
      pickMode: $Enums.TournamentPickMode
      gameMode: $Enums.GameMode
      status: $Enums.TournamentStatus
      teamSize: number
      slots: number
      teamRequest: boolean
      maxRequests: number
      casters: string[]
      videoLink: string | null
      creatorID: number
      teams: string
      requests: string[]
      bracket: string | null
      bracketType: $Enums.BracketType
      matchesCount: number
      results: string | null
    }, ExtArgs["result"]["tournament"]>
    composites: {}
  }


  type TournamentGetPayload<S extends boolean | null | undefined | TournamentDefaultArgs> = $Result.GetResult<Prisma.$TournamentPayload, S>

  type TournamentCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<TournamentFindManyArgs, 'select' | 'include'> & {
      select?: TournamentCountAggregateInputType | true
    }

  export interface TournamentDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Tournament'], meta: { name: 'Tournament' } }
    /**
     * Find zero or one Tournament that matches the filter.
     * @param {TournamentFindUniqueArgs} args - Arguments to find a Tournament
     * @example
     * // Get one Tournament
     * const tournament = await prisma.tournament.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends TournamentFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, TournamentFindUniqueArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Tournament that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {TournamentFindUniqueOrThrowArgs} args - Arguments to find a Tournament
     * @example
     * // Get one Tournament
     * const tournament = await prisma.tournament.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends TournamentFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, TournamentFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Tournament that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TournamentFindFirstArgs} args - Arguments to find a Tournament
     * @example
     * // Get one Tournament
     * const tournament = await prisma.tournament.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends TournamentFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, TournamentFindFirstArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Tournament that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TournamentFindFirstOrThrowArgs} args - Arguments to find a Tournament
     * @example
     * // Get one Tournament
     * const tournament = await prisma.tournament.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends TournamentFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, TournamentFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Tournaments that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TournamentFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Tournaments
     * const tournaments = await prisma.tournament.findMany()
     * 
     * // Get first 10 Tournaments
     * const tournaments = await prisma.tournament.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const tournamentWithIdOnly = await prisma.tournament.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends TournamentFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, TournamentFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Tournament.
     * @param {TournamentCreateArgs} args - Arguments to create a Tournament.
     * @example
     * // Create one Tournament
     * const Tournament = await prisma.tournament.create({
     *   data: {
     *     // ... data to create a Tournament
     *   }
     * })
     * 
    **/
    create<T extends TournamentCreateArgs<ExtArgs>>(
      args: SelectSubset<T, TournamentCreateArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Tournaments.
     *     @param {TournamentCreateManyArgs} args - Arguments to create many Tournaments.
     *     @example
     *     // Create many Tournaments
     *     const tournament = await prisma.tournament.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends TournamentCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, TournamentCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Tournament.
     * @param {TournamentDeleteArgs} args - Arguments to delete one Tournament.
     * @example
     * // Delete one Tournament
     * const Tournament = await prisma.tournament.delete({
     *   where: {
     *     // ... filter to delete one Tournament
     *   }
     * })
     * 
    **/
    delete<T extends TournamentDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, TournamentDeleteArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Tournament.
     * @param {TournamentUpdateArgs} args - Arguments to update one Tournament.
     * @example
     * // Update one Tournament
     * const tournament = await prisma.tournament.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends TournamentUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, TournamentUpdateArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Tournaments.
     * @param {TournamentDeleteManyArgs} args - Arguments to filter Tournaments to delete.
     * @example
     * // Delete a few Tournaments
     * const { count } = await prisma.tournament.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends TournamentDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, TournamentDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Tournaments.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TournamentUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Tournaments
     * const tournament = await prisma.tournament.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends TournamentUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, TournamentUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Tournament.
     * @param {TournamentUpsertArgs} args - Arguments to update or create a Tournament.
     * @example
     * // Update or create a Tournament
     * const tournament = await prisma.tournament.upsert({
     *   create: {
     *     // ... data to create a Tournament
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Tournament we want to update
     *   }
     * })
    **/
    upsert<T extends TournamentUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, TournamentUpsertArgs<ExtArgs>>
    ): Prisma__TournamentClient<$Result.GetResult<Prisma.$TournamentPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Tournaments.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TournamentCountArgs} args - Arguments to filter Tournaments to count.
     * @example
     * // Count the number of Tournaments
     * const count = await prisma.tournament.count({
     *   where: {
     *     // ... the filter for the Tournaments we want to count
     *   }
     * })
    **/
    count<T extends TournamentCountArgs>(
      args?: Subset<T, TournamentCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], TournamentCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Tournament.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TournamentAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends TournamentAggregateArgs>(args: Subset<T, TournamentAggregateArgs>): Prisma.PrismaPromise<GetTournamentAggregateType<T>>

    /**
     * Group by Tournament.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TournamentGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends TournamentGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: TournamentGroupByArgs['orderBy'] }
        : { orderBy?: TournamentGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, TournamentGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetTournamentGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Tournament model
   */
  readonly fields: TournamentFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Tournament.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__TournamentClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';

    creator<T extends UserDefaultArgs<ExtArgs> = {}>(args?: Subset<T, UserDefaultArgs<ExtArgs>>): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Tournament model
   */ 
  interface TournamentFieldRefs {
    readonly id: FieldRef<"Tournament", 'Int'>
    readonly createdAt: FieldRef<"Tournament", 'DateTime'>
    readonly updatedAt: FieldRef<"Tournament", 'DateTime'>
    readonly startedAt: FieldRef<"Tournament", 'DateTime'>
    readonly finishedAt: FieldRef<"Tournament", 'DateTime'>
    readonly name: FieldRef<"Tournament", 'String'>
    readonly description: FieldRef<"Tournament", 'String'>
    readonly prize: FieldRef<"Tournament", 'String'>
    readonly icon: FieldRef<"Tournament", 'String'>
    readonly map: FieldRef<"Tournament", 'String'>
    readonly pickMode: FieldRef<"Tournament", 'TournamentPickMode'>
    readonly gameMode: FieldRef<"Tournament", 'GameMode'>
    readonly status: FieldRef<"Tournament", 'TournamentStatus'>
    readonly teamSize: FieldRef<"Tournament", 'Int'>
    readonly slots: FieldRef<"Tournament", 'Int'>
    readonly teamRequest: FieldRef<"Tournament", 'Boolean'>
    readonly maxRequests: FieldRef<"Tournament", 'Int'>
    readonly casters: FieldRef<"Tournament", 'String[]'>
    readonly videoLink: FieldRef<"Tournament", 'String'>
    readonly creatorID: FieldRef<"Tournament", 'Int'>
    readonly teams: FieldRef<"Tournament", 'String'>
    readonly requests: FieldRef<"Tournament", 'String[]'>
    readonly bracket: FieldRef<"Tournament", 'String'>
    readonly bracketType: FieldRef<"Tournament", 'BracketType'>
    readonly matchesCount: FieldRef<"Tournament", 'Int'>
    readonly results: FieldRef<"Tournament", 'String'>
  }
    

  // Custom InputTypes

  /**
   * Tournament findUnique
   */
  export type TournamentFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * Filter, which Tournament to fetch.
     */
    where: TournamentWhereUniqueInput
  }


  /**
   * Tournament findUniqueOrThrow
   */
  export type TournamentFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * Filter, which Tournament to fetch.
     */
    where: TournamentWhereUniqueInput
  }


  /**
   * Tournament findFirst
   */
  export type TournamentFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * Filter, which Tournament to fetch.
     */
    where?: TournamentWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Tournaments to fetch.
     */
    orderBy?: TournamentOrderByWithRelationInput | TournamentOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Tournaments.
     */
    cursor?: TournamentWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Tournaments from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Tournaments.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Tournaments.
     */
    distinct?: TournamentScalarFieldEnum | TournamentScalarFieldEnum[]
  }


  /**
   * Tournament findFirstOrThrow
   */
  export type TournamentFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * Filter, which Tournament to fetch.
     */
    where?: TournamentWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Tournaments to fetch.
     */
    orderBy?: TournamentOrderByWithRelationInput | TournamentOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Tournaments.
     */
    cursor?: TournamentWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Tournaments from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Tournaments.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Tournaments.
     */
    distinct?: TournamentScalarFieldEnum | TournamentScalarFieldEnum[]
  }


  /**
   * Tournament findMany
   */
  export type TournamentFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * Filter, which Tournaments to fetch.
     */
    where?: TournamentWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Tournaments to fetch.
     */
    orderBy?: TournamentOrderByWithRelationInput | TournamentOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Tournaments.
     */
    cursor?: TournamentWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Tournaments from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Tournaments.
     */
    skip?: number
    distinct?: TournamentScalarFieldEnum | TournamentScalarFieldEnum[]
  }


  /**
   * Tournament create
   */
  export type TournamentCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * The data needed to create a Tournament.
     */
    data: XOR<TournamentCreateInput, TournamentUncheckedCreateInput>
  }


  /**
   * Tournament createMany
   */
  export type TournamentCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Tournaments.
     */
    data: TournamentCreateManyInput | TournamentCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Tournament update
   */
  export type TournamentUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * The data needed to update a Tournament.
     */
    data: XOR<TournamentUpdateInput, TournamentUncheckedUpdateInput>
    /**
     * Choose, which Tournament to update.
     */
    where: TournamentWhereUniqueInput
  }


  /**
   * Tournament updateMany
   */
  export type TournamentUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Tournaments.
     */
    data: XOR<TournamentUpdateManyMutationInput, TournamentUncheckedUpdateManyInput>
    /**
     * Filter which Tournaments to update
     */
    where?: TournamentWhereInput
  }


  /**
   * Tournament upsert
   */
  export type TournamentUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * The filter to search for the Tournament to update in case it exists.
     */
    where: TournamentWhereUniqueInput
    /**
     * In case the Tournament found by the `where` argument doesn't exist, create a new Tournament with this data.
     */
    create: XOR<TournamentCreateInput, TournamentUncheckedCreateInput>
    /**
     * In case the Tournament was found with the provided `where` argument, update it with this data.
     */
    update: XOR<TournamentUpdateInput, TournamentUncheckedUpdateInput>
  }


  /**
   * Tournament delete
   */
  export type TournamentDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
    /**
     * Filter which Tournament to delete.
     */
    where: TournamentWhereUniqueInput
  }


  /**
   * Tournament deleteMany
   */
  export type TournamentDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Tournaments to delete
     */
    where?: TournamentWhereInput
  }


  /**
   * Tournament without action
   */
  export type TournamentDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Tournament
     */
    select?: TournamentSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TournamentInclude<ExtArgs> | null
  }



  /**
   * Model Item
   */

  export type AggregateItem = {
    _count: ItemCountAggregateOutputType | null
    _avg: ItemAvgAggregateOutputType | null
    _sum: ItemSumAggregateOutputType | null
    _min: ItemMinAggregateOutputType | null
    _max: ItemMaxAggregateOutputType | null
  }

  export type ItemAvgAggregateOutputType = {
    id: number | null
    price: number | null
  }

  export type ItemSumAggregateOutputType = {
    id: number | null
    price: number | null
  }

  export type ItemMinAggregateOutputType = {
    id: number | null
    name: string | null
    displayName: string | null
    type: $Enums.ItemType | null
    price: number | null
    params: string | null
    effect: string | null
    effectParams: string | null
  }

  export type ItemMaxAggregateOutputType = {
    id: number | null
    name: string | null
    displayName: string | null
    type: $Enums.ItemType | null
    price: number | null
    params: string | null
    effect: string | null
    effectParams: string | null
  }

  export type ItemCountAggregateOutputType = {
    id: number
    name: number
    displayName: number
    type: number
    price: number
    params: number
    effect: number
    effectParams: number
    _all: number
  }


  export type ItemAvgAggregateInputType = {
    id?: true
    price?: true
  }

  export type ItemSumAggregateInputType = {
    id?: true
    price?: true
  }

  export type ItemMinAggregateInputType = {
    id?: true
    name?: true
    displayName?: true
    type?: true
    price?: true
    params?: true
    effect?: true
    effectParams?: true
  }

  export type ItemMaxAggregateInputType = {
    id?: true
    name?: true
    displayName?: true
    type?: true
    price?: true
    params?: true
    effect?: true
    effectParams?: true
  }

  export type ItemCountAggregateInputType = {
    id?: true
    name?: true
    displayName?: true
    type?: true
    price?: true
    params?: true
    effect?: true
    effectParams?: true
    _all?: true
  }

  export type ItemAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Item to aggregate.
     */
    where?: ItemWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Items to fetch.
     */
    orderBy?: ItemOrderByWithRelationInput | ItemOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: ItemWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Items from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Items.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Items
    **/
    _count?: true | ItemCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: ItemAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: ItemSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: ItemMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: ItemMaxAggregateInputType
  }

  export type GetItemAggregateType<T extends ItemAggregateArgs> = {
        [P in keyof T & keyof AggregateItem]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateItem[P]>
      : GetScalarType<T[P], AggregateItem[P]>
  }




  export type ItemGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: ItemWhereInput
    orderBy?: ItemOrderByWithAggregationInput | ItemOrderByWithAggregationInput[]
    by: ItemScalarFieldEnum[] | ItemScalarFieldEnum
    having?: ItemScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: ItemCountAggregateInputType | true
    _avg?: ItemAvgAggregateInputType
    _sum?: ItemSumAggregateInputType
    _min?: ItemMinAggregateInputType
    _max?: ItemMaxAggregateInputType
  }

  export type ItemGroupByOutputType = {
    id: number
    name: string
    displayName: string | null
    type: $Enums.ItemType
    price: number
    params: string
    effect: string | null
    effectParams: string | null
    _count: ItemCountAggregateOutputType | null
    _avg: ItemAvgAggregateOutputType | null
    _sum: ItemSumAggregateOutputType | null
    _min: ItemMinAggregateOutputType | null
    _max: ItemMaxAggregateOutputType | null
  }

  type GetItemGroupByPayload<T extends ItemGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<ItemGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof ItemGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], ItemGroupByOutputType[P]>
            : GetScalarType<T[P], ItemGroupByOutputType[P]>
        }
      >
    >


  export type ItemSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    name?: boolean
    displayName?: boolean
    type?: boolean
    price?: boolean
    params?: boolean
    effect?: boolean
    effectParams?: boolean
  }, ExtArgs["result"]["item"]>

  export type ItemSelectScalar = {
    id?: boolean
    name?: boolean
    displayName?: boolean
    type?: boolean
    price?: boolean
    params?: boolean
    effect?: boolean
    effectParams?: boolean
  }


  export type $ItemPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Item"
    objects: {}
    scalars: $Extensions.GetResult<{
      id: number
      name: string
      displayName: string | null
      type: $Enums.ItemType
      price: number
      params: string
      effect: string | null
      effectParams: string | null
    }, ExtArgs["result"]["item"]>
    composites: {}
  }


  type ItemGetPayload<S extends boolean | null | undefined | ItemDefaultArgs> = $Result.GetResult<Prisma.$ItemPayload, S>

  type ItemCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<ItemFindManyArgs, 'select' | 'include'> & {
      select?: ItemCountAggregateInputType | true
    }

  export interface ItemDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Item'], meta: { name: 'Item' } }
    /**
     * Find zero or one Item that matches the filter.
     * @param {ItemFindUniqueArgs} args - Arguments to find a Item
     * @example
     * // Get one Item
     * const item = await prisma.item.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends ItemFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, ItemFindUniqueArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Item that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {ItemFindUniqueOrThrowArgs} args - Arguments to find a Item
     * @example
     * // Get one Item
     * const item = await prisma.item.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends ItemFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, ItemFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Item that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ItemFindFirstArgs} args - Arguments to find a Item
     * @example
     * // Get one Item
     * const item = await prisma.item.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends ItemFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, ItemFindFirstArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Item that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ItemFindFirstOrThrowArgs} args - Arguments to find a Item
     * @example
     * // Get one Item
     * const item = await prisma.item.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends ItemFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, ItemFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Items that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ItemFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Items
     * const items = await prisma.item.findMany()
     * 
     * // Get first 10 Items
     * const items = await prisma.item.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const itemWithIdOnly = await prisma.item.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends ItemFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, ItemFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Item.
     * @param {ItemCreateArgs} args - Arguments to create a Item.
     * @example
     * // Create one Item
     * const Item = await prisma.item.create({
     *   data: {
     *     // ... data to create a Item
     *   }
     * })
     * 
    **/
    create<T extends ItemCreateArgs<ExtArgs>>(
      args: SelectSubset<T, ItemCreateArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Items.
     *     @param {ItemCreateManyArgs} args - Arguments to create many Items.
     *     @example
     *     // Create many Items
     *     const item = await prisma.item.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends ItemCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, ItemCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Item.
     * @param {ItemDeleteArgs} args - Arguments to delete one Item.
     * @example
     * // Delete one Item
     * const Item = await prisma.item.delete({
     *   where: {
     *     // ... filter to delete one Item
     *   }
     * })
     * 
    **/
    delete<T extends ItemDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, ItemDeleteArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Item.
     * @param {ItemUpdateArgs} args - Arguments to update one Item.
     * @example
     * // Update one Item
     * const item = await prisma.item.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends ItemUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, ItemUpdateArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Items.
     * @param {ItemDeleteManyArgs} args - Arguments to filter Items to delete.
     * @example
     * // Delete a few Items
     * const { count } = await prisma.item.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends ItemDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, ItemDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Items.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ItemUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Items
     * const item = await prisma.item.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends ItemUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, ItemUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Item.
     * @param {ItemUpsertArgs} args - Arguments to update or create a Item.
     * @example
     * // Update or create a Item
     * const item = await prisma.item.upsert({
     *   create: {
     *     // ... data to create a Item
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Item we want to update
     *   }
     * })
    **/
    upsert<T extends ItemUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, ItemUpsertArgs<ExtArgs>>
    ): Prisma__ItemClient<$Result.GetResult<Prisma.$ItemPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Items.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ItemCountArgs} args - Arguments to filter Items to count.
     * @example
     * // Count the number of Items
     * const count = await prisma.item.count({
     *   where: {
     *     // ... the filter for the Items we want to count
     *   }
     * })
    **/
    count<T extends ItemCountArgs>(
      args?: Subset<T, ItemCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], ItemCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Item.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ItemAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends ItemAggregateArgs>(args: Subset<T, ItemAggregateArgs>): Prisma.PrismaPromise<GetItemAggregateType<T>>

    /**
     * Group by Item.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {ItemGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends ItemGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: ItemGroupByArgs['orderBy'] }
        : { orderBy?: ItemGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, ItemGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetItemGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Item model
   */
  readonly fields: ItemFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Item.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__ItemClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';


    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Item model
   */ 
  interface ItemFieldRefs {
    readonly id: FieldRef<"Item", 'Int'>
    readonly name: FieldRef<"Item", 'String'>
    readonly displayName: FieldRef<"Item", 'String'>
    readonly type: FieldRef<"Item", 'ItemType'>
    readonly price: FieldRef<"Item", 'Int'>
    readonly params: FieldRef<"Item", 'String'>
    readonly effect: FieldRef<"Item", 'String'>
    readonly effectParams: FieldRef<"Item", 'String'>
  }
    

  // Custom InputTypes

  /**
   * Item findUnique
   */
  export type ItemFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * Filter, which Item to fetch.
     */
    where: ItemWhereUniqueInput
  }


  /**
   * Item findUniqueOrThrow
   */
  export type ItemFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * Filter, which Item to fetch.
     */
    where: ItemWhereUniqueInput
  }


  /**
   * Item findFirst
   */
  export type ItemFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * Filter, which Item to fetch.
     */
    where?: ItemWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Items to fetch.
     */
    orderBy?: ItemOrderByWithRelationInput | ItemOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Items.
     */
    cursor?: ItemWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Items from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Items.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Items.
     */
    distinct?: ItemScalarFieldEnum | ItemScalarFieldEnum[]
  }


  /**
   * Item findFirstOrThrow
   */
  export type ItemFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * Filter, which Item to fetch.
     */
    where?: ItemWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Items to fetch.
     */
    orderBy?: ItemOrderByWithRelationInput | ItemOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Items.
     */
    cursor?: ItemWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Items from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Items.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Items.
     */
    distinct?: ItemScalarFieldEnum | ItemScalarFieldEnum[]
  }


  /**
   * Item findMany
   */
  export type ItemFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * Filter, which Items to fetch.
     */
    where?: ItemWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Items to fetch.
     */
    orderBy?: ItemOrderByWithRelationInput | ItemOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Items.
     */
    cursor?: ItemWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Items from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Items.
     */
    skip?: number
    distinct?: ItemScalarFieldEnum | ItemScalarFieldEnum[]
  }


  /**
   * Item create
   */
  export type ItemCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * The data needed to create a Item.
     */
    data: XOR<ItemCreateInput, ItemUncheckedCreateInput>
  }


  /**
   * Item createMany
   */
  export type ItemCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Items.
     */
    data: ItemCreateManyInput | ItemCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Item update
   */
  export type ItemUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * The data needed to update a Item.
     */
    data: XOR<ItemUpdateInput, ItemUncheckedUpdateInput>
    /**
     * Choose, which Item to update.
     */
    where: ItemWhereUniqueInput
  }


  /**
   * Item updateMany
   */
  export type ItemUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Items.
     */
    data: XOR<ItemUpdateManyMutationInput, ItemUncheckedUpdateManyInput>
    /**
     * Filter which Items to update
     */
    where?: ItemWhereInput
  }


  /**
   * Item upsert
   */
  export type ItemUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * The filter to search for the Item to update in case it exists.
     */
    where: ItemWhereUniqueInput
    /**
     * In case the Item found by the `where` argument doesn't exist, create a new Item with this data.
     */
    create: XOR<ItemCreateInput, ItemUncheckedCreateInput>
    /**
     * In case the Item was found with the provided `where` argument, update it with this data.
     */
    update: XOR<ItemUpdateInput, ItemUncheckedUpdateInput>
  }


  /**
   * Item delete
   */
  export type ItemDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
    /**
     * Filter which Item to delete.
     */
    where: ItemWhereUniqueInput
  }


  /**
   * Item deleteMany
   */
  export type ItemDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Items to delete
     */
    where?: ItemWhereInput
  }


  /**
   * Item without action
   */
  export type ItemDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Item
     */
    select?: ItemSelect<ExtArgs> | null
  }



  /**
   * Model Log
   */

  export type AggregateLog = {
    _count: LogCountAggregateOutputType | null
    _avg: LogAvgAggregateOutputType | null
    _sum: LogSumAggregateOutputType | null
    _min: LogMinAggregateOutputType | null
    _max: LogMaxAggregateOutputType | null
  }

  export type LogAvgAggregateOutputType = {
    id: number | null
  }

  export type LogSumAggregateOutputType = {
    id: number | null
  }

  export type LogMinAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    type: $Enums.LogType | null
    message: string | null
  }

  export type LogMaxAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    type: $Enums.LogType | null
    message: string | null
  }

  export type LogCountAggregateOutputType = {
    id: number
    createdAt: number
    type: number
    message: number
    _all: number
  }


  export type LogAvgAggregateInputType = {
    id?: true
  }

  export type LogSumAggregateInputType = {
    id?: true
  }

  export type LogMinAggregateInputType = {
    id?: true
    createdAt?: true
    type?: true
    message?: true
  }

  export type LogMaxAggregateInputType = {
    id?: true
    createdAt?: true
    type?: true
    message?: true
  }

  export type LogCountAggregateInputType = {
    id?: true
    createdAt?: true
    type?: true
    message?: true
    _all?: true
  }

  export type LogAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Log to aggregate.
     */
    where?: LogWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Logs to fetch.
     */
    orderBy?: LogOrderByWithRelationInput | LogOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: LogWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Logs from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Logs.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Logs
    **/
    _count?: true | LogCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: LogAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: LogSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: LogMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: LogMaxAggregateInputType
  }

  export type GetLogAggregateType<T extends LogAggregateArgs> = {
        [P in keyof T & keyof AggregateLog]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateLog[P]>
      : GetScalarType<T[P], AggregateLog[P]>
  }




  export type LogGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: LogWhereInput
    orderBy?: LogOrderByWithAggregationInput | LogOrderByWithAggregationInput[]
    by: LogScalarFieldEnum[] | LogScalarFieldEnum
    having?: LogScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: LogCountAggregateInputType | true
    _avg?: LogAvgAggregateInputType
    _sum?: LogSumAggregateInputType
    _min?: LogMinAggregateInputType
    _max?: LogMaxAggregateInputType
  }

  export type LogGroupByOutputType = {
    id: number
    createdAt: Date
    type: $Enums.LogType
    message: string
    _count: LogCountAggregateOutputType | null
    _avg: LogAvgAggregateOutputType | null
    _sum: LogSumAggregateOutputType | null
    _min: LogMinAggregateOutputType | null
    _max: LogMaxAggregateOutputType | null
  }

  type GetLogGroupByPayload<T extends LogGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<LogGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof LogGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], LogGroupByOutputType[P]>
            : GetScalarType<T[P], LogGroupByOutputType[P]>
        }
      >
    >


  export type LogSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    createdAt?: boolean
    type?: boolean
    message?: boolean
  }, ExtArgs["result"]["log"]>

  export type LogSelectScalar = {
    id?: boolean
    createdAt?: boolean
    type?: boolean
    message?: boolean
  }


  export type $LogPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Log"
    objects: {}
    scalars: $Extensions.GetResult<{
      id: number
      createdAt: Date
      type: $Enums.LogType
      message: string
    }, ExtArgs["result"]["log"]>
    composites: {}
  }


  type LogGetPayload<S extends boolean | null | undefined | LogDefaultArgs> = $Result.GetResult<Prisma.$LogPayload, S>

  type LogCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<LogFindManyArgs, 'select' | 'include'> & {
      select?: LogCountAggregateInputType | true
    }

  export interface LogDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Log'], meta: { name: 'Log' } }
    /**
     * Find zero or one Log that matches the filter.
     * @param {LogFindUniqueArgs} args - Arguments to find a Log
     * @example
     * // Get one Log
     * const log = await prisma.log.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends LogFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, LogFindUniqueArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Log that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {LogFindUniqueOrThrowArgs} args - Arguments to find a Log
     * @example
     * // Get one Log
     * const log = await prisma.log.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends LogFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, LogFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Log that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LogFindFirstArgs} args - Arguments to find a Log
     * @example
     * // Get one Log
     * const log = await prisma.log.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends LogFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, LogFindFirstArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Log that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LogFindFirstOrThrowArgs} args - Arguments to find a Log
     * @example
     * // Get one Log
     * const log = await prisma.log.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends LogFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, LogFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Logs that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LogFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Logs
     * const logs = await prisma.log.findMany()
     * 
     * // Get first 10 Logs
     * const logs = await prisma.log.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const logWithIdOnly = await prisma.log.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends LogFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, LogFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Log.
     * @param {LogCreateArgs} args - Arguments to create a Log.
     * @example
     * // Create one Log
     * const Log = await prisma.log.create({
     *   data: {
     *     // ... data to create a Log
     *   }
     * })
     * 
    **/
    create<T extends LogCreateArgs<ExtArgs>>(
      args: SelectSubset<T, LogCreateArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Logs.
     *     @param {LogCreateManyArgs} args - Arguments to create many Logs.
     *     @example
     *     // Create many Logs
     *     const log = await prisma.log.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends LogCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, LogCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Log.
     * @param {LogDeleteArgs} args - Arguments to delete one Log.
     * @example
     * // Delete one Log
     * const Log = await prisma.log.delete({
     *   where: {
     *     // ... filter to delete one Log
     *   }
     * })
     * 
    **/
    delete<T extends LogDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, LogDeleteArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Log.
     * @param {LogUpdateArgs} args - Arguments to update one Log.
     * @example
     * // Update one Log
     * const log = await prisma.log.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends LogUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, LogUpdateArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Logs.
     * @param {LogDeleteManyArgs} args - Arguments to filter Logs to delete.
     * @example
     * // Delete a few Logs
     * const { count } = await prisma.log.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends LogDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, LogDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Logs.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LogUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Logs
     * const log = await prisma.log.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends LogUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, LogUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Log.
     * @param {LogUpsertArgs} args - Arguments to update or create a Log.
     * @example
     * // Update or create a Log
     * const log = await prisma.log.upsert({
     *   create: {
     *     // ... data to create a Log
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Log we want to update
     *   }
     * })
    **/
    upsert<T extends LogUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, LogUpsertArgs<ExtArgs>>
    ): Prisma__LogClient<$Result.GetResult<Prisma.$LogPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Logs.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LogCountArgs} args - Arguments to filter Logs to count.
     * @example
     * // Count the number of Logs
     * const count = await prisma.log.count({
     *   where: {
     *     // ... the filter for the Logs we want to count
     *   }
     * })
    **/
    count<T extends LogCountArgs>(
      args?: Subset<T, LogCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], LogCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Log.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LogAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends LogAggregateArgs>(args: Subset<T, LogAggregateArgs>): Prisma.PrismaPromise<GetLogAggregateType<T>>

    /**
     * Group by Log.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {LogGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends LogGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: LogGroupByArgs['orderBy'] }
        : { orderBy?: LogGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, LogGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetLogGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Log model
   */
  readonly fields: LogFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Log.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__LogClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';


    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Log model
   */ 
  interface LogFieldRefs {
    readonly id: FieldRef<"Log", 'Int'>
    readonly createdAt: FieldRef<"Log", 'DateTime'>
    readonly type: FieldRef<"Log", 'LogType'>
    readonly message: FieldRef<"Log", 'String'>
  }
    

  // Custom InputTypes

  /**
   * Log findUnique
   */
  export type LogFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * Filter, which Log to fetch.
     */
    where: LogWhereUniqueInput
  }


  /**
   * Log findUniqueOrThrow
   */
  export type LogFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * Filter, which Log to fetch.
     */
    where: LogWhereUniqueInput
  }


  /**
   * Log findFirst
   */
  export type LogFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * Filter, which Log to fetch.
     */
    where?: LogWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Logs to fetch.
     */
    orderBy?: LogOrderByWithRelationInput | LogOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Logs.
     */
    cursor?: LogWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Logs from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Logs.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Logs.
     */
    distinct?: LogScalarFieldEnum | LogScalarFieldEnum[]
  }


  /**
   * Log findFirstOrThrow
   */
  export type LogFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * Filter, which Log to fetch.
     */
    where?: LogWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Logs to fetch.
     */
    orderBy?: LogOrderByWithRelationInput | LogOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Logs.
     */
    cursor?: LogWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Logs from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Logs.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Logs.
     */
    distinct?: LogScalarFieldEnum | LogScalarFieldEnum[]
  }


  /**
   * Log findMany
   */
  export type LogFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * Filter, which Logs to fetch.
     */
    where?: LogWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Logs to fetch.
     */
    orderBy?: LogOrderByWithRelationInput | LogOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Logs.
     */
    cursor?: LogWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Logs from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Logs.
     */
    skip?: number
    distinct?: LogScalarFieldEnum | LogScalarFieldEnum[]
  }


  /**
   * Log create
   */
  export type LogCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * The data needed to create a Log.
     */
    data: XOR<LogCreateInput, LogUncheckedCreateInput>
  }


  /**
   * Log createMany
   */
  export type LogCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Logs.
     */
    data: LogCreateManyInput | LogCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Log update
   */
  export type LogUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * The data needed to update a Log.
     */
    data: XOR<LogUpdateInput, LogUncheckedUpdateInput>
    /**
     * Choose, which Log to update.
     */
    where: LogWhereUniqueInput
  }


  /**
   * Log updateMany
   */
  export type LogUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Logs.
     */
    data: XOR<LogUpdateManyMutationInput, LogUncheckedUpdateManyInput>
    /**
     * Filter which Logs to update
     */
    where?: LogWhereInput
  }


  /**
   * Log upsert
   */
  export type LogUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * The filter to search for the Log to update in case it exists.
     */
    where: LogWhereUniqueInput
    /**
     * In case the Log found by the `where` argument doesn't exist, create a new Log with this data.
     */
    create: XOR<LogCreateInput, LogUncheckedCreateInput>
    /**
     * In case the Log was found with the provided `where` argument, update it with this data.
     */
    update: XOR<LogUpdateInput, LogUncheckedUpdateInput>
  }


  /**
   * Log delete
   */
  export type LogDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
    /**
     * Filter which Log to delete.
     */
    where: LogWhereUniqueInput
  }


  /**
   * Log deleteMany
   */
  export type LogDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Logs to delete
     */
    where?: LogWhereInput
  }


  /**
   * Log without action
   */
  export type LogDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Log
     */
    select?: LogSelect<ExtArgs> | null
  }



  /**
   * Model Team
   */

  export type AggregateTeam = {
    _count: TeamCountAggregateOutputType | null
    _avg: TeamAvgAggregateOutputType | null
    _sum: TeamSumAggregateOutputType | null
    _min: TeamMinAggregateOutputType | null
    _max: TeamMaxAggregateOutputType | null
  }

  export type TeamAvgAggregateOutputType = {
    id: number | null
    rating: Decimal | null
    tournamentWins: number | null
    gameWins: number | null
    gameLosed: number | null
    ownerID: number | null
  }

  export type TeamSumAggregateOutputType = {
    id: number | null
    rating: Decimal | null
    tournamentWins: number[]
    gameWins: number | null
    gameLosed: number | null
    ownerID: number | null
  }

  export type TeamMinAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    name: string | null
    description: string | null
    icon: string | null
    rating: Decimal | null
    gameWins: number | null
    gameLosed: number | null
    ownerID: number | null
  }

  export type TeamMaxAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    name: string | null
    description: string | null
    icon: string | null
    rating: Decimal | null
    gameWins: number | null
    gameLosed: number | null
    ownerID: number | null
  }

  export type TeamCountAggregateOutputType = {
    id: number
    createdAt: number
    name: number
    description: number
    icon: number
    rating: number
    tournamentWins: number
    gameWins: number
    gameLosed: number
    ownerID: number
    requests: number
    _all: number
  }


  export type TeamAvgAggregateInputType = {
    id?: true
    rating?: true
    tournamentWins?: true
    gameWins?: true
    gameLosed?: true
    ownerID?: true
  }

  export type TeamSumAggregateInputType = {
    id?: true
    rating?: true
    tournamentWins?: true
    gameWins?: true
    gameLosed?: true
    ownerID?: true
  }

  export type TeamMinAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    rating?: true
    gameWins?: true
    gameLosed?: true
    ownerID?: true
  }

  export type TeamMaxAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    rating?: true
    gameWins?: true
    gameLosed?: true
    ownerID?: true
  }

  export type TeamCountAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    rating?: true
    tournamentWins?: true
    gameWins?: true
    gameLosed?: true
    ownerID?: true
    requests?: true
    _all?: true
  }

  export type TeamAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Team to aggregate.
     */
    where?: TeamWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Teams to fetch.
     */
    orderBy?: TeamOrderByWithRelationInput | TeamOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: TeamWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Teams from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Teams.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Teams
    **/
    _count?: true | TeamCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: TeamAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: TeamSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: TeamMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: TeamMaxAggregateInputType
  }

  export type GetTeamAggregateType<T extends TeamAggregateArgs> = {
        [P in keyof T & keyof AggregateTeam]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateTeam[P]>
      : GetScalarType<T[P], AggregateTeam[P]>
  }




  export type TeamGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: TeamWhereInput
    orderBy?: TeamOrderByWithAggregationInput | TeamOrderByWithAggregationInput[]
    by: TeamScalarFieldEnum[] | TeamScalarFieldEnum
    having?: TeamScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: TeamCountAggregateInputType | true
    _avg?: TeamAvgAggregateInputType
    _sum?: TeamSumAggregateInputType
    _min?: TeamMinAggregateInputType
    _max?: TeamMaxAggregateInputType
  }

  export type TeamGroupByOutputType = {
    id: number
    createdAt: Date
    name: string
    description: string
    icon: string
    rating: Decimal
    tournamentWins: number[]
    gameWins: number
    gameLosed: number
    ownerID: number
    requests: string[]
    _count: TeamCountAggregateOutputType | null
    _avg: TeamAvgAggregateOutputType | null
    _sum: TeamSumAggregateOutputType | null
    _min: TeamMinAggregateOutputType | null
    _max: TeamMaxAggregateOutputType | null
  }

  type GetTeamGroupByPayload<T extends TeamGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<TeamGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof TeamGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], TeamGroupByOutputType[P]>
            : GetScalarType<T[P], TeamGroupByOutputType[P]>
        }
      >
    >


  export type TeamSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    createdAt?: boolean
    name?: boolean
    description?: boolean
    icon?: boolean
    rating?: boolean
    tournamentWins?: boolean
    gameWins?: boolean
    gameLosed?: boolean
    ownerID?: boolean
    requests?: boolean
    owner?: boolean | UserDefaultArgs<ExtArgs>
    members?: boolean | Team$membersArgs<ExtArgs>
    _count?: boolean | TeamCountOutputTypeDefaultArgs<ExtArgs>
  }, ExtArgs["result"]["team"]>

  export type TeamSelectScalar = {
    id?: boolean
    createdAt?: boolean
    name?: boolean
    description?: boolean
    icon?: boolean
    rating?: boolean
    tournamentWins?: boolean
    gameWins?: boolean
    gameLosed?: boolean
    ownerID?: boolean
    requests?: boolean
  }

  export type TeamInclude<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    owner?: boolean | UserDefaultArgs<ExtArgs>
    members?: boolean | Team$membersArgs<ExtArgs>
    _count?: boolean | TeamCountOutputTypeDefaultArgs<ExtArgs>
  }


  export type $TeamPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Team"
    objects: {
      owner: Prisma.$UserPayload<ExtArgs>
      members: Prisma.$UserPayload<ExtArgs>[]
    }
    scalars: $Extensions.GetResult<{
      id: number
      createdAt: Date
      name: string
      description: string
      icon: string
      rating: Prisma.Decimal
      tournamentWins: number[]
      gameWins: number
      gameLosed: number
      ownerID: number
      requests: string[]
    }, ExtArgs["result"]["team"]>
    composites: {}
  }


  type TeamGetPayload<S extends boolean | null | undefined | TeamDefaultArgs> = $Result.GetResult<Prisma.$TeamPayload, S>

  type TeamCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<TeamFindManyArgs, 'select' | 'include'> & {
      select?: TeamCountAggregateInputType | true
    }

  export interface TeamDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Team'], meta: { name: 'Team' } }
    /**
     * Find zero or one Team that matches the filter.
     * @param {TeamFindUniqueArgs} args - Arguments to find a Team
     * @example
     * // Get one Team
     * const team = await prisma.team.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends TeamFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, TeamFindUniqueArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Team that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {TeamFindUniqueOrThrowArgs} args - Arguments to find a Team
     * @example
     * // Get one Team
     * const team = await prisma.team.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends TeamFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, TeamFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Team that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TeamFindFirstArgs} args - Arguments to find a Team
     * @example
     * // Get one Team
     * const team = await prisma.team.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends TeamFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, TeamFindFirstArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Team that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TeamFindFirstOrThrowArgs} args - Arguments to find a Team
     * @example
     * // Get one Team
     * const team = await prisma.team.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends TeamFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, TeamFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Teams that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TeamFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Teams
     * const teams = await prisma.team.findMany()
     * 
     * // Get first 10 Teams
     * const teams = await prisma.team.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const teamWithIdOnly = await prisma.team.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends TeamFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, TeamFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Team.
     * @param {TeamCreateArgs} args - Arguments to create a Team.
     * @example
     * // Create one Team
     * const Team = await prisma.team.create({
     *   data: {
     *     // ... data to create a Team
     *   }
     * })
     * 
    **/
    create<T extends TeamCreateArgs<ExtArgs>>(
      args: SelectSubset<T, TeamCreateArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Teams.
     *     @param {TeamCreateManyArgs} args - Arguments to create many Teams.
     *     @example
     *     // Create many Teams
     *     const team = await prisma.team.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends TeamCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, TeamCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Team.
     * @param {TeamDeleteArgs} args - Arguments to delete one Team.
     * @example
     * // Delete one Team
     * const Team = await prisma.team.delete({
     *   where: {
     *     // ... filter to delete one Team
     *   }
     * })
     * 
    **/
    delete<T extends TeamDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, TeamDeleteArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Team.
     * @param {TeamUpdateArgs} args - Arguments to update one Team.
     * @example
     * // Update one Team
     * const team = await prisma.team.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends TeamUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, TeamUpdateArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Teams.
     * @param {TeamDeleteManyArgs} args - Arguments to filter Teams to delete.
     * @example
     * // Delete a few Teams
     * const { count } = await prisma.team.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends TeamDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, TeamDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Teams.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TeamUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Teams
     * const team = await prisma.team.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends TeamUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, TeamUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Team.
     * @param {TeamUpsertArgs} args - Arguments to update or create a Team.
     * @example
     * // Update or create a Team
     * const team = await prisma.team.upsert({
     *   create: {
     *     // ... data to create a Team
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Team we want to update
     *   }
     * })
    **/
    upsert<T extends TeamUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, TeamUpsertArgs<ExtArgs>>
    ): Prisma__TeamClient<$Result.GetResult<Prisma.$TeamPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Teams.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TeamCountArgs} args - Arguments to filter Teams to count.
     * @example
     * // Count the number of Teams
     * const count = await prisma.team.count({
     *   where: {
     *     // ... the filter for the Teams we want to count
     *   }
     * })
    **/
    count<T extends TeamCountArgs>(
      args?: Subset<T, TeamCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], TeamCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Team.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TeamAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends TeamAggregateArgs>(args: Subset<T, TeamAggregateArgs>): Prisma.PrismaPromise<GetTeamAggregateType<T>>

    /**
     * Group by Team.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {TeamGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends TeamGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: TeamGroupByArgs['orderBy'] }
        : { orderBy?: TeamGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, TeamGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetTeamGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Team model
   */
  readonly fields: TeamFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Team.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__TeamClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';

    owner<T extends UserDefaultArgs<ExtArgs> = {}>(args?: Subset<T, UserDefaultArgs<ExtArgs>>): Prisma__UserClient<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findUniqueOrThrow'> | Null, Null, ExtArgs>;

    members<T extends Team$membersArgs<ExtArgs> = {}>(args?: Subset<T, Team$membersArgs<ExtArgs>>): Prisma.PrismaPromise<$Result.GetResult<Prisma.$UserPayload<ExtArgs>, T, 'findMany'> | Null>;

    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Team model
   */ 
  interface TeamFieldRefs {
    readonly id: FieldRef<"Team", 'Int'>
    readonly createdAt: FieldRef<"Team", 'DateTime'>
    readonly name: FieldRef<"Team", 'String'>
    readonly description: FieldRef<"Team", 'String'>
    readonly icon: FieldRef<"Team", 'String'>
    readonly rating: FieldRef<"Team", 'Decimal'>
    readonly tournamentWins: FieldRef<"Team", 'Int[]'>
    readonly gameWins: FieldRef<"Team", 'Int'>
    readonly gameLosed: FieldRef<"Team", 'Int'>
    readonly ownerID: FieldRef<"Team", 'Int'>
    readonly requests: FieldRef<"Team", 'String[]'>
  }
    

  // Custom InputTypes

  /**
   * Team findUnique
   */
  export type TeamFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * Filter, which Team to fetch.
     */
    where: TeamWhereUniqueInput
  }


  /**
   * Team findUniqueOrThrow
   */
  export type TeamFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * Filter, which Team to fetch.
     */
    where: TeamWhereUniqueInput
  }


  /**
   * Team findFirst
   */
  export type TeamFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * Filter, which Team to fetch.
     */
    where?: TeamWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Teams to fetch.
     */
    orderBy?: TeamOrderByWithRelationInput | TeamOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Teams.
     */
    cursor?: TeamWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Teams from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Teams.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Teams.
     */
    distinct?: TeamScalarFieldEnum | TeamScalarFieldEnum[]
  }


  /**
   * Team findFirstOrThrow
   */
  export type TeamFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * Filter, which Team to fetch.
     */
    where?: TeamWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Teams to fetch.
     */
    orderBy?: TeamOrderByWithRelationInput | TeamOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Teams.
     */
    cursor?: TeamWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Teams from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Teams.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Teams.
     */
    distinct?: TeamScalarFieldEnum | TeamScalarFieldEnum[]
  }


  /**
   * Team findMany
   */
  export type TeamFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * Filter, which Teams to fetch.
     */
    where?: TeamWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Teams to fetch.
     */
    orderBy?: TeamOrderByWithRelationInput | TeamOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Teams.
     */
    cursor?: TeamWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Teams from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Teams.
     */
    skip?: number
    distinct?: TeamScalarFieldEnum | TeamScalarFieldEnum[]
  }


  /**
   * Team create
   */
  export type TeamCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * The data needed to create a Team.
     */
    data: XOR<TeamCreateInput, TeamUncheckedCreateInput>
  }


  /**
   * Team createMany
   */
  export type TeamCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Teams.
     */
    data: TeamCreateManyInput | TeamCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Team update
   */
  export type TeamUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * The data needed to update a Team.
     */
    data: XOR<TeamUpdateInput, TeamUncheckedUpdateInput>
    /**
     * Choose, which Team to update.
     */
    where: TeamWhereUniqueInput
  }


  /**
   * Team updateMany
   */
  export type TeamUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Teams.
     */
    data: XOR<TeamUpdateManyMutationInput, TeamUncheckedUpdateManyInput>
    /**
     * Filter which Teams to update
     */
    where?: TeamWhereInput
  }


  /**
   * Team upsert
   */
  export type TeamUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * The filter to search for the Team to update in case it exists.
     */
    where: TeamWhereUniqueInput
    /**
     * In case the Team found by the `where` argument doesn't exist, create a new Team with this data.
     */
    create: XOR<TeamCreateInput, TeamUncheckedCreateInput>
    /**
     * In case the Team was found with the provided `where` argument, update it with this data.
     */
    update: XOR<TeamUpdateInput, TeamUncheckedUpdateInput>
  }


  /**
   * Team delete
   */
  export type TeamDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
    /**
     * Filter which Team to delete.
     */
    where: TeamWhereUniqueInput
  }


  /**
   * Team deleteMany
   */
  export type TeamDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Teams to delete
     */
    where?: TeamWhereInput
  }


  /**
   * Team.members
   */
  export type Team$membersArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the User
     */
    select?: UserSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: UserInclude<ExtArgs> | null
    where?: UserWhereInput
    orderBy?: UserOrderByWithRelationInput | UserOrderByWithRelationInput[]
    cursor?: UserWhereUniqueInput
    take?: number
    skip?: number
    distinct?: UserScalarFieldEnum | UserScalarFieldEnum[]
  }


  /**
   * Team without action
   */
  export type TeamDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Team
     */
    select?: TeamSelect<ExtArgs> | null
    /**
     * Choose, which related nodes to fetch as well.
     */
    include?: TeamInclude<ExtArgs> | null
  }



  /**
   * Model News
   */

  export type AggregateNews = {
    _count: NewsCountAggregateOutputType | null
    _avg: NewsAvgAggregateOutputType | null
    _sum: NewsSumAggregateOutputType | null
    _min: NewsMinAggregateOutputType | null
    _max: NewsMaxAggregateOutputType | null
  }

  export type NewsAvgAggregateOutputType = {
    id: number | null
  }

  export type NewsSumAggregateOutputType = {
    id: number | null
  }

  export type NewsMinAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    name: string | null
    description: string | null
    icon: string | null
    patch: string | null
    bugfixes: string | null
  }

  export type NewsMaxAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    name: string | null
    description: string | null
    icon: string | null
    patch: string | null
    bugfixes: string | null
  }

  export type NewsCountAggregateOutputType = {
    id: number
    createdAt: number
    name: number
    description: number
    icon: number
    patch: number
    champions: number
    items: number
    bugfixes: number
    _all: number
  }


  export type NewsAvgAggregateInputType = {
    id?: true
  }

  export type NewsSumAggregateInputType = {
    id?: true
  }

  export type NewsMinAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    patch?: true
    bugfixes?: true
  }

  export type NewsMaxAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    patch?: true
    bugfixes?: true
  }

  export type NewsCountAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    patch?: true
    champions?: true
    items?: true
    bugfixes?: true
    _all?: true
  }

  export type NewsAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which News to aggregate.
     */
    where?: NewsWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of News to fetch.
     */
    orderBy?: NewsOrderByWithRelationInput | NewsOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: NewsWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` News from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` News.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned News
    **/
    _count?: true | NewsCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: NewsAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: NewsSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: NewsMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: NewsMaxAggregateInputType
  }

  export type GetNewsAggregateType<T extends NewsAggregateArgs> = {
        [P in keyof T & keyof AggregateNews]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateNews[P]>
      : GetScalarType<T[P], AggregateNews[P]>
  }




  export type NewsGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: NewsWhereInput
    orderBy?: NewsOrderByWithAggregationInput | NewsOrderByWithAggregationInput[]
    by: NewsScalarFieldEnum[] | NewsScalarFieldEnum
    having?: NewsScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: NewsCountAggregateInputType | true
    _avg?: NewsAvgAggregateInputType
    _sum?: NewsSumAggregateInputType
    _min?: NewsMinAggregateInputType
    _max?: NewsMaxAggregateInputType
  }

  export type NewsGroupByOutputType = {
    id: number
    createdAt: Date
    name: string
    description: string
    icon: string
    patch: string
    champions: string[]
    items: string[]
    bugfixes: string
    _count: NewsCountAggregateOutputType | null
    _avg: NewsAvgAggregateOutputType | null
    _sum: NewsSumAggregateOutputType | null
    _min: NewsMinAggregateOutputType | null
    _max: NewsMaxAggregateOutputType | null
  }

  type GetNewsGroupByPayload<T extends NewsGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<NewsGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof NewsGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], NewsGroupByOutputType[P]>
            : GetScalarType<T[P], NewsGroupByOutputType[P]>
        }
      >
    >


  export type NewsSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    createdAt?: boolean
    name?: boolean
    description?: boolean
    icon?: boolean
    patch?: boolean
    champions?: boolean
    items?: boolean
    bugfixes?: boolean
  }, ExtArgs["result"]["news"]>

  export type NewsSelectScalar = {
    id?: boolean
    createdAt?: boolean
    name?: boolean
    description?: boolean
    icon?: boolean
    patch?: boolean
    champions?: boolean
    items?: boolean
    bugfixes?: boolean
  }


  export type $NewsPayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "News"
    objects: {}
    scalars: $Extensions.GetResult<{
      id: number
      createdAt: Date
      name: string
      description: string
      icon: string
      patch: string
      champions: string[]
      items: string[]
      bugfixes: string
    }, ExtArgs["result"]["news"]>
    composites: {}
  }


  type NewsGetPayload<S extends boolean | null | undefined | NewsDefaultArgs> = $Result.GetResult<Prisma.$NewsPayload, S>

  type NewsCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<NewsFindManyArgs, 'select' | 'include'> & {
      select?: NewsCountAggregateInputType | true
    }

  export interface NewsDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['News'], meta: { name: 'News' } }
    /**
     * Find zero or one News that matches the filter.
     * @param {NewsFindUniqueArgs} args - Arguments to find a News
     * @example
     * // Get one News
     * const news = await prisma.news.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends NewsFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, NewsFindUniqueArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one News that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {NewsFindUniqueOrThrowArgs} args - Arguments to find a News
     * @example
     * // Get one News
     * const news = await prisma.news.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends NewsFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, NewsFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first News that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {NewsFindFirstArgs} args - Arguments to find a News
     * @example
     * // Get one News
     * const news = await prisma.news.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends NewsFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, NewsFindFirstArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first News that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {NewsFindFirstOrThrowArgs} args - Arguments to find a News
     * @example
     * // Get one News
     * const news = await prisma.news.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends NewsFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, NewsFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more News that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {NewsFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all News
     * const news = await prisma.news.findMany()
     * 
     * // Get first 10 News
     * const news = await prisma.news.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const newsWithIdOnly = await prisma.news.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends NewsFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, NewsFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a News.
     * @param {NewsCreateArgs} args - Arguments to create a News.
     * @example
     * // Create one News
     * const News = await prisma.news.create({
     *   data: {
     *     // ... data to create a News
     *   }
     * })
     * 
    **/
    create<T extends NewsCreateArgs<ExtArgs>>(
      args: SelectSubset<T, NewsCreateArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many News.
     *     @param {NewsCreateManyArgs} args - Arguments to create many News.
     *     @example
     *     // Create many News
     *     const news = await prisma.news.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends NewsCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, NewsCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a News.
     * @param {NewsDeleteArgs} args - Arguments to delete one News.
     * @example
     * // Delete one News
     * const News = await prisma.news.delete({
     *   where: {
     *     // ... filter to delete one News
     *   }
     * })
     * 
    **/
    delete<T extends NewsDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, NewsDeleteArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one News.
     * @param {NewsUpdateArgs} args - Arguments to update one News.
     * @example
     * // Update one News
     * const news = await prisma.news.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends NewsUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, NewsUpdateArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more News.
     * @param {NewsDeleteManyArgs} args - Arguments to filter News to delete.
     * @example
     * // Delete a few News
     * const { count } = await prisma.news.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends NewsDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, NewsDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more News.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {NewsUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many News
     * const news = await prisma.news.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends NewsUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, NewsUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one News.
     * @param {NewsUpsertArgs} args - Arguments to update or create a News.
     * @example
     * // Update or create a News
     * const news = await prisma.news.upsert({
     *   create: {
     *     // ... data to create a News
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the News we want to update
     *   }
     * })
    **/
    upsert<T extends NewsUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, NewsUpsertArgs<ExtArgs>>
    ): Prisma__NewsClient<$Result.GetResult<Prisma.$NewsPayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of News.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {NewsCountArgs} args - Arguments to filter News to count.
     * @example
     * // Count the number of News
     * const count = await prisma.news.count({
     *   where: {
     *     // ... the filter for the News we want to count
     *   }
     * })
    **/
    count<T extends NewsCountArgs>(
      args?: Subset<T, NewsCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], NewsCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a News.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {NewsAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends NewsAggregateArgs>(args: Subset<T, NewsAggregateArgs>): Prisma.PrismaPromise<GetNewsAggregateType<T>>

    /**
     * Group by News.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {NewsGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends NewsGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: NewsGroupByArgs['orderBy'] }
        : { orderBy?: NewsGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, NewsGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetNewsGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the News model
   */
  readonly fields: NewsFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for News.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__NewsClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';


    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the News model
   */ 
  interface NewsFieldRefs {
    readonly id: FieldRef<"News", 'Int'>
    readonly createdAt: FieldRef<"News", 'DateTime'>
    readonly name: FieldRef<"News", 'String'>
    readonly description: FieldRef<"News", 'String'>
    readonly icon: FieldRef<"News", 'String'>
    readonly patch: FieldRef<"News", 'String'>
    readonly champions: FieldRef<"News", 'String[]'>
    readonly items: FieldRef<"News", 'String[]'>
    readonly bugfixes: FieldRef<"News", 'String'>
  }
    

  // Custom InputTypes

  /**
   * News findUnique
   */
  export type NewsFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * Filter, which News to fetch.
     */
    where: NewsWhereUniqueInput
  }


  /**
   * News findUniqueOrThrow
   */
  export type NewsFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * Filter, which News to fetch.
     */
    where: NewsWhereUniqueInput
  }


  /**
   * News findFirst
   */
  export type NewsFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * Filter, which News to fetch.
     */
    where?: NewsWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of News to fetch.
     */
    orderBy?: NewsOrderByWithRelationInput | NewsOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for News.
     */
    cursor?: NewsWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` News from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` News.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of News.
     */
    distinct?: NewsScalarFieldEnum | NewsScalarFieldEnum[]
  }


  /**
   * News findFirstOrThrow
   */
  export type NewsFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * Filter, which News to fetch.
     */
    where?: NewsWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of News to fetch.
     */
    orderBy?: NewsOrderByWithRelationInput | NewsOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for News.
     */
    cursor?: NewsWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` News from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` News.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of News.
     */
    distinct?: NewsScalarFieldEnum | NewsScalarFieldEnum[]
  }


  /**
   * News findMany
   */
  export type NewsFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * Filter, which News to fetch.
     */
    where?: NewsWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of News to fetch.
     */
    orderBy?: NewsOrderByWithRelationInput | NewsOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing News.
     */
    cursor?: NewsWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` News from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` News.
     */
    skip?: number
    distinct?: NewsScalarFieldEnum | NewsScalarFieldEnum[]
  }


  /**
   * News create
   */
  export type NewsCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * The data needed to create a News.
     */
    data: XOR<NewsCreateInput, NewsUncheckedCreateInput>
  }


  /**
   * News createMany
   */
  export type NewsCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many News.
     */
    data: NewsCreateManyInput | NewsCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * News update
   */
  export type NewsUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * The data needed to update a News.
     */
    data: XOR<NewsUpdateInput, NewsUncheckedUpdateInput>
    /**
     * Choose, which News to update.
     */
    where: NewsWhereUniqueInput
  }


  /**
   * News updateMany
   */
  export type NewsUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update News.
     */
    data: XOR<NewsUpdateManyMutationInput, NewsUncheckedUpdateManyInput>
    /**
     * Filter which News to update
     */
    where?: NewsWhereInput
  }


  /**
   * News upsert
   */
  export type NewsUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * The filter to search for the News to update in case it exists.
     */
    where: NewsWhereUniqueInput
    /**
     * In case the News found by the `where` argument doesn't exist, create a new News with this data.
     */
    create: XOR<NewsCreateInput, NewsUncheckedCreateInput>
    /**
     * In case the News was found with the provided `where` argument, update it with this data.
     */
    update: XOR<NewsUpdateInput, NewsUncheckedUpdateInput>
  }


  /**
   * News delete
   */
  export type NewsDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
    /**
     * Filter which News to delete.
     */
    where: NewsWhereUniqueInput
  }


  /**
   * News deleteMany
   */
  export type NewsDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which News to delete
     */
    where?: NewsWhereInput
  }


  /**
   * News without action
   */
  export type NewsDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the News
     */
    select?: NewsSelect<ExtArgs> | null
  }



  /**
   * Model Creature
   */

  export type AggregateCreature = {
    _count: CreatureCountAggregateOutputType | null
    _avg: CreatureAvgAggregateOutputType | null
    _sum: CreatureSumAggregateOutputType | null
    _min: CreatureMinAggregateOutputType | null
    _max: CreatureMaxAggregateOutputType | null
  }

  export type CreatureAvgAggregateOutputType = {
    id: number | null
  }

  export type CreatureSumAggregateOutputType = {
    id: number | null
  }

  export type CreatureMinAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    name: string | null
    description: string | null
    icon: string | null
    type: $Enums.CreatureType | null
    health: string | null
    gold: string | null
    exp: string | null
    spawnTime: string | null
    respawnTime: string | null
    buff: string | null
  }

  export type CreatureMaxAggregateOutputType = {
    id: number | null
    createdAt: Date | null
    name: string | null
    description: string | null
    icon: string | null
    type: $Enums.CreatureType | null
    health: string | null
    gold: string | null
    exp: string | null
    spawnTime: string | null
    respawnTime: string | null
    buff: string | null
  }

  export type CreatureCountAggregateOutputType = {
    id: number
    createdAt: number
    name: number
    description: number
    icon: number
    type: number
    health: number
    gold: number
    exp: number
    spawnTime: number
    respawnTime: number
    buff: number
    _all: number
  }


  export type CreatureAvgAggregateInputType = {
    id?: true
  }

  export type CreatureSumAggregateInputType = {
    id?: true
  }

  export type CreatureMinAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    type?: true
    health?: true
    gold?: true
    exp?: true
    spawnTime?: true
    respawnTime?: true
    buff?: true
  }

  export type CreatureMaxAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    type?: true
    health?: true
    gold?: true
    exp?: true
    spawnTime?: true
    respawnTime?: true
    buff?: true
  }

  export type CreatureCountAggregateInputType = {
    id?: true
    createdAt?: true
    name?: true
    description?: true
    icon?: true
    type?: true
    health?: true
    gold?: true
    exp?: true
    spawnTime?: true
    respawnTime?: true
    buff?: true
    _all?: true
  }

  export type CreatureAggregateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Creature to aggregate.
     */
    where?: CreatureWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Creatures to fetch.
     */
    orderBy?: CreatureOrderByWithRelationInput | CreatureOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the start position
     */
    cursor?: CreatureWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Creatures from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Creatures.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Count returned Creatures
    **/
    _count?: true | CreatureCountAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to average
    **/
    _avg?: CreatureAvgAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to sum
    **/
    _sum?: CreatureSumAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the minimum value
    **/
    _min?: CreatureMinAggregateInputType
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/aggregations Aggregation Docs}
     * 
     * Select which fields to find the maximum value
    **/
    _max?: CreatureMaxAggregateInputType
  }

  export type GetCreatureAggregateType<T extends CreatureAggregateArgs> = {
        [P in keyof T & keyof AggregateCreature]: P extends '_count' | 'count'
      ? T[P] extends true
        ? number
        : GetScalarType<T[P], AggregateCreature[P]>
      : GetScalarType<T[P], AggregateCreature[P]>
  }




  export type CreatureGroupByArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    where?: CreatureWhereInput
    orderBy?: CreatureOrderByWithAggregationInput | CreatureOrderByWithAggregationInput[]
    by: CreatureScalarFieldEnum[] | CreatureScalarFieldEnum
    having?: CreatureScalarWhereWithAggregatesInput
    take?: number
    skip?: number
    _count?: CreatureCountAggregateInputType | true
    _avg?: CreatureAvgAggregateInputType
    _sum?: CreatureSumAggregateInputType
    _min?: CreatureMinAggregateInputType
    _max?: CreatureMaxAggregateInputType
  }

  export type CreatureGroupByOutputType = {
    id: number
    createdAt: Date
    name: string
    description: string
    icon: string
    type: $Enums.CreatureType
    health: string
    gold: string
    exp: string
    spawnTime: string
    respawnTime: string
    buff: string
    _count: CreatureCountAggregateOutputType | null
    _avg: CreatureAvgAggregateOutputType | null
    _sum: CreatureSumAggregateOutputType | null
    _min: CreatureMinAggregateOutputType | null
    _max: CreatureMaxAggregateOutputType | null
  }

  type GetCreatureGroupByPayload<T extends CreatureGroupByArgs> = Prisma.PrismaPromise<
    Array<
      PickEnumerable<CreatureGroupByOutputType, T['by']> &
        {
          [P in ((keyof T) & (keyof CreatureGroupByOutputType))]: P extends '_count'
            ? T[P] extends boolean
              ? number
              : GetScalarType<T[P], CreatureGroupByOutputType[P]>
            : GetScalarType<T[P], CreatureGroupByOutputType[P]>
        }
      >
    >


  export type CreatureSelect<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = $Extensions.GetSelect<{
    id?: boolean
    createdAt?: boolean
    name?: boolean
    description?: boolean
    icon?: boolean
    type?: boolean
    health?: boolean
    gold?: boolean
    exp?: boolean
    spawnTime?: boolean
    respawnTime?: boolean
    buff?: boolean
  }, ExtArgs["result"]["creature"]>

  export type CreatureSelectScalar = {
    id?: boolean
    createdAt?: boolean
    name?: boolean
    description?: boolean
    icon?: boolean
    type?: boolean
    health?: boolean
    gold?: boolean
    exp?: boolean
    spawnTime?: boolean
    respawnTime?: boolean
    buff?: boolean
  }


  export type $CreaturePayload<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    name: "Creature"
    objects: {}
    scalars: $Extensions.GetResult<{
      id: number
      createdAt: Date
      name: string
      description: string
      icon: string
      type: $Enums.CreatureType
      health: string
      gold: string
      exp: string
      spawnTime: string
      respawnTime: string
      buff: string
    }, ExtArgs["result"]["creature"]>
    composites: {}
  }


  type CreatureGetPayload<S extends boolean | null | undefined | CreatureDefaultArgs> = $Result.GetResult<Prisma.$CreaturePayload, S>

  type CreatureCountArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = 
    Omit<CreatureFindManyArgs, 'select' | 'include'> & {
      select?: CreatureCountAggregateInputType | true
    }

  export interface CreatureDelegate<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> {
    [K: symbol]: { types: Prisma.TypeMap<ExtArgs>['model']['Creature'], meta: { name: 'Creature' } }
    /**
     * Find zero or one Creature that matches the filter.
     * @param {CreatureFindUniqueArgs} args - Arguments to find a Creature
     * @example
     * // Get one Creature
     * const creature = await prisma.creature.findUnique({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUnique<T extends CreatureFindUniqueArgs<ExtArgs>>(
      args: SelectSubset<T, CreatureFindUniqueArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'findUnique'> | null, null, ExtArgs>

    /**
     * Find one Creature that matches the filter or throw an error  with `error.code='P2025'` 
     *     if no matches were found.
     * @param {CreatureFindUniqueOrThrowArgs} args - Arguments to find a Creature
     * @example
     * // Get one Creature
     * const creature = await prisma.creature.findUniqueOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findUniqueOrThrow<T extends CreatureFindUniqueOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, CreatureFindUniqueOrThrowArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'findUniqueOrThrow'>, never, ExtArgs>

    /**
     * Find the first Creature that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {CreatureFindFirstArgs} args - Arguments to find a Creature
     * @example
     * // Get one Creature
     * const creature = await prisma.creature.findFirst({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirst<T extends CreatureFindFirstArgs<ExtArgs>>(
      args?: SelectSubset<T, CreatureFindFirstArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'findFirst'> | null, null, ExtArgs>

    /**
     * Find the first Creature that matches the filter or
     * throw `PrismaKnownClientError` with `P2025` code if no matches were found.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {CreatureFindFirstOrThrowArgs} args - Arguments to find a Creature
     * @example
     * // Get one Creature
     * const creature = await prisma.creature.findFirstOrThrow({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
    **/
    findFirstOrThrow<T extends CreatureFindFirstOrThrowArgs<ExtArgs>>(
      args?: SelectSubset<T, CreatureFindFirstOrThrowArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'findFirstOrThrow'>, never, ExtArgs>

    /**
     * Find zero or more Creatures that matches the filter.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {CreatureFindManyArgs=} args - Arguments to filter and select certain fields only.
     * @example
     * // Get all Creatures
     * const creatures = await prisma.creature.findMany()
     * 
     * // Get first 10 Creatures
     * const creatures = await prisma.creature.findMany({ take: 10 })
     * 
     * // Only select the `id`
     * const creatureWithIdOnly = await prisma.creature.findMany({ select: { id: true } })
     * 
    **/
    findMany<T extends CreatureFindManyArgs<ExtArgs>>(
      args?: SelectSubset<T, CreatureFindManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'findMany'>>

    /**
     * Create a Creature.
     * @param {CreatureCreateArgs} args - Arguments to create a Creature.
     * @example
     * // Create one Creature
     * const Creature = await prisma.creature.create({
     *   data: {
     *     // ... data to create a Creature
     *   }
     * })
     * 
    **/
    create<T extends CreatureCreateArgs<ExtArgs>>(
      args: SelectSubset<T, CreatureCreateArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'create'>, never, ExtArgs>

    /**
     * Create many Creatures.
     *     @param {CreatureCreateManyArgs} args - Arguments to create many Creatures.
     *     @example
     *     // Create many Creatures
     *     const creature = await prisma.creature.createMany({
     *       data: {
     *         // ... provide data here
     *       }
     *     })
     *     
    **/
    createMany<T extends CreatureCreateManyArgs<ExtArgs>>(
      args?: SelectSubset<T, CreatureCreateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Delete a Creature.
     * @param {CreatureDeleteArgs} args - Arguments to delete one Creature.
     * @example
     * // Delete one Creature
     * const Creature = await prisma.creature.delete({
     *   where: {
     *     // ... filter to delete one Creature
     *   }
     * })
     * 
    **/
    delete<T extends CreatureDeleteArgs<ExtArgs>>(
      args: SelectSubset<T, CreatureDeleteArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'delete'>, never, ExtArgs>

    /**
     * Update one Creature.
     * @param {CreatureUpdateArgs} args - Arguments to update one Creature.
     * @example
     * // Update one Creature
     * const creature = await prisma.creature.update({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    update<T extends CreatureUpdateArgs<ExtArgs>>(
      args: SelectSubset<T, CreatureUpdateArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'update'>, never, ExtArgs>

    /**
     * Delete zero or more Creatures.
     * @param {CreatureDeleteManyArgs} args - Arguments to filter Creatures to delete.
     * @example
     * // Delete a few Creatures
     * const { count } = await prisma.creature.deleteMany({
     *   where: {
     *     // ... provide filter here
     *   }
     * })
     * 
    **/
    deleteMany<T extends CreatureDeleteManyArgs<ExtArgs>>(
      args?: SelectSubset<T, CreatureDeleteManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Update zero or more Creatures.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {CreatureUpdateManyArgs} args - Arguments to update one or more rows.
     * @example
     * // Update many Creatures
     * const creature = await prisma.creature.updateMany({
     *   where: {
     *     // ... provide filter here
     *   },
     *   data: {
     *     // ... provide data here
     *   }
     * })
     * 
    **/
    updateMany<T extends CreatureUpdateManyArgs<ExtArgs>>(
      args: SelectSubset<T, CreatureUpdateManyArgs<ExtArgs>>
    ): Prisma.PrismaPromise<BatchPayload>

    /**
     * Create or update one Creature.
     * @param {CreatureUpsertArgs} args - Arguments to update or create a Creature.
     * @example
     * // Update or create a Creature
     * const creature = await prisma.creature.upsert({
     *   create: {
     *     // ... data to create a Creature
     *   },
     *   update: {
     *     // ... in case it already exists, update
     *   },
     *   where: {
     *     // ... the filter for the Creature we want to update
     *   }
     * })
    **/
    upsert<T extends CreatureUpsertArgs<ExtArgs>>(
      args: SelectSubset<T, CreatureUpsertArgs<ExtArgs>>
    ): Prisma__CreatureClient<$Result.GetResult<Prisma.$CreaturePayload<ExtArgs>, T, 'upsert'>, never, ExtArgs>

    /**
     * Count the number of Creatures.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {CreatureCountArgs} args - Arguments to filter Creatures to count.
     * @example
     * // Count the number of Creatures
     * const count = await prisma.creature.count({
     *   where: {
     *     // ... the filter for the Creatures we want to count
     *   }
     * })
    **/
    count<T extends CreatureCountArgs>(
      args?: Subset<T, CreatureCountArgs>,
    ): Prisma.PrismaPromise<
      T extends $Utils.Record<'select', any>
        ? T['select'] extends true
          ? number
          : GetScalarType<T['select'], CreatureCountAggregateOutputType>
        : number
    >

    /**
     * Allows you to perform aggregations operations on a Creature.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {CreatureAggregateArgs} args - Select which aggregations you would like to apply and on what fields.
     * @example
     * // Ordered by age ascending
     * // Where email contains prisma.io
     * // Limited to the 10 users
     * const aggregations = await prisma.user.aggregate({
     *   _avg: {
     *     age: true,
     *   },
     *   where: {
     *     email: {
     *       contains: "prisma.io",
     *     },
     *   },
     *   orderBy: {
     *     age: "asc",
     *   },
     *   take: 10,
     * })
    **/
    aggregate<T extends CreatureAggregateArgs>(args: Subset<T, CreatureAggregateArgs>): Prisma.PrismaPromise<GetCreatureAggregateType<T>>

    /**
     * Group by Creature.
     * Note, that providing `undefined` is treated as the value not being there.
     * Read more here: https://pris.ly/d/null-undefined
     * @param {CreatureGroupByArgs} args - Group by arguments.
     * @example
     * // Group by city, order by createdAt, get count
     * const result = await prisma.user.groupBy({
     *   by: ['city', 'createdAt'],
     *   orderBy: {
     *     createdAt: true
     *   },
     *   _count: {
     *     _all: true
     *   },
     * })
     * 
    **/
    groupBy<
      T extends CreatureGroupByArgs,
      HasSelectOrTake extends Or<
        Extends<'skip', Keys<T>>,
        Extends<'take', Keys<T>>
      >,
      OrderByArg extends True extends HasSelectOrTake
        ? { orderBy: CreatureGroupByArgs['orderBy'] }
        : { orderBy?: CreatureGroupByArgs['orderBy'] },
      OrderFields extends ExcludeUnderscoreKeys<Keys<MaybeTupleToUnion<T['orderBy']>>>,
      ByFields extends MaybeTupleToUnion<T['by']>,
      ByValid extends Has<ByFields, OrderFields>,
      HavingFields extends GetHavingFields<T['having']>,
      HavingValid extends Has<ByFields, HavingFields>,
      ByEmpty extends T['by'] extends never[] ? True : False,
      InputErrors extends ByEmpty extends True
      ? `Error: "by" must not be empty.`
      : HavingValid extends False
      ? {
          [P in HavingFields]: P extends ByFields
            ? never
            : P extends string
            ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
            : [
                Error,
                'Field ',
                P,
                ` in "having" needs to be provided in "by"`,
              ]
        }[HavingFields]
      : 'take' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "take", you also need to provide "orderBy"'
      : 'skip' extends Keys<T>
      ? 'orderBy' extends Keys<T>
        ? ByValid extends True
          ? {}
          : {
              [P in OrderFields]: P extends ByFields
                ? never
                : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
            }[OrderFields]
        : 'Error: If you provide "skip", you also need to provide "orderBy"'
      : ByValid extends True
      ? {}
      : {
          [P in OrderFields]: P extends ByFields
            ? never
            : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`
        }[OrderFields]
    >(args: SubsetIntersection<T, CreatureGroupByArgs, OrderByArg> & InputErrors): {} extends InputErrors ? GetCreatureGroupByPayload<T> : Prisma.PrismaPromise<InputErrors>
  /**
   * Fields of the Creature model
   */
  readonly fields: CreatureFieldRefs;
  }

  /**
   * The delegate class that acts as a "Promise-like" for Creature.
   * Why is this prefixed with `Prisma__`?
   * Because we want to prevent naming conflicts as mentioned in
   * https://github.com/prisma/prisma-client-js/issues/707
   */
  export interface Prisma__CreatureClient<T, Null = never, ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> extends Prisma.PrismaPromise<T> {
    readonly [Symbol.toStringTag]: 'PrismaPromise';


    /**
     * Attaches callbacks for the resolution and/or rejection of the Promise.
     * @param onfulfilled The callback to execute when the Promise is resolved.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of which ever callback is executed.
     */
    then<TResult1 = T, TResult2 = never>(onfulfilled?: ((value: T) => TResult1 | PromiseLike<TResult1>) | undefined | null, onrejected?: ((reason: any) => TResult2 | PromiseLike<TResult2>) | undefined | null): $Utils.JsPromise<TResult1 | TResult2>;
    /**
     * Attaches a callback for only the rejection of the Promise.
     * @param onrejected The callback to execute when the Promise is rejected.
     * @returns A Promise for the completion of the callback.
     */
    catch<TResult = never>(onrejected?: ((reason: any) => TResult | PromiseLike<TResult>) | undefined | null): $Utils.JsPromise<T | TResult>;
    /**
     * Attaches a callback that is invoked when the Promise is settled (fulfilled or rejected). The
     * resolved value cannot be modified from the callback.
     * @param onfinally The callback to execute when the Promise is settled (fulfilled or rejected).
     * @returns A Promise for the completion of the callback.
     */
    finally(onfinally?: (() => void) | undefined | null): $Utils.JsPromise<T>;
  }



  /**
   * Fields of the Creature model
   */ 
  interface CreatureFieldRefs {
    readonly id: FieldRef<"Creature", 'Int'>
    readonly createdAt: FieldRef<"Creature", 'DateTime'>
    readonly name: FieldRef<"Creature", 'String'>
    readonly description: FieldRef<"Creature", 'String'>
    readonly icon: FieldRef<"Creature", 'String'>
    readonly type: FieldRef<"Creature", 'CreatureType'>
    readonly health: FieldRef<"Creature", 'String'>
    readonly gold: FieldRef<"Creature", 'String'>
    readonly exp: FieldRef<"Creature", 'String'>
    readonly spawnTime: FieldRef<"Creature", 'String'>
    readonly respawnTime: FieldRef<"Creature", 'String'>
    readonly buff: FieldRef<"Creature", 'String'>
  }
    

  // Custom InputTypes

  /**
   * Creature findUnique
   */
  export type CreatureFindUniqueArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * Filter, which Creature to fetch.
     */
    where: CreatureWhereUniqueInput
  }


  /**
   * Creature findUniqueOrThrow
   */
  export type CreatureFindUniqueOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * Filter, which Creature to fetch.
     */
    where: CreatureWhereUniqueInput
  }


  /**
   * Creature findFirst
   */
  export type CreatureFindFirstArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * Filter, which Creature to fetch.
     */
    where?: CreatureWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Creatures to fetch.
     */
    orderBy?: CreatureOrderByWithRelationInput | CreatureOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Creatures.
     */
    cursor?: CreatureWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Creatures from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Creatures.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Creatures.
     */
    distinct?: CreatureScalarFieldEnum | CreatureScalarFieldEnum[]
  }


  /**
   * Creature findFirstOrThrow
   */
  export type CreatureFindFirstOrThrowArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * Filter, which Creature to fetch.
     */
    where?: CreatureWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Creatures to fetch.
     */
    orderBy?: CreatureOrderByWithRelationInput | CreatureOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for searching for Creatures.
     */
    cursor?: CreatureWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Creatures from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Creatures.
     */
    skip?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/distinct Distinct Docs}
     * 
     * Filter by unique combinations of Creatures.
     */
    distinct?: CreatureScalarFieldEnum | CreatureScalarFieldEnum[]
  }


  /**
   * Creature findMany
   */
  export type CreatureFindManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * Filter, which Creatures to fetch.
     */
    where?: CreatureWhereInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/sorting Sorting Docs}
     * 
     * Determine the order of Creatures to fetch.
     */
    orderBy?: CreatureOrderByWithRelationInput | CreatureOrderByWithRelationInput[]
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination#cursor-based-pagination Cursor Docs}
     * 
     * Sets the position for listing Creatures.
     */
    cursor?: CreatureWhereUniqueInput
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Take `±n` Creatures from the position of the cursor.
     */
    take?: number
    /**
     * {@link https://www.prisma.io/docs/concepts/components/prisma-client/pagination Pagination Docs}
     * 
     * Skip the first `n` Creatures.
     */
    skip?: number
    distinct?: CreatureScalarFieldEnum | CreatureScalarFieldEnum[]
  }


  /**
   * Creature create
   */
  export type CreatureCreateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * The data needed to create a Creature.
     */
    data: XOR<CreatureCreateInput, CreatureUncheckedCreateInput>
  }


  /**
   * Creature createMany
   */
  export type CreatureCreateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to create many Creatures.
     */
    data: CreatureCreateManyInput | CreatureCreateManyInput[]
    skipDuplicates?: boolean
  }


  /**
   * Creature update
   */
  export type CreatureUpdateArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * The data needed to update a Creature.
     */
    data: XOR<CreatureUpdateInput, CreatureUncheckedUpdateInput>
    /**
     * Choose, which Creature to update.
     */
    where: CreatureWhereUniqueInput
  }


  /**
   * Creature updateMany
   */
  export type CreatureUpdateManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * The data used to update Creatures.
     */
    data: XOR<CreatureUpdateManyMutationInput, CreatureUncheckedUpdateManyInput>
    /**
     * Filter which Creatures to update
     */
    where?: CreatureWhereInput
  }


  /**
   * Creature upsert
   */
  export type CreatureUpsertArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * The filter to search for the Creature to update in case it exists.
     */
    where: CreatureWhereUniqueInput
    /**
     * In case the Creature found by the `where` argument doesn't exist, create a new Creature with this data.
     */
    create: XOR<CreatureCreateInput, CreatureUncheckedCreateInput>
    /**
     * In case the Creature was found with the provided `where` argument, update it with this data.
     */
    update: XOR<CreatureUpdateInput, CreatureUncheckedUpdateInput>
  }


  /**
   * Creature delete
   */
  export type CreatureDeleteArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
    /**
     * Filter which Creature to delete.
     */
    where: CreatureWhereUniqueInput
  }


  /**
   * Creature deleteMany
   */
  export type CreatureDeleteManyArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Filter which Creatures to delete
     */
    where?: CreatureWhereInput
  }


  /**
   * Creature without action
   */
  export type CreatureDefaultArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = {
    /**
     * Select specific fields to fetch from the Creature
     */
    select?: CreatureSelect<ExtArgs> | null
  }



  /**
   * Enums
   */

  export const TransactionIsolationLevel: {
    ReadUncommitted: 'ReadUncommitted',
    ReadCommitted: 'ReadCommitted',
    RepeatableRead: 'RepeatableRead',
    Serializable: 'Serializable'
  };

  export type TransactionIsolationLevel = (typeof TransactionIsolationLevel)[keyof typeof TransactionIsolationLevel]


  export const UserScalarFieldEnum: {
    id: 'id',
    login: 'login',
    displayName: 'displayName',
    discord: 'discord',
    telegram: 'telegram',
    gameRank: 'gameRank',
    email: 'email',
    isEmailConfirmed: 'isEmailConfirmed',
    password: 'password',
    icon: 'icon',
    role: 'role',
    refreshToken: 'refreshToken',
    isDisabled: 'isDisabled',
    kills: 'kills',
    deaths: 'deaths',
    assists: 'assists',
    tournamentWins: 'tournamentWins',
    gameWins: 'gameWins',
    gameLosed: 'gameLosed',
    teamID: 'teamID'
  };

  export type UserScalarFieldEnum = (typeof UserScalarFieldEnum)[keyof typeof UserScalarFieldEnum]


  export const RegistrationTokenScalarFieldEnum: {
    id: 'id',
    token: 'token'
  };

  export type RegistrationTokenScalarFieldEnum = (typeof RegistrationTokenScalarFieldEnum)[keyof typeof RegistrationTokenScalarFieldEnum]


  export const ChampionScalarFieldEnum: {
    id: 'id',
    name: 'name',
    role: 'role',
    tier: 'tier',
    counterPicks: 'counterPicks'
  };

  export type ChampionScalarFieldEnum = (typeof ChampionScalarFieldEnum)[keyof typeof ChampionScalarFieldEnum]


  export const BuildScalarFieldEnum: {
    id: 'id',
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    name: 'name',
    role: 'role',
    championID: 'championID',
    creatorID: 'creatorID',
    items: 'items',
    coreItems: 'coreItems',
    situationalItems: 'situationalItems',
    skillsOrder: 'skillsOrder',
    karmas: 'karmas',
    keyInsights: 'keyInsights',
    strengths: 'strengths',
    weaknesses: 'weaknesses'
  };

  export type BuildScalarFieldEnum = (typeof BuildScalarFieldEnum)[keyof typeof BuildScalarFieldEnum]


  export const LikesScalarFieldEnum: {
    userID: 'userID',
    buildID: 'buildID',
    assignedAt: 'assignedAt'
  };

  export type LikesScalarFieldEnum = (typeof LikesScalarFieldEnum)[keyof typeof LikesScalarFieldEnum]


  export const DisLikesScalarFieldEnum: {
    userID: 'userID',
    buildID: 'buildID',
    assignedAt: 'assignedAt'
  };

  export type DisLikesScalarFieldEnum = (typeof DisLikesScalarFieldEnum)[keyof typeof DisLikesScalarFieldEnum]


  export const TournamentScalarFieldEnum: {
    id: 'id',
    createdAt: 'createdAt',
    updatedAt: 'updatedAt',
    startedAt: 'startedAt',
    finishedAt: 'finishedAt',
    name: 'name',
    description: 'description',
    prize: 'prize',
    icon: 'icon',
    map: 'map',
    pickMode: 'pickMode',
    gameMode: 'gameMode',
    status: 'status',
    teamSize: 'teamSize',
    slots: 'slots',
    teamRequest: 'teamRequest',
    maxRequests: 'maxRequests',
    casters: 'casters',
    videoLink: 'videoLink',
    creatorID: 'creatorID',
    teams: 'teams',
    requests: 'requests',
    bracket: 'bracket',
    bracketType: 'bracketType',
    matchesCount: 'matchesCount',
    results: 'results'
  };

  export type TournamentScalarFieldEnum = (typeof TournamentScalarFieldEnum)[keyof typeof TournamentScalarFieldEnum]


  export const ItemScalarFieldEnum: {
    id: 'id',
    name: 'name',
    displayName: 'displayName',
    type: 'type',
    price: 'price',
    params: 'params',
    effect: 'effect',
    effectParams: 'effectParams'
  };

  export type ItemScalarFieldEnum = (typeof ItemScalarFieldEnum)[keyof typeof ItemScalarFieldEnum]


  export const LogScalarFieldEnum: {
    id: 'id',
    createdAt: 'createdAt',
    type: 'type',
    message: 'message'
  };

  export type LogScalarFieldEnum = (typeof LogScalarFieldEnum)[keyof typeof LogScalarFieldEnum]


  export const TeamScalarFieldEnum: {
    id: 'id',
    createdAt: 'createdAt',
    name: 'name',
    description: 'description',
    icon: 'icon',
    rating: 'rating',
    tournamentWins: 'tournamentWins',
    gameWins: 'gameWins',
    gameLosed: 'gameLosed',
    ownerID: 'ownerID',
    requests: 'requests'
  };

  export type TeamScalarFieldEnum = (typeof TeamScalarFieldEnum)[keyof typeof TeamScalarFieldEnum]


  export const NewsScalarFieldEnum: {
    id: 'id',
    createdAt: 'createdAt',
    name: 'name',
    description: 'description',
    icon: 'icon',
    patch: 'patch',
    champions: 'champions',
    items: 'items',
    bugfixes: 'bugfixes'
  };

  export type NewsScalarFieldEnum = (typeof NewsScalarFieldEnum)[keyof typeof NewsScalarFieldEnum]


  export const CreatureScalarFieldEnum: {
    id: 'id',
    createdAt: 'createdAt',
    name: 'name',
    description: 'description',
    icon: 'icon',
    type: 'type',
    health: 'health',
    gold: 'gold',
    exp: 'exp',
    spawnTime: 'spawnTime',
    respawnTime: 'respawnTime',
    buff: 'buff'
  };

  export type CreatureScalarFieldEnum = (typeof CreatureScalarFieldEnum)[keyof typeof CreatureScalarFieldEnum]


  export const SortOrder: {
    asc: 'asc',
    desc: 'desc'
  };

  export type SortOrder = (typeof SortOrder)[keyof typeof SortOrder]


  export const QueryMode: {
    default: 'default',
    insensitive: 'insensitive'
  };

  export type QueryMode = (typeof QueryMode)[keyof typeof QueryMode]


  export const NullsOrder: {
    first: 'first',
    last: 'last'
  };

  export type NullsOrder = (typeof NullsOrder)[keyof typeof NullsOrder]


  /**
   * Field references 
   */


  /**
   * Reference to a field of type 'Int'
   */
  export type IntFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Int'>
    


  /**
   * Reference to a field of type 'Int[]'
   */
  export type ListIntFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Int[]'>
    


  /**
   * Reference to a field of type 'String'
   */
  export type StringFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'String'>
    


  /**
   * Reference to a field of type 'String[]'
   */
  export type ListStringFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'String[]'>
    


  /**
   * Reference to a field of type 'Boolean'
   */
  export type BooleanFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Boolean'>
    


  /**
   * Reference to a field of type 'Role'
   */
  export type EnumRoleFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Role'>
    


  /**
   * Reference to a field of type 'Role[]'
   */
  export type ListEnumRoleFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Role[]'>
    


  /**
   * Reference to a field of type 'DateTime'
   */
  export type DateTimeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'DateTime'>
    


  /**
   * Reference to a field of type 'DateTime[]'
   */
  export type ListDateTimeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'DateTime[]'>
    


  /**
   * Reference to a field of type 'TournamentPickMode'
   */
  export type EnumTournamentPickModeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'TournamentPickMode'>
    


  /**
   * Reference to a field of type 'TournamentPickMode[]'
   */
  export type ListEnumTournamentPickModeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'TournamentPickMode[]'>
    


  /**
   * Reference to a field of type 'GameMode'
   */
  export type EnumGameModeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'GameMode'>
    


  /**
   * Reference to a field of type 'GameMode[]'
   */
  export type ListEnumGameModeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'GameMode[]'>
    


  /**
   * Reference to a field of type 'TournamentStatus'
   */
  export type EnumTournamentStatusFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'TournamentStatus'>
    


  /**
   * Reference to a field of type 'TournamentStatus[]'
   */
  export type ListEnumTournamentStatusFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'TournamentStatus[]'>
    


  /**
   * Reference to a field of type 'BracketType'
   */
  export type EnumBracketTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'BracketType'>
    


  /**
   * Reference to a field of type 'BracketType[]'
   */
  export type ListEnumBracketTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'BracketType[]'>
    


  /**
   * Reference to a field of type 'ItemType'
   */
  export type EnumItemTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'ItemType'>
    


  /**
   * Reference to a field of type 'ItemType[]'
   */
  export type ListEnumItemTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'ItemType[]'>
    


  /**
   * Reference to a field of type 'LogType'
   */
  export type EnumLogTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'LogType'>
    


  /**
   * Reference to a field of type 'LogType[]'
   */
  export type ListEnumLogTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'LogType[]'>
    


  /**
   * Reference to a field of type 'Decimal'
   */
  export type DecimalFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Decimal'>
    


  /**
   * Reference to a field of type 'Decimal[]'
   */
  export type ListDecimalFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Decimal[]'>
    


  /**
   * Reference to a field of type 'CreatureType'
   */
  export type EnumCreatureTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'CreatureType'>
    


  /**
   * Reference to a field of type 'CreatureType[]'
   */
  export type ListEnumCreatureTypeFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'CreatureType[]'>
    


  /**
   * Reference to a field of type 'Float'
   */
  export type FloatFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Float'>
    


  /**
   * Reference to a field of type 'Float[]'
   */
  export type ListFloatFieldRefInput<$PrismaModel> = FieldRefInputType<$PrismaModel, 'Float[]'>
    
  /**
   * Deep Input Types
   */


  export type UserWhereInput = {
    AND?: UserWhereInput | UserWhereInput[]
    OR?: UserWhereInput[]
    NOT?: UserWhereInput | UserWhereInput[]
    id?: IntFilter<"User"> | number
    login?: StringFilter<"User"> | string
    displayName?: StringFilter<"User"> | string
    discord?: StringFilter<"User"> | string
    telegram?: StringFilter<"User"> | string
    gameRank?: IntFilter<"User"> | number
    email?: StringFilter<"User"> | string
    isEmailConfirmed?: BoolFilter<"User"> | boolean
    password?: StringFilter<"User"> | string
    icon?: StringFilter<"User"> | string
    role?: StringFilter<"User"> | string
    refreshToken?: StringFilter<"User"> | string
    isDisabled?: BoolFilter<"User"> | boolean
    kills?: IntFilter<"User"> | number
    deaths?: IntFilter<"User"> | number
    assists?: IntFilter<"User"> | number
    tournamentWins?: IntNullableListFilter<"User">
    gameWins?: IntFilter<"User"> | number
    gameLosed?: IntFilter<"User"> | number
    teamID?: IntNullableFilter<"User"> | number | null
    team?: XOR<TeamNullableRelationFilter, TeamWhereInput> | null
    ownedTeam?: XOR<TeamNullableRelationFilter, TeamWhereInput> | null
    builds?: BuildListRelationFilter
    tournaments?: TournamentListRelationFilter
    likesBuilds?: LikesListRelationFilter
    dislikesBuilds?: DisLikesListRelationFilter
  }

  export type UserOrderByWithRelationInput = {
    id?: SortOrder
    login?: SortOrder
    displayName?: SortOrder
    discord?: SortOrder
    telegram?: SortOrder
    gameRank?: SortOrder
    email?: SortOrder
    isEmailConfirmed?: SortOrder
    password?: SortOrder
    icon?: SortOrder
    role?: SortOrder
    refreshToken?: SortOrder
    isDisabled?: SortOrder
    kills?: SortOrder
    deaths?: SortOrder
    assists?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    teamID?: SortOrderInput | SortOrder
    team?: TeamOrderByWithRelationInput
    ownedTeam?: TeamOrderByWithRelationInput
    builds?: BuildOrderByRelationAggregateInput
    tournaments?: TournamentOrderByRelationAggregateInput
    likesBuilds?: LikesOrderByRelationAggregateInput
    dislikesBuilds?: DisLikesOrderByRelationAggregateInput
  }

  export type UserWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    login?: string
    email?: string
    AND?: UserWhereInput | UserWhereInput[]
    OR?: UserWhereInput[]
    NOT?: UserWhereInput | UserWhereInput[]
    displayName?: StringFilter<"User"> | string
    discord?: StringFilter<"User"> | string
    telegram?: StringFilter<"User"> | string
    gameRank?: IntFilter<"User"> | number
    isEmailConfirmed?: BoolFilter<"User"> | boolean
    password?: StringFilter<"User"> | string
    icon?: StringFilter<"User"> | string
    role?: StringFilter<"User"> | string
    refreshToken?: StringFilter<"User"> | string
    isDisabled?: BoolFilter<"User"> | boolean
    kills?: IntFilter<"User"> | number
    deaths?: IntFilter<"User"> | number
    assists?: IntFilter<"User"> | number
    tournamentWins?: IntNullableListFilter<"User">
    gameWins?: IntFilter<"User"> | number
    gameLosed?: IntFilter<"User"> | number
    teamID?: IntNullableFilter<"User"> | number | null
    team?: XOR<TeamNullableRelationFilter, TeamWhereInput> | null
    ownedTeam?: XOR<TeamNullableRelationFilter, TeamWhereInput> | null
    builds?: BuildListRelationFilter
    tournaments?: TournamentListRelationFilter
    likesBuilds?: LikesListRelationFilter
    dislikesBuilds?: DisLikesListRelationFilter
  }, "id" | "login" | "email">

  export type UserOrderByWithAggregationInput = {
    id?: SortOrder
    login?: SortOrder
    displayName?: SortOrder
    discord?: SortOrder
    telegram?: SortOrder
    gameRank?: SortOrder
    email?: SortOrder
    isEmailConfirmed?: SortOrder
    password?: SortOrder
    icon?: SortOrder
    role?: SortOrder
    refreshToken?: SortOrder
    isDisabled?: SortOrder
    kills?: SortOrder
    deaths?: SortOrder
    assists?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    teamID?: SortOrderInput | SortOrder
    _count?: UserCountOrderByAggregateInput
    _avg?: UserAvgOrderByAggregateInput
    _max?: UserMaxOrderByAggregateInput
    _min?: UserMinOrderByAggregateInput
    _sum?: UserSumOrderByAggregateInput
  }

  export type UserScalarWhereWithAggregatesInput = {
    AND?: UserScalarWhereWithAggregatesInput | UserScalarWhereWithAggregatesInput[]
    OR?: UserScalarWhereWithAggregatesInput[]
    NOT?: UserScalarWhereWithAggregatesInput | UserScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"User"> | number
    login?: StringWithAggregatesFilter<"User"> | string
    displayName?: StringWithAggregatesFilter<"User"> | string
    discord?: StringWithAggregatesFilter<"User"> | string
    telegram?: StringWithAggregatesFilter<"User"> | string
    gameRank?: IntWithAggregatesFilter<"User"> | number
    email?: StringWithAggregatesFilter<"User"> | string
    isEmailConfirmed?: BoolWithAggregatesFilter<"User"> | boolean
    password?: StringWithAggregatesFilter<"User"> | string
    icon?: StringWithAggregatesFilter<"User"> | string
    role?: StringWithAggregatesFilter<"User"> | string
    refreshToken?: StringWithAggregatesFilter<"User"> | string
    isDisabled?: BoolWithAggregatesFilter<"User"> | boolean
    kills?: IntWithAggregatesFilter<"User"> | number
    deaths?: IntWithAggregatesFilter<"User"> | number
    assists?: IntWithAggregatesFilter<"User"> | number
    tournamentWins?: IntNullableListFilter<"User">
    gameWins?: IntWithAggregatesFilter<"User"> | number
    gameLosed?: IntWithAggregatesFilter<"User"> | number
    teamID?: IntNullableWithAggregatesFilter<"User"> | number | null
  }

  export type RegistrationTokenWhereInput = {
    AND?: RegistrationTokenWhereInput | RegistrationTokenWhereInput[]
    OR?: RegistrationTokenWhereInput[]
    NOT?: RegistrationTokenWhereInput | RegistrationTokenWhereInput[]
    id?: IntFilter<"RegistrationToken"> | number
    token?: StringFilter<"RegistrationToken"> | string
  }

  export type RegistrationTokenOrderByWithRelationInput = {
    id?: SortOrder
    token?: SortOrder
  }

  export type RegistrationTokenWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: RegistrationTokenWhereInput | RegistrationTokenWhereInput[]
    OR?: RegistrationTokenWhereInput[]
    NOT?: RegistrationTokenWhereInput | RegistrationTokenWhereInput[]
    token?: StringFilter<"RegistrationToken"> | string
  }, "id">

  export type RegistrationTokenOrderByWithAggregationInput = {
    id?: SortOrder
    token?: SortOrder
    _count?: RegistrationTokenCountOrderByAggregateInput
    _avg?: RegistrationTokenAvgOrderByAggregateInput
    _max?: RegistrationTokenMaxOrderByAggregateInput
    _min?: RegistrationTokenMinOrderByAggregateInput
    _sum?: RegistrationTokenSumOrderByAggregateInput
  }

  export type RegistrationTokenScalarWhereWithAggregatesInput = {
    AND?: RegistrationTokenScalarWhereWithAggregatesInput | RegistrationTokenScalarWhereWithAggregatesInput[]
    OR?: RegistrationTokenScalarWhereWithAggregatesInput[]
    NOT?: RegistrationTokenScalarWhereWithAggregatesInput | RegistrationTokenScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"RegistrationToken"> | number
    token?: StringWithAggregatesFilter<"RegistrationToken"> | string
  }

  export type ChampionWhereInput = {
    AND?: ChampionWhereInput | ChampionWhereInput[]
    OR?: ChampionWhereInput[]
    NOT?: ChampionWhereInput | ChampionWhereInput[]
    id?: IntFilter<"Champion"> | number
    name?: StringFilter<"Champion"> | string
    role?: EnumRoleFilter<"Champion"> | $Enums.Role
    tier?: IntFilter<"Champion"> | number
    counterPicks?: StringNullableListFilter<"Champion">
    builds?: BuildListRelationFilter
  }

  export type ChampionOrderByWithRelationInput = {
    id?: SortOrder
    name?: SortOrder
    role?: SortOrder
    tier?: SortOrder
    counterPicks?: SortOrder
    builds?: BuildOrderByRelationAggregateInput
  }

  export type ChampionWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: ChampionWhereInput | ChampionWhereInput[]
    OR?: ChampionWhereInput[]
    NOT?: ChampionWhereInput | ChampionWhereInput[]
    name?: StringFilter<"Champion"> | string
    role?: EnumRoleFilter<"Champion"> | $Enums.Role
    tier?: IntFilter<"Champion"> | number
    counterPicks?: StringNullableListFilter<"Champion">
    builds?: BuildListRelationFilter
  }, "id">

  export type ChampionOrderByWithAggregationInput = {
    id?: SortOrder
    name?: SortOrder
    role?: SortOrder
    tier?: SortOrder
    counterPicks?: SortOrder
    _count?: ChampionCountOrderByAggregateInput
    _avg?: ChampionAvgOrderByAggregateInput
    _max?: ChampionMaxOrderByAggregateInput
    _min?: ChampionMinOrderByAggregateInput
    _sum?: ChampionSumOrderByAggregateInput
  }

  export type ChampionScalarWhereWithAggregatesInput = {
    AND?: ChampionScalarWhereWithAggregatesInput | ChampionScalarWhereWithAggregatesInput[]
    OR?: ChampionScalarWhereWithAggregatesInput[]
    NOT?: ChampionScalarWhereWithAggregatesInput | ChampionScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"Champion"> | number
    name?: StringWithAggregatesFilter<"Champion"> | string
    role?: EnumRoleWithAggregatesFilter<"Champion"> | $Enums.Role
    tier?: IntWithAggregatesFilter<"Champion"> | number
    counterPicks?: StringNullableListFilter<"Champion">
  }

  export type BuildWhereInput = {
    AND?: BuildWhereInput | BuildWhereInput[]
    OR?: BuildWhereInput[]
    NOT?: BuildWhereInput | BuildWhereInput[]
    id?: IntFilter<"Build"> | number
    createdAt?: DateTimeFilter<"Build"> | Date | string
    updatedAt?: DateTimeFilter<"Build"> | Date | string
    name?: StringFilter<"Build"> | string
    role?: EnumRoleFilter<"Build"> | $Enums.Role
    championID?: IntFilter<"Build"> | number
    creatorID?: IntFilter<"Build"> | number
    items?: StringNullableListFilter<"Build">
    coreItems?: StringNullableListFilter<"Build">
    situationalItems?: StringNullableListFilter<"Build">
    skillsOrder?: StringNullableListFilter<"Build">
    karmas?: StringNullableListFilter<"Build">
    keyInsights?: StringNullableFilter<"Build"> | string | null
    strengths?: StringNullableFilter<"Build"> | string | null
    weaknesses?: StringNullableFilter<"Build"> | string | null
    champion?: XOR<ChampionRelationFilter, ChampionWhereInput>
    creator?: XOR<UserRelationFilter, UserWhereInput>
    Likes?: LikesListRelationFilter
    DisLikes?: DisLikesListRelationFilter
  }

  export type BuildOrderByWithRelationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    name?: SortOrder
    role?: SortOrder
    championID?: SortOrder
    creatorID?: SortOrder
    items?: SortOrder
    coreItems?: SortOrder
    situationalItems?: SortOrder
    skillsOrder?: SortOrder
    karmas?: SortOrder
    keyInsights?: SortOrderInput | SortOrder
    strengths?: SortOrderInput | SortOrder
    weaknesses?: SortOrderInput | SortOrder
    champion?: ChampionOrderByWithRelationInput
    creator?: UserOrderByWithRelationInput
    Likes?: LikesOrderByRelationAggregateInput
    DisLikes?: DisLikesOrderByRelationAggregateInput
  }

  export type BuildWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: BuildWhereInput | BuildWhereInput[]
    OR?: BuildWhereInput[]
    NOT?: BuildWhereInput | BuildWhereInput[]
    createdAt?: DateTimeFilter<"Build"> | Date | string
    updatedAt?: DateTimeFilter<"Build"> | Date | string
    name?: StringFilter<"Build"> | string
    role?: EnumRoleFilter<"Build"> | $Enums.Role
    championID?: IntFilter<"Build"> | number
    creatorID?: IntFilter<"Build"> | number
    items?: StringNullableListFilter<"Build">
    coreItems?: StringNullableListFilter<"Build">
    situationalItems?: StringNullableListFilter<"Build">
    skillsOrder?: StringNullableListFilter<"Build">
    karmas?: StringNullableListFilter<"Build">
    keyInsights?: StringNullableFilter<"Build"> | string | null
    strengths?: StringNullableFilter<"Build"> | string | null
    weaknesses?: StringNullableFilter<"Build"> | string | null
    champion?: XOR<ChampionRelationFilter, ChampionWhereInput>
    creator?: XOR<UserRelationFilter, UserWhereInput>
    Likes?: LikesListRelationFilter
    DisLikes?: DisLikesListRelationFilter
  }, "id">

  export type BuildOrderByWithAggregationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    name?: SortOrder
    role?: SortOrder
    championID?: SortOrder
    creatorID?: SortOrder
    items?: SortOrder
    coreItems?: SortOrder
    situationalItems?: SortOrder
    skillsOrder?: SortOrder
    karmas?: SortOrder
    keyInsights?: SortOrderInput | SortOrder
    strengths?: SortOrderInput | SortOrder
    weaknesses?: SortOrderInput | SortOrder
    _count?: BuildCountOrderByAggregateInput
    _avg?: BuildAvgOrderByAggregateInput
    _max?: BuildMaxOrderByAggregateInput
    _min?: BuildMinOrderByAggregateInput
    _sum?: BuildSumOrderByAggregateInput
  }

  export type BuildScalarWhereWithAggregatesInput = {
    AND?: BuildScalarWhereWithAggregatesInput | BuildScalarWhereWithAggregatesInput[]
    OR?: BuildScalarWhereWithAggregatesInput[]
    NOT?: BuildScalarWhereWithAggregatesInput | BuildScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"Build"> | number
    createdAt?: DateTimeWithAggregatesFilter<"Build"> | Date | string
    updatedAt?: DateTimeWithAggregatesFilter<"Build"> | Date | string
    name?: StringWithAggregatesFilter<"Build"> | string
    role?: EnumRoleWithAggregatesFilter<"Build"> | $Enums.Role
    championID?: IntWithAggregatesFilter<"Build"> | number
    creatorID?: IntWithAggregatesFilter<"Build"> | number
    items?: StringNullableListFilter<"Build">
    coreItems?: StringNullableListFilter<"Build">
    situationalItems?: StringNullableListFilter<"Build">
    skillsOrder?: StringNullableListFilter<"Build">
    karmas?: StringNullableListFilter<"Build">
    keyInsights?: StringNullableWithAggregatesFilter<"Build"> | string | null
    strengths?: StringNullableWithAggregatesFilter<"Build"> | string | null
    weaknesses?: StringNullableWithAggregatesFilter<"Build"> | string | null
  }

  export type LikesWhereInput = {
    AND?: LikesWhereInput | LikesWhereInput[]
    OR?: LikesWhereInput[]
    NOT?: LikesWhereInput | LikesWhereInput[]
    userID?: IntFilter<"Likes"> | number
    buildID?: IntFilter<"Likes"> | number
    assignedAt?: DateTimeFilter<"Likes"> | Date | string
    user?: XOR<UserRelationFilter, UserWhereInput>
    build?: XOR<BuildRelationFilter, BuildWhereInput>
  }

  export type LikesOrderByWithRelationInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
    user?: UserOrderByWithRelationInput
    build?: BuildOrderByWithRelationInput
  }

  export type LikesWhereUniqueInput = Prisma.AtLeast<{
    userID_buildID?: LikesUserIDBuildIDCompoundUniqueInput
    AND?: LikesWhereInput | LikesWhereInput[]
    OR?: LikesWhereInput[]
    NOT?: LikesWhereInput | LikesWhereInput[]
    userID?: IntFilter<"Likes"> | number
    buildID?: IntFilter<"Likes"> | number
    assignedAt?: DateTimeFilter<"Likes"> | Date | string
    user?: XOR<UserRelationFilter, UserWhereInput>
    build?: XOR<BuildRelationFilter, BuildWhereInput>
  }, "userID_buildID">

  export type LikesOrderByWithAggregationInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
    _count?: LikesCountOrderByAggregateInput
    _avg?: LikesAvgOrderByAggregateInput
    _max?: LikesMaxOrderByAggregateInput
    _min?: LikesMinOrderByAggregateInput
    _sum?: LikesSumOrderByAggregateInput
  }

  export type LikesScalarWhereWithAggregatesInput = {
    AND?: LikesScalarWhereWithAggregatesInput | LikesScalarWhereWithAggregatesInput[]
    OR?: LikesScalarWhereWithAggregatesInput[]
    NOT?: LikesScalarWhereWithAggregatesInput | LikesScalarWhereWithAggregatesInput[]
    userID?: IntWithAggregatesFilter<"Likes"> | number
    buildID?: IntWithAggregatesFilter<"Likes"> | number
    assignedAt?: DateTimeWithAggregatesFilter<"Likes"> | Date | string
  }

  export type DisLikesWhereInput = {
    AND?: DisLikesWhereInput | DisLikesWhereInput[]
    OR?: DisLikesWhereInput[]
    NOT?: DisLikesWhereInput | DisLikesWhereInput[]
    userID?: IntFilter<"DisLikes"> | number
    buildID?: IntFilter<"DisLikes"> | number
    assignedAt?: DateTimeFilter<"DisLikes"> | Date | string
    user?: XOR<UserRelationFilter, UserWhereInput>
    build?: XOR<BuildRelationFilter, BuildWhereInput>
  }

  export type DisLikesOrderByWithRelationInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
    user?: UserOrderByWithRelationInput
    build?: BuildOrderByWithRelationInput
  }

  export type DisLikesWhereUniqueInput = Prisma.AtLeast<{
    userID_buildID?: DisLikesUserIDBuildIDCompoundUniqueInput
    AND?: DisLikesWhereInput | DisLikesWhereInput[]
    OR?: DisLikesWhereInput[]
    NOT?: DisLikesWhereInput | DisLikesWhereInput[]
    userID?: IntFilter<"DisLikes"> | number
    buildID?: IntFilter<"DisLikes"> | number
    assignedAt?: DateTimeFilter<"DisLikes"> | Date | string
    user?: XOR<UserRelationFilter, UserWhereInput>
    build?: XOR<BuildRelationFilter, BuildWhereInput>
  }, "userID_buildID">

  export type DisLikesOrderByWithAggregationInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
    _count?: DisLikesCountOrderByAggregateInput
    _avg?: DisLikesAvgOrderByAggregateInput
    _max?: DisLikesMaxOrderByAggregateInput
    _min?: DisLikesMinOrderByAggregateInput
    _sum?: DisLikesSumOrderByAggregateInput
  }

  export type DisLikesScalarWhereWithAggregatesInput = {
    AND?: DisLikesScalarWhereWithAggregatesInput | DisLikesScalarWhereWithAggregatesInput[]
    OR?: DisLikesScalarWhereWithAggregatesInput[]
    NOT?: DisLikesScalarWhereWithAggregatesInput | DisLikesScalarWhereWithAggregatesInput[]
    userID?: IntWithAggregatesFilter<"DisLikes"> | number
    buildID?: IntWithAggregatesFilter<"DisLikes"> | number
    assignedAt?: DateTimeWithAggregatesFilter<"DisLikes"> | Date | string
  }

  export type TournamentWhereInput = {
    AND?: TournamentWhereInput | TournamentWhereInput[]
    OR?: TournamentWhereInput[]
    NOT?: TournamentWhereInput | TournamentWhereInput[]
    id?: IntFilter<"Tournament"> | number
    createdAt?: DateTimeFilter<"Tournament"> | Date | string
    updatedAt?: DateTimeFilter<"Tournament"> | Date | string
    startedAt?: DateTimeFilter<"Tournament"> | Date | string
    finishedAt?: DateTimeFilter<"Tournament"> | Date | string
    name?: StringFilter<"Tournament"> | string
    description?: StringFilter<"Tournament"> | string
    prize?: StringFilter<"Tournament"> | string
    icon?: StringNullableFilter<"Tournament"> | string | null
    map?: StringNullableFilter<"Tournament"> | string | null
    pickMode?: EnumTournamentPickModeFilter<"Tournament"> | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFilter<"Tournament"> | $Enums.GameMode
    status?: EnumTournamentStatusFilter<"Tournament"> | $Enums.TournamentStatus
    teamSize?: IntFilter<"Tournament"> | number
    slots?: IntFilter<"Tournament"> | number
    teamRequest?: BoolFilter<"Tournament"> | boolean
    maxRequests?: IntFilter<"Tournament"> | number
    casters?: StringNullableListFilter<"Tournament">
    videoLink?: StringNullableFilter<"Tournament"> | string | null
    creatorID?: IntFilter<"Tournament"> | number
    teams?: StringFilter<"Tournament"> | string
    requests?: StringNullableListFilter<"Tournament">
    bracket?: StringNullableFilter<"Tournament"> | string | null
    bracketType?: EnumBracketTypeFilter<"Tournament"> | $Enums.BracketType
    matchesCount?: IntFilter<"Tournament"> | number
    results?: StringNullableFilter<"Tournament"> | string | null
    creator?: XOR<UserRelationFilter, UserWhereInput>
  }

  export type TournamentOrderByWithRelationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    startedAt?: SortOrder
    finishedAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    prize?: SortOrder
    icon?: SortOrderInput | SortOrder
    map?: SortOrderInput | SortOrder
    pickMode?: SortOrder
    gameMode?: SortOrder
    status?: SortOrder
    teamSize?: SortOrder
    slots?: SortOrder
    teamRequest?: SortOrder
    maxRequests?: SortOrder
    casters?: SortOrder
    videoLink?: SortOrderInput | SortOrder
    creatorID?: SortOrder
    teams?: SortOrder
    requests?: SortOrder
    bracket?: SortOrderInput | SortOrder
    bracketType?: SortOrder
    matchesCount?: SortOrder
    results?: SortOrderInput | SortOrder
    creator?: UserOrderByWithRelationInput
  }

  export type TournamentWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: TournamentWhereInput | TournamentWhereInput[]
    OR?: TournamentWhereInput[]
    NOT?: TournamentWhereInput | TournamentWhereInput[]
    createdAt?: DateTimeFilter<"Tournament"> | Date | string
    updatedAt?: DateTimeFilter<"Tournament"> | Date | string
    startedAt?: DateTimeFilter<"Tournament"> | Date | string
    finishedAt?: DateTimeFilter<"Tournament"> | Date | string
    name?: StringFilter<"Tournament"> | string
    description?: StringFilter<"Tournament"> | string
    prize?: StringFilter<"Tournament"> | string
    icon?: StringNullableFilter<"Tournament"> | string | null
    map?: StringNullableFilter<"Tournament"> | string | null
    pickMode?: EnumTournamentPickModeFilter<"Tournament"> | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFilter<"Tournament"> | $Enums.GameMode
    status?: EnumTournamentStatusFilter<"Tournament"> | $Enums.TournamentStatus
    teamSize?: IntFilter<"Tournament"> | number
    slots?: IntFilter<"Tournament"> | number
    teamRequest?: BoolFilter<"Tournament"> | boolean
    maxRequests?: IntFilter<"Tournament"> | number
    casters?: StringNullableListFilter<"Tournament">
    videoLink?: StringNullableFilter<"Tournament"> | string | null
    creatorID?: IntFilter<"Tournament"> | number
    teams?: StringFilter<"Tournament"> | string
    requests?: StringNullableListFilter<"Tournament">
    bracket?: StringNullableFilter<"Tournament"> | string | null
    bracketType?: EnumBracketTypeFilter<"Tournament"> | $Enums.BracketType
    matchesCount?: IntFilter<"Tournament"> | number
    results?: StringNullableFilter<"Tournament"> | string | null
    creator?: XOR<UserRelationFilter, UserWhereInput>
  }, "id">

  export type TournamentOrderByWithAggregationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    startedAt?: SortOrder
    finishedAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    prize?: SortOrder
    icon?: SortOrderInput | SortOrder
    map?: SortOrderInput | SortOrder
    pickMode?: SortOrder
    gameMode?: SortOrder
    status?: SortOrder
    teamSize?: SortOrder
    slots?: SortOrder
    teamRequest?: SortOrder
    maxRequests?: SortOrder
    casters?: SortOrder
    videoLink?: SortOrderInput | SortOrder
    creatorID?: SortOrder
    teams?: SortOrder
    requests?: SortOrder
    bracket?: SortOrderInput | SortOrder
    bracketType?: SortOrder
    matchesCount?: SortOrder
    results?: SortOrderInput | SortOrder
    _count?: TournamentCountOrderByAggregateInput
    _avg?: TournamentAvgOrderByAggregateInput
    _max?: TournamentMaxOrderByAggregateInput
    _min?: TournamentMinOrderByAggregateInput
    _sum?: TournamentSumOrderByAggregateInput
  }

  export type TournamentScalarWhereWithAggregatesInput = {
    AND?: TournamentScalarWhereWithAggregatesInput | TournamentScalarWhereWithAggregatesInput[]
    OR?: TournamentScalarWhereWithAggregatesInput[]
    NOT?: TournamentScalarWhereWithAggregatesInput | TournamentScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"Tournament"> | number
    createdAt?: DateTimeWithAggregatesFilter<"Tournament"> | Date | string
    updatedAt?: DateTimeWithAggregatesFilter<"Tournament"> | Date | string
    startedAt?: DateTimeWithAggregatesFilter<"Tournament"> | Date | string
    finishedAt?: DateTimeWithAggregatesFilter<"Tournament"> | Date | string
    name?: StringWithAggregatesFilter<"Tournament"> | string
    description?: StringWithAggregatesFilter<"Tournament"> | string
    prize?: StringWithAggregatesFilter<"Tournament"> | string
    icon?: StringNullableWithAggregatesFilter<"Tournament"> | string | null
    map?: StringNullableWithAggregatesFilter<"Tournament"> | string | null
    pickMode?: EnumTournamentPickModeWithAggregatesFilter<"Tournament"> | $Enums.TournamentPickMode
    gameMode?: EnumGameModeWithAggregatesFilter<"Tournament"> | $Enums.GameMode
    status?: EnumTournamentStatusWithAggregatesFilter<"Tournament"> | $Enums.TournamentStatus
    teamSize?: IntWithAggregatesFilter<"Tournament"> | number
    slots?: IntWithAggregatesFilter<"Tournament"> | number
    teamRequest?: BoolWithAggregatesFilter<"Tournament"> | boolean
    maxRequests?: IntWithAggregatesFilter<"Tournament"> | number
    casters?: StringNullableListFilter<"Tournament">
    videoLink?: StringNullableWithAggregatesFilter<"Tournament"> | string | null
    creatorID?: IntWithAggregatesFilter<"Tournament"> | number
    teams?: StringWithAggregatesFilter<"Tournament"> | string
    requests?: StringNullableListFilter<"Tournament">
    bracket?: StringNullableWithAggregatesFilter<"Tournament"> | string | null
    bracketType?: EnumBracketTypeWithAggregatesFilter<"Tournament"> | $Enums.BracketType
    matchesCount?: IntWithAggregatesFilter<"Tournament"> | number
    results?: StringNullableWithAggregatesFilter<"Tournament"> | string | null
  }

  export type ItemWhereInput = {
    AND?: ItemWhereInput | ItemWhereInput[]
    OR?: ItemWhereInput[]
    NOT?: ItemWhereInput | ItemWhereInput[]
    id?: IntFilter<"Item"> | number
    name?: StringFilter<"Item"> | string
    displayName?: StringNullableFilter<"Item"> | string | null
    type?: EnumItemTypeFilter<"Item"> | $Enums.ItemType
    price?: IntFilter<"Item"> | number
    params?: StringFilter<"Item"> | string
    effect?: StringNullableFilter<"Item"> | string | null
    effectParams?: StringNullableFilter<"Item"> | string | null
  }

  export type ItemOrderByWithRelationInput = {
    id?: SortOrder
    name?: SortOrder
    displayName?: SortOrderInput | SortOrder
    type?: SortOrder
    price?: SortOrder
    params?: SortOrder
    effect?: SortOrderInput | SortOrder
    effectParams?: SortOrderInput | SortOrder
  }

  export type ItemWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: ItemWhereInput | ItemWhereInput[]
    OR?: ItemWhereInput[]
    NOT?: ItemWhereInput | ItemWhereInput[]
    name?: StringFilter<"Item"> | string
    displayName?: StringNullableFilter<"Item"> | string | null
    type?: EnumItemTypeFilter<"Item"> | $Enums.ItemType
    price?: IntFilter<"Item"> | number
    params?: StringFilter<"Item"> | string
    effect?: StringNullableFilter<"Item"> | string | null
    effectParams?: StringNullableFilter<"Item"> | string | null
  }, "id">

  export type ItemOrderByWithAggregationInput = {
    id?: SortOrder
    name?: SortOrder
    displayName?: SortOrderInput | SortOrder
    type?: SortOrder
    price?: SortOrder
    params?: SortOrder
    effect?: SortOrderInput | SortOrder
    effectParams?: SortOrderInput | SortOrder
    _count?: ItemCountOrderByAggregateInput
    _avg?: ItemAvgOrderByAggregateInput
    _max?: ItemMaxOrderByAggregateInput
    _min?: ItemMinOrderByAggregateInput
    _sum?: ItemSumOrderByAggregateInput
  }

  export type ItemScalarWhereWithAggregatesInput = {
    AND?: ItemScalarWhereWithAggregatesInput | ItemScalarWhereWithAggregatesInput[]
    OR?: ItemScalarWhereWithAggregatesInput[]
    NOT?: ItemScalarWhereWithAggregatesInput | ItemScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"Item"> | number
    name?: StringWithAggregatesFilter<"Item"> | string
    displayName?: StringNullableWithAggregatesFilter<"Item"> | string | null
    type?: EnumItemTypeWithAggregatesFilter<"Item"> | $Enums.ItemType
    price?: IntWithAggregatesFilter<"Item"> | number
    params?: StringWithAggregatesFilter<"Item"> | string
    effect?: StringNullableWithAggregatesFilter<"Item"> | string | null
    effectParams?: StringNullableWithAggregatesFilter<"Item"> | string | null
  }

  export type LogWhereInput = {
    AND?: LogWhereInput | LogWhereInput[]
    OR?: LogWhereInput[]
    NOT?: LogWhereInput | LogWhereInput[]
    id?: IntFilter<"Log"> | number
    createdAt?: DateTimeFilter<"Log"> | Date | string
    type?: EnumLogTypeFilter<"Log"> | $Enums.LogType
    message?: StringFilter<"Log"> | string
  }

  export type LogOrderByWithRelationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    type?: SortOrder
    message?: SortOrder
  }

  export type LogWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: LogWhereInput | LogWhereInput[]
    OR?: LogWhereInput[]
    NOT?: LogWhereInput | LogWhereInput[]
    createdAt?: DateTimeFilter<"Log"> | Date | string
    type?: EnumLogTypeFilter<"Log"> | $Enums.LogType
    message?: StringFilter<"Log"> | string
  }, "id">

  export type LogOrderByWithAggregationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    type?: SortOrder
    message?: SortOrder
    _count?: LogCountOrderByAggregateInput
    _avg?: LogAvgOrderByAggregateInput
    _max?: LogMaxOrderByAggregateInput
    _min?: LogMinOrderByAggregateInput
    _sum?: LogSumOrderByAggregateInput
  }

  export type LogScalarWhereWithAggregatesInput = {
    AND?: LogScalarWhereWithAggregatesInput | LogScalarWhereWithAggregatesInput[]
    OR?: LogScalarWhereWithAggregatesInput[]
    NOT?: LogScalarWhereWithAggregatesInput | LogScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"Log"> | number
    createdAt?: DateTimeWithAggregatesFilter<"Log"> | Date | string
    type?: EnumLogTypeWithAggregatesFilter<"Log"> | $Enums.LogType
    message?: StringWithAggregatesFilter<"Log"> | string
  }

  export type TeamWhereInput = {
    AND?: TeamWhereInput | TeamWhereInput[]
    OR?: TeamWhereInput[]
    NOT?: TeamWhereInput | TeamWhereInput[]
    id?: IntFilter<"Team"> | number
    createdAt?: DateTimeFilter<"Team"> | Date | string
    name?: StringFilter<"Team"> | string
    description?: StringFilter<"Team"> | string
    icon?: StringFilter<"Team"> | string
    rating?: DecimalFilter<"Team"> | Decimal | DecimalJsLike | number | string
    tournamentWins?: IntNullableListFilter<"Team">
    gameWins?: IntFilter<"Team"> | number
    gameLosed?: IntFilter<"Team"> | number
    ownerID?: IntFilter<"Team"> | number
    requests?: StringNullableListFilter<"Team">
    owner?: XOR<UserRelationFilter, UserWhereInput>
    members?: UserListRelationFilter
  }

  export type TeamOrderByWithRelationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    rating?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    ownerID?: SortOrder
    requests?: SortOrder
    owner?: UserOrderByWithRelationInput
    members?: UserOrderByRelationAggregateInput
  }

  export type TeamWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    ownerID?: number
    AND?: TeamWhereInput | TeamWhereInput[]
    OR?: TeamWhereInput[]
    NOT?: TeamWhereInput | TeamWhereInput[]
    createdAt?: DateTimeFilter<"Team"> | Date | string
    name?: StringFilter<"Team"> | string
    description?: StringFilter<"Team"> | string
    icon?: StringFilter<"Team"> | string
    rating?: DecimalFilter<"Team"> | Decimal | DecimalJsLike | number | string
    tournamentWins?: IntNullableListFilter<"Team">
    gameWins?: IntFilter<"Team"> | number
    gameLosed?: IntFilter<"Team"> | number
    requests?: StringNullableListFilter<"Team">
    owner?: XOR<UserRelationFilter, UserWhereInput>
    members?: UserListRelationFilter
  }, "id" | "ownerID">

  export type TeamOrderByWithAggregationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    rating?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    ownerID?: SortOrder
    requests?: SortOrder
    _count?: TeamCountOrderByAggregateInput
    _avg?: TeamAvgOrderByAggregateInput
    _max?: TeamMaxOrderByAggregateInput
    _min?: TeamMinOrderByAggregateInput
    _sum?: TeamSumOrderByAggregateInput
  }

  export type TeamScalarWhereWithAggregatesInput = {
    AND?: TeamScalarWhereWithAggregatesInput | TeamScalarWhereWithAggregatesInput[]
    OR?: TeamScalarWhereWithAggregatesInput[]
    NOT?: TeamScalarWhereWithAggregatesInput | TeamScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"Team"> | number
    createdAt?: DateTimeWithAggregatesFilter<"Team"> | Date | string
    name?: StringWithAggregatesFilter<"Team"> | string
    description?: StringWithAggregatesFilter<"Team"> | string
    icon?: StringWithAggregatesFilter<"Team"> | string
    rating?: DecimalWithAggregatesFilter<"Team"> | Decimal | DecimalJsLike | number | string
    tournamentWins?: IntNullableListFilter<"Team">
    gameWins?: IntWithAggregatesFilter<"Team"> | number
    gameLosed?: IntWithAggregatesFilter<"Team"> | number
    ownerID?: IntWithAggregatesFilter<"Team"> | number
    requests?: StringNullableListFilter<"Team">
  }

  export type NewsWhereInput = {
    AND?: NewsWhereInput | NewsWhereInput[]
    OR?: NewsWhereInput[]
    NOT?: NewsWhereInput | NewsWhereInput[]
    id?: IntFilter<"News"> | number
    createdAt?: DateTimeFilter<"News"> | Date | string
    name?: StringFilter<"News"> | string
    description?: StringFilter<"News"> | string
    icon?: StringFilter<"News"> | string
    patch?: StringFilter<"News"> | string
    champions?: StringNullableListFilter<"News">
    items?: StringNullableListFilter<"News">
    bugfixes?: StringFilter<"News"> | string
  }

  export type NewsOrderByWithRelationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    patch?: SortOrder
    champions?: SortOrder
    items?: SortOrder
    bugfixes?: SortOrder
  }

  export type NewsWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: NewsWhereInput | NewsWhereInput[]
    OR?: NewsWhereInput[]
    NOT?: NewsWhereInput | NewsWhereInput[]
    createdAt?: DateTimeFilter<"News"> | Date | string
    name?: StringFilter<"News"> | string
    description?: StringFilter<"News"> | string
    icon?: StringFilter<"News"> | string
    patch?: StringFilter<"News"> | string
    champions?: StringNullableListFilter<"News">
    items?: StringNullableListFilter<"News">
    bugfixes?: StringFilter<"News"> | string
  }, "id">

  export type NewsOrderByWithAggregationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    patch?: SortOrder
    champions?: SortOrder
    items?: SortOrder
    bugfixes?: SortOrder
    _count?: NewsCountOrderByAggregateInput
    _avg?: NewsAvgOrderByAggregateInput
    _max?: NewsMaxOrderByAggregateInput
    _min?: NewsMinOrderByAggregateInput
    _sum?: NewsSumOrderByAggregateInput
  }

  export type NewsScalarWhereWithAggregatesInput = {
    AND?: NewsScalarWhereWithAggregatesInput | NewsScalarWhereWithAggregatesInput[]
    OR?: NewsScalarWhereWithAggregatesInput[]
    NOT?: NewsScalarWhereWithAggregatesInput | NewsScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"News"> | number
    createdAt?: DateTimeWithAggregatesFilter<"News"> | Date | string
    name?: StringWithAggregatesFilter<"News"> | string
    description?: StringWithAggregatesFilter<"News"> | string
    icon?: StringWithAggregatesFilter<"News"> | string
    patch?: StringWithAggregatesFilter<"News"> | string
    champions?: StringNullableListFilter<"News">
    items?: StringNullableListFilter<"News">
    bugfixes?: StringWithAggregatesFilter<"News"> | string
  }

  export type CreatureWhereInput = {
    AND?: CreatureWhereInput | CreatureWhereInput[]
    OR?: CreatureWhereInput[]
    NOT?: CreatureWhereInput | CreatureWhereInput[]
    id?: IntFilter<"Creature"> | number
    createdAt?: DateTimeFilter<"Creature"> | Date | string
    name?: StringFilter<"Creature"> | string
    description?: StringFilter<"Creature"> | string
    icon?: StringFilter<"Creature"> | string
    type?: EnumCreatureTypeFilter<"Creature"> | $Enums.CreatureType
    health?: StringFilter<"Creature"> | string
    gold?: StringFilter<"Creature"> | string
    exp?: StringFilter<"Creature"> | string
    spawnTime?: StringFilter<"Creature"> | string
    respawnTime?: StringFilter<"Creature"> | string
    buff?: StringFilter<"Creature"> | string
  }

  export type CreatureOrderByWithRelationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    type?: SortOrder
    health?: SortOrder
    gold?: SortOrder
    exp?: SortOrder
    spawnTime?: SortOrder
    respawnTime?: SortOrder
    buff?: SortOrder
  }

  export type CreatureWhereUniqueInput = Prisma.AtLeast<{
    id?: number
    AND?: CreatureWhereInput | CreatureWhereInput[]
    OR?: CreatureWhereInput[]
    NOT?: CreatureWhereInput | CreatureWhereInput[]
    createdAt?: DateTimeFilter<"Creature"> | Date | string
    name?: StringFilter<"Creature"> | string
    description?: StringFilter<"Creature"> | string
    icon?: StringFilter<"Creature"> | string
    type?: EnumCreatureTypeFilter<"Creature"> | $Enums.CreatureType
    health?: StringFilter<"Creature"> | string
    gold?: StringFilter<"Creature"> | string
    exp?: StringFilter<"Creature"> | string
    spawnTime?: StringFilter<"Creature"> | string
    respawnTime?: StringFilter<"Creature"> | string
    buff?: StringFilter<"Creature"> | string
  }, "id">

  export type CreatureOrderByWithAggregationInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    type?: SortOrder
    health?: SortOrder
    gold?: SortOrder
    exp?: SortOrder
    spawnTime?: SortOrder
    respawnTime?: SortOrder
    buff?: SortOrder
    _count?: CreatureCountOrderByAggregateInput
    _avg?: CreatureAvgOrderByAggregateInput
    _max?: CreatureMaxOrderByAggregateInput
    _min?: CreatureMinOrderByAggregateInput
    _sum?: CreatureSumOrderByAggregateInput
  }

  export type CreatureScalarWhereWithAggregatesInput = {
    AND?: CreatureScalarWhereWithAggregatesInput | CreatureScalarWhereWithAggregatesInput[]
    OR?: CreatureScalarWhereWithAggregatesInput[]
    NOT?: CreatureScalarWhereWithAggregatesInput | CreatureScalarWhereWithAggregatesInput[]
    id?: IntWithAggregatesFilter<"Creature"> | number
    createdAt?: DateTimeWithAggregatesFilter<"Creature"> | Date | string
    name?: StringWithAggregatesFilter<"Creature"> | string
    description?: StringWithAggregatesFilter<"Creature"> | string
    icon?: StringWithAggregatesFilter<"Creature"> | string
    type?: EnumCreatureTypeWithAggregatesFilter<"Creature"> | $Enums.CreatureType
    health?: StringWithAggregatesFilter<"Creature"> | string
    gold?: StringWithAggregatesFilter<"Creature"> | string
    exp?: StringWithAggregatesFilter<"Creature"> | string
    spawnTime?: StringWithAggregatesFilter<"Creature"> | string
    respawnTime?: StringWithAggregatesFilter<"Creature"> | string
    buff?: StringWithAggregatesFilter<"Creature"> | string
  }

  export type UserCreateInput = {
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    team?: TeamCreateNestedOneWithoutMembersInput
    ownedTeam?: TeamCreateNestedOneWithoutOwnerInput
    builds?: BuildCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesCreateNestedManyWithoutUserInput
  }

  export type UserUncheckedCreateInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    teamID?: number | null
    ownedTeam?: TeamUncheckedCreateNestedOneWithoutOwnerInput
    builds?: BuildUncheckedCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentUncheckedCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesUncheckedCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesUncheckedCreateNestedManyWithoutUserInput
  }

  export type UserUpdateInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    team?: TeamUpdateOneWithoutMembersNestedInput
    ownedTeam?: TeamUpdateOneWithoutOwnerNestedInput
    builds?: BuildUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    teamID?: NullableIntFieldUpdateOperationsInput | number | null
    ownedTeam?: TeamUncheckedUpdateOneWithoutOwnerNestedInput
    builds?: BuildUncheckedUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUncheckedUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUncheckedUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUncheckedUpdateManyWithoutUserNestedInput
  }

  export type UserCreateManyInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    teamID?: number | null
  }

  export type UserUpdateManyMutationInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
  }

  export type UserUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    teamID?: NullableIntFieldUpdateOperationsInput | number | null
  }

  export type RegistrationTokenCreateInput = {
    token: string
  }

  export type RegistrationTokenUncheckedCreateInput = {
    id?: number
    token: string
  }

  export type RegistrationTokenUpdateInput = {
    token?: StringFieldUpdateOperationsInput | string
  }

  export type RegistrationTokenUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    token?: StringFieldUpdateOperationsInput | string
  }

  export type RegistrationTokenCreateManyInput = {
    id?: number
    token: string
  }

  export type RegistrationTokenUpdateManyMutationInput = {
    token?: StringFieldUpdateOperationsInput | string
  }

  export type RegistrationTokenUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    token?: StringFieldUpdateOperationsInput | string
  }

  export type ChampionCreateInput = {
    name: string
    role?: $Enums.Role
    tier?: number
    counterPicks?: ChampionCreatecounterPicksInput | string[]
    builds?: BuildCreateNestedManyWithoutChampionInput
  }

  export type ChampionUncheckedCreateInput = {
    id?: number
    name: string
    role?: $Enums.Role
    tier?: number
    counterPicks?: ChampionCreatecounterPicksInput | string[]
    builds?: BuildUncheckedCreateNestedManyWithoutChampionInput
  }

  export type ChampionUpdateInput = {
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    tier?: IntFieldUpdateOperationsInput | number
    counterPicks?: ChampionUpdatecounterPicksInput | string[]
    builds?: BuildUpdateManyWithoutChampionNestedInput
  }

  export type ChampionUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    tier?: IntFieldUpdateOperationsInput | number
    counterPicks?: ChampionUpdatecounterPicksInput | string[]
    builds?: BuildUncheckedUpdateManyWithoutChampionNestedInput
  }

  export type ChampionCreateManyInput = {
    id?: number
    name: string
    role?: $Enums.Role
    tier?: number
    counterPicks?: ChampionCreatecounterPicksInput | string[]
  }

  export type ChampionUpdateManyMutationInput = {
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    tier?: IntFieldUpdateOperationsInput | number
    counterPicks?: ChampionUpdatecounterPicksInput | string[]
  }

  export type ChampionUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    tier?: IntFieldUpdateOperationsInput | number
    counterPicks?: ChampionUpdatecounterPicksInput | string[]
  }

  export type BuildCreateInput = {
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    champion: ChampionCreateNestedOneWithoutBuildsInput
    creator: UserCreateNestedOneWithoutBuildsInput
    Likes?: LikesCreateNestedManyWithoutBuildInput
    DisLikes?: DisLikesCreateNestedManyWithoutBuildInput
  }

  export type BuildUncheckedCreateInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    championID: number
    creatorID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    Likes?: LikesUncheckedCreateNestedManyWithoutBuildInput
    DisLikes?: DisLikesUncheckedCreateNestedManyWithoutBuildInput
  }

  export type BuildUpdateInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    champion?: ChampionUpdateOneRequiredWithoutBuildsNestedInput
    creator?: UserUpdateOneRequiredWithoutBuildsNestedInput
    Likes?: LikesUpdateManyWithoutBuildNestedInput
    DisLikes?: DisLikesUpdateManyWithoutBuildNestedInput
  }

  export type BuildUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    championID?: IntFieldUpdateOperationsInput | number
    creatorID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    Likes?: LikesUncheckedUpdateManyWithoutBuildNestedInput
    DisLikes?: DisLikesUncheckedUpdateManyWithoutBuildNestedInput
  }

  export type BuildCreateManyInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    championID: number
    creatorID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
  }

  export type BuildUpdateManyMutationInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type BuildUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    championID?: IntFieldUpdateOperationsInput | number
    creatorID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type LikesCreateInput = {
    assignedAt?: Date | string
    user: UserCreateNestedOneWithoutLikesBuildsInput
    build: BuildCreateNestedOneWithoutLikesInput
  }

  export type LikesUncheckedCreateInput = {
    userID: number
    buildID: number
    assignedAt?: Date | string
  }

  export type LikesUpdateInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    user?: UserUpdateOneRequiredWithoutLikesBuildsNestedInput
    build?: BuildUpdateOneRequiredWithoutLikesNestedInput
  }

  export type LikesUncheckedUpdateInput = {
    userID?: IntFieldUpdateOperationsInput | number
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type LikesCreateManyInput = {
    userID: number
    buildID: number
    assignedAt?: Date | string
  }

  export type LikesUpdateManyMutationInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type LikesUncheckedUpdateManyInput = {
    userID?: IntFieldUpdateOperationsInput | number
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type DisLikesCreateInput = {
    assignedAt?: Date | string
    user: UserCreateNestedOneWithoutDislikesBuildsInput
    build: BuildCreateNestedOneWithoutDisLikesInput
  }

  export type DisLikesUncheckedCreateInput = {
    userID: number
    buildID: number
    assignedAt?: Date | string
  }

  export type DisLikesUpdateInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    user?: UserUpdateOneRequiredWithoutDislikesBuildsNestedInput
    build?: BuildUpdateOneRequiredWithoutDisLikesNestedInput
  }

  export type DisLikesUncheckedUpdateInput = {
    userID?: IntFieldUpdateOperationsInput | number
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type DisLikesCreateManyInput = {
    userID: number
    buildID: number
    assignedAt?: Date | string
  }

  export type DisLikesUpdateManyMutationInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type DisLikesUncheckedUpdateManyInput = {
    userID?: IntFieldUpdateOperationsInput | number
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type TournamentCreateInput = {
    createdAt?: Date | string
    updatedAt?: Date | string
    startedAt: Date | string
    finishedAt: Date | string
    name: string
    description: string
    prize: string
    icon?: string | null
    map?: string | null
    pickMode?: $Enums.TournamentPickMode
    gameMode?: $Enums.GameMode
    status?: $Enums.TournamentStatus
    teamSize?: number
    slots?: number
    teamRequest?: boolean
    maxRequests?: number
    casters?: TournamentCreatecastersInput | string[]
    videoLink?: string | null
    teams: string
    requests?: TournamentCreaterequestsInput | string[]
    bracket?: string | null
    bracketType?: $Enums.BracketType
    matchesCount?: number
    results?: string | null
    creator: UserCreateNestedOneWithoutTournamentsInput
  }

  export type TournamentUncheckedCreateInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    startedAt: Date | string
    finishedAt: Date | string
    name: string
    description: string
    prize: string
    icon?: string | null
    map?: string | null
    pickMode?: $Enums.TournamentPickMode
    gameMode?: $Enums.GameMode
    status?: $Enums.TournamentStatus
    teamSize?: number
    slots?: number
    teamRequest?: boolean
    maxRequests?: number
    casters?: TournamentCreatecastersInput | string[]
    videoLink?: string | null
    creatorID: number
    teams: string
    requests?: TournamentCreaterequestsInput | string[]
    bracket?: string | null
    bracketType?: $Enums.BracketType
    matchesCount?: number
    results?: string | null
  }

  export type TournamentUpdateInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    startedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    finishedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    prize?: StringFieldUpdateOperationsInput | string
    icon?: NullableStringFieldUpdateOperationsInput | string | null
    map?: NullableStringFieldUpdateOperationsInput | string | null
    pickMode?: EnumTournamentPickModeFieldUpdateOperationsInput | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFieldUpdateOperationsInput | $Enums.GameMode
    status?: EnumTournamentStatusFieldUpdateOperationsInput | $Enums.TournamentStatus
    teamSize?: IntFieldUpdateOperationsInput | number
    slots?: IntFieldUpdateOperationsInput | number
    teamRequest?: BoolFieldUpdateOperationsInput | boolean
    maxRequests?: IntFieldUpdateOperationsInput | number
    casters?: TournamentUpdatecastersInput | string[]
    videoLink?: NullableStringFieldUpdateOperationsInput | string | null
    teams?: StringFieldUpdateOperationsInput | string
    requests?: TournamentUpdaterequestsInput | string[]
    bracket?: NullableStringFieldUpdateOperationsInput | string | null
    bracketType?: EnumBracketTypeFieldUpdateOperationsInput | $Enums.BracketType
    matchesCount?: IntFieldUpdateOperationsInput | number
    results?: NullableStringFieldUpdateOperationsInput | string | null
    creator?: UserUpdateOneRequiredWithoutTournamentsNestedInput
  }

  export type TournamentUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    startedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    finishedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    prize?: StringFieldUpdateOperationsInput | string
    icon?: NullableStringFieldUpdateOperationsInput | string | null
    map?: NullableStringFieldUpdateOperationsInput | string | null
    pickMode?: EnumTournamentPickModeFieldUpdateOperationsInput | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFieldUpdateOperationsInput | $Enums.GameMode
    status?: EnumTournamentStatusFieldUpdateOperationsInput | $Enums.TournamentStatus
    teamSize?: IntFieldUpdateOperationsInput | number
    slots?: IntFieldUpdateOperationsInput | number
    teamRequest?: BoolFieldUpdateOperationsInput | boolean
    maxRequests?: IntFieldUpdateOperationsInput | number
    casters?: TournamentUpdatecastersInput | string[]
    videoLink?: NullableStringFieldUpdateOperationsInput | string | null
    creatorID?: IntFieldUpdateOperationsInput | number
    teams?: StringFieldUpdateOperationsInput | string
    requests?: TournamentUpdaterequestsInput | string[]
    bracket?: NullableStringFieldUpdateOperationsInput | string | null
    bracketType?: EnumBracketTypeFieldUpdateOperationsInput | $Enums.BracketType
    matchesCount?: IntFieldUpdateOperationsInput | number
    results?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type TournamentCreateManyInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    startedAt: Date | string
    finishedAt: Date | string
    name: string
    description: string
    prize: string
    icon?: string | null
    map?: string | null
    pickMode?: $Enums.TournamentPickMode
    gameMode?: $Enums.GameMode
    status?: $Enums.TournamentStatus
    teamSize?: number
    slots?: number
    teamRequest?: boolean
    maxRequests?: number
    casters?: TournamentCreatecastersInput | string[]
    videoLink?: string | null
    creatorID: number
    teams: string
    requests?: TournamentCreaterequestsInput | string[]
    bracket?: string | null
    bracketType?: $Enums.BracketType
    matchesCount?: number
    results?: string | null
  }

  export type TournamentUpdateManyMutationInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    startedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    finishedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    prize?: StringFieldUpdateOperationsInput | string
    icon?: NullableStringFieldUpdateOperationsInput | string | null
    map?: NullableStringFieldUpdateOperationsInput | string | null
    pickMode?: EnumTournamentPickModeFieldUpdateOperationsInput | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFieldUpdateOperationsInput | $Enums.GameMode
    status?: EnumTournamentStatusFieldUpdateOperationsInput | $Enums.TournamentStatus
    teamSize?: IntFieldUpdateOperationsInput | number
    slots?: IntFieldUpdateOperationsInput | number
    teamRequest?: BoolFieldUpdateOperationsInput | boolean
    maxRequests?: IntFieldUpdateOperationsInput | number
    casters?: TournamentUpdatecastersInput | string[]
    videoLink?: NullableStringFieldUpdateOperationsInput | string | null
    teams?: StringFieldUpdateOperationsInput | string
    requests?: TournamentUpdaterequestsInput | string[]
    bracket?: NullableStringFieldUpdateOperationsInput | string | null
    bracketType?: EnumBracketTypeFieldUpdateOperationsInput | $Enums.BracketType
    matchesCount?: IntFieldUpdateOperationsInput | number
    results?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type TournamentUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    startedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    finishedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    prize?: StringFieldUpdateOperationsInput | string
    icon?: NullableStringFieldUpdateOperationsInput | string | null
    map?: NullableStringFieldUpdateOperationsInput | string | null
    pickMode?: EnumTournamentPickModeFieldUpdateOperationsInput | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFieldUpdateOperationsInput | $Enums.GameMode
    status?: EnumTournamentStatusFieldUpdateOperationsInput | $Enums.TournamentStatus
    teamSize?: IntFieldUpdateOperationsInput | number
    slots?: IntFieldUpdateOperationsInput | number
    teamRequest?: BoolFieldUpdateOperationsInput | boolean
    maxRequests?: IntFieldUpdateOperationsInput | number
    casters?: TournamentUpdatecastersInput | string[]
    videoLink?: NullableStringFieldUpdateOperationsInput | string | null
    creatorID?: IntFieldUpdateOperationsInput | number
    teams?: StringFieldUpdateOperationsInput | string
    requests?: TournamentUpdaterequestsInput | string[]
    bracket?: NullableStringFieldUpdateOperationsInput | string | null
    bracketType?: EnumBracketTypeFieldUpdateOperationsInput | $Enums.BracketType
    matchesCount?: IntFieldUpdateOperationsInput | number
    results?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type ItemCreateInput = {
    name: string
    displayName?: string | null
    type: $Enums.ItemType
    price: number
    params: string
    effect?: string | null
    effectParams?: string | null
  }

  export type ItemUncheckedCreateInput = {
    id?: number
    name: string
    displayName?: string | null
    type: $Enums.ItemType
    price: number
    params: string
    effect?: string | null
    effectParams?: string | null
  }

  export type ItemUpdateInput = {
    name?: StringFieldUpdateOperationsInput | string
    displayName?: NullableStringFieldUpdateOperationsInput | string | null
    type?: EnumItemTypeFieldUpdateOperationsInput | $Enums.ItemType
    price?: IntFieldUpdateOperationsInput | number
    params?: StringFieldUpdateOperationsInput | string
    effect?: NullableStringFieldUpdateOperationsInput | string | null
    effectParams?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type ItemUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    name?: StringFieldUpdateOperationsInput | string
    displayName?: NullableStringFieldUpdateOperationsInput | string | null
    type?: EnumItemTypeFieldUpdateOperationsInput | $Enums.ItemType
    price?: IntFieldUpdateOperationsInput | number
    params?: StringFieldUpdateOperationsInput | string
    effect?: NullableStringFieldUpdateOperationsInput | string | null
    effectParams?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type ItemCreateManyInput = {
    id?: number
    name: string
    displayName?: string | null
    type: $Enums.ItemType
    price: number
    params: string
    effect?: string | null
    effectParams?: string | null
  }

  export type ItemUpdateManyMutationInput = {
    name?: StringFieldUpdateOperationsInput | string
    displayName?: NullableStringFieldUpdateOperationsInput | string | null
    type?: EnumItemTypeFieldUpdateOperationsInput | $Enums.ItemType
    price?: IntFieldUpdateOperationsInput | number
    params?: StringFieldUpdateOperationsInput | string
    effect?: NullableStringFieldUpdateOperationsInput | string | null
    effectParams?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type ItemUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    name?: StringFieldUpdateOperationsInput | string
    displayName?: NullableStringFieldUpdateOperationsInput | string | null
    type?: EnumItemTypeFieldUpdateOperationsInput | $Enums.ItemType
    price?: IntFieldUpdateOperationsInput | number
    params?: StringFieldUpdateOperationsInput | string
    effect?: NullableStringFieldUpdateOperationsInput | string | null
    effectParams?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type LogCreateInput = {
    createdAt?: Date | string
    type: $Enums.LogType
    message: string
  }

  export type LogUncheckedCreateInput = {
    id?: number
    createdAt?: Date | string
    type: $Enums.LogType
    message: string
  }

  export type LogUpdateInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    type?: EnumLogTypeFieldUpdateOperationsInput | $Enums.LogType
    message?: StringFieldUpdateOperationsInput | string
  }

  export type LogUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    type?: EnumLogTypeFieldUpdateOperationsInput | $Enums.LogType
    message?: StringFieldUpdateOperationsInput | string
  }

  export type LogCreateManyInput = {
    id?: number
    createdAt?: Date | string
    type: $Enums.LogType
    message: string
  }

  export type LogUpdateManyMutationInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    type?: EnumLogTypeFieldUpdateOperationsInput | $Enums.LogType
    message?: StringFieldUpdateOperationsInput | string
  }

  export type LogUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    type?: EnumLogTypeFieldUpdateOperationsInput | $Enums.LogType
    message?: StringFieldUpdateOperationsInput | string
  }

  export type TeamCreateInput = {
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    rating?: Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    requests?: TeamCreaterequestsInput | string[]
    owner: UserCreateNestedOneWithoutOwnedTeamInput
    members?: UserCreateNestedManyWithoutTeamInput
  }

  export type TeamUncheckedCreateInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    rating?: Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    ownerID: number
    requests?: TeamCreaterequestsInput | string[]
    members?: UserUncheckedCreateNestedManyWithoutTeamInput
  }

  export type TeamUpdateInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
    owner?: UserUpdateOneRequiredWithoutOwnedTeamNestedInput
    members?: UserUpdateManyWithoutTeamNestedInput
  }

  export type TeamUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    ownerID?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
    members?: UserUncheckedUpdateManyWithoutTeamNestedInput
  }

  export type TeamCreateManyInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    rating?: Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    ownerID: number
    requests?: TeamCreaterequestsInput | string[]
  }

  export type TeamUpdateManyMutationInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
  }

  export type TeamUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    ownerID?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
  }

  export type NewsCreateInput = {
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    patch: string
    champions?: NewsCreatechampionsInput | string[]
    items?: NewsCreateitemsInput | string[]
    bugfixes: string
  }

  export type NewsUncheckedCreateInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    patch: string
    champions?: NewsCreatechampionsInput | string[]
    items?: NewsCreateitemsInput | string[]
    bugfixes: string
  }

  export type NewsUpdateInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    patch?: StringFieldUpdateOperationsInput | string
    champions?: NewsUpdatechampionsInput | string[]
    items?: NewsUpdateitemsInput | string[]
    bugfixes?: StringFieldUpdateOperationsInput | string
  }

  export type NewsUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    patch?: StringFieldUpdateOperationsInput | string
    champions?: NewsUpdatechampionsInput | string[]
    items?: NewsUpdateitemsInput | string[]
    bugfixes?: StringFieldUpdateOperationsInput | string
  }

  export type NewsCreateManyInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    patch: string
    champions?: NewsCreatechampionsInput | string[]
    items?: NewsCreateitemsInput | string[]
    bugfixes: string
  }

  export type NewsUpdateManyMutationInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    patch?: StringFieldUpdateOperationsInput | string
    champions?: NewsUpdatechampionsInput | string[]
    items?: NewsUpdateitemsInput | string[]
    bugfixes?: StringFieldUpdateOperationsInput | string
  }

  export type NewsUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    patch?: StringFieldUpdateOperationsInput | string
    champions?: NewsUpdatechampionsInput | string[]
    items?: NewsUpdateitemsInput | string[]
    bugfixes?: StringFieldUpdateOperationsInput | string
  }

  export type CreatureCreateInput = {
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    type: $Enums.CreatureType
    health: string
    gold: string
    exp: string
    spawnTime: string
    respawnTime: string
    buff: string
  }

  export type CreatureUncheckedCreateInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    type: $Enums.CreatureType
    health: string
    gold: string
    exp: string
    spawnTime: string
    respawnTime: string
    buff: string
  }

  export type CreatureUpdateInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    type?: EnumCreatureTypeFieldUpdateOperationsInput | $Enums.CreatureType
    health?: StringFieldUpdateOperationsInput | string
    gold?: StringFieldUpdateOperationsInput | string
    exp?: StringFieldUpdateOperationsInput | string
    spawnTime?: StringFieldUpdateOperationsInput | string
    respawnTime?: StringFieldUpdateOperationsInput | string
    buff?: StringFieldUpdateOperationsInput | string
  }

  export type CreatureUncheckedUpdateInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    type?: EnumCreatureTypeFieldUpdateOperationsInput | $Enums.CreatureType
    health?: StringFieldUpdateOperationsInput | string
    gold?: StringFieldUpdateOperationsInput | string
    exp?: StringFieldUpdateOperationsInput | string
    spawnTime?: StringFieldUpdateOperationsInput | string
    respawnTime?: StringFieldUpdateOperationsInput | string
    buff?: StringFieldUpdateOperationsInput | string
  }

  export type CreatureCreateManyInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    type: $Enums.CreatureType
    health: string
    gold: string
    exp: string
    spawnTime: string
    respawnTime: string
    buff: string
  }

  export type CreatureUpdateManyMutationInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    type?: EnumCreatureTypeFieldUpdateOperationsInput | $Enums.CreatureType
    health?: StringFieldUpdateOperationsInput | string
    gold?: StringFieldUpdateOperationsInput | string
    exp?: StringFieldUpdateOperationsInput | string
    spawnTime?: StringFieldUpdateOperationsInput | string
    respawnTime?: StringFieldUpdateOperationsInput | string
    buff?: StringFieldUpdateOperationsInput | string
  }

  export type CreatureUncheckedUpdateManyInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    type?: EnumCreatureTypeFieldUpdateOperationsInput | $Enums.CreatureType
    health?: StringFieldUpdateOperationsInput | string
    gold?: StringFieldUpdateOperationsInput | string
    exp?: StringFieldUpdateOperationsInput | string
    spawnTime?: StringFieldUpdateOperationsInput | string
    respawnTime?: StringFieldUpdateOperationsInput | string
    buff?: StringFieldUpdateOperationsInput | string
  }

  export type IntFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel>
    in?: number[] | ListIntFieldRefInput<$PrismaModel>
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel>
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntFilter<$PrismaModel> | number
  }

  export type StringFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel>
    in?: string[] | ListStringFieldRefInput<$PrismaModel>
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel>
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    mode?: QueryMode
    not?: NestedStringFilter<$PrismaModel> | string
  }

  export type BoolFilter<$PrismaModel = never> = {
    equals?: boolean | BooleanFieldRefInput<$PrismaModel>
    not?: NestedBoolFilter<$PrismaModel> | boolean
  }

  export type IntNullableListFilter<$PrismaModel = never> = {
    equals?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    has?: number | IntFieldRefInput<$PrismaModel> | null
    hasEvery?: number[] | ListIntFieldRefInput<$PrismaModel>
    hasSome?: number[] | ListIntFieldRefInput<$PrismaModel>
    isEmpty?: boolean
  }

  export type IntNullableFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel> | null
    in?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntNullableFilter<$PrismaModel> | number | null
  }

  export type TeamNullableRelationFilter = {
    is?: TeamWhereInput | null
    isNot?: TeamWhereInput | null
  }

  export type BuildListRelationFilter = {
    every?: BuildWhereInput
    some?: BuildWhereInput
    none?: BuildWhereInput
  }

  export type TournamentListRelationFilter = {
    every?: TournamentWhereInput
    some?: TournamentWhereInput
    none?: TournamentWhereInput
  }

  export type LikesListRelationFilter = {
    every?: LikesWhereInput
    some?: LikesWhereInput
    none?: LikesWhereInput
  }

  export type DisLikesListRelationFilter = {
    every?: DisLikesWhereInput
    some?: DisLikesWhereInput
    none?: DisLikesWhereInput
  }

  export type SortOrderInput = {
    sort: SortOrder
    nulls?: NullsOrder
  }

  export type BuildOrderByRelationAggregateInput = {
    _count?: SortOrder
  }

  export type TournamentOrderByRelationAggregateInput = {
    _count?: SortOrder
  }

  export type LikesOrderByRelationAggregateInput = {
    _count?: SortOrder
  }

  export type DisLikesOrderByRelationAggregateInput = {
    _count?: SortOrder
  }

  export type UserCountOrderByAggregateInput = {
    id?: SortOrder
    login?: SortOrder
    displayName?: SortOrder
    discord?: SortOrder
    telegram?: SortOrder
    gameRank?: SortOrder
    email?: SortOrder
    isEmailConfirmed?: SortOrder
    password?: SortOrder
    icon?: SortOrder
    role?: SortOrder
    refreshToken?: SortOrder
    isDisabled?: SortOrder
    kills?: SortOrder
    deaths?: SortOrder
    assists?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    teamID?: SortOrder
  }

  export type UserAvgOrderByAggregateInput = {
    id?: SortOrder
    gameRank?: SortOrder
    kills?: SortOrder
    deaths?: SortOrder
    assists?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    teamID?: SortOrder
  }

  export type UserMaxOrderByAggregateInput = {
    id?: SortOrder
    login?: SortOrder
    displayName?: SortOrder
    discord?: SortOrder
    telegram?: SortOrder
    gameRank?: SortOrder
    email?: SortOrder
    isEmailConfirmed?: SortOrder
    password?: SortOrder
    icon?: SortOrder
    role?: SortOrder
    refreshToken?: SortOrder
    isDisabled?: SortOrder
    kills?: SortOrder
    deaths?: SortOrder
    assists?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    teamID?: SortOrder
  }

  export type UserMinOrderByAggregateInput = {
    id?: SortOrder
    login?: SortOrder
    displayName?: SortOrder
    discord?: SortOrder
    telegram?: SortOrder
    gameRank?: SortOrder
    email?: SortOrder
    isEmailConfirmed?: SortOrder
    password?: SortOrder
    icon?: SortOrder
    role?: SortOrder
    refreshToken?: SortOrder
    isDisabled?: SortOrder
    kills?: SortOrder
    deaths?: SortOrder
    assists?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    teamID?: SortOrder
  }

  export type UserSumOrderByAggregateInput = {
    id?: SortOrder
    gameRank?: SortOrder
    kills?: SortOrder
    deaths?: SortOrder
    assists?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    teamID?: SortOrder
  }

  export type IntWithAggregatesFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel>
    in?: number[] | ListIntFieldRefInput<$PrismaModel>
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel>
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntWithAggregatesFilter<$PrismaModel> | number
    _count?: NestedIntFilter<$PrismaModel>
    _avg?: NestedFloatFilter<$PrismaModel>
    _sum?: NestedIntFilter<$PrismaModel>
    _min?: NestedIntFilter<$PrismaModel>
    _max?: NestedIntFilter<$PrismaModel>
  }

  export type StringWithAggregatesFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel>
    in?: string[] | ListStringFieldRefInput<$PrismaModel>
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel>
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    mode?: QueryMode
    not?: NestedStringWithAggregatesFilter<$PrismaModel> | string
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedStringFilter<$PrismaModel>
    _max?: NestedStringFilter<$PrismaModel>
  }

  export type BoolWithAggregatesFilter<$PrismaModel = never> = {
    equals?: boolean | BooleanFieldRefInput<$PrismaModel>
    not?: NestedBoolWithAggregatesFilter<$PrismaModel> | boolean
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedBoolFilter<$PrismaModel>
    _max?: NestedBoolFilter<$PrismaModel>
  }

  export type IntNullableWithAggregatesFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel> | null
    in?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntNullableWithAggregatesFilter<$PrismaModel> | number | null
    _count?: NestedIntNullableFilter<$PrismaModel>
    _avg?: NestedFloatNullableFilter<$PrismaModel>
    _sum?: NestedIntNullableFilter<$PrismaModel>
    _min?: NestedIntNullableFilter<$PrismaModel>
    _max?: NestedIntNullableFilter<$PrismaModel>
  }

  export type RegistrationTokenCountOrderByAggregateInput = {
    id?: SortOrder
    token?: SortOrder
  }

  export type RegistrationTokenAvgOrderByAggregateInput = {
    id?: SortOrder
  }

  export type RegistrationTokenMaxOrderByAggregateInput = {
    id?: SortOrder
    token?: SortOrder
  }

  export type RegistrationTokenMinOrderByAggregateInput = {
    id?: SortOrder
    token?: SortOrder
  }

  export type RegistrationTokenSumOrderByAggregateInput = {
    id?: SortOrder
  }

  export type EnumRoleFilter<$PrismaModel = never> = {
    equals?: $Enums.Role | EnumRoleFieldRefInput<$PrismaModel>
    in?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    notIn?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    not?: NestedEnumRoleFilter<$PrismaModel> | $Enums.Role
  }

  export type StringNullableListFilter<$PrismaModel = never> = {
    equals?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    has?: string | StringFieldRefInput<$PrismaModel> | null
    hasEvery?: string[] | ListStringFieldRefInput<$PrismaModel>
    hasSome?: string[] | ListStringFieldRefInput<$PrismaModel>
    isEmpty?: boolean
  }

  export type ChampionCountOrderByAggregateInput = {
    id?: SortOrder
    name?: SortOrder
    role?: SortOrder
    tier?: SortOrder
    counterPicks?: SortOrder
  }

  export type ChampionAvgOrderByAggregateInput = {
    id?: SortOrder
    tier?: SortOrder
  }

  export type ChampionMaxOrderByAggregateInput = {
    id?: SortOrder
    name?: SortOrder
    role?: SortOrder
    tier?: SortOrder
  }

  export type ChampionMinOrderByAggregateInput = {
    id?: SortOrder
    name?: SortOrder
    role?: SortOrder
    tier?: SortOrder
  }

  export type ChampionSumOrderByAggregateInput = {
    id?: SortOrder
    tier?: SortOrder
  }

  export type EnumRoleWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.Role | EnumRoleFieldRefInput<$PrismaModel>
    in?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    notIn?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    not?: NestedEnumRoleWithAggregatesFilter<$PrismaModel> | $Enums.Role
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumRoleFilter<$PrismaModel>
    _max?: NestedEnumRoleFilter<$PrismaModel>
  }

  export type DateTimeFilter<$PrismaModel = never> = {
    equals?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    in?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    notIn?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    lt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    lte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    not?: NestedDateTimeFilter<$PrismaModel> | Date | string
  }

  export type StringNullableFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel> | null
    in?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    mode?: QueryMode
    not?: NestedStringNullableFilter<$PrismaModel> | string | null
  }

  export type ChampionRelationFilter = {
    is?: ChampionWhereInput
    isNot?: ChampionWhereInput
  }

  export type UserRelationFilter = {
    is?: UserWhereInput
    isNot?: UserWhereInput
  }

  export type BuildCountOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    name?: SortOrder
    role?: SortOrder
    championID?: SortOrder
    creatorID?: SortOrder
    items?: SortOrder
    coreItems?: SortOrder
    situationalItems?: SortOrder
    skillsOrder?: SortOrder
    karmas?: SortOrder
    keyInsights?: SortOrder
    strengths?: SortOrder
    weaknesses?: SortOrder
  }

  export type BuildAvgOrderByAggregateInput = {
    id?: SortOrder
    championID?: SortOrder
    creatorID?: SortOrder
  }

  export type BuildMaxOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    name?: SortOrder
    role?: SortOrder
    championID?: SortOrder
    creatorID?: SortOrder
    keyInsights?: SortOrder
    strengths?: SortOrder
    weaknesses?: SortOrder
  }

  export type BuildMinOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    name?: SortOrder
    role?: SortOrder
    championID?: SortOrder
    creatorID?: SortOrder
    keyInsights?: SortOrder
    strengths?: SortOrder
    weaknesses?: SortOrder
  }

  export type BuildSumOrderByAggregateInput = {
    id?: SortOrder
    championID?: SortOrder
    creatorID?: SortOrder
  }

  export type DateTimeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    in?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    notIn?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    lt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    lte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    not?: NestedDateTimeWithAggregatesFilter<$PrismaModel> | Date | string
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedDateTimeFilter<$PrismaModel>
    _max?: NestedDateTimeFilter<$PrismaModel>
  }

  export type StringNullableWithAggregatesFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel> | null
    in?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    mode?: QueryMode
    not?: NestedStringNullableWithAggregatesFilter<$PrismaModel> | string | null
    _count?: NestedIntNullableFilter<$PrismaModel>
    _min?: NestedStringNullableFilter<$PrismaModel>
    _max?: NestedStringNullableFilter<$PrismaModel>
  }

  export type BuildRelationFilter = {
    is?: BuildWhereInput
    isNot?: BuildWhereInput
  }

  export type LikesUserIDBuildIDCompoundUniqueInput = {
    userID: number
    buildID: number
  }

  export type LikesCountOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
  }

  export type LikesAvgOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
  }

  export type LikesMaxOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
  }

  export type LikesMinOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
  }

  export type LikesSumOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
  }

  export type DisLikesUserIDBuildIDCompoundUniqueInput = {
    userID: number
    buildID: number
  }

  export type DisLikesCountOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
  }

  export type DisLikesAvgOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
  }

  export type DisLikesMaxOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
  }

  export type DisLikesMinOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
    assignedAt?: SortOrder
  }

  export type DisLikesSumOrderByAggregateInput = {
    userID?: SortOrder
    buildID?: SortOrder
  }

  export type EnumTournamentPickModeFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentPickMode | EnumTournamentPickModeFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentPickModeFilter<$PrismaModel> | $Enums.TournamentPickMode
  }

  export type EnumGameModeFilter<$PrismaModel = never> = {
    equals?: $Enums.GameMode | EnumGameModeFieldRefInput<$PrismaModel>
    in?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    not?: NestedEnumGameModeFilter<$PrismaModel> | $Enums.GameMode
  }

  export type EnumTournamentStatusFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentStatus | EnumTournamentStatusFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentStatusFilter<$PrismaModel> | $Enums.TournamentStatus
  }

  export type EnumBracketTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.BracketType | EnumBracketTypeFieldRefInput<$PrismaModel>
    in?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumBracketTypeFilter<$PrismaModel> | $Enums.BracketType
  }

  export type TournamentCountOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    startedAt?: SortOrder
    finishedAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    prize?: SortOrder
    icon?: SortOrder
    map?: SortOrder
    pickMode?: SortOrder
    gameMode?: SortOrder
    status?: SortOrder
    teamSize?: SortOrder
    slots?: SortOrder
    teamRequest?: SortOrder
    maxRequests?: SortOrder
    casters?: SortOrder
    videoLink?: SortOrder
    creatorID?: SortOrder
    teams?: SortOrder
    requests?: SortOrder
    bracket?: SortOrder
    bracketType?: SortOrder
    matchesCount?: SortOrder
    results?: SortOrder
  }

  export type TournamentAvgOrderByAggregateInput = {
    id?: SortOrder
    teamSize?: SortOrder
    slots?: SortOrder
    maxRequests?: SortOrder
    creatorID?: SortOrder
    matchesCount?: SortOrder
  }

  export type TournamentMaxOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    startedAt?: SortOrder
    finishedAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    prize?: SortOrder
    icon?: SortOrder
    map?: SortOrder
    pickMode?: SortOrder
    gameMode?: SortOrder
    status?: SortOrder
    teamSize?: SortOrder
    slots?: SortOrder
    teamRequest?: SortOrder
    maxRequests?: SortOrder
    videoLink?: SortOrder
    creatorID?: SortOrder
    teams?: SortOrder
    bracket?: SortOrder
    bracketType?: SortOrder
    matchesCount?: SortOrder
    results?: SortOrder
  }

  export type TournamentMinOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    updatedAt?: SortOrder
    startedAt?: SortOrder
    finishedAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    prize?: SortOrder
    icon?: SortOrder
    map?: SortOrder
    pickMode?: SortOrder
    gameMode?: SortOrder
    status?: SortOrder
    teamSize?: SortOrder
    slots?: SortOrder
    teamRequest?: SortOrder
    maxRequests?: SortOrder
    videoLink?: SortOrder
    creatorID?: SortOrder
    teams?: SortOrder
    bracket?: SortOrder
    bracketType?: SortOrder
    matchesCount?: SortOrder
    results?: SortOrder
  }

  export type TournamentSumOrderByAggregateInput = {
    id?: SortOrder
    teamSize?: SortOrder
    slots?: SortOrder
    maxRequests?: SortOrder
    creatorID?: SortOrder
    matchesCount?: SortOrder
  }

  export type EnumTournamentPickModeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentPickMode | EnumTournamentPickModeFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentPickModeWithAggregatesFilter<$PrismaModel> | $Enums.TournamentPickMode
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumTournamentPickModeFilter<$PrismaModel>
    _max?: NestedEnumTournamentPickModeFilter<$PrismaModel>
  }

  export type EnumGameModeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.GameMode | EnumGameModeFieldRefInput<$PrismaModel>
    in?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    not?: NestedEnumGameModeWithAggregatesFilter<$PrismaModel> | $Enums.GameMode
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumGameModeFilter<$PrismaModel>
    _max?: NestedEnumGameModeFilter<$PrismaModel>
  }

  export type EnumTournamentStatusWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentStatus | EnumTournamentStatusFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentStatusWithAggregatesFilter<$PrismaModel> | $Enums.TournamentStatus
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumTournamentStatusFilter<$PrismaModel>
    _max?: NestedEnumTournamentStatusFilter<$PrismaModel>
  }

  export type EnumBracketTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.BracketType | EnumBracketTypeFieldRefInput<$PrismaModel>
    in?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumBracketTypeWithAggregatesFilter<$PrismaModel> | $Enums.BracketType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumBracketTypeFilter<$PrismaModel>
    _max?: NestedEnumBracketTypeFilter<$PrismaModel>
  }

  export type EnumItemTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.ItemType | EnumItemTypeFieldRefInput<$PrismaModel>
    in?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumItemTypeFilter<$PrismaModel> | $Enums.ItemType
  }

  export type ItemCountOrderByAggregateInput = {
    id?: SortOrder
    name?: SortOrder
    displayName?: SortOrder
    type?: SortOrder
    price?: SortOrder
    params?: SortOrder
    effect?: SortOrder
    effectParams?: SortOrder
  }

  export type ItemAvgOrderByAggregateInput = {
    id?: SortOrder
    price?: SortOrder
  }

  export type ItemMaxOrderByAggregateInput = {
    id?: SortOrder
    name?: SortOrder
    displayName?: SortOrder
    type?: SortOrder
    price?: SortOrder
    params?: SortOrder
    effect?: SortOrder
    effectParams?: SortOrder
  }

  export type ItemMinOrderByAggregateInput = {
    id?: SortOrder
    name?: SortOrder
    displayName?: SortOrder
    type?: SortOrder
    price?: SortOrder
    params?: SortOrder
    effect?: SortOrder
    effectParams?: SortOrder
  }

  export type ItemSumOrderByAggregateInput = {
    id?: SortOrder
    price?: SortOrder
  }

  export type EnumItemTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.ItemType | EnumItemTypeFieldRefInput<$PrismaModel>
    in?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumItemTypeWithAggregatesFilter<$PrismaModel> | $Enums.ItemType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumItemTypeFilter<$PrismaModel>
    _max?: NestedEnumItemTypeFilter<$PrismaModel>
  }

  export type EnumLogTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.LogType | EnumLogTypeFieldRefInput<$PrismaModel>
    in?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumLogTypeFilter<$PrismaModel> | $Enums.LogType
  }

  export type LogCountOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    type?: SortOrder
    message?: SortOrder
  }

  export type LogAvgOrderByAggregateInput = {
    id?: SortOrder
  }

  export type LogMaxOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    type?: SortOrder
    message?: SortOrder
  }

  export type LogMinOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    type?: SortOrder
    message?: SortOrder
  }

  export type LogSumOrderByAggregateInput = {
    id?: SortOrder
  }

  export type EnumLogTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.LogType | EnumLogTypeFieldRefInput<$PrismaModel>
    in?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumLogTypeWithAggregatesFilter<$PrismaModel> | $Enums.LogType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumLogTypeFilter<$PrismaModel>
    _max?: NestedEnumLogTypeFilter<$PrismaModel>
  }

  export type DecimalFilter<$PrismaModel = never> = {
    equals?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    in?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    notIn?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    lt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    lte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    not?: NestedDecimalFilter<$PrismaModel> | Decimal | DecimalJsLike | number | string
  }

  export type UserListRelationFilter = {
    every?: UserWhereInput
    some?: UserWhereInput
    none?: UserWhereInput
  }

  export type UserOrderByRelationAggregateInput = {
    _count?: SortOrder
  }

  export type TeamCountOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    rating?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    ownerID?: SortOrder
    requests?: SortOrder
  }

  export type TeamAvgOrderByAggregateInput = {
    id?: SortOrder
    rating?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    ownerID?: SortOrder
  }

  export type TeamMaxOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    rating?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    ownerID?: SortOrder
  }

  export type TeamMinOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    rating?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    ownerID?: SortOrder
  }

  export type TeamSumOrderByAggregateInput = {
    id?: SortOrder
    rating?: SortOrder
    tournamentWins?: SortOrder
    gameWins?: SortOrder
    gameLosed?: SortOrder
    ownerID?: SortOrder
  }

  export type DecimalWithAggregatesFilter<$PrismaModel = never> = {
    equals?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    in?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    notIn?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    lt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    lte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    not?: NestedDecimalWithAggregatesFilter<$PrismaModel> | Decimal | DecimalJsLike | number | string
    _count?: NestedIntFilter<$PrismaModel>
    _avg?: NestedDecimalFilter<$PrismaModel>
    _sum?: NestedDecimalFilter<$PrismaModel>
    _min?: NestedDecimalFilter<$PrismaModel>
    _max?: NestedDecimalFilter<$PrismaModel>
  }

  export type NewsCountOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    patch?: SortOrder
    champions?: SortOrder
    items?: SortOrder
    bugfixes?: SortOrder
  }

  export type NewsAvgOrderByAggregateInput = {
    id?: SortOrder
  }

  export type NewsMaxOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    patch?: SortOrder
    bugfixes?: SortOrder
  }

  export type NewsMinOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    patch?: SortOrder
    bugfixes?: SortOrder
  }

  export type NewsSumOrderByAggregateInput = {
    id?: SortOrder
  }

  export type EnumCreatureTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.CreatureType | EnumCreatureTypeFieldRefInput<$PrismaModel>
    in?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumCreatureTypeFilter<$PrismaModel> | $Enums.CreatureType
  }

  export type CreatureCountOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    type?: SortOrder
    health?: SortOrder
    gold?: SortOrder
    exp?: SortOrder
    spawnTime?: SortOrder
    respawnTime?: SortOrder
    buff?: SortOrder
  }

  export type CreatureAvgOrderByAggregateInput = {
    id?: SortOrder
  }

  export type CreatureMaxOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    type?: SortOrder
    health?: SortOrder
    gold?: SortOrder
    exp?: SortOrder
    spawnTime?: SortOrder
    respawnTime?: SortOrder
    buff?: SortOrder
  }

  export type CreatureMinOrderByAggregateInput = {
    id?: SortOrder
    createdAt?: SortOrder
    name?: SortOrder
    description?: SortOrder
    icon?: SortOrder
    type?: SortOrder
    health?: SortOrder
    gold?: SortOrder
    exp?: SortOrder
    spawnTime?: SortOrder
    respawnTime?: SortOrder
    buff?: SortOrder
  }

  export type CreatureSumOrderByAggregateInput = {
    id?: SortOrder
  }

  export type EnumCreatureTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.CreatureType | EnumCreatureTypeFieldRefInput<$PrismaModel>
    in?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumCreatureTypeWithAggregatesFilter<$PrismaModel> | $Enums.CreatureType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumCreatureTypeFilter<$PrismaModel>
    _max?: NestedEnumCreatureTypeFilter<$PrismaModel>
  }

  export type UserCreatetournamentWinsInput = {
    set: number[]
  }

  export type TeamCreateNestedOneWithoutMembersInput = {
    create?: XOR<TeamCreateWithoutMembersInput, TeamUncheckedCreateWithoutMembersInput>
    connectOrCreate?: TeamCreateOrConnectWithoutMembersInput
    connect?: TeamWhereUniqueInput
  }

  export type TeamCreateNestedOneWithoutOwnerInput = {
    create?: XOR<TeamCreateWithoutOwnerInput, TeamUncheckedCreateWithoutOwnerInput>
    connectOrCreate?: TeamCreateOrConnectWithoutOwnerInput
    connect?: TeamWhereUniqueInput
  }

  export type BuildCreateNestedManyWithoutCreatorInput = {
    create?: XOR<BuildCreateWithoutCreatorInput, BuildUncheckedCreateWithoutCreatorInput> | BuildCreateWithoutCreatorInput[] | BuildUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutCreatorInput | BuildCreateOrConnectWithoutCreatorInput[]
    createMany?: BuildCreateManyCreatorInputEnvelope
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
  }

  export type TournamentCreateNestedManyWithoutCreatorInput = {
    create?: XOR<TournamentCreateWithoutCreatorInput, TournamentUncheckedCreateWithoutCreatorInput> | TournamentCreateWithoutCreatorInput[] | TournamentUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: TournamentCreateOrConnectWithoutCreatorInput | TournamentCreateOrConnectWithoutCreatorInput[]
    createMany?: TournamentCreateManyCreatorInputEnvelope
    connect?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
  }

  export type LikesCreateNestedManyWithoutUserInput = {
    create?: XOR<LikesCreateWithoutUserInput, LikesUncheckedCreateWithoutUserInput> | LikesCreateWithoutUserInput[] | LikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutUserInput | LikesCreateOrConnectWithoutUserInput[]
    createMany?: LikesCreateManyUserInputEnvelope
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
  }

  export type DisLikesCreateNestedManyWithoutUserInput = {
    create?: XOR<DisLikesCreateWithoutUserInput, DisLikesUncheckedCreateWithoutUserInput> | DisLikesCreateWithoutUserInput[] | DisLikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutUserInput | DisLikesCreateOrConnectWithoutUserInput[]
    createMany?: DisLikesCreateManyUserInputEnvelope
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
  }

  export type TeamUncheckedCreateNestedOneWithoutOwnerInput = {
    create?: XOR<TeamCreateWithoutOwnerInput, TeamUncheckedCreateWithoutOwnerInput>
    connectOrCreate?: TeamCreateOrConnectWithoutOwnerInput
    connect?: TeamWhereUniqueInput
  }

  export type BuildUncheckedCreateNestedManyWithoutCreatorInput = {
    create?: XOR<BuildCreateWithoutCreatorInput, BuildUncheckedCreateWithoutCreatorInput> | BuildCreateWithoutCreatorInput[] | BuildUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutCreatorInput | BuildCreateOrConnectWithoutCreatorInput[]
    createMany?: BuildCreateManyCreatorInputEnvelope
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
  }

  export type TournamentUncheckedCreateNestedManyWithoutCreatorInput = {
    create?: XOR<TournamentCreateWithoutCreatorInput, TournamentUncheckedCreateWithoutCreatorInput> | TournamentCreateWithoutCreatorInput[] | TournamentUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: TournamentCreateOrConnectWithoutCreatorInput | TournamentCreateOrConnectWithoutCreatorInput[]
    createMany?: TournamentCreateManyCreatorInputEnvelope
    connect?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
  }

  export type LikesUncheckedCreateNestedManyWithoutUserInput = {
    create?: XOR<LikesCreateWithoutUserInput, LikesUncheckedCreateWithoutUserInput> | LikesCreateWithoutUserInput[] | LikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutUserInput | LikesCreateOrConnectWithoutUserInput[]
    createMany?: LikesCreateManyUserInputEnvelope
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
  }

  export type DisLikesUncheckedCreateNestedManyWithoutUserInput = {
    create?: XOR<DisLikesCreateWithoutUserInput, DisLikesUncheckedCreateWithoutUserInput> | DisLikesCreateWithoutUserInput[] | DisLikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutUserInput | DisLikesCreateOrConnectWithoutUserInput[]
    createMany?: DisLikesCreateManyUserInputEnvelope
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
  }

  export type StringFieldUpdateOperationsInput = {
    set?: string
  }

  export type IntFieldUpdateOperationsInput = {
    set?: number
    increment?: number
    decrement?: number
    multiply?: number
    divide?: number
  }

  export type BoolFieldUpdateOperationsInput = {
    set?: boolean
  }

  export type UserUpdatetournamentWinsInput = {
    set?: number[]
    push?: number | number[]
  }

  export type TeamUpdateOneWithoutMembersNestedInput = {
    create?: XOR<TeamCreateWithoutMembersInput, TeamUncheckedCreateWithoutMembersInput>
    connectOrCreate?: TeamCreateOrConnectWithoutMembersInput
    upsert?: TeamUpsertWithoutMembersInput
    disconnect?: TeamWhereInput | boolean
    delete?: TeamWhereInput | boolean
    connect?: TeamWhereUniqueInput
    update?: XOR<XOR<TeamUpdateToOneWithWhereWithoutMembersInput, TeamUpdateWithoutMembersInput>, TeamUncheckedUpdateWithoutMembersInput>
  }

  export type TeamUpdateOneWithoutOwnerNestedInput = {
    create?: XOR<TeamCreateWithoutOwnerInput, TeamUncheckedCreateWithoutOwnerInput>
    connectOrCreate?: TeamCreateOrConnectWithoutOwnerInput
    upsert?: TeamUpsertWithoutOwnerInput
    disconnect?: TeamWhereInput | boolean
    delete?: TeamWhereInput | boolean
    connect?: TeamWhereUniqueInput
    update?: XOR<XOR<TeamUpdateToOneWithWhereWithoutOwnerInput, TeamUpdateWithoutOwnerInput>, TeamUncheckedUpdateWithoutOwnerInput>
  }

  export type BuildUpdateManyWithoutCreatorNestedInput = {
    create?: XOR<BuildCreateWithoutCreatorInput, BuildUncheckedCreateWithoutCreatorInput> | BuildCreateWithoutCreatorInput[] | BuildUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutCreatorInput | BuildCreateOrConnectWithoutCreatorInput[]
    upsert?: BuildUpsertWithWhereUniqueWithoutCreatorInput | BuildUpsertWithWhereUniqueWithoutCreatorInput[]
    createMany?: BuildCreateManyCreatorInputEnvelope
    set?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    disconnect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    delete?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    update?: BuildUpdateWithWhereUniqueWithoutCreatorInput | BuildUpdateWithWhereUniqueWithoutCreatorInput[]
    updateMany?: BuildUpdateManyWithWhereWithoutCreatorInput | BuildUpdateManyWithWhereWithoutCreatorInput[]
    deleteMany?: BuildScalarWhereInput | BuildScalarWhereInput[]
  }

  export type TournamentUpdateManyWithoutCreatorNestedInput = {
    create?: XOR<TournamentCreateWithoutCreatorInput, TournamentUncheckedCreateWithoutCreatorInput> | TournamentCreateWithoutCreatorInput[] | TournamentUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: TournamentCreateOrConnectWithoutCreatorInput | TournamentCreateOrConnectWithoutCreatorInput[]
    upsert?: TournamentUpsertWithWhereUniqueWithoutCreatorInput | TournamentUpsertWithWhereUniqueWithoutCreatorInput[]
    createMany?: TournamentCreateManyCreatorInputEnvelope
    set?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    disconnect?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    delete?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    connect?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    update?: TournamentUpdateWithWhereUniqueWithoutCreatorInput | TournamentUpdateWithWhereUniqueWithoutCreatorInput[]
    updateMany?: TournamentUpdateManyWithWhereWithoutCreatorInput | TournamentUpdateManyWithWhereWithoutCreatorInput[]
    deleteMany?: TournamentScalarWhereInput | TournamentScalarWhereInput[]
  }

  export type LikesUpdateManyWithoutUserNestedInput = {
    create?: XOR<LikesCreateWithoutUserInput, LikesUncheckedCreateWithoutUserInput> | LikesCreateWithoutUserInput[] | LikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutUserInput | LikesCreateOrConnectWithoutUserInput[]
    upsert?: LikesUpsertWithWhereUniqueWithoutUserInput | LikesUpsertWithWhereUniqueWithoutUserInput[]
    createMany?: LikesCreateManyUserInputEnvelope
    set?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    disconnect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    delete?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    update?: LikesUpdateWithWhereUniqueWithoutUserInput | LikesUpdateWithWhereUniqueWithoutUserInput[]
    updateMany?: LikesUpdateManyWithWhereWithoutUserInput | LikesUpdateManyWithWhereWithoutUserInput[]
    deleteMany?: LikesScalarWhereInput | LikesScalarWhereInput[]
  }

  export type DisLikesUpdateManyWithoutUserNestedInput = {
    create?: XOR<DisLikesCreateWithoutUserInput, DisLikesUncheckedCreateWithoutUserInput> | DisLikesCreateWithoutUserInput[] | DisLikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutUserInput | DisLikesCreateOrConnectWithoutUserInput[]
    upsert?: DisLikesUpsertWithWhereUniqueWithoutUserInput | DisLikesUpsertWithWhereUniqueWithoutUserInput[]
    createMany?: DisLikesCreateManyUserInputEnvelope
    set?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    disconnect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    delete?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    update?: DisLikesUpdateWithWhereUniqueWithoutUserInput | DisLikesUpdateWithWhereUniqueWithoutUserInput[]
    updateMany?: DisLikesUpdateManyWithWhereWithoutUserInput | DisLikesUpdateManyWithWhereWithoutUserInput[]
    deleteMany?: DisLikesScalarWhereInput | DisLikesScalarWhereInput[]
  }

  export type NullableIntFieldUpdateOperationsInput = {
    set?: number | null
    increment?: number
    decrement?: number
    multiply?: number
    divide?: number
  }

  export type TeamUncheckedUpdateOneWithoutOwnerNestedInput = {
    create?: XOR<TeamCreateWithoutOwnerInput, TeamUncheckedCreateWithoutOwnerInput>
    connectOrCreate?: TeamCreateOrConnectWithoutOwnerInput
    upsert?: TeamUpsertWithoutOwnerInput
    disconnect?: TeamWhereInput | boolean
    delete?: TeamWhereInput | boolean
    connect?: TeamWhereUniqueInput
    update?: XOR<XOR<TeamUpdateToOneWithWhereWithoutOwnerInput, TeamUpdateWithoutOwnerInput>, TeamUncheckedUpdateWithoutOwnerInput>
  }

  export type BuildUncheckedUpdateManyWithoutCreatorNestedInput = {
    create?: XOR<BuildCreateWithoutCreatorInput, BuildUncheckedCreateWithoutCreatorInput> | BuildCreateWithoutCreatorInput[] | BuildUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutCreatorInput | BuildCreateOrConnectWithoutCreatorInput[]
    upsert?: BuildUpsertWithWhereUniqueWithoutCreatorInput | BuildUpsertWithWhereUniqueWithoutCreatorInput[]
    createMany?: BuildCreateManyCreatorInputEnvelope
    set?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    disconnect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    delete?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    update?: BuildUpdateWithWhereUniqueWithoutCreatorInput | BuildUpdateWithWhereUniqueWithoutCreatorInput[]
    updateMany?: BuildUpdateManyWithWhereWithoutCreatorInput | BuildUpdateManyWithWhereWithoutCreatorInput[]
    deleteMany?: BuildScalarWhereInput | BuildScalarWhereInput[]
  }

  export type TournamentUncheckedUpdateManyWithoutCreatorNestedInput = {
    create?: XOR<TournamentCreateWithoutCreatorInput, TournamentUncheckedCreateWithoutCreatorInput> | TournamentCreateWithoutCreatorInput[] | TournamentUncheckedCreateWithoutCreatorInput[]
    connectOrCreate?: TournamentCreateOrConnectWithoutCreatorInput | TournamentCreateOrConnectWithoutCreatorInput[]
    upsert?: TournamentUpsertWithWhereUniqueWithoutCreatorInput | TournamentUpsertWithWhereUniqueWithoutCreatorInput[]
    createMany?: TournamentCreateManyCreatorInputEnvelope
    set?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    disconnect?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    delete?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    connect?: TournamentWhereUniqueInput | TournamentWhereUniqueInput[]
    update?: TournamentUpdateWithWhereUniqueWithoutCreatorInput | TournamentUpdateWithWhereUniqueWithoutCreatorInput[]
    updateMany?: TournamentUpdateManyWithWhereWithoutCreatorInput | TournamentUpdateManyWithWhereWithoutCreatorInput[]
    deleteMany?: TournamentScalarWhereInput | TournamentScalarWhereInput[]
  }

  export type LikesUncheckedUpdateManyWithoutUserNestedInput = {
    create?: XOR<LikesCreateWithoutUserInput, LikesUncheckedCreateWithoutUserInput> | LikesCreateWithoutUserInput[] | LikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutUserInput | LikesCreateOrConnectWithoutUserInput[]
    upsert?: LikesUpsertWithWhereUniqueWithoutUserInput | LikesUpsertWithWhereUniqueWithoutUserInput[]
    createMany?: LikesCreateManyUserInputEnvelope
    set?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    disconnect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    delete?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    update?: LikesUpdateWithWhereUniqueWithoutUserInput | LikesUpdateWithWhereUniqueWithoutUserInput[]
    updateMany?: LikesUpdateManyWithWhereWithoutUserInput | LikesUpdateManyWithWhereWithoutUserInput[]
    deleteMany?: LikesScalarWhereInput | LikesScalarWhereInput[]
  }

  export type DisLikesUncheckedUpdateManyWithoutUserNestedInput = {
    create?: XOR<DisLikesCreateWithoutUserInput, DisLikesUncheckedCreateWithoutUserInput> | DisLikesCreateWithoutUserInput[] | DisLikesUncheckedCreateWithoutUserInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutUserInput | DisLikesCreateOrConnectWithoutUserInput[]
    upsert?: DisLikesUpsertWithWhereUniqueWithoutUserInput | DisLikesUpsertWithWhereUniqueWithoutUserInput[]
    createMany?: DisLikesCreateManyUserInputEnvelope
    set?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    disconnect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    delete?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    update?: DisLikesUpdateWithWhereUniqueWithoutUserInput | DisLikesUpdateWithWhereUniqueWithoutUserInput[]
    updateMany?: DisLikesUpdateManyWithWhereWithoutUserInput | DisLikesUpdateManyWithWhereWithoutUserInput[]
    deleteMany?: DisLikesScalarWhereInput | DisLikesScalarWhereInput[]
  }

  export type ChampionCreatecounterPicksInput = {
    set: string[]
  }

  export type BuildCreateNestedManyWithoutChampionInput = {
    create?: XOR<BuildCreateWithoutChampionInput, BuildUncheckedCreateWithoutChampionInput> | BuildCreateWithoutChampionInput[] | BuildUncheckedCreateWithoutChampionInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutChampionInput | BuildCreateOrConnectWithoutChampionInput[]
    createMany?: BuildCreateManyChampionInputEnvelope
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
  }

  export type BuildUncheckedCreateNestedManyWithoutChampionInput = {
    create?: XOR<BuildCreateWithoutChampionInput, BuildUncheckedCreateWithoutChampionInput> | BuildCreateWithoutChampionInput[] | BuildUncheckedCreateWithoutChampionInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutChampionInput | BuildCreateOrConnectWithoutChampionInput[]
    createMany?: BuildCreateManyChampionInputEnvelope
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
  }

  export type EnumRoleFieldUpdateOperationsInput = {
    set?: $Enums.Role
  }

  export type ChampionUpdatecounterPicksInput = {
    set?: string[]
    push?: string | string[]
  }

  export type BuildUpdateManyWithoutChampionNestedInput = {
    create?: XOR<BuildCreateWithoutChampionInput, BuildUncheckedCreateWithoutChampionInput> | BuildCreateWithoutChampionInput[] | BuildUncheckedCreateWithoutChampionInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutChampionInput | BuildCreateOrConnectWithoutChampionInput[]
    upsert?: BuildUpsertWithWhereUniqueWithoutChampionInput | BuildUpsertWithWhereUniqueWithoutChampionInput[]
    createMany?: BuildCreateManyChampionInputEnvelope
    set?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    disconnect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    delete?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    update?: BuildUpdateWithWhereUniqueWithoutChampionInput | BuildUpdateWithWhereUniqueWithoutChampionInput[]
    updateMany?: BuildUpdateManyWithWhereWithoutChampionInput | BuildUpdateManyWithWhereWithoutChampionInput[]
    deleteMany?: BuildScalarWhereInput | BuildScalarWhereInput[]
  }

  export type BuildUncheckedUpdateManyWithoutChampionNestedInput = {
    create?: XOR<BuildCreateWithoutChampionInput, BuildUncheckedCreateWithoutChampionInput> | BuildCreateWithoutChampionInput[] | BuildUncheckedCreateWithoutChampionInput[]
    connectOrCreate?: BuildCreateOrConnectWithoutChampionInput | BuildCreateOrConnectWithoutChampionInput[]
    upsert?: BuildUpsertWithWhereUniqueWithoutChampionInput | BuildUpsertWithWhereUniqueWithoutChampionInput[]
    createMany?: BuildCreateManyChampionInputEnvelope
    set?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    disconnect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    delete?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    connect?: BuildWhereUniqueInput | BuildWhereUniqueInput[]
    update?: BuildUpdateWithWhereUniqueWithoutChampionInput | BuildUpdateWithWhereUniqueWithoutChampionInput[]
    updateMany?: BuildUpdateManyWithWhereWithoutChampionInput | BuildUpdateManyWithWhereWithoutChampionInput[]
    deleteMany?: BuildScalarWhereInput | BuildScalarWhereInput[]
  }

  export type BuildCreateitemsInput = {
    set: string[]
  }

  export type BuildCreatecoreItemsInput = {
    set: string[]
  }

  export type BuildCreatesituationalItemsInput = {
    set: string[]
  }

  export type BuildCreateskillsOrderInput = {
    set: string[]
  }

  export type BuildCreatekarmasInput = {
    set: string[]
  }

  export type ChampionCreateNestedOneWithoutBuildsInput = {
    create?: XOR<ChampionCreateWithoutBuildsInput, ChampionUncheckedCreateWithoutBuildsInput>
    connectOrCreate?: ChampionCreateOrConnectWithoutBuildsInput
    connect?: ChampionWhereUniqueInput
  }

  export type UserCreateNestedOneWithoutBuildsInput = {
    create?: XOR<UserCreateWithoutBuildsInput, UserUncheckedCreateWithoutBuildsInput>
    connectOrCreate?: UserCreateOrConnectWithoutBuildsInput
    connect?: UserWhereUniqueInput
  }

  export type LikesCreateNestedManyWithoutBuildInput = {
    create?: XOR<LikesCreateWithoutBuildInput, LikesUncheckedCreateWithoutBuildInput> | LikesCreateWithoutBuildInput[] | LikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutBuildInput | LikesCreateOrConnectWithoutBuildInput[]
    createMany?: LikesCreateManyBuildInputEnvelope
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
  }

  export type DisLikesCreateNestedManyWithoutBuildInput = {
    create?: XOR<DisLikesCreateWithoutBuildInput, DisLikesUncheckedCreateWithoutBuildInput> | DisLikesCreateWithoutBuildInput[] | DisLikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutBuildInput | DisLikesCreateOrConnectWithoutBuildInput[]
    createMany?: DisLikesCreateManyBuildInputEnvelope
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
  }

  export type LikesUncheckedCreateNestedManyWithoutBuildInput = {
    create?: XOR<LikesCreateWithoutBuildInput, LikesUncheckedCreateWithoutBuildInput> | LikesCreateWithoutBuildInput[] | LikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutBuildInput | LikesCreateOrConnectWithoutBuildInput[]
    createMany?: LikesCreateManyBuildInputEnvelope
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
  }

  export type DisLikesUncheckedCreateNestedManyWithoutBuildInput = {
    create?: XOR<DisLikesCreateWithoutBuildInput, DisLikesUncheckedCreateWithoutBuildInput> | DisLikesCreateWithoutBuildInput[] | DisLikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutBuildInput | DisLikesCreateOrConnectWithoutBuildInput[]
    createMany?: DisLikesCreateManyBuildInputEnvelope
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
  }

  export type DateTimeFieldUpdateOperationsInput = {
    set?: Date | string
  }

  export type BuildUpdateitemsInput = {
    set?: string[]
    push?: string | string[]
  }

  export type BuildUpdatecoreItemsInput = {
    set?: string[]
    push?: string | string[]
  }

  export type BuildUpdatesituationalItemsInput = {
    set?: string[]
    push?: string | string[]
  }

  export type BuildUpdateskillsOrderInput = {
    set?: string[]
    push?: string | string[]
  }

  export type BuildUpdatekarmasInput = {
    set?: string[]
    push?: string | string[]
  }

  export type NullableStringFieldUpdateOperationsInput = {
    set?: string | null
  }

  export type ChampionUpdateOneRequiredWithoutBuildsNestedInput = {
    create?: XOR<ChampionCreateWithoutBuildsInput, ChampionUncheckedCreateWithoutBuildsInput>
    connectOrCreate?: ChampionCreateOrConnectWithoutBuildsInput
    upsert?: ChampionUpsertWithoutBuildsInput
    connect?: ChampionWhereUniqueInput
    update?: XOR<XOR<ChampionUpdateToOneWithWhereWithoutBuildsInput, ChampionUpdateWithoutBuildsInput>, ChampionUncheckedUpdateWithoutBuildsInput>
  }

  export type UserUpdateOneRequiredWithoutBuildsNestedInput = {
    create?: XOR<UserCreateWithoutBuildsInput, UserUncheckedCreateWithoutBuildsInput>
    connectOrCreate?: UserCreateOrConnectWithoutBuildsInput
    upsert?: UserUpsertWithoutBuildsInput
    connect?: UserWhereUniqueInput
    update?: XOR<XOR<UserUpdateToOneWithWhereWithoutBuildsInput, UserUpdateWithoutBuildsInput>, UserUncheckedUpdateWithoutBuildsInput>
  }

  export type LikesUpdateManyWithoutBuildNestedInput = {
    create?: XOR<LikesCreateWithoutBuildInput, LikesUncheckedCreateWithoutBuildInput> | LikesCreateWithoutBuildInput[] | LikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutBuildInput | LikesCreateOrConnectWithoutBuildInput[]
    upsert?: LikesUpsertWithWhereUniqueWithoutBuildInput | LikesUpsertWithWhereUniqueWithoutBuildInput[]
    createMany?: LikesCreateManyBuildInputEnvelope
    set?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    disconnect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    delete?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    update?: LikesUpdateWithWhereUniqueWithoutBuildInput | LikesUpdateWithWhereUniqueWithoutBuildInput[]
    updateMany?: LikesUpdateManyWithWhereWithoutBuildInput | LikesUpdateManyWithWhereWithoutBuildInput[]
    deleteMany?: LikesScalarWhereInput | LikesScalarWhereInput[]
  }

  export type DisLikesUpdateManyWithoutBuildNestedInput = {
    create?: XOR<DisLikesCreateWithoutBuildInput, DisLikesUncheckedCreateWithoutBuildInput> | DisLikesCreateWithoutBuildInput[] | DisLikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutBuildInput | DisLikesCreateOrConnectWithoutBuildInput[]
    upsert?: DisLikesUpsertWithWhereUniqueWithoutBuildInput | DisLikesUpsertWithWhereUniqueWithoutBuildInput[]
    createMany?: DisLikesCreateManyBuildInputEnvelope
    set?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    disconnect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    delete?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    update?: DisLikesUpdateWithWhereUniqueWithoutBuildInput | DisLikesUpdateWithWhereUniqueWithoutBuildInput[]
    updateMany?: DisLikesUpdateManyWithWhereWithoutBuildInput | DisLikesUpdateManyWithWhereWithoutBuildInput[]
    deleteMany?: DisLikesScalarWhereInput | DisLikesScalarWhereInput[]
  }

  export type LikesUncheckedUpdateManyWithoutBuildNestedInput = {
    create?: XOR<LikesCreateWithoutBuildInput, LikesUncheckedCreateWithoutBuildInput> | LikesCreateWithoutBuildInput[] | LikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: LikesCreateOrConnectWithoutBuildInput | LikesCreateOrConnectWithoutBuildInput[]
    upsert?: LikesUpsertWithWhereUniqueWithoutBuildInput | LikesUpsertWithWhereUniqueWithoutBuildInput[]
    createMany?: LikesCreateManyBuildInputEnvelope
    set?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    disconnect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    delete?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    connect?: LikesWhereUniqueInput | LikesWhereUniqueInput[]
    update?: LikesUpdateWithWhereUniqueWithoutBuildInput | LikesUpdateWithWhereUniqueWithoutBuildInput[]
    updateMany?: LikesUpdateManyWithWhereWithoutBuildInput | LikesUpdateManyWithWhereWithoutBuildInput[]
    deleteMany?: LikesScalarWhereInput | LikesScalarWhereInput[]
  }

  export type DisLikesUncheckedUpdateManyWithoutBuildNestedInput = {
    create?: XOR<DisLikesCreateWithoutBuildInput, DisLikesUncheckedCreateWithoutBuildInput> | DisLikesCreateWithoutBuildInput[] | DisLikesUncheckedCreateWithoutBuildInput[]
    connectOrCreate?: DisLikesCreateOrConnectWithoutBuildInput | DisLikesCreateOrConnectWithoutBuildInput[]
    upsert?: DisLikesUpsertWithWhereUniqueWithoutBuildInput | DisLikesUpsertWithWhereUniqueWithoutBuildInput[]
    createMany?: DisLikesCreateManyBuildInputEnvelope
    set?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    disconnect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    delete?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    connect?: DisLikesWhereUniqueInput | DisLikesWhereUniqueInput[]
    update?: DisLikesUpdateWithWhereUniqueWithoutBuildInput | DisLikesUpdateWithWhereUniqueWithoutBuildInput[]
    updateMany?: DisLikesUpdateManyWithWhereWithoutBuildInput | DisLikesUpdateManyWithWhereWithoutBuildInput[]
    deleteMany?: DisLikesScalarWhereInput | DisLikesScalarWhereInput[]
  }

  export type UserCreateNestedOneWithoutLikesBuildsInput = {
    create?: XOR<UserCreateWithoutLikesBuildsInput, UserUncheckedCreateWithoutLikesBuildsInput>
    connectOrCreate?: UserCreateOrConnectWithoutLikesBuildsInput
    connect?: UserWhereUniqueInput
  }

  export type BuildCreateNestedOneWithoutLikesInput = {
    create?: XOR<BuildCreateWithoutLikesInput, BuildUncheckedCreateWithoutLikesInput>
    connectOrCreate?: BuildCreateOrConnectWithoutLikesInput
    connect?: BuildWhereUniqueInput
  }

  export type UserUpdateOneRequiredWithoutLikesBuildsNestedInput = {
    create?: XOR<UserCreateWithoutLikesBuildsInput, UserUncheckedCreateWithoutLikesBuildsInput>
    connectOrCreate?: UserCreateOrConnectWithoutLikesBuildsInput
    upsert?: UserUpsertWithoutLikesBuildsInput
    connect?: UserWhereUniqueInput
    update?: XOR<XOR<UserUpdateToOneWithWhereWithoutLikesBuildsInput, UserUpdateWithoutLikesBuildsInput>, UserUncheckedUpdateWithoutLikesBuildsInput>
  }

  export type BuildUpdateOneRequiredWithoutLikesNestedInput = {
    create?: XOR<BuildCreateWithoutLikesInput, BuildUncheckedCreateWithoutLikesInput>
    connectOrCreate?: BuildCreateOrConnectWithoutLikesInput
    upsert?: BuildUpsertWithoutLikesInput
    connect?: BuildWhereUniqueInput
    update?: XOR<XOR<BuildUpdateToOneWithWhereWithoutLikesInput, BuildUpdateWithoutLikesInput>, BuildUncheckedUpdateWithoutLikesInput>
  }

  export type UserCreateNestedOneWithoutDislikesBuildsInput = {
    create?: XOR<UserCreateWithoutDislikesBuildsInput, UserUncheckedCreateWithoutDislikesBuildsInput>
    connectOrCreate?: UserCreateOrConnectWithoutDislikesBuildsInput
    connect?: UserWhereUniqueInput
  }

  export type BuildCreateNestedOneWithoutDisLikesInput = {
    create?: XOR<BuildCreateWithoutDisLikesInput, BuildUncheckedCreateWithoutDisLikesInput>
    connectOrCreate?: BuildCreateOrConnectWithoutDisLikesInput
    connect?: BuildWhereUniqueInput
  }

  export type UserUpdateOneRequiredWithoutDislikesBuildsNestedInput = {
    create?: XOR<UserCreateWithoutDislikesBuildsInput, UserUncheckedCreateWithoutDislikesBuildsInput>
    connectOrCreate?: UserCreateOrConnectWithoutDislikesBuildsInput
    upsert?: UserUpsertWithoutDislikesBuildsInput
    connect?: UserWhereUniqueInput
    update?: XOR<XOR<UserUpdateToOneWithWhereWithoutDislikesBuildsInput, UserUpdateWithoutDislikesBuildsInput>, UserUncheckedUpdateWithoutDislikesBuildsInput>
  }

  export type BuildUpdateOneRequiredWithoutDisLikesNestedInput = {
    create?: XOR<BuildCreateWithoutDisLikesInput, BuildUncheckedCreateWithoutDisLikesInput>
    connectOrCreate?: BuildCreateOrConnectWithoutDisLikesInput
    upsert?: BuildUpsertWithoutDisLikesInput
    connect?: BuildWhereUniqueInput
    update?: XOR<XOR<BuildUpdateToOneWithWhereWithoutDisLikesInput, BuildUpdateWithoutDisLikesInput>, BuildUncheckedUpdateWithoutDisLikesInput>
  }

  export type TournamentCreatecastersInput = {
    set: string[]
  }

  export type TournamentCreaterequestsInput = {
    set: string[]
  }

  export type UserCreateNestedOneWithoutTournamentsInput = {
    create?: XOR<UserCreateWithoutTournamentsInput, UserUncheckedCreateWithoutTournamentsInput>
    connectOrCreate?: UserCreateOrConnectWithoutTournamentsInput
    connect?: UserWhereUniqueInput
  }

  export type EnumTournamentPickModeFieldUpdateOperationsInput = {
    set?: $Enums.TournamentPickMode
  }

  export type EnumGameModeFieldUpdateOperationsInput = {
    set?: $Enums.GameMode
  }

  export type EnumTournamentStatusFieldUpdateOperationsInput = {
    set?: $Enums.TournamentStatus
  }

  export type TournamentUpdatecastersInput = {
    set?: string[]
    push?: string | string[]
  }

  export type TournamentUpdaterequestsInput = {
    set?: string[]
    push?: string | string[]
  }

  export type EnumBracketTypeFieldUpdateOperationsInput = {
    set?: $Enums.BracketType
  }

  export type UserUpdateOneRequiredWithoutTournamentsNestedInput = {
    create?: XOR<UserCreateWithoutTournamentsInput, UserUncheckedCreateWithoutTournamentsInput>
    connectOrCreate?: UserCreateOrConnectWithoutTournamentsInput
    upsert?: UserUpsertWithoutTournamentsInput
    connect?: UserWhereUniqueInput
    update?: XOR<XOR<UserUpdateToOneWithWhereWithoutTournamentsInput, UserUpdateWithoutTournamentsInput>, UserUncheckedUpdateWithoutTournamentsInput>
  }

  export type EnumItemTypeFieldUpdateOperationsInput = {
    set?: $Enums.ItemType
  }

  export type EnumLogTypeFieldUpdateOperationsInput = {
    set?: $Enums.LogType
  }

  export type TeamCreatetournamentWinsInput = {
    set: number[]
  }

  export type TeamCreaterequestsInput = {
    set: string[]
  }

  export type UserCreateNestedOneWithoutOwnedTeamInput = {
    create?: XOR<UserCreateWithoutOwnedTeamInput, UserUncheckedCreateWithoutOwnedTeamInput>
    connectOrCreate?: UserCreateOrConnectWithoutOwnedTeamInput
    connect?: UserWhereUniqueInput
  }

  export type UserCreateNestedManyWithoutTeamInput = {
    create?: XOR<UserCreateWithoutTeamInput, UserUncheckedCreateWithoutTeamInput> | UserCreateWithoutTeamInput[] | UserUncheckedCreateWithoutTeamInput[]
    connectOrCreate?: UserCreateOrConnectWithoutTeamInput | UserCreateOrConnectWithoutTeamInput[]
    createMany?: UserCreateManyTeamInputEnvelope
    connect?: UserWhereUniqueInput | UserWhereUniqueInput[]
  }

  export type UserUncheckedCreateNestedManyWithoutTeamInput = {
    create?: XOR<UserCreateWithoutTeamInput, UserUncheckedCreateWithoutTeamInput> | UserCreateWithoutTeamInput[] | UserUncheckedCreateWithoutTeamInput[]
    connectOrCreate?: UserCreateOrConnectWithoutTeamInput | UserCreateOrConnectWithoutTeamInput[]
    createMany?: UserCreateManyTeamInputEnvelope
    connect?: UserWhereUniqueInput | UserWhereUniqueInput[]
  }

  export type DecimalFieldUpdateOperationsInput = {
    set?: Decimal | DecimalJsLike | number | string
    increment?: Decimal | DecimalJsLike | number | string
    decrement?: Decimal | DecimalJsLike | number | string
    multiply?: Decimal | DecimalJsLike | number | string
    divide?: Decimal | DecimalJsLike | number | string
  }

  export type TeamUpdatetournamentWinsInput = {
    set?: number[]
    push?: number | number[]
  }

  export type TeamUpdaterequestsInput = {
    set?: string[]
    push?: string | string[]
  }

  export type UserUpdateOneRequiredWithoutOwnedTeamNestedInput = {
    create?: XOR<UserCreateWithoutOwnedTeamInput, UserUncheckedCreateWithoutOwnedTeamInput>
    connectOrCreate?: UserCreateOrConnectWithoutOwnedTeamInput
    upsert?: UserUpsertWithoutOwnedTeamInput
    connect?: UserWhereUniqueInput
    update?: XOR<XOR<UserUpdateToOneWithWhereWithoutOwnedTeamInput, UserUpdateWithoutOwnedTeamInput>, UserUncheckedUpdateWithoutOwnedTeamInput>
  }

  export type UserUpdateManyWithoutTeamNestedInput = {
    create?: XOR<UserCreateWithoutTeamInput, UserUncheckedCreateWithoutTeamInput> | UserCreateWithoutTeamInput[] | UserUncheckedCreateWithoutTeamInput[]
    connectOrCreate?: UserCreateOrConnectWithoutTeamInput | UserCreateOrConnectWithoutTeamInput[]
    upsert?: UserUpsertWithWhereUniqueWithoutTeamInput | UserUpsertWithWhereUniqueWithoutTeamInput[]
    createMany?: UserCreateManyTeamInputEnvelope
    set?: UserWhereUniqueInput | UserWhereUniqueInput[]
    disconnect?: UserWhereUniqueInput | UserWhereUniqueInput[]
    delete?: UserWhereUniqueInput | UserWhereUniqueInput[]
    connect?: UserWhereUniqueInput | UserWhereUniqueInput[]
    update?: UserUpdateWithWhereUniqueWithoutTeamInput | UserUpdateWithWhereUniqueWithoutTeamInput[]
    updateMany?: UserUpdateManyWithWhereWithoutTeamInput | UserUpdateManyWithWhereWithoutTeamInput[]
    deleteMany?: UserScalarWhereInput | UserScalarWhereInput[]
  }

  export type UserUncheckedUpdateManyWithoutTeamNestedInput = {
    create?: XOR<UserCreateWithoutTeamInput, UserUncheckedCreateWithoutTeamInput> | UserCreateWithoutTeamInput[] | UserUncheckedCreateWithoutTeamInput[]
    connectOrCreate?: UserCreateOrConnectWithoutTeamInput | UserCreateOrConnectWithoutTeamInput[]
    upsert?: UserUpsertWithWhereUniqueWithoutTeamInput | UserUpsertWithWhereUniqueWithoutTeamInput[]
    createMany?: UserCreateManyTeamInputEnvelope
    set?: UserWhereUniqueInput | UserWhereUniqueInput[]
    disconnect?: UserWhereUniqueInput | UserWhereUniqueInput[]
    delete?: UserWhereUniqueInput | UserWhereUniqueInput[]
    connect?: UserWhereUniqueInput | UserWhereUniqueInput[]
    update?: UserUpdateWithWhereUniqueWithoutTeamInput | UserUpdateWithWhereUniqueWithoutTeamInput[]
    updateMany?: UserUpdateManyWithWhereWithoutTeamInput | UserUpdateManyWithWhereWithoutTeamInput[]
    deleteMany?: UserScalarWhereInput | UserScalarWhereInput[]
  }

  export type NewsCreatechampionsInput = {
    set: string[]
  }

  export type NewsCreateitemsInput = {
    set: string[]
  }

  export type NewsUpdatechampionsInput = {
    set?: string[]
    push?: string | string[]
  }

  export type NewsUpdateitemsInput = {
    set?: string[]
    push?: string | string[]
  }

  export type EnumCreatureTypeFieldUpdateOperationsInput = {
    set?: $Enums.CreatureType
  }

  export type NestedIntFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel>
    in?: number[] | ListIntFieldRefInput<$PrismaModel>
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel>
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntFilter<$PrismaModel> | number
  }

  export type NestedStringFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel>
    in?: string[] | ListStringFieldRefInput<$PrismaModel>
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel>
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    not?: NestedStringFilter<$PrismaModel> | string
  }

  export type NestedBoolFilter<$PrismaModel = never> = {
    equals?: boolean | BooleanFieldRefInput<$PrismaModel>
    not?: NestedBoolFilter<$PrismaModel> | boolean
  }

  export type NestedIntNullableFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel> | null
    in?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntNullableFilter<$PrismaModel> | number | null
  }

  export type NestedIntWithAggregatesFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel>
    in?: number[] | ListIntFieldRefInput<$PrismaModel>
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel>
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntWithAggregatesFilter<$PrismaModel> | number
    _count?: NestedIntFilter<$PrismaModel>
    _avg?: NestedFloatFilter<$PrismaModel>
    _sum?: NestedIntFilter<$PrismaModel>
    _min?: NestedIntFilter<$PrismaModel>
    _max?: NestedIntFilter<$PrismaModel>
  }

  export type NestedFloatFilter<$PrismaModel = never> = {
    equals?: number | FloatFieldRefInput<$PrismaModel>
    in?: number[] | ListFloatFieldRefInput<$PrismaModel>
    notIn?: number[] | ListFloatFieldRefInput<$PrismaModel>
    lt?: number | FloatFieldRefInput<$PrismaModel>
    lte?: number | FloatFieldRefInput<$PrismaModel>
    gt?: number | FloatFieldRefInput<$PrismaModel>
    gte?: number | FloatFieldRefInput<$PrismaModel>
    not?: NestedFloatFilter<$PrismaModel> | number
  }

  export type NestedStringWithAggregatesFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel>
    in?: string[] | ListStringFieldRefInput<$PrismaModel>
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel>
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    not?: NestedStringWithAggregatesFilter<$PrismaModel> | string
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedStringFilter<$PrismaModel>
    _max?: NestedStringFilter<$PrismaModel>
  }

  export type NestedBoolWithAggregatesFilter<$PrismaModel = never> = {
    equals?: boolean | BooleanFieldRefInput<$PrismaModel>
    not?: NestedBoolWithAggregatesFilter<$PrismaModel> | boolean
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedBoolFilter<$PrismaModel>
    _max?: NestedBoolFilter<$PrismaModel>
  }

  export type NestedIntNullableWithAggregatesFilter<$PrismaModel = never> = {
    equals?: number | IntFieldRefInput<$PrismaModel> | null
    in?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    notIn?: number[] | ListIntFieldRefInput<$PrismaModel> | null
    lt?: number | IntFieldRefInput<$PrismaModel>
    lte?: number | IntFieldRefInput<$PrismaModel>
    gt?: number | IntFieldRefInput<$PrismaModel>
    gte?: number | IntFieldRefInput<$PrismaModel>
    not?: NestedIntNullableWithAggregatesFilter<$PrismaModel> | number | null
    _count?: NestedIntNullableFilter<$PrismaModel>
    _avg?: NestedFloatNullableFilter<$PrismaModel>
    _sum?: NestedIntNullableFilter<$PrismaModel>
    _min?: NestedIntNullableFilter<$PrismaModel>
    _max?: NestedIntNullableFilter<$PrismaModel>
  }

  export type NestedFloatNullableFilter<$PrismaModel = never> = {
    equals?: number | FloatFieldRefInput<$PrismaModel> | null
    in?: number[] | ListFloatFieldRefInput<$PrismaModel> | null
    notIn?: number[] | ListFloatFieldRefInput<$PrismaModel> | null
    lt?: number | FloatFieldRefInput<$PrismaModel>
    lte?: number | FloatFieldRefInput<$PrismaModel>
    gt?: number | FloatFieldRefInput<$PrismaModel>
    gte?: number | FloatFieldRefInput<$PrismaModel>
    not?: NestedFloatNullableFilter<$PrismaModel> | number | null
  }

  export type NestedEnumRoleFilter<$PrismaModel = never> = {
    equals?: $Enums.Role | EnumRoleFieldRefInput<$PrismaModel>
    in?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    notIn?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    not?: NestedEnumRoleFilter<$PrismaModel> | $Enums.Role
  }

  export type NestedEnumRoleWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.Role | EnumRoleFieldRefInput<$PrismaModel>
    in?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    notIn?: $Enums.Role[] | ListEnumRoleFieldRefInput<$PrismaModel>
    not?: NestedEnumRoleWithAggregatesFilter<$PrismaModel> | $Enums.Role
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumRoleFilter<$PrismaModel>
    _max?: NestedEnumRoleFilter<$PrismaModel>
  }

  export type NestedDateTimeFilter<$PrismaModel = never> = {
    equals?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    in?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    notIn?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    lt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    lte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    not?: NestedDateTimeFilter<$PrismaModel> | Date | string
  }

  export type NestedStringNullableFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel> | null
    in?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    not?: NestedStringNullableFilter<$PrismaModel> | string | null
  }

  export type NestedDateTimeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    in?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    notIn?: Date[] | string[] | ListDateTimeFieldRefInput<$PrismaModel>
    lt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    lte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gt?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    gte?: Date | string | DateTimeFieldRefInput<$PrismaModel>
    not?: NestedDateTimeWithAggregatesFilter<$PrismaModel> | Date | string
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedDateTimeFilter<$PrismaModel>
    _max?: NestedDateTimeFilter<$PrismaModel>
  }

  export type NestedStringNullableWithAggregatesFilter<$PrismaModel = never> = {
    equals?: string | StringFieldRefInput<$PrismaModel> | null
    in?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    notIn?: string[] | ListStringFieldRefInput<$PrismaModel> | null
    lt?: string | StringFieldRefInput<$PrismaModel>
    lte?: string | StringFieldRefInput<$PrismaModel>
    gt?: string | StringFieldRefInput<$PrismaModel>
    gte?: string | StringFieldRefInput<$PrismaModel>
    contains?: string | StringFieldRefInput<$PrismaModel>
    startsWith?: string | StringFieldRefInput<$PrismaModel>
    endsWith?: string | StringFieldRefInput<$PrismaModel>
    not?: NestedStringNullableWithAggregatesFilter<$PrismaModel> | string | null
    _count?: NestedIntNullableFilter<$PrismaModel>
    _min?: NestedStringNullableFilter<$PrismaModel>
    _max?: NestedStringNullableFilter<$PrismaModel>
  }

  export type NestedEnumTournamentPickModeFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentPickMode | EnumTournamentPickModeFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentPickModeFilter<$PrismaModel> | $Enums.TournamentPickMode
  }

  export type NestedEnumGameModeFilter<$PrismaModel = never> = {
    equals?: $Enums.GameMode | EnumGameModeFieldRefInput<$PrismaModel>
    in?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    not?: NestedEnumGameModeFilter<$PrismaModel> | $Enums.GameMode
  }

  export type NestedEnumTournamentStatusFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentStatus | EnumTournamentStatusFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentStatusFilter<$PrismaModel> | $Enums.TournamentStatus
  }

  export type NestedEnumBracketTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.BracketType | EnumBracketTypeFieldRefInput<$PrismaModel>
    in?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumBracketTypeFilter<$PrismaModel> | $Enums.BracketType
  }

  export type NestedEnumTournamentPickModeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentPickMode | EnumTournamentPickModeFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentPickMode[] | ListEnumTournamentPickModeFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentPickModeWithAggregatesFilter<$PrismaModel> | $Enums.TournamentPickMode
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumTournamentPickModeFilter<$PrismaModel>
    _max?: NestedEnumTournamentPickModeFilter<$PrismaModel>
  }

  export type NestedEnumGameModeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.GameMode | EnumGameModeFieldRefInput<$PrismaModel>
    in?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    notIn?: $Enums.GameMode[] | ListEnumGameModeFieldRefInput<$PrismaModel>
    not?: NestedEnumGameModeWithAggregatesFilter<$PrismaModel> | $Enums.GameMode
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumGameModeFilter<$PrismaModel>
    _max?: NestedEnumGameModeFilter<$PrismaModel>
  }

  export type NestedEnumTournamentStatusWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.TournamentStatus | EnumTournamentStatusFieldRefInput<$PrismaModel>
    in?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    notIn?: $Enums.TournamentStatus[] | ListEnumTournamentStatusFieldRefInput<$PrismaModel>
    not?: NestedEnumTournamentStatusWithAggregatesFilter<$PrismaModel> | $Enums.TournamentStatus
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumTournamentStatusFilter<$PrismaModel>
    _max?: NestedEnumTournamentStatusFilter<$PrismaModel>
  }

  export type NestedEnumBracketTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.BracketType | EnumBracketTypeFieldRefInput<$PrismaModel>
    in?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.BracketType[] | ListEnumBracketTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumBracketTypeWithAggregatesFilter<$PrismaModel> | $Enums.BracketType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumBracketTypeFilter<$PrismaModel>
    _max?: NestedEnumBracketTypeFilter<$PrismaModel>
  }

  export type NestedEnumItemTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.ItemType | EnumItemTypeFieldRefInput<$PrismaModel>
    in?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumItemTypeFilter<$PrismaModel> | $Enums.ItemType
  }

  export type NestedEnumItemTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.ItemType | EnumItemTypeFieldRefInput<$PrismaModel>
    in?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.ItemType[] | ListEnumItemTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumItemTypeWithAggregatesFilter<$PrismaModel> | $Enums.ItemType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumItemTypeFilter<$PrismaModel>
    _max?: NestedEnumItemTypeFilter<$PrismaModel>
  }

  export type NestedEnumLogTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.LogType | EnumLogTypeFieldRefInput<$PrismaModel>
    in?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumLogTypeFilter<$PrismaModel> | $Enums.LogType
  }

  export type NestedEnumLogTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.LogType | EnumLogTypeFieldRefInput<$PrismaModel>
    in?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.LogType[] | ListEnumLogTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumLogTypeWithAggregatesFilter<$PrismaModel> | $Enums.LogType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumLogTypeFilter<$PrismaModel>
    _max?: NestedEnumLogTypeFilter<$PrismaModel>
  }

  export type NestedDecimalFilter<$PrismaModel = never> = {
    equals?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    in?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    notIn?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    lt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    lte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    not?: NestedDecimalFilter<$PrismaModel> | Decimal | DecimalJsLike | number | string
  }

  export type NestedDecimalWithAggregatesFilter<$PrismaModel = never> = {
    equals?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    in?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    notIn?: Decimal[] | DecimalJsLike[] | number[] | string[] | ListDecimalFieldRefInput<$PrismaModel>
    lt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    lte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gt?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    gte?: Decimal | DecimalJsLike | number | string | DecimalFieldRefInput<$PrismaModel>
    not?: NestedDecimalWithAggregatesFilter<$PrismaModel> | Decimal | DecimalJsLike | number | string
    _count?: NestedIntFilter<$PrismaModel>
    _avg?: NestedDecimalFilter<$PrismaModel>
    _sum?: NestedDecimalFilter<$PrismaModel>
    _min?: NestedDecimalFilter<$PrismaModel>
    _max?: NestedDecimalFilter<$PrismaModel>
  }

  export type NestedEnumCreatureTypeFilter<$PrismaModel = never> = {
    equals?: $Enums.CreatureType | EnumCreatureTypeFieldRefInput<$PrismaModel>
    in?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumCreatureTypeFilter<$PrismaModel> | $Enums.CreatureType
  }

  export type NestedEnumCreatureTypeWithAggregatesFilter<$PrismaModel = never> = {
    equals?: $Enums.CreatureType | EnumCreatureTypeFieldRefInput<$PrismaModel>
    in?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    notIn?: $Enums.CreatureType[] | ListEnumCreatureTypeFieldRefInput<$PrismaModel>
    not?: NestedEnumCreatureTypeWithAggregatesFilter<$PrismaModel> | $Enums.CreatureType
    _count?: NestedIntFilter<$PrismaModel>
    _min?: NestedEnumCreatureTypeFilter<$PrismaModel>
    _max?: NestedEnumCreatureTypeFilter<$PrismaModel>
  }

  export type TeamCreateWithoutMembersInput = {
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    rating?: Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    requests?: TeamCreaterequestsInput | string[]
    owner: UserCreateNestedOneWithoutOwnedTeamInput
  }

  export type TeamUncheckedCreateWithoutMembersInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    rating?: Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    ownerID: number
    requests?: TeamCreaterequestsInput | string[]
  }

  export type TeamCreateOrConnectWithoutMembersInput = {
    where: TeamWhereUniqueInput
    create: XOR<TeamCreateWithoutMembersInput, TeamUncheckedCreateWithoutMembersInput>
  }

  export type TeamCreateWithoutOwnerInput = {
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    rating?: Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    requests?: TeamCreaterequestsInput | string[]
    members?: UserCreateNestedManyWithoutTeamInput
  }

  export type TeamUncheckedCreateWithoutOwnerInput = {
    id?: number
    createdAt?: Date | string
    name: string
    description: string
    icon: string
    rating?: Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    requests?: TeamCreaterequestsInput | string[]
    members?: UserUncheckedCreateNestedManyWithoutTeamInput
  }

  export type TeamCreateOrConnectWithoutOwnerInput = {
    where: TeamWhereUniqueInput
    create: XOR<TeamCreateWithoutOwnerInput, TeamUncheckedCreateWithoutOwnerInput>
  }

  export type BuildCreateWithoutCreatorInput = {
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    champion: ChampionCreateNestedOneWithoutBuildsInput
    Likes?: LikesCreateNestedManyWithoutBuildInput
    DisLikes?: DisLikesCreateNestedManyWithoutBuildInput
  }

  export type BuildUncheckedCreateWithoutCreatorInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    championID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    Likes?: LikesUncheckedCreateNestedManyWithoutBuildInput
    DisLikes?: DisLikesUncheckedCreateNestedManyWithoutBuildInput
  }

  export type BuildCreateOrConnectWithoutCreatorInput = {
    where: BuildWhereUniqueInput
    create: XOR<BuildCreateWithoutCreatorInput, BuildUncheckedCreateWithoutCreatorInput>
  }

  export type BuildCreateManyCreatorInputEnvelope = {
    data: BuildCreateManyCreatorInput | BuildCreateManyCreatorInput[]
    skipDuplicates?: boolean
  }

  export type TournamentCreateWithoutCreatorInput = {
    createdAt?: Date | string
    updatedAt?: Date | string
    startedAt: Date | string
    finishedAt: Date | string
    name: string
    description: string
    prize: string
    icon?: string | null
    map?: string | null
    pickMode?: $Enums.TournamentPickMode
    gameMode?: $Enums.GameMode
    status?: $Enums.TournamentStatus
    teamSize?: number
    slots?: number
    teamRequest?: boolean
    maxRequests?: number
    casters?: TournamentCreatecastersInput | string[]
    videoLink?: string | null
    teams: string
    requests?: TournamentCreaterequestsInput | string[]
    bracket?: string | null
    bracketType?: $Enums.BracketType
    matchesCount?: number
    results?: string | null
  }

  export type TournamentUncheckedCreateWithoutCreatorInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    startedAt: Date | string
    finishedAt: Date | string
    name: string
    description: string
    prize: string
    icon?: string | null
    map?: string | null
    pickMode?: $Enums.TournamentPickMode
    gameMode?: $Enums.GameMode
    status?: $Enums.TournamentStatus
    teamSize?: number
    slots?: number
    teamRequest?: boolean
    maxRequests?: number
    casters?: TournamentCreatecastersInput | string[]
    videoLink?: string | null
    teams: string
    requests?: TournamentCreaterequestsInput | string[]
    bracket?: string | null
    bracketType?: $Enums.BracketType
    matchesCount?: number
    results?: string | null
  }

  export type TournamentCreateOrConnectWithoutCreatorInput = {
    where: TournamentWhereUniqueInput
    create: XOR<TournamentCreateWithoutCreatorInput, TournamentUncheckedCreateWithoutCreatorInput>
  }

  export type TournamentCreateManyCreatorInputEnvelope = {
    data: TournamentCreateManyCreatorInput | TournamentCreateManyCreatorInput[]
    skipDuplicates?: boolean
  }

  export type LikesCreateWithoutUserInput = {
    assignedAt?: Date | string
    build: BuildCreateNestedOneWithoutLikesInput
  }

  export type LikesUncheckedCreateWithoutUserInput = {
    buildID: number
    assignedAt?: Date | string
  }

  export type LikesCreateOrConnectWithoutUserInput = {
    where: LikesWhereUniqueInput
    create: XOR<LikesCreateWithoutUserInput, LikesUncheckedCreateWithoutUserInput>
  }

  export type LikesCreateManyUserInputEnvelope = {
    data: LikesCreateManyUserInput | LikesCreateManyUserInput[]
    skipDuplicates?: boolean
  }

  export type DisLikesCreateWithoutUserInput = {
    assignedAt?: Date | string
    build: BuildCreateNestedOneWithoutDisLikesInput
  }

  export type DisLikesUncheckedCreateWithoutUserInput = {
    buildID: number
    assignedAt?: Date | string
  }

  export type DisLikesCreateOrConnectWithoutUserInput = {
    where: DisLikesWhereUniqueInput
    create: XOR<DisLikesCreateWithoutUserInput, DisLikesUncheckedCreateWithoutUserInput>
  }

  export type DisLikesCreateManyUserInputEnvelope = {
    data: DisLikesCreateManyUserInput | DisLikesCreateManyUserInput[]
    skipDuplicates?: boolean
  }

  export type TeamUpsertWithoutMembersInput = {
    update: XOR<TeamUpdateWithoutMembersInput, TeamUncheckedUpdateWithoutMembersInput>
    create: XOR<TeamCreateWithoutMembersInput, TeamUncheckedCreateWithoutMembersInput>
    where?: TeamWhereInput
  }

  export type TeamUpdateToOneWithWhereWithoutMembersInput = {
    where?: TeamWhereInput
    data: XOR<TeamUpdateWithoutMembersInput, TeamUncheckedUpdateWithoutMembersInput>
  }

  export type TeamUpdateWithoutMembersInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
    owner?: UserUpdateOneRequiredWithoutOwnedTeamNestedInput
  }

  export type TeamUncheckedUpdateWithoutMembersInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    ownerID?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
  }

  export type TeamUpsertWithoutOwnerInput = {
    update: XOR<TeamUpdateWithoutOwnerInput, TeamUncheckedUpdateWithoutOwnerInput>
    create: XOR<TeamCreateWithoutOwnerInput, TeamUncheckedCreateWithoutOwnerInput>
    where?: TeamWhereInput
  }

  export type TeamUpdateToOneWithWhereWithoutOwnerInput = {
    where?: TeamWhereInput
    data: XOR<TeamUpdateWithoutOwnerInput, TeamUncheckedUpdateWithoutOwnerInput>
  }

  export type TeamUpdateWithoutOwnerInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
    members?: UserUpdateManyWithoutTeamNestedInput
  }

  export type TeamUncheckedUpdateWithoutOwnerInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    rating?: DecimalFieldUpdateOperationsInput | Decimal | DecimalJsLike | number | string
    tournamentWins?: TeamUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    requests?: TeamUpdaterequestsInput | string[]
    members?: UserUncheckedUpdateManyWithoutTeamNestedInput
  }

  export type BuildUpsertWithWhereUniqueWithoutCreatorInput = {
    where: BuildWhereUniqueInput
    update: XOR<BuildUpdateWithoutCreatorInput, BuildUncheckedUpdateWithoutCreatorInput>
    create: XOR<BuildCreateWithoutCreatorInput, BuildUncheckedCreateWithoutCreatorInput>
  }

  export type BuildUpdateWithWhereUniqueWithoutCreatorInput = {
    where: BuildWhereUniqueInput
    data: XOR<BuildUpdateWithoutCreatorInput, BuildUncheckedUpdateWithoutCreatorInput>
  }

  export type BuildUpdateManyWithWhereWithoutCreatorInput = {
    where: BuildScalarWhereInput
    data: XOR<BuildUpdateManyMutationInput, BuildUncheckedUpdateManyWithoutCreatorInput>
  }

  export type BuildScalarWhereInput = {
    AND?: BuildScalarWhereInput | BuildScalarWhereInput[]
    OR?: BuildScalarWhereInput[]
    NOT?: BuildScalarWhereInput | BuildScalarWhereInput[]
    id?: IntFilter<"Build"> | number
    createdAt?: DateTimeFilter<"Build"> | Date | string
    updatedAt?: DateTimeFilter<"Build"> | Date | string
    name?: StringFilter<"Build"> | string
    role?: EnumRoleFilter<"Build"> | $Enums.Role
    championID?: IntFilter<"Build"> | number
    creatorID?: IntFilter<"Build"> | number
    items?: StringNullableListFilter<"Build">
    coreItems?: StringNullableListFilter<"Build">
    situationalItems?: StringNullableListFilter<"Build">
    skillsOrder?: StringNullableListFilter<"Build">
    karmas?: StringNullableListFilter<"Build">
    keyInsights?: StringNullableFilter<"Build"> | string | null
    strengths?: StringNullableFilter<"Build"> | string | null
    weaknesses?: StringNullableFilter<"Build"> | string | null
  }

  export type TournamentUpsertWithWhereUniqueWithoutCreatorInput = {
    where: TournamentWhereUniqueInput
    update: XOR<TournamentUpdateWithoutCreatorInput, TournamentUncheckedUpdateWithoutCreatorInput>
    create: XOR<TournamentCreateWithoutCreatorInput, TournamentUncheckedCreateWithoutCreatorInput>
  }

  export type TournamentUpdateWithWhereUniqueWithoutCreatorInput = {
    where: TournamentWhereUniqueInput
    data: XOR<TournamentUpdateWithoutCreatorInput, TournamentUncheckedUpdateWithoutCreatorInput>
  }

  export type TournamentUpdateManyWithWhereWithoutCreatorInput = {
    where: TournamentScalarWhereInput
    data: XOR<TournamentUpdateManyMutationInput, TournamentUncheckedUpdateManyWithoutCreatorInput>
  }

  export type TournamentScalarWhereInput = {
    AND?: TournamentScalarWhereInput | TournamentScalarWhereInput[]
    OR?: TournamentScalarWhereInput[]
    NOT?: TournamentScalarWhereInput | TournamentScalarWhereInput[]
    id?: IntFilter<"Tournament"> | number
    createdAt?: DateTimeFilter<"Tournament"> | Date | string
    updatedAt?: DateTimeFilter<"Tournament"> | Date | string
    startedAt?: DateTimeFilter<"Tournament"> | Date | string
    finishedAt?: DateTimeFilter<"Tournament"> | Date | string
    name?: StringFilter<"Tournament"> | string
    description?: StringFilter<"Tournament"> | string
    prize?: StringFilter<"Tournament"> | string
    icon?: StringNullableFilter<"Tournament"> | string | null
    map?: StringNullableFilter<"Tournament"> | string | null
    pickMode?: EnumTournamentPickModeFilter<"Tournament"> | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFilter<"Tournament"> | $Enums.GameMode
    status?: EnumTournamentStatusFilter<"Tournament"> | $Enums.TournamentStatus
    teamSize?: IntFilter<"Tournament"> | number
    slots?: IntFilter<"Tournament"> | number
    teamRequest?: BoolFilter<"Tournament"> | boolean
    maxRequests?: IntFilter<"Tournament"> | number
    casters?: StringNullableListFilter<"Tournament">
    videoLink?: StringNullableFilter<"Tournament"> | string | null
    creatorID?: IntFilter<"Tournament"> | number
    teams?: StringFilter<"Tournament"> | string
    requests?: StringNullableListFilter<"Tournament">
    bracket?: StringNullableFilter<"Tournament"> | string | null
    bracketType?: EnumBracketTypeFilter<"Tournament"> | $Enums.BracketType
    matchesCount?: IntFilter<"Tournament"> | number
    results?: StringNullableFilter<"Tournament"> | string | null
  }

  export type LikesUpsertWithWhereUniqueWithoutUserInput = {
    where: LikesWhereUniqueInput
    update: XOR<LikesUpdateWithoutUserInput, LikesUncheckedUpdateWithoutUserInput>
    create: XOR<LikesCreateWithoutUserInput, LikesUncheckedCreateWithoutUserInput>
  }

  export type LikesUpdateWithWhereUniqueWithoutUserInput = {
    where: LikesWhereUniqueInput
    data: XOR<LikesUpdateWithoutUserInput, LikesUncheckedUpdateWithoutUserInput>
  }

  export type LikesUpdateManyWithWhereWithoutUserInput = {
    where: LikesScalarWhereInput
    data: XOR<LikesUpdateManyMutationInput, LikesUncheckedUpdateManyWithoutUserInput>
  }

  export type LikesScalarWhereInput = {
    AND?: LikesScalarWhereInput | LikesScalarWhereInput[]
    OR?: LikesScalarWhereInput[]
    NOT?: LikesScalarWhereInput | LikesScalarWhereInput[]
    userID?: IntFilter<"Likes"> | number
    buildID?: IntFilter<"Likes"> | number
    assignedAt?: DateTimeFilter<"Likes"> | Date | string
  }

  export type DisLikesUpsertWithWhereUniqueWithoutUserInput = {
    where: DisLikesWhereUniqueInput
    update: XOR<DisLikesUpdateWithoutUserInput, DisLikesUncheckedUpdateWithoutUserInput>
    create: XOR<DisLikesCreateWithoutUserInput, DisLikesUncheckedCreateWithoutUserInput>
  }

  export type DisLikesUpdateWithWhereUniqueWithoutUserInput = {
    where: DisLikesWhereUniqueInput
    data: XOR<DisLikesUpdateWithoutUserInput, DisLikesUncheckedUpdateWithoutUserInput>
  }

  export type DisLikesUpdateManyWithWhereWithoutUserInput = {
    where: DisLikesScalarWhereInput
    data: XOR<DisLikesUpdateManyMutationInput, DisLikesUncheckedUpdateManyWithoutUserInput>
  }

  export type DisLikesScalarWhereInput = {
    AND?: DisLikesScalarWhereInput | DisLikesScalarWhereInput[]
    OR?: DisLikesScalarWhereInput[]
    NOT?: DisLikesScalarWhereInput | DisLikesScalarWhereInput[]
    userID?: IntFilter<"DisLikes"> | number
    buildID?: IntFilter<"DisLikes"> | number
    assignedAt?: DateTimeFilter<"DisLikes"> | Date | string
  }

  export type BuildCreateWithoutChampionInput = {
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    creator: UserCreateNestedOneWithoutBuildsInput
    Likes?: LikesCreateNestedManyWithoutBuildInput
    DisLikes?: DisLikesCreateNestedManyWithoutBuildInput
  }

  export type BuildUncheckedCreateWithoutChampionInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    creatorID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    Likes?: LikesUncheckedCreateNestedManyWithoutBuildInput
    DisLikes?: DisLikesUncheckedCreateNestedManyWithoutBuildInput
  }

  export type BuildCreateOrConnectWithoutChampionInput = {
    where: BuildWhereUniqueInput
    create: XOR<BuildCreateWithoutChampionInput, BuildUncheckedCreateWithoutChampionInput>
  }

  export type BuildCreateManyChampionInputEnvelope = {
    data: BuildCreateManyChampionInput | BuildCreateManyChampionInput[]
    skipDuplicates?: boolean
  }

  export type BuildUpsertWithWhereUniqueWithoutChampionInput = {
    where: BuildWhereUniqueInput
    update: XOR<BuildUpdateWithoutChampionInput, BuildUncheckedUpdateWithoutChampionInput>
    create: XOR<BuildCreateWithoutChampionInput, BuildUncheckedCreateWithoutChampionInput>
  }

  export type BuildUpdateWithWhereUniqueWithoutChampionInput = {
    where: BuildWhereUniqueInput
    data: XOR<BuildUpdateWithoutChampionInput, BuildUncheckedUpdateWithoutChampionInput>
  }

  export type BuildUpdateManyWithWhereWithoutChampionInput = {
    where: BuildScalarWhereInput
    data: XOR<BuildUpdateManyMutationInput, BuildUncheckedUpdateManyWithoutChampionInput>
  }

  export type ChampionCreateWithoutBuildsInput = {
    name: string
    role?: $Enums.Role
    tier?: number
    counterPicks?: ChampionCreatecounterPicksInput | string[]
  }

  export type ChampionUncheckedCreateWithoutBuildsInput = {
    id?: number
    name: string
    role?: $Enums.Role
    tier?: number
    counterPicks?: ChampionCreatecounterPicksInput | string[]
  }

  export type ChampionCreateOrConnectWithoutBuildsInput = {
    where: ChampionWhereUniqueInput
    create: XOR<ChampionCreateWithoutBuildsInput, ChampionUncheckedCreateWithoutBuildsInput>
  }

  export type UserCreateWithoutBuildsInput = {
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    team?: TeamCreateNestedOneWithoutMembersInput
    ownedTeam?: TeamCreateNestedOneWithoutOwnerInput
    tournaments?: TournamentCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesCreateNestedManyWithoutUserInput
  }

  export type UserUncheckedCreateWithoutBuildsInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    teamID?: number | null
    ownedTeam?: TeamUncheckedCreateNestedOneWithoutOwnerInput
    tournaments?: TournamentUncheckedCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesUncheckedCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesUncheckedCreateNestedManyWithoutUserInput
  }

  export type UserCreateOrConnectWithoutBuildsInput = {
    where: UserWhereUniqueInput
    create: XOR<UserCreateWithoutBuildsInput, UserUncheckedCreateWithoutBuildsInput>
  }

  export type LikesCreateWithoutBuildInput = {
    assignedAt?: Date | string
    user: UserCreateNestedOneWithoutLikesBuildsInput
  }

  export type LikesUncheckedCreateWithoutBuildInput = {
    userID: number
    assignedAt?: Date | string
  }

  export type LikesCreateOrConnectWithoutBuildInput = {
    where: LikesWhereUniqueInput
    create: XOR<LikesCreateWithoutBuildInput, LikesUncheckedCreateWithoutBuildInput>
  }

  export type LikesCreateManyBuildInputEnvelope = {
    data: LikesCreateManyBuildInput | LikesCreateManyBuildInput[]
    skipDuplicates?: boolean
  }

  export type DisLikesCreateWithoutBuildInput = {
    assignedAt?: Date | string
    user: UserCreateNestedOneWithoutDislikesBuildsInput
  }

  export type DisLikesUncheckedCreateWithoutBuildInput = {
    userID: number
    assignedAt?: Date | string
  }

  export type DisLikesCreateOrConnectWithoutBuildInput = {
    where: DisLikesWhereUniqueInput
    create: XOR<DisLikesCreateWithoutBuildInput, DisLikesUncheckedCreateWithoutBuildInput>
  }

  export type DisLikesCreateManyBuildInputEnvelope = {
    data: DisLikesCreateManyBuildInput | DisLikesCreateManyBuildInput[]
    skipDuplicates?: boolean
  }

  export type ChampionUpsertWithoutBuildsInput = {
    update: XOR<ChampionUpdateWithoutBuildsInput, ChampionUncheckedUpdateWithoutBuildsInput>
    create: XOR<ChampionCreateWithoutBuildsInput, ChampionUncheckedCreateWithoutBuildsInput>
    where?: ChampionWhereInput
  }

  export type ChampionUpdateToOneWithWhereWithoutBuildsInput = {
    where?: ChampionWhereInput
    data: XOR<ChampionUpdateWithoutBuildsInput, ChampionUncheckedUpdateWithoutBuildsInput>
  }

  export type ChampionUpdateWithoutBuildsInput = {
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    tier?: IntFieldUpdateOperationsInput | number
    counterPicks?: ChampionUpdatecounterPicksInput | string[]
  }

  export type ChampionUncheckedUpdateWithoutBuildsInput = {
    id?: IntFieldUpdateOperationsInput | number
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    tier?: IntFieldUpdateOperationsInput | number
    counterPicks?: ChampionUpdatecounterPicksInput | string[]
  }

  export type UserUpsertWithoutBuildsInput = {
    update: XOR<UserUpdateWithoutBuildsInput, UserUncheckedUpdateWithoutBuildsInput>
    create: XOR<UserCreateWithoutBuildsInput, UserUncheckedCreateWithoutBuildsInput>
    where?: UserWhereInput
  }

  export type UserUpdateToOneWithWhereWithoutBuildsInput = {
    where?: UserWhereInput
    data: XOR<UserUpdateWithoutBuildsInput, UserUncheckedUpdateWithoutBuildsInput>
  }

  export type UserUpdateWithoutBuildsInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    team?: TeamUpdateOneWithoutMembersNestedInput
    ownedTeam?: TeamUpdateOneWithoutOwnerNestedInput
    tournaments?: TournamentUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateWithoutBuildsInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    teamID?: NullableIntFieldUpdateOperationsInput | number | null
    ownedTeam?: TeamUncheckedUpdateOneWithoutOwnerNestedInput
    tournaments?: TournamentUncheckedUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUncheckedUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUncheckedUpdateManyWithoutUserNestedInput
  }

  export type LikesUpsertWithWhereUniqueWithoutBuildInput = {
    where: LikesWhereUniqueInput
    update: XOR<LikesUpdateWithoutBuildInput, LikesUncheckedUpdateWithoutBuildInput>
    create: XOR<LikesCreateWithoutBuildInput, LikesUncheckedCreateWithoutBuildInput>
  }

  export type LikesUpdateWithWhereUniqueWithoutBuildInput = {
    where: LikesWhereUniqueInput
    data: XOR<LikesUpdateWithoutBuildInput, LikesUncheckedUpdateWithoutBuildInput>
  }

  export type LikesUpdateManyWithWhereWithoutBuildInput = {
    where: LikesScalarWhereInput
    data: XOR<LikesUpdateManyMutationInput, LikesUncheckedUpdateManyWithoutBuildInput>
  }

  export type DisLikesUpsertWithWhereUniqueWithoutBuildInput = {
    where: DisLikesWhereUniqueInput
    update: XOR<DisLikesUpdateWithoutBuildInput, DisLikesUncheckedUpdateWithoutBuildInput>
    create: XOR<DisLikesCreateWithoutBuildInput, DisLikesUncheckedCreateWithoutBuildInput>
  }

  export type DisLikesUpdateWithWhereUniqueWithoutBuildInput = {
    where: DisLikesWhereUniqueInput
    data: XOR<DisLikesUpdateWithoutBuildInput, DisLikesUncheckedUpdateWithoutBuildInput>
  }

  export type DisLikesUpdateManyWithWhereWithoutBuildInput = {
    where: DisLikesScalarWhereInput
    data: XOR<DisLikesUpdateManyMutationInput, DisLikesUncheckedUpdateManyWithoutBuildInput>
  }

  export type UserCreateWithoutLikesBuildsInput = {
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    team?: TeamCreateNestedOneWithoutMembersInput
    ownedTeam?: TeamCreateNestedOneWithoutOwnerInput
    builds?: BuildCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentCreateNestedManyWithoutCreatorInput
    dislikesBuilds?: DisLikesCreateNestedManyWithoutUserInput
  }

  export type UserUncheckedCreateWithoutLikesBuildsInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    teamID?: number | null
    ownedTeam?: TeamUncheckedCreateNestedOneWithoutOwnerInput
    builds?: BuildUncheckedCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentUncheckedCreateNestedManyWithoutCreatorInput
    dislikesBuilds?: DisLikesUncheckedCreateNestedManyWithoutUserInput
  }

  export type UserCreateOrConnectWithoutLikesBuildsInput = {
    where: UserWhereUniqueInput
    create: XOR<UserCreateWithoutLikesBuildsInput, UserUncheckedCreateWithoutLikesBuildsInput>
  }

  export type BuildCreateWithoutLikesInput = {
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    champion: ChampionCreateNestedOneWithoutBuildsInput
    creator: UserCreateNestedOneWithoutBuildsInput
    DisLikes?: DisLikesCreateNestedManyWithoutBuildInput
  }

  export type BuildUncheckedCreateWithoutLikesInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    championID: number
    creatorID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    DisLikes?: DisLikesUncheckedCreateNestedManyWithoutBuildInput
  }

  export type BuildCreateOrConnectWithoutLikesInput = {
    where: BuildWhereUniqueInput
    create: XOR<BuildCreateWithoutLikesInput, BuildUncheckedCreateWithoutLikesInput>
  }

  export type UserUpsertWithoutLikesBuildsInput = {
    update: XOR<UserUpdateWithoutLikesBuildsInput, UserUncheckedUpdateWithoutLikesBuildsInput>
    create: XOR<UserCreateWithoutLikesBuildsInput, UserUncheckedCreateWithoutLikesBuildsInput>
    where?: UserWhereInput
  }

  export type UserUpdateToOneWithWhereWithoutLikesBuildsInput = {
    where?: UserWhereInput
    data: XOR<UserUpdateWithoutLikesBuildsInput, UserUncheckedUpdateWithoutLikesBuildsInput>
  }

  export type UserUpdateWithoutLikesBuildsInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    team?: TeamUpdateOneWithoutMembersNestedInput
    ownedTeam?: TeamUpdateOneWithoutOwnerNestedInput
    builds?: BuildUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUpdateManyWithoutCreatorNestedInput
    dislikesBuilds?: DisLikesUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateWithoutLikesBuildsInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    teamID?: NullableIntFieldUpdateOperationsInput | number | null
    ownedTeam?: TeamUncheckedUpdateOneWithoutOwnerNestedInput
    builds?: BuildUncheckedUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUncheckedUpdateManyWithoutCreatorNestedInput
    dislikesBuilds?: DisLikesUncheckedUpdateManyWithoutUserNestedInput
  }

  export type BuildUpsertWithoutLikesInput = {
    update: XOR<BuildUpdateWithoutLikesInput, BuildUncheckedUpdateWithoutLikesInput>
    create: XOR<BuildCreateWithoutLikesInput, BuildUncheckedCreateWithoutLikesInput>
    where?: BuildWhereInput
  }

  export type BuildUpdateToOneWithWhereWithoutLikesInput = {
    where?: BuildWhereInput
    data: XOR<BuildUpdateWithoutLikesInput, BuildUncheckedUpdateWithoutLikesInput>
  }

  export type BuildUpdateWithoutLikesInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    champion?: ChampionUpdateOneRequiredWithoutBuildsNestedInput
    creator?: UserUpdateOneRequiredWithoutBuildsNestedInput
    DisLikes?: DisLikesUpdateManyWithoutBuildNestedInput
  }

  export type BuildUncheckedUpdateWithoutLikesInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    championID?: IntFieldUpdateOperationsInput | number
    creatorID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    DisLikes?: DisLikesUncheckedUpdateManyWithoutBuildNestedInput
  }

  export type UserCreateWithoutDislikesBuildsInput = {
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    team?: TeamCreateNestedOneWithoutMembersInput
    ownedTeam?: TeamCreateNestedOneWithoutOwnerInput
    builds?: BuildCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesCreateNestedManyWithoutUserInput
  }

  export type UserUncheckedCreateWithoutDislikesBuildsInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    teamID?: number | null
    ownedTeam?: TeamUncheckedCreateNestedOneWithoutOwnerInput
    builds?: BuildUncheckedCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentUncheckedCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesUncheckedCreateNestedManyWithoutUserInput
  }

  export type UserCreateOrConnectWithoutDislikesBuildsInput = {
    where: UserWhereUniqueInput
    create: XOR<UserCreateWithoutDislikesBuildsInput, UserUncheckedCreateWithoutDislikesBuildsInput>
  }

  export type BuildCreateWithoutDisLikesInput = {
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    champion: ChampionCreateNestedOneWithoutBuildsInput
    creator: UserCreateNestedOneWithoutBuildsInput
    Likes?: LikesCreateNestedManyWithoutBuildInput
  }

  export type BuildUncheckedCreateWithoutDisLikesInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    championID: number
    creatorID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
    Likes?: LikesUncheckedCreateNestedManyWithoutBuildInput
  }

  export type BuildCreateOrConnectWithoutDisLikesInput = {
    where: BuildWhereUniqueInput
    create: XOR<BuildCreateWithoutDisLikesInput, BuildUncheckedCreateWithoutDisLikesInput>
  }

  export type UserUpsertWithoutDislikesBuildsInput = {
    update: XOR<UserUpdateWithoutDislikesBuildsInput, UserUncheckedUpdateWithoutDislikesBuildsInput>
    create: XOR<UserCreateWithoutDislikesBuildsInput, UserUncheckedCreateWithoutDislikesBuildsInput>
    where?: UserWhereInput
  }

  export type UserUpdateToOneWithWhereWithoutDislikesBuildsInput = {
    where?: UserWhereInput
    data: XOR<UserUpdateWithoutDislikesBuildsInput, UserUncheckedUpdateWithoutDislikesBuildsInput>
  }

  export type UserUpdateWithoutDislikesBuildsInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    team?: TeamUpdateOneWithoutMembersNestedInput
    ownedTeam?: TeamUpdateOneWithoutOwnerNestedInput
    builds?: BuildUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateWithoutDislikesBuildsInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    teamID?: NullableIntFieldUpdateOperationsInput | number | null
    ownedTeam?: TeamUncheckedUpdateOneWithoutOwnerNestedInput
    builds?: BuildUncheckedUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUncheckedUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUncheckedUpdateManyWithoutUserNestedInput
  }

  export type BuildUpsertWithoutDisLikesInput = {
    update: XOR<BuildUpdateWithoutDisLikesInput, BuildUncheckedUpdateWithoutDisLikesInput>
    create: XOR<BuildCreateWithoutDisLikesInput, BuildUncheckedCreateWithoutDisLikesInput>
    where?: BuildWhereInput
  }

  export type BuildUpdateToOneWithWhereWithoutDisLikesInput = {
    where?: BuildWhereInput
    data: XOR<BuildUpdateWithoutDisLikesInput, BuildUncheckedUpdateWithoutDisLikesInput>
  }

  export type BuildUpdateWithoutDisLikesInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    champion?: ChampionUpdateOneRequiredWithoutBuildsNestedInput
    creator?: UserUpdateOneRequiredWithoutBuildsNestedInput
    Likes?: LikesUpdateManyWithoutBuildNestedInput
  }

  export type BuildUncheckedUpdateWithoutDisLikesInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    championID?: IntFieldUpdateOperationsInput | number
    creatorID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    Likes?: LikesUncheckedUpdateManyWithoutBuildNestedInput
  }

  export type UserCreateWithoutTournamentsInput = {
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    team?: TeamCreateNestedOneWithoutMembersInput
    ownedTeam?: TeamCreateNestedOneWithoutOwnerInput
    builds?: BuildCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesCreateNestedManyWithoutUserInput
  }

  export type UserUncheckedCreateWithoutTournamentsInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    teamID?: number | null
    ownedTeam?: TeamUncheckedCreateNestedOneWithoutOwnerInput
    builds?: BuildUncheckedCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesUncheckedCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesUncheckedCreateNestedManyWithoutUserInput
  }

  export type UserCreateOrConnectWithoutTournamentsInput = {
    where: UserWhereUniqueInput
    create: XOR<UserCreateWithoutTournamentsInput, UserUncheckedCreateWithoutTournamentsInput>
  }

  export type UserUpsertWithoutTournamentsInput = {
    update: XOR<UserUpdateWithoutTournamentsInput, UserUncheckedUpdateWithoutTournamentsInput>
    create: XOR<UserCreateWithoutTournamentsInput, UserUncheckedCreateWithoutTournamentsInput>
    where?: UserWhereInput
  }

  export type UserUpdateToOneWithWhereWithoutTournamentsInput = {
    where?: UserWhereInput
    data: XOR<UserUpdateWithoutTournamentsInput, UserUncheckedUpdateWithoutTournamentsInput>
  }

  export type UserUpdateWithoutTournamentsInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    team?: TeamUpdateOneWithoutMembersNestedInput
    ownedTeam?: TeamUpdateOneWithoutOwnerNestedInput
    builds?: BuildUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateWithoutTournamentsInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    teamID?: NullableIntFieldUpdateOperationsInput | number | null
    ownedTeam?: TeamUncheckedUpdateOneWithoutOwnerNestedInput
    builds?: BuildUncheckedUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUncheckedUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUncheckedUpdateManyWithoutUserNestedInput
  }

  export type UserCreateWithoutOwnedTeamInput = {
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    team?: TeamCreateNestedOneWithoutMembersInput
    builds?: BuildCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesCreateNestedManyWithoutUserInput
  }

  export type UserUncheckedCreateWithoutOwnedTeamInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    teamID?: number | null
    builds?: BuildUncheckedCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentUncheckedCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesUncheckedCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesUncheckedCreateNestedManyWithoutUserInput
  }

  export type UserCreateOrConnectWithoutOwnedTeamInput = {
    where: UserWhereUniqueInput
    create: XOR<UserCreateWithoutOwnedTeamInput, UserUncheckedCreateWithoutOwnedTeamInput>
  }

  export type UserCreateWithoutTeamInput = {
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    ownedTeam?: TeamCreateNestedOneWithoutOwnerInput
    builds?: BuildCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesCreateNestedManyWithoutUserInput
  }

  export type UserUncheckedCreateWithoutTeamInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
    ownedTeam?: TeamUncheckedCreateNestedOneWithoutOwnerInput
    builds?: BuildUncheckedCreateNestedManyWithoutCreatorInput
    tournaments?: TournamentUncheckedCreateNestedManyWithoutCreatorInput
    likesBuilds?: LikesUncheckedCreateNestedManyWithoutUserInput
    dislikesBuilds?: DisLikesUncheckedCreateNestedManyWithoutUserInput
  }

  export type UserCreateOrConnectWithoutTeamInput = {
    where: UserWhereUniqueInput
    create: XOR<UserCreateWithoutTeamInput, UserUncheckedCreateWithoutTeamInput>
  }

  export type UserCreateManyTeamInputEnvelope = {
    data: UserCreateManyTeamInput | UserCreateManyTeamInput[]
    skipDuplicates?: boolean
  }

  export type UserUpsertWithoutOwnedTeamInput = {
    update: XOR<UserUpdateWithoutOwnedTeamInput, UserUncheckedUpdateWithoutOwnedTeamInput>
    create: XOR<UserCreateWithoutOwnedTeamInput, UserUncheckedCreateWithoutOwnedTeamInput>
    where?: UserWhereInput
  }

  export type UserUpdateToOneWithWhereWithoutOwnedTeamInput = {
    where?: UserWhereInput
    data: XOR<UserUpdateWithoutOwnedTeamInput, UserUncheckedUpdateWithoutOwnedTeamInput>
  }

  export type UserUpdateWithoutOwnedTeamInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    team?: TeamUpdateOneWithoutMembersNestedInput
    builds?: BuildUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateWithoutOwnedTeamInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    teamID?: NullableIntFieldUpdateOperationsInput | number | null
    builds?: BuildUncheckedUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUncheckedUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUncheckedUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUncheckedUpdateManyWithoutUserNestedInput
  }

  export type UserUpsertWithWhereUniqueWithoutTeamInput = {
    where: UserWhereUniqueInput
    update: XOR<UserUpdateWithoutTeamInput, UserUncheckedUpdateWithoutTeamInput>
    create: XOR<UserCreateWithoutTeamInput, UserUncheckedCreateWithoutTeamInput>
  }

  export type UserUpdateWithWhereUniqueWithoutTeamInput = {
    where: UserWhereUniqueInput
    data: XOR<UserUpdateWithoutTeamInput, UserUncheckedUpdateWithoutTeamInput>
  }

  export type UserUpdateManyWithWhereWithoutTeamInput = {
    where: UserScalarWhereInput
    data: XOR<UserUpdateManyMutationInput, UserUncheckedUpdateManyWithoutTeamInput>
  }

  export type UserScalarWhereInput = {
    AND?: UserScalarWhereInput | UserScalarWhereInput[]
    OR?: UserScalarWhereInput[]
    NOT?: UserScalarWhereInput | UserScalarWhereInput[]
    id?: IntFilter<"User"> | number
    login?: StringFilter<"User"> | string
    displayName?: StringFilter<"User"> | string
    discord?: StringFilter<"User"> | string
    telegram?: StringFilter<"User"> | string
    gameRank?: IntFilter<"User"> | number
    email?: StringFilter<"User"> | string
    isEmailConfirmed?: BoolFilter<"User"> | boolean
    password?: StringFilter<"User"> | string
    icon?: StringFilter<"User"> | string
    role?: StringFilter<"User"> | string
    refreshToken?: StringFilter<"User"> | string
    isDisabled?: BoolFilter<"User"> | boolean
    kills?: IntFilter<"User"> | number
    deaths?: IntFilter<"User"> | number
    assists?: IntFilter<"User"> | number
    tournamentWins?: IntNullableListFilter<"User">
    gameWins?: IntFilter<"User"> | number
    gameLosed?: IntFilter<"User"> | number
    teamID?: IntNullableFilter<"User"> | number | null
  }

  export type BuildCreateManyCreatorInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    championID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
  }

  export type TournamentCreateManyCreatorInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    startedAt: Date | string
    finishedAt: Date | string
    name: string
    description: string
    prize: string
    icon?: string | null
    map?: string | null
    pickMode?: $Enums.TournamentPickMode
    gameMode?: $Enums.GameMode
    status?: $Enums.TournamentStatus
    teamSize?: number
    slots?: number
    teamRequest?: boolean
    maxRequests?: number
    casters?: TournamentCreatecastersInput | string[]
    videoLink?: string | null
    teams: string
    requests?: TournamentCreaterequestsInput | string[]
    bracket?: string | null
    bracketType?: $Enums.BracketType
    matchesCount?: number
    results?: string | null
  }

  export type LikesCreateManyUserInput = {
    buildID: number
    assignedAt?: Date | string
  }

  export type DisLikesCreateManyUserInput = {
    buildID: number
    assignedAt?: Date | string
  }

  export type BuildUpdateWithoutCreatorInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    champion?: ChampionUpdateOneRequiredWithoutBuildsNestedInput
    Likes?: LikesUpdateManyWithoutBuildNestedInput
    DisLikes?: DisLikesUpdateManyWithoutBuildNestedInput
  }

  export type BuildUncheckedUpdateWithoutCreatorInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    championID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    Likes?: LikesUncheckedUpdateManyWithoutBuildNestedInput
    DisLikes?: DisLikesUncheckedUpdateManyWithoutBuildNestedInput
  }

  export type BuildUncheckedUpdateManyWithoutCreatorInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    championID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type TournamentUpdateWithoutCreatorInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    startedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    finishedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    prize?: StringFieldUpdateOperationsInput | string
    icon?: NullableStringFieldUpdateOperationsInput | string | null
    map?: NullableStringFieldUpdateOperationsInput | string | null
    pickMode?: EnumTournamentPickModeFieldUpdateOperationsInput | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFieldUpdateOperationsInput | $Enums.GameMode
    status?: EnumTournamentStatusFieldUpdateOperationsInput | $Enums.TournamentStatus
    teamSize?: IntFieldUpdateOperationsInput | number
    slots?: IntFieldUpdateOperationsInput | number
    teamRequest?: BoolFieldUpdateOperationsInput | boolean
    maxRequests?: IntFieldUpdateOperationsInput | number
    casters?: TournamentUpdatecastersInput | string[]
    videoLink?: NullableStringFieldUpdateOperationsInput | string | null
    teams?: StringFieldUpdateOperationsInput | string
    requests?: TournamentUpdaterequestsInput | string[]
    bracket?: NullableStringFieldUpdateOperationsInput | string | null
    bracketType?: EnumBracketTypeFieldUpdateOperationsInput | $Enums.BracketType
    matchesCount?: IntFieldUpdateOperationsInput | number
    results?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type TournamentUncheckedUpdateWithoutCreatorInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    startedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    finishedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    prize?: StringFieldUpdateOperationsInput | string
    icon?: NullableStringFieldUpdateOperationsInput | string | null
    map?: NullableStringFieldUpdateOperationsInput | string | null
    pickMode?: EnumTournamentPickModeFieldUpdateOperationsInput | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFieldUpdateOperationsInput | $Enums.GameMode
    status?: EnumTournamentStatusFieldUpdateOperationsInput | $Enums.TournamentStatus
    teamSize?: IntFieldUpdateOperationsInput | number
    slots?: IntFieldUpdateOperationsInput | number
    teamRequest?: BoolFieldUpdateOperationsInput | boolean
    maxRequests?: IntFieldUpdateOperationsInput | number
    casters?: TournamentUpdatecastersInput | string[]
    videoLink?: NullableStringFieldUpdateOperationsInput | string | null
    teams?: StringFieldUpdateOperationsInput | string
    requests?: TournamentUpdaterequestsInput | string[]
    bracket?: NullableStringFieldUpdateOperationsInput | string | null
    bracketType?: EnumBracketTypeFieldUpdateOperationsInput | $Enums.BracketType
    matchesCount?: IntFieldUpdateOperationsInput | number
    results?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type TournamentUncheckedUpdateManyWithoutCreatorInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    startedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    finishedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    description?: StringFieldUpdateOperationsInput | string
    prize?: StringFieldUpdateOperationsInput | string
    icon?: NullableStringFieldUpdateOperationsInput | string | null
    map?: NullableStringFieldUpdateOperationsInput | string | null
    pickMode?: EnumTournamentPickModeFieldUpdateOperationsInput | $Enums.TournamentPickMode
    gameMode?: EnumGameModeFieldUpdateOperationsInput | $Enums.GameMode
    status?: EnumTournamentStatusFieldUpdateOperationsInput | $Enums.TournamentStatus
    teamSize?: IntFieldUpdateOperationsInput | number
    slots?: IntFieldUpdateOperationsInput | number
    teamRequest?: BoolFieldUpdateOperationsInput | boolean
    maxRequests?: IntFieldUpdateOperationsInput | number
    casters?: TournamentUpdatecastersInput | string[]
    videoLink?: NullableStringFieldUpdateOperationsInput | string | null
    teams?: StringFieldUpdateOperationsInput | string
    requests?: TournamentUpdaterequestsInput | string[]
    bracket?: NullableStringFieldUpdateOperationsInput | string | null
    bracketType?: EnumBracketTypeFieldUpdateOperationsInput | $Enums.BracketType
    matchesCount?: IntFieldUpdateOperationsInput | number
    results?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type LikesUpdateWithoutUserInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    build?: BuildUpdateOneRequiredWithoutLikesNestedInput
  }

  export type LikesUncheckedUpdateWithoutUserInput = {
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type LikesUncheckedUpdateManyWithoutUserInput = {
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type DisLikesUpdateWithoutUserInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    build?: BuildUpdateOneRequiredWithoutDisLikesNestedInput
  }

  export type DisLikesUncheckedUpdateWithoutUserInput = {
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type DisLikesUncheckedUpdateManyWithoutUserInput = {
    buildID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type BuildCreateManyChampionInput = {
    id?: number
    createdAt?: Date | string
    updatedAt?: Date | string
    name: string
    role: $Enums.Role
    creatorID: number
    items?: BuildCreateitemsInput | string[]
    coreItems?: BuildCreatecoreItemsInput | string[]
    situationalItems?: BuildCreatesituationalItemsInput | string[]
    skillsOrder?: BuildCreateskillsOrderInput | string[]
    karmas?: BuildCreatekarmasInput | string[]
    keyInsights?: string | null
    strengths?: string | null
    weaknesses?: string | null
  }

  export type BuildUpdateWithoutChampionInput = {
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    creator?: UserUpdateOneRequiredWithoutBuildsNestedInput
    Likes?: LikesUpdateManyWithoutBuildNestedInput
    DisLikes?: DisLikesUpdateManyWithoutBuildNestedInput
  }

  export type BuildUncheckedUpdateWithoutChampionInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    creatorID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
    Likes?: LikesUncheckedUpdateManyWithoutBuildNestedInput
    DisLikes?: DisLikesUncheckedUpdateManyWithoutBuildNestedInput
  }

  export type BuildUncheckedUpdateManyWithoutChampionInput = {
    id?: IntFieldUpdateOperationsInput | number
    createdAt?: DateTimeFieldUpdateOperationsInput | Date | string
    updatedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    name?: StringFieldUpdateOperationsInput | string
    role?: EnumRoleFieldUpdateOperationsInput | $Enums.Role
    creatorID?: IntFieldUpdateOperationsInput | number
    items?: BuildUpdateitemsInput | string[]
    coreItems?: BuildUpdatecoreItemsInput | string[]
    situationalItems?: BuildUpdatesituationalItemsInput | string[]
    skillsOrder?: BuildUpdateskillsOrderInput | string[]
    karmas?: BuildUpdatekarmasInput | string[]
    keyInsights?: NullableStringFieldUpdateOperationsInput | string | null
    strengths?: NullableStringFieldUpdateOperationsInput | string | null
    weaknesses?: NullableStringFieldUpdateOperationsInput | string | null
  }

  export type LikesCreateManyBuildInput = {
    userID: number
    assignedAt?: Date | string
  }

  export type DisLikesCreateManyBuildInput = {
    userID: number
    assignedAt?: Date | string
  }

  export type LikesUpdateWithoutBuildInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    user?: UserUpdateOneRequiredWithoutLikesBuildsNestedInput
  }

  export type LikesUncheckedUpdateWithoutBuildInput = {
    userID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type LikesUncheckedUpdateManyWithoutBuildInput = {
    userID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type DisLikesUpdateWithoutBuildInput = {
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
    user?: UserUpdateOneRequiredWithoutDislikesBuildsNestedInput
  }

  export type DisLikesUncheckedUpdateWithoutBuildInput = {
    userID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type DisLikesUncheckedUpdateManyWithoutBuildInput = {
    userID?: IntFieldUpdateOperationsInput | number
    assignedAt?: DateTimeFieldUpdateOperationsInput | Date | string
  }

  export type UserCreateManyTeamInput = {
    id?: number
    login: string
    displayName?: string
    discord?: string
    telegram?: string
    gameRank?: number
    email: string
    isEmailConfirmed?: boolean
    password: string
    icon?: string
    role: string
    refreshToken?: string
    isDisabled?: boolean
    kills?: number
    deaths?: number
    assists?: number
    tournamentWins?: UserCreatetournamentWinsInput | number[]
    gameWins?: number
    gameLosed?: number
  }

  export type UserUpdateWithoutTeamInput = {
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    ownedTeam?: TeamUpdateOneWithoutOwnerNestedInput
    builds?: BuildUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateWithoutTeamInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
    ownedTeam?: TeamUncheckedUpdateOneWithoutOwnerNestedInput
    builds?: BuildUncheckedUpdateManyWithoutCreatorNestedInput
    tournaments?: TournamentUncheckedUpdateManyWithoutCreatorNestedInput
    likesBuilds?: LikesUncheckedUpdateManyWithoutUserNestedInput
    dislikesBuilds?: DisLikesUncheckedUpdateManyWithoutUserNestedInput
  }

  export type UserUncheckedUpdateManyWithoutTeamInput = {
    id?: IntFieldUpdateOperationsInput | number
    login?: StringFieldUpdateOperationsInput | string
    displayName?: StringFieldUpdateOperationsInput | string
    discord?: StringFieldUpdateOperationsInput | string
    telegram?: StringFieldUpdateOperationsInput | string
    gameRank?: IntFieldUpdateOperationsInput | number
    email?: StringFieldUpdateOperationsInput | string
    isEmailConfirmed?: BoolFieldUpdateOperationsInput | boolean
    password?: StringFieldUpdateOperationsInput | string
    icon?: StringFieldUpdateOperationsInput | string
    role?: StringFieldUpdateOperationsInput | string
    refreshToken?: StringFieldUpdateOperationsInput | string
    isDisabled?: BoolFieldUpdateOperationsInput | boolean
    kills?: IntFieldUpdateOperationsInput | number
    deaths?: IntFieldUpdateOperationsInput | number
    assists?: IntFieldUpdateOperationsInput | number
    tournamentWins?: UserUpdatetournamentWinsInput | number[]
    gameWins?: IntFieldUpdateOperationsInput | number
    gameLosed?: IntFieldUpdateOperationsInput | number
  }



  /**
   * Aliases for legacy arg types
   */
    /**
     * @deprecated Use UserCountOutputTypeDefaultArgs instead
     */
    export type UserCountOutputTypeArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = UserCountOutputTypeDefaultArgs<ExtArgs>
    /**
     * @deprecated Use ChampionCountOutputTypeDefaultArgs instead
     */
    export type ChampionCountOutputTypeArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = ChampionCountOutputTypeDefaultArgs<ExtArgs>
    /**
     * @deprecated Use BuildCountOutputTypeDefaultArgs instead
     */
    export type BuildCountOutputTypeArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = BuildCountOutputTypeDefaultArgs<ExtArgs>
    /**
     * @deprecated Use TeamCountOutputTypeDefaultArgs instead
     */
    export type TeamCountOutputTypeArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = TeamCountOutputTypeDefaultArgs<ExtArgs>
    /**
     * @deprecated Use UserDefaultArgs instead
     */
    export type UserArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = UserDefaultArgs<ExtArgs>
    /**
     * @deprecated Use RegistrationTokenDefaultArgs instead
     */
    export type RegistrationTokenArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = RegistrationTokenDefaultArgs<ExtArgs>
    /**
     * @deprecated Use ChampionDefaultArgs instead
     */
    export type ChampionArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = ChampionDefaultArgs<ExtArgs>
    /**
     * @deprecated Use BuildDefaultArgs instead
     */
    export type BuildArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = BuildDefaultArgs<ExtArgs>
    /**
     * @deprecated Use LikesDefaultArgs instead
     */
    export type LikesArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = LikesDefaultArgs<ExtArgs>
    /**
     * @deprecated Use DisLikesDefaultArgs instead
     */
    export type DisLikesArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = DisLikesDefaultArgs<ExtArgs>
    /**
     * @deprecated Use TournamentDefaultArgs instead
     */
    export type TournamentArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = TournamentDefaultArgs<ExtArgs>
    /**
     * @deprecated Use ItemDefaultArgs instead
     */
    export type ItemArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = ItemDefaultArgs<ExtArgs>
    /**
     * @deprecated Use LogDefaultArgs instead
     */
    export type LogArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = LogDefaultArgs<ExtArgs>
    /**
     * @deprecated Use TeamDefaultArgs instead
     */
    export type TeamArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = TeamDefaultArgs<ExtArgs>
    /**
     * @deprecated Use NewsDefaultArgs instead
     */
    export type NewsArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = NewsDefaultArgs<ExtArgs>
    /**
     * @deprecated Use CreatureDefaultArgs instead
     */
    export type CreatureArgs<ExtArgs extends $Extensions.Args = $Extensions.DefaultArgs> = CreatureDefaultArgs<ExtArgs>

  /**
   * Batch Payload for updateMany & deleteMany & createMany
   */

  export type BatchPayload = {
    count: number
  }

  /**
   * DMMF
   */
  export const dmmf: runtime.BaseDMMF
}