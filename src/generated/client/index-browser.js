
Object.defineProperty(exports, "__esModule", { value: true });

const {
  Decimal,
  objectEnumValues,
  makeStrictEnum,
  Public,
  detectRuntime,
} = require('./runtime/index-browser')


const Prisma = {}

exports.Prisma = Prisma
exports.$Enums = {}

/**
 * Prisma Client JS version: 5.3.1
 * Query Engine version: 61e140623197a131c2a6189271ffee05a7aa9a59
 */
Prisma.prismaVersion = {
  client: "5.3.1",
  engine: "61e140623197a131c2a6189271ffee05a7aa9a59"
}

Prisma.PrismaClientKnownRequestError = () => {
  throw new Error(`PrismaClientKnownRequestError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)};
Prisma.PrismaClientUnknownRequestError = () => {
  throw new Error(`PrismaClientUnknownRequestError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientRustPanicError = () => {
  throw new Error(`PrismaClientRustPanicError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientInitializationError = () => {
  throw new Error(`PrismaClientInitializationError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.PrismaClientValidationError = () => {
  throw new Error(`PrismaClientValidationError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.NotFoundError = () => {
  throw new Error(`NotFoundError is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.Decimal = Decimal

/**
 * Re-export of sql-template-tag
 */
Prisma.sql = () => {
  throw new Error(`sqltag is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.empty = () => {
  throw new Error(`empty is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.join = () => {
  throw new Error(`join is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.raw = () => {
  throw new Error(`raw is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.validator = Public.validator

/**
* Extensions
*/
Prisma.getExtensionContext = () => {
  throw new Error(`Extensions.getExtensionContext is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}
Prisma.defineExtension = () => {
  throw new Error(`Extensions.defineExtension is unable to be run ${runtimeDescription}.
In case this error is unexpected for you, please report it in https://github.com/prisma/prisma/issues`,
)}

/**
 * Shorthand utilities for JSON filtering
 */
Prisma.DbNull = objectEnumValues.instances.DbNull
Prisma.JsonNull = objectEnumValues.instances.JsonNull
Prisma.AnyNull = objectEnumValues.instances.AnyNull

Prisma.NullTypes = {
  DbNull: objectEnumValues.classes.DbNull,
  JsonNull: objectEnumValues.classes.JsonNull,
  AnyNull: objectEnumValues.classes.AnyNull
}

/**
 * Enums
 */

exports.Prisma.TransactionIsolationLevel = makeStrictEnum({
  ReadUncommitted: 'ReadUncommitted',
  ReadCommitted: 'ReadCommitted',
  RepeatableRead: 'RepeatableRead',
  Serializable: 'Serializable'
});

exports.Prisma.UserScalarFieldEnum = {
  id: 'id',
  login: 'login',
  displayName: 'displayName',
  discord: 'discord',
  telegram: 'telegram',
  gameRank: 'gameRank',
  email: 'email',
  isEmailConfirmed: 'isEmailConfirmed',
  password: 'password',
  icon: 'icon',
  role: 'role',
  refreshToken: 'refreshToken',
  isDisabled: 'isDisabled',
  kills: 'kills',
  deaths: 'deaths',
  assists: 'assists',
  tournamentWins: 'tournamentWins',
  gameWins: 'gameWins',
  gameLosed: 'gameLosed',
  teamID: 'teamID'
};

exports.Prisma.RegistrationTokenScalarFieldEnum = {
  id: 'id',
  token: 'token'
};

exports.Prisma.ChampionScalarFieldEnum = {
  id: 'id',
  name: 'name',
  role: 'role',
  tier: 'tier',
  counterPicks: 'counterPicks'
};

exports.Prisma.BuildScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  name: 'name',
  role: 'role',
  championID: 'championID',
  creatorID: 'creatorID',
  items: 'items',
  coreItems: 'coreItems',
  situationalItems: 'situationalItems',
  skillsOrder: 'skillsOrder',
  karmas: 'karmas',
  keyInsights: 'keyInsights',
  strengths: 'strengths',
  weaknesses: 'weaknesses'
};

exports.Prisma.LikesScalarFieldEnum = {
  userID: 'userID',
  buildID: 'buildID',
  assignedAt: 'assignedAt'
};

exports.Prisma.DisLikesScalarFieldEnum = {
  userID: 'userID',
  buildID: 'buildID',
  assignedAt: 'assignedAt'
};

exports.Prisma.TournamentScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  updatedAt: 'updatedAt',
  startedAt: 'startedAt',
  finishedAt: 'finishedAt',
  name: 'name',
  description: 'description',
  prize: 'prize',
  icon: 'icon',
  map: 'map',
  pickMode: 'pickMode',
  gameMode: 'gameMode',
  status: 'status',
  teamSize: 'teamSize',
  slots: 'slots',
  teamRequest: 'teamRequest',
  maxRequests: 'maxRequests',
  casters: 'casters',
  videoLink: 'videoLink',
  creatorID: 'creatorID',
  teams: 'teams',
  requests: 'requests',
  bracket: 'bracket',
  bracketType: 'bracketType',
  matchesCount: 'matchesCount',
  results: 'results'
};

exports.Prisma.ItemScalarFieldEnum = {
  id: 'id',
  name: 'name',
  displayName: 'displayName',
  type: 'type',
  price: 'price',
  params: 'params',
  effect: 'effect',
  effectParams: 'effectParams'
};

exports.Prisma.LogScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  type: 'type',
  message: 'message'
};

exports.Prisma.TeamScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  name: 'name',
  description: 'description',
  icon: 'icon',
  rating: 'rating',
  tournamentWins: 'tournamentWins',
  gameWins: 'gameWins',
  gameLosed: 'gameLosed',
  ownerID: 'ownerID',
  requests: 'requests'
};

exports.Prisma.NewsScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  name: 'name',
  description: 'description',
  icon: 'icon',
  patch: 'patch',
  champions: 'champions',
  items: 'items',
  bugfixes: 'bugfixes'
};

exports.Prisma.CreatureScalarFieldEnum = {
  id: 'id',
  createdAt: 'createdAt',
  name: 'name',
  description: 'description',
  icon: 'icon',
  type: 'type',
  health: 'health',
  gold: 'gold',
  exp: 'exp',
  spawnTime: 'spawnTime',
  respawnTime: 'respawnTime',
  buff: 'buff'
};

exports.Prisma.SortOrder = {
  asc: 'asc',
  desc: 'desc'
};

exports.Prisma.QueryMode = {
  default: 'default',
  insensitive: 'insensitive'
};

exports.Prisma.NullsOrder = {
  first: 'first',
  last: 'last'
};
exports.Role = exports.$Enums.Role = {
  Jungle: 'Jungle',
  Mid: 'Mid',
  Adc: 'Adc',
  Support: 'Support',
  Top: 'Top'
};

exports.TournamentPickMode = exports.$Enums.TournamentPickMode = {
  Quick: 'Quick',
  Draft: 'Draft'
};

exports.GameMode = exports.$Enums.GameMode = {
  DestroyCore: 'DestroyCore',
  FirstBlood: 'FirstBlood',
  FirstTower: 'FirstTower'
};

exports.TournamentStatus = exports.$Enums.TournamentStatus = {
  Pending: 'Pending',
  InProgress: 'InProgress',
  Finished: 'Finished'
};

exports.BracketType = exports.$Enums.BracketType = {
  RoundRobin: 'RoundRobin',
  SingleElimination: 'SingleElimination'
};

exports.ItemType = exports.$Enums.ItemType = {
  Basic: 'Basic',
  Rare: 'Rare',
  Epic: 'Epic'
};

exports.LogType = exports.$Enums.LogType = {
  Info: 'Info',
  Warning: 'Warning',
  Error: 'Error'
};

exports.CreatureType = exports.$Enums.CreatureType = {
  Jungle: 'Jungle',
  Minion: 'Minion',
  Tower: 'Tower',
  Core: 'Core',
  Boss: 'Boss'
};

exports.Prisma.ModelName = {
  User: 'User',
  RegistrationToken: 'RegistrationToken',
  Champion: 'Champion',
  Build: 'Build',
  Likes: 'Likes',
  DisLikes: 'DisLikes',
  Tournament: 'Tournament',
  Item: 'Item',
  Log: 'Log',
  Team: 'Team',
  News: 'News',
  Creature: 'Creature'
};

/**
 * This is a stub Prisma Client that will error at runtime if called.
 */
class PrismaClient {
  constructor() {
    return new Proxy(this, {
      get(target, prop) {
        const runtime = detectRuntime()
        const edgeRuntimeName = {
          'workerd': 'Cloudflare Workers',
          'deno': 'Deno and Deno Deploy',
          'netlify': 'Netlify Edge Functions',
          'edge-light': 'Vercel Edge Functions',
        }[runtime]

        let message = 'PrismaClient is unable to run in '
        if (edgeRuntimeName !== undefined) {
          message += edgeRuntimeName + '. As an alternative, try Accelerate: https://pris.ly/d/accelerate.'
        } else {
          message += 'this browser environment, or has been bundled for the browser (running in `' + runtime + '`).'
        }
        
        message += `
If this is unexpected, please open an issue: https://github.com/prisma/prisma/issues`

        throw new Error(message)
      }
    })
  }
}

exports.PrismaClient = PrismaClient

Object.assign(exports, Prisma)
