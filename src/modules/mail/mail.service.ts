import {MailerService} from '@nestjs-modules/mailer';
import {Injectable} from '@nestjs/common';
import {User} from "@prisma-client";
import console from "console";

@Injectable()
export class MailService {
    constructor(private mailerService: MailerService) {}

    async sendConfirmationEmail(user: User, token: string): Promise<void> {
        const url = `${process.env['WEB_BASE_URL']}/auth?token=${token}`;

        await this.mailerService.sendMail({
            to: user.email,
            subject: 'Paragon Analytics - Confirm your Email!',
            template: '../../../mail/templates/confirmation',
            context: {
                login: user.login,
                url,
            },
        });
    }
}