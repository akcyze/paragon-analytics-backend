import {Injectable} from "@nestjs/common";
import {PrismaService} from "../../prisma/prisma.service";
import {Creature} from "@prisma-client";

@Injectable()
export class CreatureService {

    constructor(private readonly prismaService: PrismaService) {
    }

    async create(data: Creature): Promise<Creature> {
        return this.prismaService.creature.create({data});
    }

    async getById(id: number): Promise<Creature> {
        return this.prismaService.creature.findFirst({where: {id}});
    }

    async getMany(): Promise<Creature[]> {
        return this.prismaService.creature.findMany();
    }

    async update(data: Creature): Promise<Creature> {
        return this.prismaService.creature.update({data, where: {id: data.id}});
    }

    async delete(id: number): Promise<void> {
        await this.prismaService.creature.delete({where: {id}});
    }
}
