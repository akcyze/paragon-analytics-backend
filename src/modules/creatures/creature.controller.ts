import {
    BadRequestException, Body,
    Controller, Delete,
    Get,
    Param,
    ParseIntPipe,
    Post, Put
} from "@nestjs/common";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {CurrentUser} from "@common/decorators/current-user.decorator";
import {LogService} from "../log/log.service";
import {CreatureService} from "./creature.service";
import {CreatureDto, CreatureReadOnlyDto} from "./creature.dto";
import {CreatureEntity} from "./creature.entity";

@Controller('creature')
export class CreatureController {
    constructor(private readonly creatureService: CreatureService,
                private readonly logService: LogService) {
    }

    @Put('')
    @Auth(Role.CreatureManager)
    async create(@CurrentUser('id') currentUserID: number, @Body() dto: CreatureDto): Promise<CreatureDto> {
        const impl = new CreatureEntity();

        impl.fromDto(dto);
        impl.id = undefined;
        impl.createdAt = new Date();

        const creature = await this.creatureService.create(impl.toModel());

        this.logService.info(`Created creature ${creature.id} by user ${currentUserID}`);

        return CreatureEntity.createFromModel(creature).toDto();
    }

    @Get(':id')
    async getById(@Param('id', ParseIntPipe) id: number): Promise<CreatureDto> {
        const creature = await this.creatureService.getById(id);
        if (!creature) {
            throw new BadRequestException('Creature not found');
        }

        return CreatureEntity.createFromModel(creature).toDto();
    }

    @Get('')
    async getMany(): Promise<CreatureReadOnlyDto[]> {
        const creatures = await this.creatureService.getMany();
        const entities = creatures.map(creature => CreatureEntity.createFromModel(creature));

        return entities.map(impl => impl.toReadOnly());
    }

    @Post('')
    @Auth(Role.CreatureManager)
    async update(@Body() dto: CreatureDto): Promise<CreatureDto> {
        let creature = await this.creatureService.getById(dto.id);
        if (!creature) {
            throw new BadRequestException("Tournament not found");
        }

        const entity = CreatureEntity.createFromModel(creature);

        entity.fromDto(dto);

        await this.creatureService.update(entity.toModel());

        creature = await this.creatureService.getById(creature.id);

        this.logService.info(`Updated creature ${creature.id}`);

        entity.fromModel(creature);

        return entity.toDto();
    }

    @Delete(':id')
    @Auth(Role.CreatureManager)
    async delete(@Param('id', ParseIntPipe) id: number): Promise<boolean> {
        const creature = await this.creatureService.getById(id);
        if (!creature) {
            throw new BadRequestException("Creature not found");
        }

        await this.creatureService.delete(creature.id);

        this.logService.warn(`Deleted creature ${creature.id}`);

        return true;
    }
}
