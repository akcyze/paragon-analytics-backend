import {BaseEntity} from "@common/base/base.entity";
import {Creature, CreatureType,} from "@prisma-client";
import {CreatureDto, CreatureReadOnlyDto} from "./creature.dto";

export class CreatureEntity extends BaseEntity<CreatureDto, Creature> {
    id: number;
    createdAt: Date;

    name: string;
    description: string;
    icon: string;
    type: CreatureType;
    health: string;
    gold: string;
    exp: string;
    spawnTime: string;
    respawnTime: string;
    buff: string;

    public fromDto(dto: CreatureDto) {
        super.fromDto(dto);

        this.id = dto.id;
        this.createdAt = dto.createdAt;

        this.name = dto.name;
        this.description = dto.description;
        this.icon = dto.icon;
        this.type = dto.type;
        this.health = dto.health;
        this.gold = dto.gold;
        this.exp = dto.exp;
        this.spawnTime = dto.spawnTime;
        this.respawnTime = dto.respawnTime;
        this.buff = dto.buff;
    }

    public toDto(): CreatureDto {
        const dto = super.toDto();

        dto.id = this.id;
        dto.createdAt = this.createdAt;

        dto.name = this.name;
        dto.description = this.description;
        dto.icon = this.icon;
        dto.type = this.type;
        dto.health = this.health;
        dto.gold = this.gold;
        dto.exp = this.exp;
        dto.spawnTime = this.spawnTime;
        dto.respawnTime = this.respawnTime;
        dto.buff = this.buff;

        return dto;
    }

    public toReadOnly(): CreatureReadOnlyDto {
        const readOnly = new CreatureReadOnlyDto();

        readOnly.id = this.id;
        readOnly.name = this.name;
        readOnly.icon = this.icon;
        readOnly.type = this.type;

        return readOnly;
    }

    public fromModel(model: Creature) {
        this.id = model.id;
        this.createdAt = model.createdAt;

        this.name = model.name;
        this.description = model.description;
        this.icon = model.icon;
        this.type = model.type;
        this.health = model.health;
        this.gold = model.gold;
        this.exp = model.exp;
        this.spawnTime = model.spawnTime;
        this.respawnTime = model.respawnTime;
        this.buff = model.buff;
    }

    public toModel(): Creature {
        const model = super.toModel();

        model.id = this.id;
        model.createdAt = this.createdAt;

        model.name = this.name;
        model.description = this.description;
        model.icon = this.icon;
        model.type = this.type;
        model.health = this.health;
        model.gold = this.gold;
        model.exp = this.exp;
        model.spawnTime = this.spawnTime;
        model.respawnTime = this.respawnTime;
        model.buff = this.buff;

        return model;
    }

    public static createFromModel(model: Creature): CreatureEntity {
        const impl = new CreatureEntity();

        impl.fromModel(model);

        return impl;
    }
}