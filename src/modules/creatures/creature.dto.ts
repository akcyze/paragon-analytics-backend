import {BaseDto} from "@common/base/base.dto";
import {CreatureType} from "@prisma-client";

export class CreatureDto extends BaseDto {
    id: number;
    createdAt: Date;
    name: string;
    description: string;
    icon: string;
    type: CreatureType;
    health: string;
    gold: string;
    exp: string;
    spawnTime: string;
    respawnTime: string;
    buff: string;
}

export class CreatureReadOnlyDto {
    id: number;
    name: string;
    icon: string;
    type: CreatureType;
}

