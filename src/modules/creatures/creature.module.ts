import { Module } from '@nestjs/common';
import { PrismaModule } from "../../prisma/prisma.module";
import {LogModule} from "../log/log.module";
import {CreatureController} from "./creature.controller";
import {CreatureService} from "./creature.service";

@Module({
    imports: [PrismaModule, LogModule],
    controllers: [CreatureController],
    providers: [CreatureService],
    exports: [CreatureService]
})
export class CreatureModule {}
