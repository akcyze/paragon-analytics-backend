export interface SignUpDto {
    email: string;
    login: string;
    password: string;
}