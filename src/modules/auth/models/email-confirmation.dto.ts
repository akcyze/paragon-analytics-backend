export class EmailConfirmationDto {
    token: string;
}

export default EmailConfirmationDto;