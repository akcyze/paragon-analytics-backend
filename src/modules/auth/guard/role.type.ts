export enum Role {
    User = 'User',
    Manager = 'Manager',
    Admin = 'Admin',
    TournamentManager = 'TournamentManager',
    NewsManager = 'NewsManager',
    ItemsManager = 'ItemsManager',
    ChampionManager = 'ChampionManager',
    CreatureManager = 'CreatureManager',
}