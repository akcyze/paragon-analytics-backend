import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { JwtPayload } from "../interfaces/jwt-payload.interface";
import { JwtUserCredentials } from "../interfaces/jwt-user-credentials";
import console from "console";

@Injectable()
export class AtStrategy extends PassportStrategy(Strategy, 'jwt') {
    constructor() {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env["JWT_ACCESS_TOKEN_SECRET"],
        });
    }

    async validate(payload: JwtPayload): Promise<JwtUserCredentials> {
        return { login: payload.login, password: payload.password, roles: payload.roles };
    }
}