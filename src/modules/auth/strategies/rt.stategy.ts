import {ExtractJwt, Strategy} from 'passport-jwt';
import {PassportStrategy} from '@nestjs/passport';
import {Injectable} from '@nestjs/common';
import {Request} from 'express';
import {JwtPayload} from "../interfaces/jwt-payload.interface";
import {AuthService} from "../auth.service";
import {UserImpl} from "../../user/user.impl";

@Injectable()
export class RtStrategy extends PassportStrategy(Strategy, 'jwt-refresh-token') {
    constructor(private authService: AuthService) {
        super({
            jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
            secretOrKey: process.env["JWT_REFRESH_TOKEN_SECRET"],
            passReqToCallback: true,
        });
    }

    async validate(request: Request, payload: JwtPayload): Promise<UserImpl> {
        const refreshToken = request
            ?.get('authorization')
            ?.replace('Bearer', '')
            .trim();

        return await this.authService.signInRT(payload.login, refreshToken);
    }
}