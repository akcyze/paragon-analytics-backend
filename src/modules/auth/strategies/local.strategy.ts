import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from '../auth.service';
import { UserImpl } from "../../user/user.impl";
import * as console from "console";

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy, 'local') {
    constructor(private authService: AuthService) {
        super({ usernameField: 'login' });
    }

    async validate(login: string, password: string): Promise<UserImpl> {
        return await this.authService.signInLocal(login, password);
    }
}