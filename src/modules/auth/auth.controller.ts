import {
    Controller, Get,
    Post,
    Req,
    Put,
    UseGuards, UseInterceptors, Body, BadRequestException, Response
} from "@nestjs/common";
import { AuthService } from "./auth.service";
import { LocalAuthGuard } from "./guard/local-auth.guard";
import { JwtRefreshTokenGuard } from "./guard/jwt-refresh-token.guard";
import { Tokens } from "./interfaces/tokens";
import { JwtAuthGuard } from "./guard/jwt-auth.guard";
import {CurrentUserInterceptor} from "@common/interceptors/current-user.interceptor";
import {CurrentUser} from "@common/decorators/current-user.decorator";
import {SignUpDto} from "./models/sign-up.dto";
import EmailConfirmationDto from "./models/email-confirmation.dto";
import { Response as Res, Request } from 'express';
import {LogService} from "../log/log.service";

@Controller('auth')
@UseInterceptors(CurrentUserInterceptor)
export class AuthController {
    constructor(private readonly authService: AuthService,
                private readonly logService: LogService) {}

    @UseGuards(LocalAuthGuard)
    @Post('signIn')
    async signIn(@Req() request: Request, @Response() res: Res): Promise<Res> {
        const tokens = await this.authService.getTokens(request.user);

        this.logService.info(`User ${request.user.login} signed in`);

        res.setHeader('Authorization', `Bearer ${tokens.accessToken}`);
        res.setHeader('Refresh', `Bearer ${tokens.refreshToken}`);
        res.status(202);

        return res.json();
    }

    @Put('signUp')
    async signUp(@Body() dto: SignUpDto): Promise<void> {
        const user = await this.authService.signUp(dto);

        this.logService.info(`Created user ${user.login}:${user.email} with id ${user.id}`);

        await this.authService.sendConfirmationEmail(user.email);

        this.logService.info(`Sent confirmation email to ${user.email}`);
    }

    @Post('confirm')
    async confirmEmail(@Body() dto: EmailConfirmationDto): Promise<void> {
        this.logService.info(`Received email confirmation token`);

        const email = await this.authService.decodeConfirmationToken(dto.token);

        await this.authService.confirmEmail(email);

        this.logService.info(`Confirmed email ${email}`);
    }

    @UseGuards(JwtRefreshTokenGuard)
    @Get('refresh')
    async refreshToken(@Req() request: Request, @Response() res: Res): Promise<Res> {
        const tokens = await this.authService.getTokens(request.user);

        res.setHeader('Authorization', `Bearer ${tokens.accessToken}`);
        res.setHeader('Refresh', `Bearer ${tokens.refreshToken}`);
        res.status(202);

        return res.json();
    }

    @UseGuards(JwtAuthGuard)
    @Post('logout')
    async logout(@CurrentUser('login') login: string): Promise<void> {
        await this.authService.logout(login);

        this.logService.info(`User ${login} logged out`);
    }
}
