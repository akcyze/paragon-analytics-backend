import { applyDecorators, SetMetadata, UseGuards } from "@nestjs/common";
import { Role } from "../guard/role.type";
import { JwtAuthGuard } from "../guard/jwt-auth.guard";
import { RolesGuard } from "../guard/roles.guard";

const ROLES_KEY = "roles";

export function Auth(...roles: Role[]) {
    return applyDecorators(
      SetMetadata(ROLES_KEY, roles),
      UseGuards(JwtAuthGuard, RolesGuard)
    );
}