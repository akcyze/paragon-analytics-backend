import { Role } from "../guard/role.type";

export interface JwtUserCredentials {
    login: string;
    password: string;
    roles: Role[];
}