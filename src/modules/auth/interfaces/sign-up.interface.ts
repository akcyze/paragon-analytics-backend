import { Contains, IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class SignUpData {
    @MinLength(3)
    @MaxLength(20)
    login: string;

    @MinLength(3)
    @Contains('@')
    email: string;

    @MinLength(8)
    @MaxLength(16)
    password: string;

    @IsNotEmpty()
    name: string;

    @IsNotEmpty()
    surname: string;

    @IsNotEmpty()
    jobTitle: string;

    @IsNotEmpty()
    telegram: string;
}