import { JwtUserCredentials } from "./jwt-user-credentials";

export interface JwtPayload extends JwtUserCredentials {
    sub: string;
    iat?: number;
    exp?: number;
    jti?: string;
}
  