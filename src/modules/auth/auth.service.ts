import {BadRequestException, ForbiddenException, Injectable} from "@nestjs/common";
import {UserService} from "../user/user.service";
import * as bcrypt from 'bcryptjs';
import {JwtUserCredentials} from "./interfaces/jwt-user-credentials";
import {JwtService} from "@nestjs/jwt";
import {UserImpl} from "../user/user.impl";
import {encrypt} from "../../common/utils";
import {Tokens} from "./interfaces/tokens";
import {SignUpDto} from "./models/sign-up.dto";
import {User} from "@prisma-client";
import {MailService} from "../mail/mail.service";
import {EmailNotConfirmedException} from "@common/exceptions/email-not-confirmed.exception";

@Injectable()
export class AuthService {
    constructor(private readonly userService: UserService,
                private readonly mailService: MailService,
                private readonly jwtService: JwtService) {
    }

    async signUp(signUpDto: SignUpDto): Promise<UserImpl> {
        if (!signUpDto.login || !signUpDto.email || !signUpDto.password)
            throw new BadRequestException(`Invalid data`);

        const existedLogin = await this.userService.getByLogin(signUpDto.login);
        if (existedLogin)
            throw new BadRequestException(`Login ${signUpDto.login} already exists`);

        const existedEmail = await this.userService.getByEmail(signUpDto.email);
        if (existedEmail)
            throw new BadRequestException(`Email ${signUpDto.email} already exists`);

        signUpDto.password = await encrypt(signUpDto.password.trim());

        return await this.userService.create({
            email: signUpDto.email.trim(),
            login: signUpDto.login.trim(),
            password: signUpDto.password,
            role: 'User',
            refreshToken: '',
        } as User);
    }

    //#region Email Confirmation

    async sendConfirmationEmail(email: string): Promise<void> {
        const user = await this.userService.getByEmail(email);
        if (!user) {
            throw new BadRequestException(`User with email ${email} not found`);
        }

        const token = this.jwtService.sign({email}, {
            secret: process.env['JWT_VERIFICATION_TOKEN_SECRET'],
            expiresIn: process.env['JWT_VERIFICATION_TOKEN_EXPIRATION_TIME']
        });

        await this.mailService.sendConfirmationEmail(user, token);
    }

    async decodeConfirmationToken(token: string): Promise<string> {
        try {
            return await this.verifyConfirmationToken(token);
        } catch (error) {
            if (error?.name === 'TokenExpiredError') {
                throw new BadRequestException('Email confirmation token expired');
            }
            throw new BadRequestException('Bad confirmation token');
        }
    }

    async verifyConfirmationToken(token: string): Promise<string> {
        const payload = await this.jwtService.verify(token, {
            secret: process.env['JWT_VERIFICATION_TOKEN_SECRET']
        });

        if (typeof payload === 'object' && 'email' in payload) {
            return payload.email;
        }

        throw new BadRequestException();
    }

    async confirmEmail(email: string): Promise<void> {
        const user = await this.userService.getByEmail(email);
        if (!user) {
            throw new BadRequestException(`User with email ${email} not found`);
        }

        user.isEmailConfirmed = true;

        await this.userService.update(user);
    }

    //#endregion


    async signInLocal(login: string, password: string): Promise<UserImpl> {
        const user = await this.userService.getByLogin(login);
        if (!user) {
            throw new ForbiddenException('Access denied')
        }

        const impl = UserImpl.createFromModel(user);

        const isPasswordCorrect = await bcrypt.compare(password, impl.password);
        if (!isPasswordCorrect) {
            throw new ForbiddenException('Access denied')
        }

        if (!impl.isEmailConfirmed) {
            throw new EmailNotConfirmedException();
        }

        if (impl.isDisabled) {
            throw new ForbiddenException('Account Banned');
        }

        return impl;
    }

    async signInRT(login: string, rt: string): Promise<UserImpl> {
        const user = await this.userService.getByLogin(login);
        if (!user) {
            throw new ForbiddenException('Access denied')
        }

        const impl = UserImpl.createFromModel(user);

        const isRefreshTokenCorrect = await bcrypt.compare(rt, impl.refreshToken);
        if (!isRefreshTokenCorrect || impl.isDisabled) {
            throw new ForbiddenException('Access denied');
        }

        return impl;
    }

    async logout(login: string): Promise<void> {
        const user = await this.userService.getByLogin(login);
        if (!user) {
            return;
        }

        const impl = UserImpl.createFromModel(user);

        impl.refreshToken = '';

        await this.userService.update(impl.toModel());
    }

    async getTokens(user: UserImpl): Promise<Tokens> {
        const payload: JwtUserCredentials = {
            login: user.login,
            password: user.password,
            roles: user.role
        };

        const [at, rt] = await Promise.all([
            this.jwtService.signAsync(payload, {
                secret: process.env["JWT_ACCESS_TOKEN_SECRET"],
                expiresIn: process.env["JWT_ACCESS_TOKEN_EXPIRATION_TIME"]
            }),
            this.jwtService.signAsync(payload, {
                secret: process.env["JWT_REFRESH_TOKEN_SECRET"],
                expiresIn: process.env["JWT_REFRESH_TOKEN_EXPIRATION_TIME"]
            })
        ]);

        await this.updateUserRefreshToken(user, rt);

        return {
            accessToken: at,
            refreshToken: rt
        };
    }

    async updateUserRefreshToken(user: UserImpl, rt: string) {
        user.refreshToken = await encrypt(rt);

        await this.userService.update(user.toModel());
    }
}
