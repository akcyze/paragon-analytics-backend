import {Module} from "@nestjs/common";
import {AuthController} from "./auth.controller";
import {AuthService} from "./auth.service";
import {UserModule} from "../user/user.module";
import {PassportModule} from "@nestjs/passport";
import {JwtModule} from "@nestjs/jwt";
import {AtStrategy, LocalStrategy, RtStrategy} from "./strategies";
import {MailModule} from "../mail/mail.module";
import {LogModule} from "../log/log.module";

@Module({
    imports: [
        UserModule,
        PassportModule,
        MailModule,
        LogModule,
        JwtModule.register({})
    ],
    controllers: [AuthController],
    providers: [
        AuthService,
        LocalStrategy,
        AtStrategy,
        RtStrategy
    ]
})
export class AuthModule {
}
