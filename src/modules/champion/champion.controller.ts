import {
    BadRequestException,
    Body, ClassSerializerInterceptor,
    Controller,
    Delete,
    Get,
    Param,
    ParseIntPipe,
    Post,
    Put, Query, UseInterceptors
} from "@nestjs/common";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {ChampionService} from "./champion.service";
import {ChampionDto} from "./champion.dto";
import {ChampionImpl} from "./champion.impl";
import * as console from "console";

import championsJSON from './champions.json';
import {LogService} from "../log/log.service";

@Controller("champion")
export class ChampionController {

    constructor(private readonly championService: ChampionService,
                private readonly logService: LogService) {
    }

    @Auth(Role.ChampionManager)
    @Put("")
    async create(@Body() dto: ChampionDto): Promise<ChampionDto> {
        if (await this.championService.getByName(dto.name)) {
            throw new BadRequestException(`Champion with name ${dto.name} already exists`);
        }

        const championImpl = new ChampionImpl();

        championImpl.fromDto(dto);
        championImpl.id = undefined;
        const champion = await this.championService.create(championImpl.toModel());

        this.logService.info(`Created champion: ${champion.name}`);

        championImpl.fromModel(champion);

        return championImpl.toDto();
    }

    @Auth(Role.ChampionManager)
    @Post("")
    async save(@Body() dto: ChampionDto): Promise<ChampionDto> {
        const championImpl = new ChampionImpl();

        championImpl.fromDto(dto);

        const champion = await this.championService.update(championImpl.toModel());

        this.logService.info(`Saved champion: ${champion.name}`);

        championImpl.fromModel(champion);

        return championImpl.toDto();
    }

    @Get(':name')
    async getByName(@Param('name') name: string): Promise<ChampionDto> {
        const data = await this.championService.getByName(name);
        if (!data)
            throw new BadRequestException(`Champion with name ${name} not found`);

        const championImpl = ChampionImpl.createFromModel(data);

        const dto = championImpl.toDto();

        dto.builds = dto.builds.sort((a, b) => (a.rating < b.rating) ? 1 : -1)

        return dto;
    }

    @UseInterceptors(ClassSerializerInterceptor)
    @Get('')
    async getAll(): Promise<ChampionDto[]> {
        const champions = await this.championService.getMany();

        return champions.map(ch => ChampionImpl.createFromModel(ch).toDto());
    }
}
