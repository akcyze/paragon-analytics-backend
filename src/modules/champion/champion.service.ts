import {Injectable} from "@nestjs/common";
import {PrismaService} from "../../prisma/prisma.service";
import {Champion} from "@prisma-client";

@Injectable()
export class ChampionService {
    constructor(private readonly prismaService: PrismaService) {
    }

    async create(data: Champion): Promise<Champion> {
        return this.prismaService.champion.create({data});
    }

    async getByName(name: string): Promise<any> {
        return this.prismaService.champion.findFirst({
            where: {name}, include: {
                builds: {
                    include: {
                        creator: true,
                        Likes: true,
                        DisLikes: true,
                    }
                }
            }
        });
    }

    async getMany(): Promise<Champion[]> {
        return this.prismaService.champion.findMany({
            orderBy: {
                id: "asc"
            },
            include: {
                builds: true
            }
        });
    }

    async update(data: Champion): Promise<Champion> {
        return this.prismaService.champion.update({
            data,
            where: {
                id: data.id
            }
        });
    }

    async delete(id: number): Promise<boolean> {
        try {
            await this.prismaService.champion.delete({
                where: {
                    id
                }
            });

            return true;
        } catch (error) {
            return false;
        }
    }
}
