import { Champion} from "@prisma-client";
import {BuildWithCreator} from "../build/build.model";

export type ChampionWithBuilds = Champion & { builds: BuildWithCreator[] };