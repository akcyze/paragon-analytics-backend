import { BaseDto } from "@common/base/base.dto";
import {Role} from "@prisma-client";
import {BuildDto} from "../build/build.dto";

export class ChampionDto extends BaseDto {
    id: number;
    name: string;
    role: Role;
    tier: number;
    counterPicks: string[]

    builds?: BuildDto[];
}

