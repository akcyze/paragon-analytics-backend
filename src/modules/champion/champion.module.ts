import {Module} from "@nestjs/common";
import {PrismaModule} from "../../prisma/prisma.module";
import {ChampionService} from "./champion.service";
import {ChampionController} from "./champion.controller";
import {LogModule} from "../log/log.module";

@Module({
    imports: [
        PrismaModule,
        LogModule
    ],
    controllers: [ChampionController],
    providers: [
        ChampionService
    ],
    exports: [ChampionService]
})
export class ChampionModule {
}
