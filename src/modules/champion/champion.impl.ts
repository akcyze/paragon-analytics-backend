
import { BaseEntity } from "@common/base/base.entity";
import {ChampionDto} from "./champion.dto";
import {Champion, Role} from "@prisma-client";
import {ChampionWithBuilds} from "./champion.model";
import {BuildImpl} from "../build/build.impl";

export class ChampionImpl extends BaseEntity<ChampionDto, Champion> {
    id: number;
    name: string;
    role: Role;
    tier: number;
    counterPicks: string[]

    builds: BuildImpl[];

    public fromDto(dto: ChampionDto) {
        super.fromDto(dto);

        this.id = dto.id;
        this.name = dto.name;
        this.role = dto.role;
        this.tier = dto.tier;
        this.counterPicks = dto.counterPicks;
    }

    public toDto(): ChampionDto {
        const dto = super.toDto();

        dto.id = this.id;
        dto.name = this.name;
        dto.role = this.role;
        dto.tier = this.tier;
        dto.counterPicks = this.counterPicks;

        dto.builds = this.builds ? this.builds.map((b) => b.toDto()) : undefined;

        return dto;
    }

    public fromModel(model: Champion) {
        this.id = model.id;
        this.name = model.name;
        this.role = model.role;
        this.tier = model.tier;
        this.counterPicks = model.counterPicks ?? [];

        if (this.isChampionWithBuilds(model)) {
            this.builds = model.builds.map((b) => BuildImpl.createFromModel(b));
        }
    }

    public toModel(): Champion {
        const model = super.toModel();

        model.id = this.id;
        model.name = this.name;
        model.role = this.role;
        model.tier = this.tier;
        model.counterPicks = this.counterPicks ?? [];

        return model;
    }

    private isChampionWithBuilds(champion: Champion): champion is ChampionWithBuilds {
        return (champion as ChampionWithBuilds).builds !== undefined;
    }

    public static createFromModel(model: Champion): ChampionImpl {
        const impl = new ChampionImpl();

        impl.fromModel(model);

        return impl;
    }
}