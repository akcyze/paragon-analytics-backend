import { Module } from '@nestjs/common';
import { UserService } from "./user.service";
import { PrismaModule } from "../../prisma/prisma.module";
import { UserController } from "./user.controller";
import {LogModule} from "../log/log.module";

@Module({
    imports: [PrismaModule, LogModule],
    controllers: [UserController],
    providers: [UserService],
    exports: [UserService]
})
export class UserModule {}
