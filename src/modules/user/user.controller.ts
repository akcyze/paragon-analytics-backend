import {
    BadRequestException, Body,
    Controller, Delete,
    ForbiddenException,
    Get,
    Param,
    ParseIntPipe,
    Post,
    UseInterceptors,
} from "@nestjs/common";
import {UserService} from "./user.service";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {ProfileDto, UserDto, UserReadOnly} from "./user.dto";
import {UserImpl} from "./user.impl";
import {CurrentUserInterceptor} from "@common/interceptors/current-user.interceptor";
import {CurrentUser} from "@common/decorators/current-user.decorator";
import * as console from "console";
import {LogService} from "../log/log.service";

@Controller('user')
@UseInterceptors(CurrentUserInterceptor)
export class UserController {
    constructor(private readonly userService: UserService,
                private readonly logService: LogService) {
    }

    @Get(':id')
    @Auth(Role.User)
    async getById(@CurrentUser('id') currentUserID: number, @CurrentUser('role') roles: string[], @Param('id', ParseIntPipe) id: number): Promise<UserDto> {
        if (currentUserID !== id && !roles.includes(Role.Admin) && !roles.includes(Role.TournamentManager)) {
            throw new ForbiddenException("User can fetch only self data");
        }

        const user = await this.userService.getById(id);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        return UserImpl.createFromModel(user).toDto();
    }

    @Get('')
    @Auth(Role.Admin)
    async getMany(): Promise<UserDto[]> {
        const users = await this.userService.getMany();
        const impls = users.map(user => UserImpl.createFromModel(user));

        return impls.map(impl => impl.toDto());
    }

    @Post('')
    @Auth(Role.User)
    async updateUser(@Body() userDto: UserDto, @CurrentUser('id') currentUserID: number, @CurrentUser('role') roles: string[]): Promise<UserDto> {
        let user = await this.userService.getById(userDto.id);
        if (!user) {
            throw new BadRequestException('User not found');
        }

        if (user.id !== currentUserID && !roles.includes(Role.Admin)) {
            throw new ForbiddenException("User can update only self data");
        }

        const impl = UserImpl.createFromModel(user);

        impl.fromDto(userDto);

        await this.userService.update(impl.toModel());

        user = await this.userService.getById(userDto.id);

        this.logService.info(`Updated user ${user.id}:${user.login}`);

        impl.fromModel(user);

        return impl.toDto();
    }

    @Delete(':id')
    @Auth(Role.Admin)
    async delete(@Param('id', ParseIntPipe) id: number): Promise<boolean> {
        const user = await this.userService.getById(id);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        await this.userService.delete(user.id);

        this.logService.warn(`Deleted user ${user.id}:${user.login}`)

        return true;
    }

    @Delete('likes/:id')
    @Auth(Role.Admin)
    async deleteLikesByUser(@Param('id', ParseIntPipe) id: number): Promise<boolean> {
        const user = await this.userService.getById(id);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        await this.userService.deleteLikesByUser(user.id);

        this.logService.warn(`Deleted likes by user ${user.id}:${user.login}`);

        return true;
    }

    @Delete('dislikes/:id')
    @Auth(Role.Admin)
    async deleteDisLikesByUser(@Param('id', ParseIntPipe) id: number): Promise<boolean> {
        const user = await this.userService.getById(id);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        await this.userService.deleteDisLikesByUser(user.id);

        this.logService.warn(`Deleted dislikes by user ${user.id}:${user.login}`);

        return true;
    }

    @Get('readonly/many')
    async getManyReadOnly(): Promise<UserReadOnly[]> {
        const users = await this.userService.getMany();
        const impls = users.map(user => UserImpl.createFromModel(user));

        return impls.map(impl => impl.toReadOnly());
    }

    @Get('profile/:login')
    async getProfileByLogin(@CurrentUser('id') currentUserID: number, @Param('login') login: string): Promise<ProfileDto> {
        const user = await this.userService.getProfileByLogin(login);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        const userImpl = UserImpl.createFromModel(user);

        return userImpl.toProfileDto();
    }

    @Get('profiles/builds')
    async getProfilesWithBuilds(): Promise<ProfileDto[]> {
        const users = await this.userService.getProfilesWithBuilds();
        const impls = users.map(user => UserImpl.createFromModel(user));
        const usersWithBuilds = impls.filter(impl => impl.builds.length > 0).sort((a, b) => b.rating - a.rating);

        return usersWithBuilds.map(impl => impl.toProfileDto());
    }

    @Post('profile/icon/:icon')
    @Auth(Role.User)
    async changeIcon(@CurrentUser('id') currentUserID: number, @Param('icon') icon: string): Promise<boolean> {
        const user = await this.userService.getById(currentUserID);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        const impl = UserImpl.createFromModel(user);

        impl.icon = icon;

        await this.userService.update(impl.toModel());

        return true;
    }
}
