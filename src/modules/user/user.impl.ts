import {UserDto, ProfileDto, UserReadOnly} from "./user.dto";
import {BaseEntity} from "@common/base/base.entity";
import {Role} from "../auth/guard/role.type";
import {Build, User} from "@prisma-client";
import {UserWithBuilds} from "./user.model";
import {BuildImpl} from "../build/build.impl";
import {getRating} from "@common/utils";
import {TeamImpl} from "../team/team.impl";

export class UserImpl extends BaseEntity<UserDto, User> {
    id: number;

    login: string;
    displayName: string;

    discord: string;
    telegram: string;
    gameRank: number;

    email: string;
    isEmailConfirmed: boolean;

    password: string;

    isDisabled: boolean;

    kills: number;
    deaths: number;
    assists: number;
    tournamentWins: number[];
    gameWins: number;
    gameLosed: number;

    role: Role[];

    refreshToken: string;
    icon: string;

    rating: number;
    rank: number;
    builds: BuildImpl[];

    teamID: number;
    team: TeamImpl;

    likedBuilds: BuildImpl[];

    public fromDto(dto: UserDto) {
        super.fromDto(dto);

        this.id = dto.id;

        this.login = dto.login;
        this.displayName = dto.displayName;

        this.discord = dto.discord;
        this.telegram = dto.telegram;
        this.gameRank = dto.gameRank;

        this.email = dto.email;
        this.isEmailConfirmed = dto.isEmailConfirmed;

        this.isDisabled = dto.isDisabled;

        this.kills = dto.kills;
        this.deaths = dto.deaths;
        this.assists = dto.assists;
        this.tournamentWins = dto.tournamentWins;
        this.gameWins = dto.gameWins;
        this.gameLosed = dto.gameLosed;

        this.role = dto.role;

        this.teamID = dto.teamID;
    }

    public toDto(): UserDto {
        const dto = super.toDto();

        dto.id = this.id;

        dto.login = this.login;
        dto.displayName = this.displayName;

        dto.discord = this.discord;
        dto.telegram = this.telegram;
        dto.gameRank = this.gameRank;

        dto.email = this.email;
        dto.isEmailConfirmed = this.isEmailConfirmed;

        dto.isDisabled = this.isDisabled;

        dto.kills = this.kills;
        dto.deaths = this.deaths;
        dto.assists = this.assists;
        dto.tournamentWins = this.tournamentWins;
        dto.gameWins = this.gameWins;
        dto.gameLosed = this.gameLosed;

        dto.role = this.role;

        dto.teamID = this.teamID;

        return dto;
    }

    public toProfileDto(): ProfileDto {
        const dto = new ProfileDto();

        dto.id = this.id;
        dto.login = this.login;
        dto.displayName = this.displayName;
        dto.discord = this.discord;
        dto.telegram = this.telegram;
        dto.gameRank = this.gameRank;
        dto.rating = this.rating;
        dto.rank = this.rank;
        dto.icon = this.icon;
        dto.kills = this.kills;
        dto.deaths = this.deaths;
        dto.assists = this.assists;
        dto.tournamentWins = this.tournamentWins;
        dto.gameWins = this.gameWins;
        dto.gameLosed = this.gameLosed;
        dto.builds = this.builds?.map((b) => b.toDto()) ?? [];
        dto.team = this.team?.toReadOnly() ?? null;
        dto.likedBuilds = this.likedBuilds?.map((b) => b.toReadOnlyDto()) ?? [];

        return dto;
    }

    public toReadOnly(): UserReadOnly {
        const readOnly = new UserReadOnly();

        readOnly.id = this.id;
        readOnly.login = this.login;

        return readOnly;
    }

    public fromModel(model: User) {
        this.id = model.id;

        this.login = model.login;
        this.displayName = model.displayName && model.displayName.length > 0 ? model.displayName : model.login;

        this.discord = model.discord;
        this.telegram = model.telegram;
        this.gameRank = model.gameRank;

        this.email = model.email;
        this.isEmailConfirmed = model.isEmailConfirmed;

        this.password = model.password;

        this.icon = model.icon;

        this.isDisabled = model.isDisabled;

        this.kills = model.kills;
        this.deaths = model.deaths;
        this.assists = model.assists;
        this.tournamentWins = model.tournamentWins;
        this.gameWins = model.gameWins;
        this.gameLosed = model.gameLosed;

        this.role = model.role.split(',').map(role => {
            return Role[role]
        });

        this.refreshToken = model.refreshToken;

        this.teamID = model.teamID;

        if (model['team']) {
            this.team = TeamImpl.createFromModel(model['team']);
        }

        if (model['likesBuilds']) {
            this.likedBuilds = model['likesBuilds'].map((b: Build) => BuildImpl.createFromModel(b['build']));
        }

        if (this.isUserWithBuilds(model)) {
            this.builds = model.builds.map((b) => BuildImpl.createFromModel(b));

            this.rating = this.calculateRating();
            this.rank = this.getRank();
        }
    }

    public toModel(): User {
        const model = super.toModel();

        model.id = this.id;

        model.login = this.login;
        model.displayName = this.displayName;

        model.discord = this.discord;
        model.telegram = this.telegram;
        model.gameRank = this.gameRank;

        model.email = this.email;
        model.isEmailConfirmed = this.isEmailConfirmed;

        model.password = this.password;

        model.icon = this.icon;

        model.isDisabled = this.isDisabled;

        model.kills = this.kills;
        model.deaths = this.deaths;
        model.assists = this.assists;
        model.tournamentWins = this.tournamentWins;
        model.gameWins = this.gameWins;
        model.gameLosed = this.gameLosed;

        model.role = this.role.toString();

        model.refreshToken = this.isDisabled ? '' : this.refreshToken

        model.teamID = this.teamID;

        return model;
    }

    private isUserWithBuilds(user: User): user is UserWithBuilds {
        return (user as UserWithBuilds).builds !== undefined;
    }

    calculateRating(): number {
        const sumOfLikes = Math.max(0, this.builds.reduce((rating, b) => rating + b.likes.length, 0));
        const sumOfDislikes = Math.max(0, this.builds.reduce((rating, b) => rating + b.dislikes.length, 0));
        const rating = getRating(this.builds.filter(b => b.likes.length > b.dislikes.length).length, this.builds.length);
        const rating2 = getRating(sumOfLikes, sumOfLikes + sumOfDislikes);
        const rating3 = (rating + rating2) / 2;

        return Number.parseFloat(rating3.toFixed(2));
    }

    getRank(): number {
        if (this.rating <= 0.14)
            return 1;
        if (this.rating > 0.14 && 0.28 >= this.rating)
            return 2;
        if (this.rating > 0.28 && 0.42 >= this.rating)
            return 3;
        if (this.rating > 0.42 && 0.60 >= this.rating)
            return 4;
        if (this.rating > 0.60 && 0.74 >= this.rating)
            return 5;
        if (this.rating > 0.74 && 0.9 >= this.rating)
            return 6;
        if (this.rating > 0.9)
            return 7;
    }

    public static createFromModel(model: User): UserImpl {
        const impl = new UserImpl();

        impl.fromModel(model);

        return impl;
    }
}