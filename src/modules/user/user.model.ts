import {Build, User} from "@prisma-client";

export type UserWithBuilds = User & { builds: Build[] };