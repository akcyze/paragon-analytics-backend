import {Injectable, InternalServerErrorException} from "@nestjs/common";
import {UserImpl} from "./user.impl";
import {PrismaService} from "../../prisma/prisma.service";
import {User} from "@prisma-client";

@Injectable()
export class UserService {

    constructor(private readonly prismaService: PrismaService) {
    }

    async create(user: User): Promise<UserImpl> {
        try {
            const model = await this.prismaService.user.create({data: user});

            return UserImpl.createFromModel(model);
        } catch (error) {
            throw new InternalServerErrorException(error);
        }
    }

    async getByLogin(login: string): Promise<User> {
        return this.prismaService.user.findFirst({
            where: {
                login: {
                    equals: login,
                    mode: "insensitive"
                }
            }
        })
    }

    async getByEmail(email: string): Promise<User> {
        return this.prismaService.user.findFirst({
            where: {
                email: {
                    equals: email,
                    mode: "insensitive"
                }
            }
        });
    }

    getProfileByLogin(login: string): Promise<User> {
        return this.prismaService.user.findFirst({
            where: {login},
            include: {
                builds: {
                    include: {
                        Likes: true,
                        DisLikes: true
                    }
                },
                team: true,
                likesBuilds: {
                    include: {
                        build: {
                            include: {
                                Likes: true,
                                DisLikes: true
                            }
                        }
                    }
                }
            }
        });
    }

    getProfilesWithBuilds(): Promise<User[]> {
        return this.prismaService.user.findMany({
            include: {
                builds: {
                    select: {
                        id: true,
                        name: true,
                        championID: true,
                        role: true,
                        Likes: true,
                        DisLikes: true
                    }
                }
            }
        })
    }

    async getById(id: number): Promise<User> {
        return this.prismaService.user.findFirst({where: {id}});
    }

    async getMany(): Promise<User[]> {
        return this.prismaService.user.findMany({
            include: {
                builds: {
                    include: {
                        Likes: true,
                        DisLikes: true
                    }
                }
            }
        });
    }

    async update(user: User) {
        await this.prismaService.user.update({data: user, where: {id: user.id}});
    }

    async delete(id: number): Promise<void> {
        await this.prismaService.likes.deleteMany({where: {userID: id}});
        await this.prismaService.disLikes.deleteMany({where: {userID: id}});

        await this.prismaService.user.delete({where: {id}, include: {builds: true}});
    }

    async deleteLikesByUser(id: number): Promise<void> {
        await this.prismaService.likes.deleteMany({where: {userID: id}});
    }

    async deleteDisLikesByUser(id: number): Promise<void> {
        await this.prismaService.disLikes.deleteMany({where: {userID: id}});
    }
}
