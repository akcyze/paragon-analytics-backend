import { BaseDto } from "@common/base/base.dto";
import { Role } from "../auth/guard/role.type";
import {BuildDto, BuildReadOnlyDto} from "../build/build.dto";
import {TeamReadOnlyDto} from "../team/team.dto";

export class UserDto extends BaseDto {
    id: number;

    login: string;
    displayName: string;

    discord: string;
    telegram: string;
    gameRank: number;

    email: string;
    isEmailConfirmed: boolean;

    isDisabled: boolean;

    kills: number;
    deaths: number;
    assists: number;
    tournamentWins: number[];
    gameWins: number;
    gameLosed: number;

    role: Role[];

    teamID: number;
}

export class ProfileDto extends BaseDto {
    id: number;

    login: string;
    displayName: string;

    discord: string;
    telegram: string;
    gameRank: number;
    
    rating: number;
    rank: number;

    icon: string;

    kills: number;
    deaths: number;
    assists: number;
    tournamentWins: number[];
    gameWins: number;
    gameLosed: number;

    builds: BuildDto[];
    likedBuilds: BuildReadOnlyDto[];
    team: TeamReadOnlyDto;
}

export class UserReadOnly {
    id: number;
    login: string;
}