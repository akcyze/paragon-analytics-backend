import {Item, ItemType} from "@prisma-client";
import { BaseEntity } from "@common/base/base.entity";
import {ItemDto, ItemParamDto} from "./item.dto";

export class ItemEntity extends BaseEntity<ItemDto, Item> {
    id: number;
    name: string;
    displayName: { [key: string]: string };
    type: ItemType;
    price: number;
    params: ItemParamDto[];
    effect: { [key: string]: string };
    effectParams: string;

    public fromDto(dto: ItemDto) {
        super.fromDto(dto);

        this.id = dto.id;
        this.name = dto.name;
        this.displayName = dto.displayName;
        this.type = dto.type;
        this.price = dto.price;
        this.params = dto.params;
        this.effect = dto.effect;
        this.effectParams = dto.effectParams;
    }

    public toDto(): ItemDto {
        const dto = super.toDto();

        dto.id = this.id;
        dto.name = this.name;
        dto.displayName = this.displayName;
        dto.type = this.type;
        dto.price = this.price;
        dto.params = this.params;
        dto.effect = this.effect;
        dto.effectParams = this.effectParams;

        return dto;
    }

    public fromModel(model: Item) {
        this.id = model.id;
        this.name = model.name;
        this.displayName = JSON.parse(model.displayName) ?? {};
        this.type = model.type;
        this.price = model.price;
        this.params = JSON.parse(model.params) ?? [];
        this.effect = JSON.parse(model.effect) ?? {};
        this.effectParams = model.effectParams;
    }

    public toModel(): Item {
        const model = super.toModel();

        model.id = this.id;
        model.name = this.name;
        model.displayName = JSON.stringify(this.displayName);
        model.type = this.type;
        model.price = this.price;
        model.params = JSON.stringify(this.params);
        model.effect = JSON.stringify(this.effect);
        model.effectParams = this.effectParams;

        return model;
    }

    public static createFromModel(model: Item): ItemEntity {
        const impl = new ItemEntity();

        impl.fromModel(model);

        return impl;
    }
}