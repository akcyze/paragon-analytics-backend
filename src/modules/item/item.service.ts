import {Injectable} from "@nestjs/common";
import {PrismaService} from "../../prisma/prisma.service";
import {Item} from "@prisma-client";

@Injectable()
export class ItemService {
    constructor(private readonly prismaService: PrismaService) {
    }

    async create(data: Item): Promise<Item> {
        return this.prismaService.item.create({data});
    }

    async getById(id: number): Promise<Item> {
        return this.prismaService.item.findFirst({where: {id}});
    }

    async getByName(name: string): Promise<Item> {
        return this.prismaService.item.findFirst({where: {name}});
    }

    async getMany(): Promise<Item[]> {
        return this.prismaService.item.findMany({
            orderBy: {
                id: "asc"
            }
        });
    }

    async update(data: Item): Promise<Item> {
        return this.prismaService.item.update({
            data,
            where: {
                id: data.id
            }
        });
    }

    async delete(id: number): Promise<boolean> {
        try {
            await this.prismaService.item.delete({
                where: {
                    id
                }
            });

            return true;
        } catch (error) {
            return false;
        }
    }
}
