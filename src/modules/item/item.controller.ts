import {
    BadRequestException,
    Body,
    Controller,
    Get, Param,
    Post,
    Put, UseInterceptors
} from "@nestjs/common";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {ItemService} from "./item.service";
import {ItemEntity} from "./item.entity";
import {CurrentUserInterceptor} from "@common/interceptors/current-user.interceptor";
import {ItemDto} from "./item.dto";
import {LogService} from "../log/log.service";

@Controller("item")
@UseInterceptors(CurrentUserInterceptor)
export class ItemController {

    constructor(private readonly itemService: ItemService,
                private readonly logService: LogService) {
    }

    @Auth(Role.ItemsManager)
    @Put("")
    async create(@Body() dto: ItemDto): Promise<ItemDto> {
        const itemImpl = new ItemEntity();

        itemImpl.fromDto(dto);
        itemImpl.id = undefined;

        const item = await this.itemService.create(itemImpl.toModel());

        this.logService.info(`Created item: ${item.name}`);

        itemImpl.fromModel(item);

        return itemImpl.toDto();
    }

    @Get(":name")
    async getByName(@Param('name') name: string): Promise<ItemDto> {
        const item = await this.itemService.getByName(name);
        if (!item) {
            throw new BadRequestException(`Item with name ${name} not existing`);
        }

        return ItemEntity.createFromModel(item).toDto();
    }

    @Get("")
    async getMany(): Promise<ItemDto[]> {
        const items = await this.itemService.getMany();

        return items.map(item => ItemEntity.createFromModel(item)).map(item => item.toDto());
    }

    @Auth(Role.ItemsManager)
    @Post("")
    async save(@Body() dto: ItemDto): Promise<ItemDto> {
        if (!dto.id) {
            throw new BadRequestException(`Invalid item ID`);
        }

        let item = await this.itemService.getById(dto.id);
        if (!item) {
            throw new BadRequestException(`Item with id ${dto.id} not existing`);
        }

        const itemImpl = ItemEntity.createFromModel(item);

        itemImpl.fromDto(dto);

        item = await this.itemService.update(itemImpl.toModel());

        this.logService.info(`Updated item: ${item.name}`);

        itemImpl.fromModel(item);

        return itemImpl.toDto();
    }
}
