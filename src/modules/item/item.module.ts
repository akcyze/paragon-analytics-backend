import {Module} from "@nestjs/common";
import {PrismaModule} from "../../prisma/prisma.module";
import {ItemService} from "./item.service";
import {ItemController} from "./item.controller";
import {UserModule} from "../user/user.module";
import {LogModule} from "../log/log.module";

@Module({
    imports: [
        PrismaModule,
        UserModule,
        LogModule
    ],
    controllers: [ItemController],
    providers: [
        ItemService
    ],
    exports: [ItemService]
})
export class ItemModule {
}
