import {ItemType} from "@prisma-client";
import { BaseDto } from "@common/base/base.dto";

export class ItemDto extends BaseDto {
    id: number;
    name: string;
    displayName: { [key: string]: string };
    type: ItemType;
    price: number;
    params: ItemParamDto[];
    effect: { [key: string]: string };
    effectParams: string;
}

export class ItemParamDto {
    value: string;
    type: ItemParamType
}

export enum ItemParamType {
    AllVamp = "AllVamp",
    AttackSpeed = "AttackSpeed",
    CooldownReduction = "CooldownReduction",
    CriticalDamage = "CriticalDamage",
    CriticalRate = "CriticalRate",
    Health = "Health",
    HealthRegen = "HealthRegen",
    Lifesteal = "Lifesteal",
    MagicalDefense = "MagicalDefense",
    MagicalPenetration = "MagicalPenetration",
    MagicalPower = "MagicalPower",
    MagicalVamp = "MagicalVamp",
    Mana = "Mana",
    ManaRegen = "ManaRegen",
    MovementSpeed = "MovementSpeed",
    PhysicalDefense = "PhysicalDefense",
    PhysicalPenetration = "PhysicalPenetration",
    PhysicalPower = "PhysicalPower",
    PhysicalVamp = "PhysicalVamp",
    Tenacity = "Tenacity"
}