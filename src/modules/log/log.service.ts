import {Injectable} from '@nestjs/common';
import {PrismaService} from "../../prisma/prisma.service";
import {interval, Observable} from "rxjs";
import {Log, LogType} from "@prisma-client";
import console from "console";

@Injectable()
export class LogService {
    interval: Observable<any>;

    logs: Log[] = [];

    constructor(private prismaService: PrismaService) {
        this.setupInterval();

        this.info(`LogService initialized`);
    }

    setupInterval() {
        this.interval = interval(10000);
        this.interval.subscribe(() => {
            const logs = JSON.parse(JSON.stringify(this.logs)) as Log[];

            const promises = [];
            logs.forEach(data => {
                try {
                    promises.push(this.prismaService.log.create({ data }));
                } catch (e) {
                    console.error(e);
                }
            })

            Promise.all(promises).then(() => {
                //
            });

            this.logs = [];
        });
    }

    info(message: string) {
        console.info(message);

        this.addLog(LogType.Info, message);
    }

    warn(message: string) {
        console.warn(message);

        this.addLog(LogType.Warning, message);
    }

    error(message: string) {
        console.error(message);

        this.addLog(LogType.Error, message);
    }

    getAll(): Promise<Log[]> {
        return this.prismaService.log.findMany();
    }
    
    clear() {
        this.prismaService.log.deleteMany().then(() => {
            this.info(`Logs cleared`);
        });
    }

    private addLog(type: LogType, message: string) {
        this.logs.push({
            type,
            message
        } as Log)
    }
}