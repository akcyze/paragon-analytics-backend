import {
    Controller, Get,
    Post,
} from "@nestjs/common";

import {LogService} from "./log.service";
import {Log} from "@prisma-client";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";

@Controller('logs')
export class LogController {
    constructor(private readonly logService: LogService) {}

    @Auth(Role.Admin)
    @Get('')
    async getMany(): Promise<Log[]> {
        return this.logService.getAll();
    }

    @Auth(Role.Admin)
    @Post('')
    async clear(): Promise<void> {
        return this.logService.clear();
    }
}
