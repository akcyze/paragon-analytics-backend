import { Module } from '@nestjs/common';
import {PrismaModule} from "../../prisma/prisma.module";
import {LogService} from "./log.service";
import {LogController} from "./log.controller";

@Module({
    imports: [PrismaModule],
    controllers: [LogController],
    providers: [LogService],
    exports: [LogService],
})
export class LogModule {}