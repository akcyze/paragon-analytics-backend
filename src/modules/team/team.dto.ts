import { BaseDto } from "@common/base/base.dto";
import {ProfileDto} from "../user/user.dto";

export class TeamDto extends BaseDto {
    id: number;
    createdAt: Date;
    name: string;
    description: string;
    icon: string;
    rating: number
    ownerID: number;
    owner?: ProfileDto;
    members: ProfileDto[];
    requests: TeamJoinRequest[];

    tournamentWins: number[];
    gameWins: number;
    gameLosed: number;
}

export class TeamReadOnlyDto {
    id: number;
    createdAt: Date;
    name: string;
    description: string;
    icon: string;
    rating: number
    members: string[];

    tournamentWins: number[];
    gameWins: number;
    gameLosed: number;
}

export class TeamJoinRequest {
    login: string;
    description: string;
}