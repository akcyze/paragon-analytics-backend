import {Prisma, Team, User} from "@prisma-client";
import { BaseEntity } from "@common/base/base.entity";
import {TeamDto, TeamJoinRequest, TeamReadOnlyDto} from "./team.dto";
import {UserImpl} from "../user/user.impl";

export class TeamImpl extends BaseEntity<TeamDto, Team> {
    id: number;
    createdAt: Date;
    name: string;
    description: string;
    icon: string;
    rating: Prisma.Decimal;
    ownerID: number;
    owner?: UserImpl;
    members: UserImpl[];
    requests: TeamJoinRequest[];

    tournamentWins: number[];
    gameWins: number;
    gameLosed: number;

    public fromDto(dto: TeamDto) {
        super.fromDto(dto);

        this.id = dto.id;
        this.createdAt = dto.createdAt;
        this.name = dto.name;
        this.description = dto.description;
        this.icon = dto.icon;
        this.rating = new Prisma.Decimal(dto.rating ?? 0);
        this.ownerID = dto.ownerID;
        this.requests = dto.requests;

        this.tournamentWins = dto.tournamentWins;
        this.gameWins = dto.gameWins;
        this.gameLosed = dto.gameLosed;
    }

    public toDto(): TeamDto {
        const dto = super.toDto();

        dto.id = this.id;
        dto.createdAt = this.createdAt;
        dto.name = this.name;
        dto.description = this.description;
        dto.icon = this.icon;
        dto.rating = this.rating.toNumber();
        dto.ownerID = this.ownerID;
        dto.owner = this.owner ? this.owner.toProfileDto() : null;
        dto.members = this.members ? this.members.map((member: UserImpl) => member.toProfileDto()) : [];
        dto.requests = this.requests;

        dto.tournamentWins = this.tournamentWins;
        dto.gameWins = this.gameWins;
        dto.gameLosed = this.gameLosed;

        return dto;
    }

    public toReadOnly(): TeamReadOnlyDto {
        const dto = new TeamReadOnlyDto();

        dto.id = this.id;
        dto.createdAt = this.createdAt;
        dto.name = this.name;
        dto.description = this.description;
        dto.icon = this.icon;
        dto.rating = this.rating.toNumber();
        dto.members = this.members ? this.members.map((member: UserImpl) => member.displayName) : [];

        dto.tournamentWins = this.tournamentWins;
        dto.gameWins = this.gameWins;
        dto.gameLosed = this.gameLosed;

        return dto;
    }

    public fromModel(model: Team) {
        super.fromModel(model);

        this.id = model.id;
        this.createdAt = model.createdAt;
        this.name = model.name;
        this.description = model.description;
        this.icon = model.icon;
        this.rating = model.rating;
        this.ownerID = model.ownerID;

        this.tournamentWins = model.tournamentWins;
        this.gameWins = model.gameWins;
        this.gameLosed = model.gameLosed;

        this.owner = model['owner'] ? UserImpl.createFromModel(model['owner']) : null;
        this.members = model['members'] ? model['members'].map((member: User) => UserImpl.createFromModel(member)) : [];
        this.requests = model.requests?.map(r => JSON.parse(r) as TeamJoinRequest);
    }

    public toModel(): Team {
        const model = super.toModel();

        model.id = this.id;
        model.createdAt = this.createdAt;
        model.name = this.name;
        model.description = this.description;
        model.icon = this.icon;
        model.rating = this.rating;
        model.ownerID = this.ownerID;
        model.requests = this.requests?.map(r => JSON.stringify(r));

        model.tournamentWins = this.tournamentWins;
        model.gameWins = this.gameWins;
        model.gameLosed = this.gameLosed;

        return model;
    }

    public static createFromModel(model: Team): TeamImpl {
        const impl = new TeamImpl();

        impl.fromModel(model);

        return impl;
    }
}