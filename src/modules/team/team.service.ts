import {Injectable} from "@nestjs/common";
import {PrismaService} from "../../prisma/prisma.service";
import {Item, Team} from "@prisma-client";

@Injectable()
export class TeamService {
    constructor(private readonly prismaService: PrismaService) {
    }

    async create(data: Team): Promise<Team> {
        return this.prismaService.team.create({data});
    }

    async getById(id: number): Promise<Team> {
        return this.prismaService.team.findFirst({where: {id}, include: {owner: true, members: true}});
    }

    async getByIdSelf(id: number): Promise<Team> {
        return this.prismaService.team.findFirst({where: {id}});
    }

    async getByName(name: string): Promise<Team> {
        return this.prismaService.team.findFirst({where: {name}, include: {owner: true, members: true}});
    }

    async getMany(): Promise<Team[]> {
        return this.prismaService.team.findMany({
            orderBy: {
                id: "asc"
            },
            include: {owner: true, members: true}
        });
    }

    async update(data: Team): Promise<Team> {
        return this.prismaService.team.update({
            data,
            where: {
                id: data.id
            }
        });
    }

    async delete(id: number): Promise<boolean> {
        try {
            await this.prismaService.team.delete({
                where: {
                    id
                }
            });

            return true;
        } catch (error) {
            return false;
        }
    }
}
