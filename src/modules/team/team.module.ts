import {Module} from "@nestjs/common";
import {PrismaModule} from "../../prisma/prisma.module";
import {TeamService} from "./team.service";
import {TeamController} from "./team.controller";
import {UserModule} from "../user/user.module";
import {LogModule} from "../log/log.module";

@Module({
    imports: [
        PrismaModule,
        UserModule,
        LogModule,
    ],
    controllers: [TeamController],
    providers: [
        TeamService
    ],
    exports: [TeamService]
})
export class TeamModule {
}
