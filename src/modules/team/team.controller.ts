import {
    BadRequestException,
    Body,
    Controller, Delete,
    Get, Param, ParseIntPipe,
    Post,
    Put, UseInterceptors
} from "@nestjs/common";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {TeamService} from "./team.service";
import {TeamImpl} from "./team.impl";
import {CurrentUserInterceptor} from "@common/interceptors/current-user.interceptor";
import {TeamDto, TeamJoinRequest, TeamReadOnlyDto} from "./team.dto";
import {LogService} from "../log/log.service";
import {CurrentUser} from "@common/decorators/current-user.decorator";
import {UserService} from "../user/user.service";

@Controller("team")
@UseInterceptors(CurrentUserInterceptor)
export class TeamController {

    constructor(private readonly teamService: TeamService,
                private readonly logService: LogService,
                private readonly userService: UserService) {
    }

    @Auth(Role.User)
    @Put("")
    async create(@Body() dto: TeamDto, @CurrentUser('id') currentUserID: number): Promise<TeamDto> {
        const teamImpl = new TeamImpl();

        teamImpl.fromDto(dto);
        teamImpl.id = undefined;
        teamImpl.ownerID = currentUserID;

        const item = await this.teamService.create(teamImpl.toModel());

        const user = await this.userService.getById(currentUserID);

        user.teamID = item.id;

        await this.userService.update(user);

        this.logService.info(`Created item: ${item.name}. Owner: ${item.ownerID}`);

        teamImpl.fromModel(item);

        return teamImpl.toDto();
    }

    @Auth(Role.User)
    @Post("")
    async save(@Body() dto: TeamDto,
               @CurrentUser('id') currentUserID: number,
               @CurrentUser('role') roles: string[]): Promise<TeamDto> {
        if (!dto.id) {
            throw new BadRequestException(`Invalid team ID`);
        }

        if (dto.ownerID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can update team`);
        }

        let team = await this.teamService.getById(dto.id);
        if (!team) {
            throw new BadRequestException(`Item with id ${dto.id} not existing`);
        }

        const teamImpl = TeamImpl.createFromModel(team);

        teamImpl.fromDto(dto);

        team = await this.teamService.update(teamImpl.toModel());

        this.logService.info(`Updated team: ${team.name}`);

        teamImpl.fromModel(team);

        return teamImpl.toDto();
    }

    @Get("")
    async getMany(): Promise<TeamReadOnlyDto[]> {
        const teams = await this.teamService.getMany();

        return teams.map(team => TeamImpl.createFromModel(team)).map(team => team.toReadOnly());
    }

    @Get(":id")
    async getById(@Param("id", ParseIntPipe) id: number): Promise<TeamDto> {
        const team = await this.teamService.getById(id);
        if (!team) {
            throw new BadRequestException(`Item with id ${id} not existing`);
        }

        const teamImpl = TeamImpl.createFromModel(team);

        return teamImpl.toDto();
    }

    @Auth(Role.User)
    @Delete(":id")
    async delete(@Param("id", ParseIntPipe) id: number,
                 @CurrentUser('id') currentUserID: number,
                 @CurrentUser('role') roles: string[]): Promise<boolean> {
        const team = await this.teamService.getById(id);
        if (!team) {
            throw new BadRequestException(`Item with id ${id} not existing`);
        }

        if (team.ownerID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can delete team`);
        }

        await this.teamService.delete(team.id);

        return true;
    }


    @Auth(Role.User)
    @Post("join/:id")
    async addJoinRequest(
        @Param("id", ParseIntPipe) id: number,
        @Body() dto: TeamJoinRequest,
        @CurrentUser('id') currentUserID: number): Promise<TeamDto> {

        const teamImpl = await this.getTeamById(id);

        const user = await this.userService.getById(currentUserID);
        if (!user) {
            throw new BadRequestException(`User with id ${currentUserID} not existing`);
        }

        if (user.teamID) {
            throw new BadRequestException(`User with id ${currentUserID} already in team`);
        }

        if (teamImpl.members.some(m => m.id === currentUserID)) {
            throw new BadRequestException(`User with id ${currentUserID} already in team`);
        }

        if (teamImpl.requests.some(r => r.login === user.login)) {
            throw new BadRequestException(`User with id ${currentUserID} already requested to join team`);
        }

        teamImpl.requests.push(dto);

        const team = await this.teamService.update(teamImpl.toModel());

        this.logService.info(`Added team join request: ${team.name}:${dto.login}`);

        teamImpl.fromModel(team);

        return teamImpl.toDto();
    }

    @Auth(Role.User)
    @Post("request/accept/:id/:login")
    async acceptJoinRequest(
        @Param("id", ParseIntPipe) id: number,
        @Param("login") login: string,
        @CurrentUser('id') currentUserID: number,
        @CurrentUser('role') roles: string[]): Promise<TeamDto> {

        const teamImpl = await this.getTeamById(id);
        if (teamImpl.ownerID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can accept team join request`);
        }

        const requestIndex = teamImpl.requests.findIndex(r => r.login === login);
        if (requestIndex < 0) {
            throw new BadRequestException(`Join request not found with login ${login}`);
        }

        teamImpl.requests.splice(requestIndex, 1);

        const team = await this.teamService.update(teamImpl.toModel());

        teamImpl.fromModel(team);

        const user = await this.userService.getByLogin(login);
        if (!user) {
            throw new BadRequestException(`User with id ${currentUserID} not existing`);
        }

        if (user.teamID) {
            throw new BadRequestException(`User with id ${currentUserID} already in team`);
        }

        if (teamImpl.members.some(m => m.id === currentUserID)) {
            throw new BadRequestException(`User with id ${currentUserID} already in team`);
        }

        user.teamID = teamImpl.id;

        await this.userService.update(user);

        this.logService.info(`Accepted team join request: ${team.name}:${login}`);

        return teamImpl.toDto();
    }

    @Auth(Role.User)
    @Post("request/reject/:id/:login")
    async rejectJoinRequest(
        @Param("id", ParseIntPipe) id: number,
        @Param("login") login: string,
        @CurrentUser('id') currentUserID: number,
        @CurrentUser('role') roles: string[]): Promise<TeamDto> {

        const teamImpl = await this.getTeamById(id);

        if (teamImpl.ownerID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can reject team join request`);
        }

        const requestIndex = teamImpl.requests.findIndex(r => r.login === login);
        if (requestIndex < 0) {
            throw new BadRequestException(`Join request not found with login ${login}`);
        }

        teamImpl.requests.splice(requestIndex, 1);

        const team = await this.teamService.update(teamImpl.toModel());

        teamImpl.fromModel(team);

        this.logService.info(`Rejected team join request: ${team.name}:${login}`);

        return teamImpl.toDto();
    }

    @Auth(Role.User)
    @Post("remove/:id/:login")
    async removeFromTeam(
        @Param("id", ParseIntPipe) id: number,
        @Param("login") login: string,
        @CurrentUser('id') currentUserID: number,
        @CurrentUser('role') roles: string[]): Promise<TeamDto> {

        const teamImpl = await this.getTeamById(id);

        if (teamImpl.ownerID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can remove user from team`);
        }

        if (teamImpl.ownerID === id) {
            throw new BadRequestException(`Owner cannot be removed from team`);
        }

        const user = await this.userService.getByLogin(login);
        if (!user) {
            throw new BadRequestException(`User with id ${currentUserID} not existing`);
        }

        if (!user.teamID || user.teamID !== id) {
            throw new BadRequestException(`User with id ${currentUserID} not in team`);
        }

        user.teamID = null;

        await this.userService.update(user);

        this.logService.info(`Removed user from team: ${teamImpl.name}:${login}`);

        return teamImpl.toDto();
    }

    async getTeamById(id: number): Promise<TeamImpl> {
        if (!id) {
            throw new BadRequestException(`Invalid team ID`);
        }

        const team = await this.teamService.getById(id);
        if (!team) {
            throw new BadRequestException(`Item with id ${id} not existing`);
        }

        return TeamImpl.createFromModel(team);
    }
}
