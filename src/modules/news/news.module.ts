import {Module} from "@nestjs/common";
import {PrismaModule} from "../../prisma/prisma.module";
import {LogModule} from "../log/log.module";
import {NewsController} from "./news.controller";
import {NewsService} from "./news.service";

@Module({
    imports: [
        PrismaModule,
        LogModule
    ],
    controllers: [NewsController],
    providers: [
        NewsService
    ],
    exports: [NewsService]
})
export class NewsModule {
}
