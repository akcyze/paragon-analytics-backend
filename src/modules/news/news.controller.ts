import {
    BadRequestException,
    Body,
    Controller, Delete,
    Get, Param, ParseIntPipe,
    Post,
    Put
} from "@nestjs/common";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {LogService} from "../log/log.service";
import {NewsService} from "./news.service";
import {NewsDto, NewsReadOnlyDto} from "./news.dto";
import {NewsImpl} from "./news.impl";

@Controller("news")
export class NewsController {

    constructor(private readonly newsService: NewsService,
                private readonly logService: LogService) {
    }

    @Auth(Role.NewsManager)
    @Put("")
    async create(@Body() dto: NewsDto): Promise<NewsDto> {
        const itemImpl = new NewsImpl();

        itemImpl.fromDto(dto);
        itemImpl.id = undefined;

        const item = await this.newsService.create(itemImpl.toModel());

        this.logService.info(`Created news: ${item.name}`);

        itemImpl.fromModel(item);

        return itemImpl.toDto();
    }

    @Auth(Role.NewsManager)
    @Post("")
    async save(@Body() dto: NewsDto): Promise<NewsDto> {
        if (!dto.id) {
            throw new BadRequestException(`Invalid news ID`);
        }

        let item = await this.newsService.getById(dto.id);
        if (!item) {
            throw new BadRequestException(`News with id ${dto.id} not existing`);
        }

        const itemImpl = NewsImpl.createFromModel(item);

        itemImpl.fromDto(dto);

        item = await this.newsService.update(itemImpl.toModel());

        this.logService.info(`Updated news: ${item.name}`);

        itemImpl.fromModel(item);

        return itemImpl.toDto();
    }

    @Get(":id")
    async getById(@Param("id", ParseIntPipe) id: number): Promise<NewsDto> {
        if (!id) {
            throw new BadRequestException(`Invalid news ID`);
        }

        const news = await this.newsService.getById(id);
        if (!news) {
            throw new BadRequestException(`News with id ${id} not existing`);
        }

        return NewsImpl.createFromModel(news).toDto();
    }

    @Get("")
    async getMany(): Promise<NewsReadOnlyDto[]> {
        const items = await this.newsService.getMany();
        
        return items.map(item => NewsImpl.createFromModel(item)).map(item => item.toReadOnly());
    }

    @Auth(Role.NewsManager)
    @Delete(":id")
    async delete(@Param("id", ParseIntPipe) id: number): Promise<boolean> {
        if (!id) {
            throw new BadRequestException(`Invalid news ID`);
        }

        const news = await this.newsService.getById(id);
        if (!news) {
            throw new BadRequestException(`News with id ${id} not existing`);
        }

        await this.newsService.delete(news.id);

        this.logService.info(`Deleted news: ${news.name}`);

        return true
    }
}
