import {BaseDto} from "@common/base/base.dto";
import {ItemParamType} from "../item/item.dto";

export class NewsDto extends BaseDto {
    id: number;
    createdAt: Date;
    name: { [key: string]: string };
    description: { [key: string]: string };
    icon: string;
    patch: string;
    champions: ChampionChanges[] = [];
    items: ItemChanges[] = [];
    bugfixes: { [key: string]: string }
}

export class NewsReadOnlyDto {
    id: number;
    createdAt: Date;
    name: { [key: string]: string };
    description: { [key: string]: string };
    icon: string;
    patch: string;
}

export class ItemChanges {
    name: string;
    isNew: boolean;
    description: { [key: string]: string };
    params: ParamChanges[] = [];
    effect: Changes[] = [];
    price: string;
}

export class ChampionChanges {
    name: string;
    isNew: boolean;
    description: { [key: string]: string };
    params: ParamChanges[] = [];
    skills: SkillChanges[] = [];
}

export class SkillChanges {
    skill: string;
    changes: Changes[] = [];
}

export class ParamChanges {
    type: ItemParamType;
    value: string;
}

export class Changes {
    name: { [key: string]: string };
    value: string;
}