import {Injectable} from "@nestjs/common";
import {PrismaService} from "../../prisma/prisma.service";
import {News} from "@prisma-client";

@Injectable()
export class NewsService {
    constructor(private readonly prismaService: PrismaService) {
    }

    async create(data: News): Promise<News> {
        return this.prismaService.news.create({data});
    }

    async getById(id: number): Promise<News> {
        return this.prismaService.news.findFirst({where: {id}});
    }

    async getMany(): Promise<News[]> {
        return this.prismaService.news.findMany({
            orderBy: {
                id: "asc"
            }
        });
    }

    async update(data: News): Promise<News> {
        return this.prismaService.news.update({
            data,
            where: {
                id: data.id
            }
        });
    }

    async delete(id: number): Promise<boolean> {
        try {
            await this.prismaService.news.delete({
                where: {
                    id
                }
            });

            return true;
        } catch (error) {
            return false;
        }
    }
}
