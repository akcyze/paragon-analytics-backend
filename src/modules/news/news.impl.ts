import {News} from "@prisma-client";
import { BaseEntity } from "@common/base/base.entity";
import {ChampionChanges, ItemChanges, NewsDto, NewsReadOnlyDto} from "./news.dto";

export class NewsImpl extends BaseEntity<NewsDto, News> {
    id: number;
    createdAt: Date;
    name: { [key: string]: string };
    description: { [key: string]: string };
    icon: string;
    patch: string;
    champions: ChampionChanges[] = [];
    items: ItemChanges[] = [];
    bugfixes: { [key: string]: string };

    public fromDto(dto: NewsDto) {
        super.fromDto(dto);

        this.id = dto.id;
        this.createdAt = dto.createdAt;
        this.name = dto.name;
        this.description = dto.description;
        this.icon = dto.icon;
        this.patch = dto.patch;
        this.champions = dto.champions;
        this.items = dto.items;
        this.bugfixes = dto.bugfixes;
    }

    public toDto(): NewsDto {
        const dto = super.toDto();

        dto.id = this.id;
        dto.createdAt = this.createdAt;
        dto.name = this.name;
        dto.description = this.description;
        dto.icon = this.icon;
        dto.patch = this.patch;
        dto.champions = this.champions;
        dto.items = this.items;
        dto.bugfixes = this.bugfixes;

        return dto;
    }

    public toReadOnly(): NewsReadOnlyDto {
        const dto = new NewsReadOnlyDto();

        dto.id = this.id;
        dto.createdAt = this.createdAt;
        dto.name = this.name;
        dto.description = this.description;
        dto.icon = this.icon;
        dto.patch = this.patch;

        return dto;
    }

    public fromModel(model: News) {
        this.id = model.id;
        this.createdAt = model.createdAt;
        this.name = JSON.parse(model.name) ?? {};
        this.description = JSON.parse(model.description) ?? {};
        this.icon = model.icon;
        this.patch = model.patch;
        this.champions = model.champions.map(item => JSON.parse(item));
        this.items = model.items.map(item => JSON.parse(item));
        this.bugfixes = JSON.parse(model.bugfixes) ?? {};
    }

    public toModel(): News {
        const model = super.toModel();

        model.id = this.id;
        model.name = JSON.stringify(this.name);
        model.description =  JSON.stringify(this.description)
        model.icon = this.icon ?? '';
        model.patch = this.patch ?? '';
        model.champions = this.champions.map(item => JSON.stringify(item));
        model.items = this.items.map(item => JSON.stringify(item));
        model.bugfixes = JSON.stringify(this.bugfixes)

        return model;
    }

    public static createFromModel(model: News): NewsImpl {
        const impl = new NewsImpl();

        impl.fromModel(model);

        return impl;
    }
}