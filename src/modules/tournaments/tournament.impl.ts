import {BaseEntity} from "@common/base/base.entity";
import {BracketType, GameMode, Tournament, TournamentPickMode, TournamentStatus} from "@prisma-client";
import {
    TournamentTeamDto,
    TournamentDto,
    TournamentJoinRequest,
    TournamentReadOnly,
    TournamentResults
} from "./tournament.dto";
import {UserImpl} from "../user/user.impl";
import {BadRequestException} from "@nestjs/common";
import {Database} from "./bracket";
import console from "console";

export class TournamentImpl extends BaseEntity<TournamentDto, Tournament> {
    id: number;

    createdAt: Date;
    updatedAt: Date;

    startedAt: Date;
    finishedAt: Date;

    name: string;
    description: string;
    prize: string;
    icon: string;
    map: string;
    pickMode: TournamentPickMode;
    gameMode: GameMode;
    status: TournamentStatus;
    teamSize: number
    slots: number;
    teamRequest: boolean;
    maxRequests: number;
    casters: string[];
    videoLink: string;
    bracketType: BracketType;
    matchesCount: number;

    creator?: UserImpl;
    creatorID: number;

    requests: TournamentJoinRequest[] = [];

    teams: TournamentTeamDto[];
    bracket: Database;
    results: TournamentResults;

    public fromDto(dto: TournamentDto) {
        super.fromDto(dto);

        this.id = dto.id;

        this.createdAt = dto.createdAt;

        const updatedAt = new Date(dto.updatedAt);
        if (this.id && this.id > 0 && this.updatedAt > updatedAt) {
            throw new BadRequestException('Tournament is outdated');
        }

        this.updatedAt = dto.updatedAt;

        this.startedAt = dto.startedAt;
        this.finishedAt = dto.finishedAt;

        this.name = dto.name;
        this.description = dto.description;
        this.prize = dto.prize;
        this.icon = dto.icon;
        this.map = dto.map;
        this.pickMode = dto.pickMode;
        this.gameMode = dto.gameMode;
        this.status = dto.status;
        this.teamSize = dto.teamSize;
        this.slots = dto.slots;
        this.teamRequest = dto.teamRequest;
        this.maxRequests = dto.maxRequests;
        this.casters = dto.casters;
        this.videoLink = dto.videoLink;
        this.bracketType = dto.bracketType;
        this.matchesCount = dto.matchesCount;

        this.creatorID = dto.creatorID;

        this.requests = dto.requests;

        this.bracket = dto.bracket;

        this.swapBracketPlayersIfNeed(dto);

        this.teams = dto.teams;

        this.results = dto.results;
    }

    private swapBracketPlayersIfNeed(dto: TournamentDto) {
        if (this.status === TournamentStatus.InProgress && this.bracket) {
            dto.teams.forEach(team => {
                const bracketTeam = this.bracket.participant.find(t => t.name === team.name);
                if (bracketTeam) {
                    const teamDto = this.teams.find(t => t.name === team.name);
                    if (teamDto) {
                        for (let i = 0; i < this.teamSize; i++) {
                            const oldPlayer = teamDto.participants[i];
                            const newPlayer = team.participants[i];

                            if (oldPlayer !== newPlayer) {
                                bracketTeam.members[i] = newPlayer;

                                console.log(`Swap players in bracket: ${oldPlayer} -> ${newPlayer}`);
                            }
                        }
                    }
                }
            });
        }
    }

    public toDto(): TournamentDto {
        const dto = super.toDto();

        dto.id = this.id;

        dto.createdAt = this.createdAt;
        dto.updatedAt = this.updatedAt;

        dto.startedAt = this.startedAt;
        dto.finishedAt = this.finishedAt;

        dto.name = this.name;
        dto.description = this.description;
        dto.prize = this.prize;
        dto.icon = this.icon;
        dto.map = this.map;
        dto.pickMode = this.pickMode;
        dto.gameMode = this.gameMode;
        dto.status = this.status;
        dto.teamSize = this.teamSize;
        dto.slots = this.slots;
        dto.teamRequest = this.teamRequest;
        dto.maxRequests = this.maxRequests;
        dto.casters = this.casters;
        dto.videoLink = this.videoLink;
        dto.bracketType = this.bracketType;
        dto.matchesCount = this.matchesCount;

        dto.creatorID = this.creatorID;
        dto.creator = this.creator ? this.creator.toDto() : null;

        dto.requests = this.requests;

        dto.teams = this.teams;
        dto.bracket = this.bracket;

        dto.results = this.results;

        return dto;
    }

    public toReadOnly(): TournamentReadOnly {
        const dto = new TournamentReadOnly();

        dto.id = this.id;
        dto.createdAt = this.createdAt;
        dto.updatedAt = this.updatedAt;
        dto.startedAt = this.startedAt;
        dto.finishedAt = this.finishedAt;
        dto.name = this.name;
        dto.description = this.description;
        dto.prize = this.prize;
        dto.icon = this.icon;
        dto.status = this.status;

        return dto;
    }

    public fromModel(model: Tournament) {
        this.id = model.id;

        this.createdAt = model.createdAt;
        this.updatedAt = model.updatedAt;

        this.startedAt = model.startedAt;
        this.finishedAt = model.finishedAt;

        this.name = model.name;
        this.description = model.description;
        this.prize = model.prize;
        this.icon = model.icon;
        this.map = model.map;
        this.pickMode = model.pickMode;
        this.gameMode = model.gameMode;
        this.status = model.status;
        this.teamSize = model.teamSize;
        this.slots = model.slots;
        this.teamRequest = model.teamRequest;
        this.maxRequests = model.maxRequests;
        this.casters = model.casters;
        this.videoLink = model.videoLink;
        this.bracketType = model.bracketType;
        this.matchesCount = model.matchesCount;

        this.creatorID = model.creatorID;

        if (model['creator']) {
            this.creator = UserImpl.createFromModel(model['creator']);
        }

        this.requests = model.requests ? model.requests.map(r => JSON.parse(r) as TournamentJoinRequest) : [];

        this.teams = JSON.parse(model.teams) as TournamentTeamDto[] ?? [];
        this.bracket = model.bracket ? JSON.parse(model.bracket) : undefined;
        this.results = model.results ? JSON.parse(model.results) : undefined;
    }

    public toModel(): Tournament {
        const model = super.toModel();

        model.id = this.id;

        model.createdAt = this.createdAt;
        model.startedAt = this.startedAt;
        model.finishedAt = this.finishedAt;

        model.name = this.name;
        model.description = this.description;
        model.prize = this.prize;
        model.icon = this.icon;
        model.map = this.map;
        model.pickMode = this.pickMode;
        model.status = this.status;
        model.teamSize = this.teamSize;
        model.slots = this.slots;
        model.teamRequest = this.teamRequest;
        model.maxRequests = this.maxRequests;
        model.casters = this.casters.filter(c => c);
        model.videoLink = this.videoLink;
        model.bracketType = this.bracketType;
        model.matchesCount = this.matchesCount;

        model.creatorID = this.creatorID;

        model.requests = this.requests?.map(r => JSON.stringify(r)) ?? [];

        model.teams = JSON.stringify(this.teams) ?? JSON.stringify([]);
        model.bracket = this.bracket ? JSON.stringify(this.bracket) : undefined;
        model.results = this.results ? JSON.stringify(this.results) : undefined;

        return model;
    }

    public static createFromModel(model: Tournament): TournamentImpl {
        const impl = new TournamentImpl();

        impl.fromModel(model);

        return impl;
    }
}