import { Module } from '@nestjs/common';
import { PrismaModule } from "../../prisma/prisma.module";
import {TournamentController} from "./tournament.controller";
import {TournamentService} from "./tournament.service";
import {UserModule} from "../user/user.module";
import {LogModule} from "../log/log.module";
import {TeamModule} from "../team/team.module";

@Module({
    imports: [PrismaModule, UserModule, LogModule, TeamModule],
    controllers: [TournamentController],
    providers: [TournamentService],
    exports: [TournamentService]
})
export class TournamentModule {}
