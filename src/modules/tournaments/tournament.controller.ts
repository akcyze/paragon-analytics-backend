import {
    BadRequestException, Body,
    Controller, Delete,
    Get,
    Param,
    ParseIntPipe,
    Post, Put, UseInterceptors,
} from "@nestjs/common";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {TournamentService} from "./tournament.service";
import {
    TournamentDto,
    TournamentJoinRequest, TournamentMemberResults,
    TournamentReadOnly,
    TournamentResults,
    TournamentTeamDto, TournamentTeamResults
} from "./tournament.dto";
import {TournamentImpl} from "./tournament.impl";
import {CurrentUserInterceptor} from "@common/interceptors/current-user.interceptor";
import {BracketType, TournamentStatus} from "@prisma-client";
import {CurrentUser} from "@common/decorators/current-user.decorator";
import {LogService} from "../log/log.service";
import {TeamService} from "../team/team.service";
import {TeamImpl} from "../team/team.impl";
import {UserImpl} from "../user/user.impl";
import {UserService} from "../user/user.service";
import {Database, Match, ParticipantResult, Ranking, RankingItem, RankingMap} from "./bracket";
import console from "console";

@Controller('tournament')
@UseInterceptors(CurrentUserInterceptor)
export class TournamentController {
    constructor(private readonly tournamentService: TournamentService,
                private readonly teamService: TeamService,
                private readonly userService: UserService,
                private readonly logService: LogService) {
    }

    @Put('')
    @Auth(Role.TournamentManager)
    async create(@CurrentUser('id') currentUserID: number, @Body() tournamentDto: TournamentDto): Promise<TournamentDto> {
        const impl = new TournamentImpl();

        impl.fromDto(tournamentDto);
        impl.id = undefined;
        impl.createdAt = new Date();
        impl.status = TournamentStatus.Pending;
        impl.creatorID = currentUserID;
        impl.bracket = undefined;

        const tournament = await this.tournamentService.create(impl.toModel());

        this.logService.info(`Created tournament ${tournament.id} by user ${currentUserID}`);

        return TournamentImpl.createFromModel(tournament).toDto();
    }

    @Get(':id')
    async getById(@Param('id', ParseIntPipe) id: number): Promise<TournamentDto> {
        const tournament = await this.tournamentService.getById(id);
        if (!tournament) {
            throw new BadRequestException('Tournament not found');
        }

        return TournamentImpl.createFromModel(tournament).toDto();
    }

    @Get('')
    async getMany(): Promise<TournamentReadOnly[]> {
        const tournaments = await this.tournamentService.getMany();
        const impls = tournaments.map(tournament => TournamentImpl.createFromModel(tournament));

        return impls.map(impl => impl.toReadOnly());
    }

    @Post('')
    @Auth(Role.TournamentManager)
    async update(@Body() tournamentDto: TournamentDto, @CurrentUser('role') roles: string[], @CurrentUser('id') currentUserID: number): Promise<TournamentDto> {
        let tournament = await this.tournamentService.getById(tournamentDto.id);
        if (!tournament) {
            throw new BadRequestException("Tournament not found");
        }

        if (tournament.creatorID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException("Only owner can change tournament");
        }

        const impl = TournamentImpl.createFromModel(tournament);

        impl.fromDto(tournamentDto);

        await this.tournamentService.update(impl.toModel());

        tournament = await this.tournamentService.getById(tournamentDto.id);

        this.logService.info(`Updated tournament ${tournament.id} by user ${currentUserID}`);

        impl.fromModel(tournament);

        return impl.toDto();
    }

    @Post('results/:id')
    @Auth(Role.TournamentManager)
    async createResults(@Param('id', ParseIntPipe) id: number, @CurrentUser('role') roles: string[], @CurrentUser('id') currentUserID: number): Promise<boolean> {
        const tournament = await this.getTournamentById(id)
        if (tournament.creatorID !== currentUserID) {
            if (!roles.includes(Role.Admin)) {
                throw new BadRequestException("Only owner can create tournament results");
            }
        }

        if (tournament.status !== TournamentStatus.Finished) {
            throw new BadRequestException("Tournament not finished");
        }

        const ranking = this.getRanking(tournament.bracket.match);
        const results = new TournamentResults();

        results.teams = [];

        ranking.forEach(rank => {
            const participant = tournament.bracket.participant.find(p => p.id === rank.id);

            const team = new TournamentTeamResults();

            team.name = participant.name;
            team.teamID = participant.teamID;
            team.members = [];
            team.primeUnderlings = 0;
            team.primeGuardians = 0;
            team.spirits = 0;
            team.wins = 0;
            team.loses = 0;

            participant.members.forEach(member => {
                const tournamentTeamMember = new TournamentMemberResults();

                tournamentTeamMember.name = member;
                tournamentTeamMember.assists = 0;
                tournamentTeamMember.deaths = 0;
                tournamentTeamMember.kills = 0;

                team.members.push(tournamentTeamMember);
            });

            const games = tournament.bracket.match_game.filter(game => !game.bye)
                .filter(game =>
                    game.opponent1.team === participant.name
                    || game.opponent2.team === participant.name);

            const participantResults = games.map(game => {
                return game.opponent1.team === team.name ? game.opponent1 : game.opponent2;
            })

            participantResults.forEach(participantResult => {
                if (participantResult.result === 'win') {
                    team.wins++;
                } else if (participantResult.result === 'loss') {
                    team.loses++;
                }

                team.primeUnderlings += participantResult.primeUnderlings;
                team.primeGuardians += participantResult.primeGuardians;
                team.spirits += participantResult.spirits;

                participantResult.members.forEach(member => {
                    const teamMember = team.members.find(m => m.name === member.name);

                    teamMember.kills += member.kills;
                    teamMember.deaths += member.deaths;
                    teamMember.assists += member.assists;
                });
            });

            results.teams.push(team);

        });


        for (let i = 0; i < results.teams.length; i++) {
            const teamResults = results.teams[i];

            console.log('Team Result:', teamResults.name, teamResults.members.map(m => `${m.name} ${m.kills}/${m.deaths}/${m.assists}`));

            if (!teamResults.teamID) {
                this.logService.warn(`Team ${teamResults.name} has created manual. Skipp adding results`);

                continue;
            }

            const team = await this.teamService.getByIdSelf(teamResults.teamID);
            if (!team) {
                this.logService.warn(`Team ${teamResults.name} not found. Skipp adding results`);

                continue;
            }

            if (i === 0) {
                team.tournamentWins.push(tournament.id);
            }

            team.gameWins += teamResults.wins;
            team.gameLosed += teamResults.loses;

            for (let j = teamResults.members.length - 1; j >= 0; j--) {
                const teamMember = teamResults.members[j];

                const user = await this.userService.getByLogin(teamMember.name);
                if (!user) {
                    this.logService.warn(`User ${teamMember.name} not found. Skipp adding results`);

                    continue;
                }

                user.gameWins += teamResults.wins;
                user.gameLosed += teamResults.loses;
                user.kills += teamMember.kills;
                user.deaths += teamMember.deaths;
                user.assists += teamMember.assists;

                if (i === 0) {
                    user.tournamentWins.push(tournament.id);
                }

                await this.userService.update(user);
            }

            await this.teamService.update(team);
        }

        tournament.results = results;

        await this.tournamentService.update(tournament.toModel());

        return true;
    }

    @Delete(':id')
    @Auth(Role.TournamentManager)
    async delete(@Param('id', ParseIntPipe) id: number, @CurrentUser('role') roles: string[], @CurrentUser('id') currentUserID: number): Promise<boolean> {
        const tournament = await this.tournamentService.getById(id);
        if (!tournament) {
            throw new BadRequestException("Tournament not found");
        }

        if (tournament.creatorID !== currentUserID) {
            if (!roles.includes(Role.Admin)) {
                throw new BadRequestException("Only owner can delete tournament");
            }
        }

        await this.tournamentService.delete(tournament.id);

        this.logService.warn(`Deleted tournament ${tournament.id}`);

        return true;
    }

    @Auth(Role.User)
    @Post("join/user/:tournamentID/:userID")
    async addUserJoinTournamentRequest(
        @Param("tournamentID", ParseIntPipe) tournamentID: number,
        @Param("userID", ParseIntPipe) userID: number): Promise<boolean> {

        const tournamentImpl = await this.getTournamentById(tournamentID);
        if (tournamentImpl.status !== TournamentStatus.Pending) {
            throw new BadRequestException("Tournament already started");
        }

        const user = await this.userService.getById(userID);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        if (tournamentImpl.requests.find(r => r.userID === user.id)) {
            throw new BadRequestException("User already requested");
        }

        tournamentImpl.requests.push({
            userID: user.id,
            teamID: 0,
            teamName: "",
            participants: []
        });

        const tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament)

        this.logService.info(`Added tournament user join request: ${tournamentImpl.name}:${user.login}`);

        return true;
    }

    @Auth(Role.User)
    @Post("join/:id")
    async addJoinTournamentRequest(
        @Param("id", ParseIntPipe) id: number,
        @Body() dto: TournamentJoinRequest): Promise<TournamentDto> {

        const tournamentImpl = await this.getTournamentById(id);

        const team = await this.teamService.getById(dto.teamID);
        if (!team) {
            throw new BadRequestException("Team not found");
        }

        if (tournamentImpl.status !== TournamentStatus.Pending) {
            throw new BadRequestException("Tournament already started");
        }

        if (tournamentImpl.teams.find(t => t.teamID === team.id)) {
            throw new BadRequestException("Team already joined");
        }

        if (tournamentImpl.requests.find(r => r.teamID === team.id)) {
            throw new BadRequestException("Team already requested");
        }

        tournamentImpl.requests.push(dto);

        const tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament)

        this.logService.info(`Added tournament join request: ${tournamentImpl.name}:${dto.teamName}`);

        return tournamentImpl.toDto();
    }

    @Auth(Role.TournamentManager)
    @Post("request/accept/user/:tournamentID/:userID")
    async acceptUserJoinTournamentRequest(@Param("tournamentID", ParseIntPipe) tournamentID: number,
                                      @Param("userID", ParseIntPipe) userID: number,
                                      @CurrentUser('id') currentUserID: number,
                                      @CurrentUser('role') roles: string[]): Promise<TournamentDto> {

        const tournamentImpl = await this.getTournamentById(tournamentID);

        if (tournamentImpl.creatorID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can accept tournament join request`);
        }

        const request = tournamentImpl.requests.find(r => r.userID === userID);
        if (!request) {
            throw new BadRequestException("User not requested");
        }

        tournamentImpl.requests = tournamentImpl.requests.filter(r => r.userID !== userID);

        let tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament);

        const user = await this.userService.getById(userID);
        if (!user) {
            throw new BadRequestException("User not found");
        }

        if (tournamentImpl.status !== TournamentStatus.Pending) {
            throw new BadRequestException("Tournament already started");
        }

        const teamsWithSpace = tournamentImpl.teams.filter(t => t.participants.filter(p => p).length < tournamentImpl.teamSize);
        if (!teamsWithSpace.length) {
            throw new BadRequestException("No space in tournament");
        }

        const teamIndex = Math.floor(Math.random() * teamsWithSpace.length);
        const randomTeamName = teamsWithSpace[teamIndex].name;
        const team = tournamentImpl.teams.find(t => t.name === randomTeamName);
        for (let i = 0; i < team.participants.length; i++) {
            if (!team.participants[i]) {
                team.participants[i] = user.login;

                break;
            }
        }

        tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament);

        this.logService.info(`Accepted tournament user join request: ${tournamentImpl.name}:${user.login}`);

        return tournamentImpl.toDto();
    }

    @Auth(Role.User)
    @Post("request/reject/user/:tournamentID/:userID")
    async rejectUserJoinTournamentRequest(@Param("tournamentID", ParseIntPipe) tournamentID: number,
                                      @Param("userID", ParseIntPipe) userID: number,
                                      @CurrentUser('id') currentUserID: number,
                                      @CurrentUser('role') roles: string[]): Promise<TournamentDto> {

        const tournamentImpl = await this.getTournamentById(tournamentID);

        if (tournamentImpl.creatorID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can reject tournament join request`);
        }

        const request = tournamentImpl.requests.find(r => r.userID === userID);
        if (!request) {
            throw new BadRequestException("Team not requested");
        }

        tournamentImpl.requests = tournamentImpl.requests.filter(r => r.userID !== userID);

        const tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament);

        return tournamentImpl.toDto();
    }

    @Auth(Role.TournamentManager)
    @Post("request/accept/:id/:teamID")
    async acceptJoinTournamentRequest(@Param("id", ParseIntPipe) id: number,
                                      @Param("teamID", ParseIntPipe) teamID: number,
                                      @CurrentUser('id') currentUserID: number,
                                      @CurrentUser('role') roles: string[]): Promise<TournamentDto> {

        const tournamentImpl = await this.getTournamentById(id);

        if (tournamentImpl.creatorID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can accept tournament join request`);
        }

        const request = tournamentImpl.requests.find(r => r.teamID === teamID);
        if (!request) {
            throw new BadRequestException("Team not requested");
        }

        tournamentImpl.requests = tournamentImpl.requests.filter(r => r.teamID !== teamID);

        let tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament);

        const team = await this.teamService.getById(teamID);
        if (!team) {
            throw new BadRequestException("Team not found");
        }

        if (tournamentImpl.status !== TournamentStatus.Pending) {
            throw new BadRequestException("Tournament already started");
        }

        if (tournamentImpl.teams.find(t => t.teamID === team.id)) {
            throw new BadRequestException("Team already joined");
        }

        tournamentImpl.teams.push({
            name: team.name,
            teamID: request.teamID,
            icon: team.icon,
            participants: request.participants
        });

        tournamentImpl.requests = tournamentImpl.requests.filter(r => r.teamID !== team.id);

        tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament);

        this.logService.info(`Accepted tournament join request: ${tournamentImpl.name}:${request.teamName}`);

        return tournamentImpl.toDto();
    }

    @Auth(Role.User)
    @Post("request/reject/:id/:teamID")
    async rejectJoinTournamentRequest(@Param("id", ParseIntPipe) id: number,
                                      @Param("teamID", ParseIntPipe) teamID: number,
                                      @CurrentUser('id') currentUserID: number,
                                      @CurrentUser('role') roles: string[]): Promise<TournamentDto> {

        const tournamentImpl = await this.getTournamentById(id);

        if (tournamentImpl.creatorID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can reject tournament join request`);
        }

        const request = tournamentImpl.requests.find(r => r.teamID === teamID);
        if (!request) {
            throw new BadRequestException("Team not requested");
        }

        tournamentImpl.requests = tournamentImpl.requests.filter(r => r.teamID !== teamID);

        const tournament = await this.tournamentService.update(tournamentImpl.toModel());

        tournamentImpl.fromModel(tournament);

        return tournamentImpl.toDto();
    }

    async getTournamentById(id: number): Promise<TournamentImpl> {
        if (!id) {
            throw new BadRequestException("Tournament id is required");
        }

        const tournament = await this.tournamentService.getById(id);
        if (!tournament) {
            throw new BadRequestException("Tournament not found");
        }

        return TournamentImpl.createFromModel(tournament);
    }

    getRanking(matches: Match[]): Ranking {
        const formula = (
            (item: RankingItem): number => 3 * item.wins + item.draws
        );

        const rankingMap: RankingMap = {};

        for (const match of matches) {
            this.processParticipant(match, rankingMap, formula, match.opponent1, match.opponent2);
            this.processParticipant(match, rankingMap, formula, match.opponent2, match.opponent1);
        }

        return this.createRanking(rankingMap);
    }

    processParticipant(match: Match, rankingMap: RankingMap, formula: any, current: ParticipantResult | null, other: ParticipantResult | null): void {
        if (!current || current.id === null) return;

        const state = rankingMap[current.id] || {
            rank: 0,
            id: 0,
            played: 0,
            wins: 0,
            draws: 0,
            losses: 0,
            forfeits: 0,
            scoreFor: 0,
            scoreAgainst: 0,
            scoreDifference: 0,
            points: 0,
        };

        state.id = current.id;

        if (current.forfeit || current.result)
            state.played++;

        if (current.result === 'win')
            state.wins++;

        if (current.result === 'draw')
            state.draws++;

        if (current.result === 'loss')
            state.losses++;

        if (current.forfeit)
            state.forfeits++;

        state.scoreFor += current.score || 0;
        state.scoreAgainst += other && other.score || 0;
        state.scoreDifference = state.scoreFor - state.scoreAgainst;

        state.points = formula(state);
        if (match.group_id !== 0) {
            state.points += -0.5;
        }

        rankingMap[current.id] = state;
    }

    createRanking(rankingMap: RankingMap): RankingItem[] {
        const ranking = Object.values(rankingMap).sort((a, b) => a.points !== b.points ? b.points - a.points : b.played - a.played);

        const rank = {
            value: 0,
            lastPoints: -1,
        };

        for (const item of ranking) {
            item.rank = rank.lastPoints !== item.points ? ++rank.value : rank.value;
            rank.lastPoints = item.points;
        }

        return ranking;
    }
}
