import {Injectable} from "@nestjs/common";
import {PrismaService} from "../../prisma/prisma.service";
import {Tournament} from "@prisma-client";
import console from "console";
import {TournamentDto} from "./tournament.dto";

@Injectable()
export class TournamentService {

    constructor(private readonly prismaService: PrismaService) {
    }

    async create(data: Tournament): Promise<Tournament> {
        return this.prismaService.tournament.create({data});
    }

    async getById(id: number): Promise<Tournament> {
        return this.prismaService.tournament.findFirst({where: {id}});
    }

    async getMany(): Promise<Tournament[]> {
        return this.prismaService.tournament.findMany();
    }

    async update(data: Tournament): Promise<Tournament> {
        return this.prismaService.tournament.update({data, where: {id: data.id}});
    }

    async delete(id: number): Promise<void> {
        await this.prismaService.tournament.delete({where: {id}});
    }
}
