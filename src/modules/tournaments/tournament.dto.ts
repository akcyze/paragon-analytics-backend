import {BaseDto} from "@common/base/base.dto";
import {UserDto} from "../user/user.dto";
import {BracketType, GameMode, TournamentPickMode, TournamentStatus} from "@prisma-client";
import {Database} from "./bracket";

export class TournamentDto extends BaseDto {
    id: number;

    createdAt: Date;
    updatedAt: Date;

    startedAt: Date;
    finishedAt: Date;

    name: string;
    description: string;
    prize: string;
    icon: string;
    map: string;
    pickMode: TournamentPickMode;
    gameMode: GameMode;
    status: TournamentStatus;
    teamSize: number
    slots: number;
    teamRequest: boolean;
    maxRequests: number;
    casters: string[];
    videoLink: string;
    bracketType: BracketType;
    matchesCount: number;

    creator?: UserDto;
    creatorID: number;

    requests: TournamentJoinRequest[] = [];

    teams: TournamentTeamDto[];
    bracket: Database;
    results: TournamentResults;
}

export class TournamentReadOnly {
    id: number;

    createdAt: Date;
    updatedAt: Date;

    startedAt: Date;
    finishedAt: Date;

    name: string;
    description: string;
    prize: string;
    icon: string;
    status: TournamentStatus;
}

export class TournamentTeamDto {
    name: string;
    teamID: number;
    icon: string;
    participants: string[];
}

export class TournamentJoinRequest {
    teamID: number;
    teamName: string;
    userID: number;
    participants: string[]
}

export class TournamentResults {
    teams: TournamentTeamResults[];
}

export class TournamentTeamResults {
    name: string;
    teamID: number;
    members: TournamentMemberResults[];
    wins: number;
    loses: number;
    primeUnderlings: number;
    primeGuardians: number;
    spirits: number;
}

export class TournamentMemberResults {
    name: string;
    kills: number;
    deaths: number;
    assists: number;
}
