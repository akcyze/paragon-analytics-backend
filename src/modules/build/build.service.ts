import {Injectable} from "@nestjs/common";
import {PrismaService} from "../../prisma/prisma.service";
import {Build} from "@prisma-client";
import console from "console";

@Injectable()
export class BuildService {
    constructor(private readonly prismaService: PrismaService) {
    }

    async create(data: Build): Promise<Build> {
        return this.prismaService.build.create({data});
    }

    async getById(id: number): Promise<any> {
        return this.prismaService.build.findFirst({
            where: {id},
            include: {
                Likes: true,
                DisLikes: true,
            }
        });
    }

    async getMany(): Promise<Build[]> {
        return this.prismaService.build.findMany({
            orderBy: {
                id: "asc"
            }
        });
    }

    async getManyReadOnly(){
        return this.prismaService.build.findMany({
            include: {
                Likes: true,
                DisLikes: true,
                creator: true,
                champion: true
            },
            orderBy:{
                createdAt: "desc",
            }
        });
    }

    async update(data: Build): Promise<Build> {
        return this.prismaService.build.update({
            data,
            where: {
                id: data.id
            }
        });
    }

    async delete(id: number): Promise<boolean> {
        try {
            await this.prismaService.likes.deleteMany({
                where: {
                    buildID: id
                }
            });

            await this.prismaService.disLikes.deleteMany({
                where: {
                    buildID: id
                }
            })

            await this.prismaService.build.delete({
                where: {
                    id
                }
            });

            return true;
        } catch (error) {
            console.log(error);

            return false;
        }
    }

    addLike(buildID: number, userID: number) {
        return this.prismaService.build.update({
            where: {
                id: buildID
            },
            data: {
                Likes: {
                    create: {
                        userID
                    }
                }
            }
        })
    }

    removeLike(buildID: number, userID: number) {
        return this.prismaService.likes.delete({
            where: {
                userID_buildID: {
                    buildID,
                    userID
                }
            }
        })
    }

    addDislike(buildID: number, userID: number) {
        return this.prismaService.build.update({
            where: {
                id: buildID
            },
            data: {
                DisLikes: {
                    create: {
                        userID
                    }
                }
            }
        })
    }

    removeDislike(buildID: number, userID: number) {
        return this.prismaService.disLikes.delete({
            where: {
                userID_buildID: {
                    buildID,
                    userID
                }
            }
        })
    }
}
