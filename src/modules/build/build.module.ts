import {Module} from "@nestjs/common";
import {PrismaModule} from "../../prisma/prisma.module";
import {BuildService} from "./build.service";
import {BuildController} from "./build.controller";
import {UserModule} from "../user/user.module";
import {LogModule} from "../log/log.module";

@Module({
    imports: [
        PrismaModule,
        UserModule,
        LogModule
    ],
    controllers: [BuildController],
    providers: [
        BuildService
    ],
    exports: [BuildService]
})
export class BuildModule {
}
