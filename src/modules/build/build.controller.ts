import {
    BadRequestException,
    Body,
    Controller,
    Delete, ForbiddenException, Get,
    Param,
    ParseIntPipe,
    Post,
    Put, UseInterceptors
} from "@nestjs/common";
import {Auth} from "../auth/decorator/auth.decorator";
import {Role} from "../auth/guard/role.type";
import {BuildService} from "./build.service";
import {BuildDto, BuildTableDto} from "./build.dto";
import {BuildImpl} from "./build.impl";
import {CurrentUserInterceptor} from "@common/interceptors/current-user.interceptor";
import {CurrentUser} from "@common/decorators/current-user.decorator";
import {LogService} from "../log/log.service";
import console from "console";

@Controller("build")
@UseInterceptors(CurrentUserInterceptor)
export class BuildController {

    constructor(private readonly buildService: BuildService,
                private readonly logService: LogService) {
    }

    @Auth(Role.User)
    @Put("")
    async create(@CurrentUser('id') currentUserID: number, @Body() dto: BuildDto): Promise<BuildDto> {
        const buildImpl = new BuildImpl();

        buildImpl.fromDto(dto);
        buildImpl.id = undefined;
        buildImpl.creatorID = currentUserID;

        const build = await this.buildService.create(buildImpl.toModel());

        this.logService.info(`Created build ${build.id} by user ${currentUserID}`);

        buildImpl.fromModel(build);

        return buildImpl.toDto();
    }

    @Get('')
    async getMany(): Promise<BuildTableDto[]> {
        const builds = await this.buildService.getManyReadOnly();
        const impls = builds.map(build => BuildImpl.createFromModel(build));

        return impls.map(impl => impl.toTableDto());
    }

    @Auth(Role.User)
    @Post("")
    async save(@Body() dto: BuildDto, @CurrentUser('id') currentUserID: number, @CurrentUser('role') roles: string[]): Promise<BuildDto> {
        if (!dto.id) {
            throw new BadRequestException(`Invalid build ID`);
        }
        
        let build = await this.buildService.getById(dto.id);
        console.log(currentUserID, build)
        if (dto.creatorID !== currentUserID && !roles.includes(Role.Admin)) {
            throw new BadRequestException(`Only owner can change build`);
        }

        const buildImpl = BuildImpl.createFromModel(build);

        buildImpl.fromDto(dto);

        build = await this.buildService.update(buildImpl.toModel());

        this.logService.info(`Updated build ${build.id} by user ${currentUserID}`);

        buildImpl.fromModel(build);

        return buildImpl.toDto();
    }

    @Auth(Role.User)
    @Post("/makeLike/:id")
    async toggleLike(@Param('id', ParseIntPipe) id: number, @CurrentUser('id') currentUserID: number): Promise<BuildDto> {
        if (!id) {
            throw new BadRequestException(`Invalid build ID`);
        }

        let build = await this.buildService.getById(id);
        if (!build) {
            throw new BadRequestException('Build not found');
        }

        if (build.creatorID === currentUserID) {
            throw new BadRequestException('You can\'t like your own build');
        }

        const buildImpl = BuildImpl.createFromModel(build);
        if (buildImpl.dislikes.includes(currentUserID)) {
            await this.buildService.removeDislike(build.id, currentUserID);
        }

        if (!buildImpl.likes.includes(currentUserID)) {
            await this.buildService.addLike(build.id, currentUserID);

            this.logService.info(`User ${currentUserID} liked build ${build.id}`);
        } else {
            await this.buildService.removeLike(build.id, currentUserID);

            this.logService.info(`User ${currentUserID} removed like from build ${build.id}`);
        }

        build = await this.buildService.update(buildImpl.toModel());

        buildImpl.fromModel(build);

        return buildImpl.toDto();
    }

    @Auth(Role.User)
    @Post("/makeDislike/:id")
    async toggleDislike(@Param('id', ParseIntPipe) id: number, @CurrentUser('id') currentUserID: number): Promise<BuildDto> {
        if (!id) {
            throw new BadRequestException(`Invalid build ID`);
        }

        let build = await this.buildService.getById(id);
        if (!build) {
            throw new BadRequestException('Build not found');
        }

        if (build.creatorID === currentUserID) {
            throw new BadRequestException('You can\'t dislike your own build');
        }

        const buildImpl = BuildImpl.createFromModel(build);
        if (buildImpl.likes.includes(currentUserID)) {
            await this.buildService.removeLike(build.id, currentUserID);
        }

        if (!buildImpl.dislikes.includes(currentUserID)) {
            await this.buildService.addDislike(build.id, currentUserID);

            this.logService.info(`User ${currentUserID} disliked build ${build.id}`);
        } else {
            await this.buildService.removeDislike(build.id, currentUserID);

            this.logService.info(`User ${currentUserID} removed dislike from build ${build.id}`);
        }

        build = await this.buildService.update(buildImpl.toModel());

        buildImpl.fromModel(build);

        return buildImpl.toDto();
    }

    @Auth(Role.User)
    @Delete("remove/:id")
    async remove(@CurrentUser('id') currentUserID: number, @Param("id", ParseIntPipe) id: number, @CurrentUser('role') roles: string[]): Promise<boolean> {
        if (!id)
            throw new BadRequestException(`Invalid build ID`);

        const build = await this.buildService.getById(id);
        if (!build)
            throw new BadRequestException('Build not found');

        if (build.creatorID !== currentUserID && !roles.includes(Role.Admin))
            throw new ForbiddenException("Build can remove only owner");

        this.logService.info(`Removed build ${build.id} by user ${currentUserID}`);

        return await this.buildService.delete(id);
    }
}
