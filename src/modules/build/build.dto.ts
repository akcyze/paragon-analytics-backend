import {Role} from "@prisma-client";
import { BaseDto } from "@common/base/base.dto";
import {Expose} from "class-transformer";

export class BuildDto extends BaseDto {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    role: Role;
    championID: number;
    creatorID: number;
    items: string[];
    coreItems: string[];
    situationalItems: string[];
    skillsOrder: string[];
    karmas: string[];
    likes: number[];
    dislikes: number[];
    keyInsights: string;
    strengths: string;
    weaknesses: string;

    rating?: number;

    creatorName?: string;
}

export class BuildTableDto {
    id: number;
    updatedAt: Date;
    name: string;
    role: Role;
    champion: string;
    items: string[];
    karmas: string[];
    rating: string;
    creatorName: string;
    tier: number;
}

export class BuildReadOnlyDto {
    name: string;
    role: Role;
    championID: number;
    rating: number;
}