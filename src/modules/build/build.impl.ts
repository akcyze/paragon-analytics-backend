import {Build, Role} from "@prisma-client";
import { BaseEntity } from "@common/base/base.entity";
import {BuildDto, BuildReadOnlyDto, BuildTableDto} from "./build.dto";
import {UserImpl} from "../user/user.impl";
import {extractArrayProperty, extractProperty} from "@common/property-extractor";
import {ChampionImpl} from "../champion/champion.impl";

export class BuildImpl extends BaseEntity<BuildDto, Build> {
    id: number;
    createdAt: Date;
    updatedAt: Date;
    name: string;
    role: Role;
    championID: number;
    creatorID: number;
    items: string[];
    coreItems: string[];
    situationalItems: string[];
    skillsOrder: string[];
    karmas: string[];
    likes: number[];
    dislikes: number[];
    keyInsights: string;
    strengths: string;
    weaknesses: string;

    creator: UserImpl;
    champion: ChampionImpl;

    rating: number;

    public fromDto(dto: BuildDto) {
        super.fromDto(dto);

        this.id = dto.id;
        this.name = dto.name;
        this.role = dto.role;
        this.championID = dto.championID;
        this.creatorID = dto.creatorID;
        this.items = dto.items;
        this.coreItems = dto.coreItems;
        this.situationalItems = dto.situationalItems;
        this.skillsOrder = dto.skillsOrder;
        this.karmas = dto.karmas;
        this.keyInsights = dto.keyInsights;
        this.strengths = dto.strengths;
        this.weaknesses = dto.weaknesses;
    }

    public toDto(): BuildDto {
        const dto = super.toDto();

        dto.id = this.id;
        dto.createdAt = this.createdAt;
        dto.updatedAt = this.updatedAt;
        dto.name = this.name;
        dto.role = this.role;
        dto.championID = this.championID;
        dto.creatorID = this.creatorID;
        dto.items = this.items;
        dto.coreItems = this.coreItems;
        dto.situationalItems = this.situationalItems;
        dto.skillsOrder = this.skillsOrder;
        dto.karmas = this.karmas;
        dto.likes = this.likes;
        dto.dislikes = this.dislikes;
        dto.keyInsights = this.keyInsights;
        dto.strengths = this.strengths;
        dto.weaknesses = this.weaknesses;

        dto.rating = this.rating;

        if (this.creator) {
            dto.creatorName = this.creator.login;
        }

        return dto;
    }

    public toReadOnlyDto() : BuildReadOnlyDto {
        const dto = new BuildReadOnlyDto();

        dto.name = this.name;
        dto.role = this.role;
        dto.championID = this.championID;
        dto.rating = this.rating;

        return dto;
    }

    public toTableDto() : BuildTableDto {
        const dto = new BuildTableDto();

        dto.id = this.id;
        dto.updatedAt = this.updatedAt;
        dto.name = this.name;
        dto.role = this.role;
        dto.champion = this.champion?.name;
        dto.items = this.items;
        dto.karmas = this.karmas;
        dto.rating = this.rating?.toString();
        dto.creatorName = this.creator?.displayName;
        dto.tier = this.champion?.tier;

        return dto;
    }

    public fromModel(model: Build) {
        this.id = model.id;
        this.createdAt = model.createdAt;
        this.updatedAt = model.updatedAt;
        this.name = model.name;
        this.role = model.role;
        this.championID = model.championID;
        this.creatorID = model.creatorID;
        this.items = model.items;
        this.coreItems = model.coreItems;
        this.situationalItems = model.situationalItems;
        this.skillsOrder = model.skillsOrder;
        this.karmas = model.karmas;
        this.keyInsights = model.keyInsights ?? '';
        this.strengths = model.strengths ?? '';
        this.weaknesses = model.weaknesses ?? '';

        this.likes = extractArrayProperty(model, 'Likes', 'userID');
        this.dislikes = extractArrayProperty(model, 'DisLikes', 'userID');

        const creator = extractProperty(model, 'creator');
        if (creator) {
            this.creator = UserImpl.createFromModel(creator);
        }

        const champion = extractProperty(model, 'champion');
        if (champion) {
            this.champion = ChampionImpl.createFromModel(champion);
        }

        this.rating = this.likes?.length - this.dislikes?.length;
    }

    public toModel(): Build {
        const model = super.toModel();

        model.id = this.id;
        model.name = this.name;
        model.role = this.role;
        model.championID = this.championID;
        model.creatorID = this.creatorID;
        model.items = this.items;
        model.coreItems = this.coreItems;
        model.situationalItems = this.situationalItems;
        model.skillsOrder = this.skillsOrder;
        model.karmas = this.karmas;
        model.keyInsights = this.keyInsights;
        model.strengths = this.strengths;
        model.weaknesses = this.weaknesses;

        return model;
    }

    public static createFromModel(model: Build): BuildImpl {
        const impl = new BuildImpl();

        impl.fromModel(model);

        return impl;
    }
}