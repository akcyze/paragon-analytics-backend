import {Build, User} from "@prisma-client";

export type BuildWithCreator = Build & { creator: User, Likes: [], DisLikes: [] };