import { UserImpl } from "../modules/user/user.impl";

declare global {
    namespace Express {
        export interface Request {
            user: UserImpl;
        }
    }
}