import {
    Controller,
    ForbiddenException,
    Get,
    UseInterceptors,
} from "@nestjs/common";
import {CurrentUserInterceptor} from "@common/interceptors";
import {Role} from "./modules/auth/guard/role.type";
import {Auth} from "./modules/auth/decorator/auth.decorator";
import {UserService} from "./modules/user/user.service";
import {UserDto} from "./modules/user/user.dto";
import {CurrentUser} from "@common/decorators/current-user.decorator";
import {UserImpl} from "./modules/user/user.impl";


@Controller('')
@UseInterceptors(CurrentUserInterceptor)
export class AppController {
    constructor(private readonly userService: UserService) {}

    @Get('')
    @Auth(Role.User)
    async getMe(@CurrentUser('id') currentUserID: number): Promise<UserDto> {
        const user = await this.userService.getById(currentUserID);
        if (!user) {
            throw new ForbiddenException(`User with id ${currentUserID} not found`);
        }

        return UserImpl.createFromModel(user).toDto();
    }
}
