import {HttpAdapterHost, NestFactory} from "@nestjs/core";
import {AppModule} from './app.module';
import {HttpExceptionFilter, GlobalExceptionsFilter, PrismaClientExceptionFilter} from "@common/filters";
import {ValidationPipe} from "@nestjs/common";
import {LogService} from "./modules/log/log.service";

import * as bodyParser from 'body-parser';
import helmet from "helmet";

async function bootstrap() {
    try {
        const app = await NestFactory.create(AppModule);

        app.use(helmet());
        app.enableCors({exposedHeaders: ['Authorization', 'Refresh']});

        const loggerService = app.get<LogService>(LogService);

        const httpAdapterHost = app.get(HttpAdapterHost);

        app.useGlobalFilters(new GlobalExceptionsFilter(loggerService, httpAdapterHost));
        app.useGlobalFilters(new PrismaClientExceptionFilter(httpAdapterHost.httpAdapter));
        app.useGlobalFilters(new HttpExceptionFilter(loggerService));

        app.useGlobalPipes(new ValidationPipe({disableErrorMessages: false}));
        app.use(bodyParser.json({limit: '5mb'}));
        app.use(bodyParser.urlencoded({limit: '5mb', extended: true}));

        await app.listen(3000);
    } catch (error) {
        console.log(error)
    }

    console.log('Server is running on port 3000');
}

bootstrap().catch(error => console.error(error));
