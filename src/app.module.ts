import {Module} from '@nestjs/common';
import {AuthModule} from "./modules/auth/auth.module";
import {PrismaModule} from "./prisma/prisma.module";
import {ChampionModule} from "./modules/champion/champion.module";
import {BuildModule} from "./modules/build/build.module";
import {ItemModule} from "./modules/item/item.module";
import {TournamentModule} from "./modules/tournaments/tournament.module";
import {MailModule} from "./modules/mail/mail.module";
import {LogModule} from "./modules/log/log.module";
import {TeamModule} from "./modules/team/team.module";
import {NewsModule} from "./modules/news/news.module";
import {ThrottlerGuard, ThrottlerModule} from "@nestjs/throttler";
import {APP_GUARD} from "@nestjs/core";
import {AppController} from "./app.controller";
import {UserModule} from "./modules/user/user.module";
import {CreatureModule} from "./modules/creatures/creature.module";

@Module({
    imports: [
        ThrottlerModule.forRoot([{
            ttl: 60000,
            limit: 360,
        }]),
        PrismaModule,
        AuthModule,
        ChampionModule,
        BuildModule,
        ItemModule,
        TournamentModule,
        MailModule,
        LogModule,
        TeamModule,
        NewsModule,
        UserModule,
        CreatureModule,
    ],
    controllers: [AppController],
    providers: [
        {
            provide: APP_GUARD,
            useClass: ThrottlerGuard
        },
    ],
})
export class AppModule {
}
