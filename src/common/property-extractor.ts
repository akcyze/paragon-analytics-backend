export const extractArrayProperty = (obj: any, property: string, subProperty?: string): [] => {
    if (obj[property]) {
        const propertyValue = obj[property];

        if (!subProperty)
            return propertyValue;

        return propertyValue.map((item: any) => item[subProperty]);
    } else {
        return [];
    }
};

export const extractProperty = (obj: any, property: string, subProperty?: string): any => {
    if (obj[property]) {
        const propertyValue = obj[property];

        if (!subProperty)
            return propertyValue;

        return propertyValue.map((item: any) => item[subProperty]);
    } else {
        return undefined;
    }
};