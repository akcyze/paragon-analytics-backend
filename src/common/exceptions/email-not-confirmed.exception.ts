import {HttpException, HttpStatus} from "@nestjs/common";

export class EmailNotConfirmedException extends HttpException {
    constructor() {
        super('Email Not Confirmed', HttpStatus.FORBIDDEN);
    }
}