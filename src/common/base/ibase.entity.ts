import { BaseDto } from "./base.dto";

export interface IBaseEntity<T extends BaseDto, G extends BaseDto> {
    toDto(): T;
    fromDto(dto: T);

    toModel(): G;
    fromModel(model: G);
}