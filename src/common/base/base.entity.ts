import { BaseDto, BaseModel } from "./base.dto";
import { IBaseEntity } from "./ibase.entity";

export class BaseEntity<T extends BaseDto, G extends BaseModel> implements IBaseEntity<T,G> {
    public toDto(): T {
        return {
        } as T;
    }

    public fromDto(dto: T): void {
    }

    public toModel(): G {
        return { } as G
    }

    public fromModel(model: G): void {
    }
}