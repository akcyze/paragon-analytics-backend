import {createParamDecorator, ExecutionContext} from "@nestjs/common";

export const CurrentUser = createParamDecorator((param: string, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest();

    return param ? request.currentUser?.[param] : request.currentUser;
});