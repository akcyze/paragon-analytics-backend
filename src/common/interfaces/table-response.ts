export interface TableResponse<T> {
    data: T[];
    pages: number;
}