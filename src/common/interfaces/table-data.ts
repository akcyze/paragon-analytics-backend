export class TableData<T> {
    data: T[];

    pages: number;
    page: number;
    pageSize: number;
}