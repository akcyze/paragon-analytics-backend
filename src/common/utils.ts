import * as bcrypt from 'bcryptjs';
import { probit } from 'simple-statistics';

const SALT = 12;

export const encrypt = async (password: string) => {
    return await bcrypt.hash(password, SALT);
}

export const getRating = (upvotes, n = 0, confidence = 0.95) => {
    if (n === 0) return 0

    // for performance purposes you might consider memoize the calcuation for z
    const z = probit(1-(1-confidence)/2)

    // p̂, the fraction of upvotes
    const phat = 1.0 * upvotes / n

    return (phat + z*z / (2*n) - z * Math.sqrt((phat * (1 - phat) + z*z / (4*n)) / n)) / (1 + z*z/n)

}