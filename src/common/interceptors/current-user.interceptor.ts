import {BadRequestException, CallHandler, ExecutionContext, Injectable, NestInterceptor} from "@nestjs/common";
import * as console from "console";
import {UserService} from "../../modules/user/user.service";

@Injectable()
export class CurrentUserInterceptor implements NestInterceptor {

    constructor(private userService: UserService) {}

    async intercept(context: ExecutionContext, handler: CallHandler) {
        const request = context.switchToHttp().getRequest();

        try {
            request.currentUser = await this.userService.getByLogin(request.user?.login);
        } catch (error) {
            console.log("Error", error);

            throw new BadRequestException();
        }

        return handler.handle();
    }
}