import { ArgumentsHost, Catch, ExceptionFilter, HttpException, HttpStatus } from "@nestjs/common";
import {LogService} from "../../modules/log/log.service";
import {HttpAdapterHost} from "@nestjs/core";

@Catch()
export class GlobalExceptionsFilter implements ExceptionFilter {
    constructor(
        private logService: LogService,
        private httpAdapterHost: HttpAdapterHost) {
    }

    catch(exception: HttpException, host: ArgumentsHost) {
        const { httpAdapter } = this.httpAdapterHost;

        const ctx = host.switchToHttp();

        const httpStatus =
            exception instanceof HttpException
                ? exception.getStatus()
                : HttpStatus.INTERNAL_SERVER_ERROR;

        const responseBody = {
            status: false,
            statusCode: httpStatus,
            errorMessage: exception.stack,
            error: exception.message,
            path: httpAdapter.getRequestUrl(ctx.getRequest()),
        };

        this.logService.error(JSON.stringify(responseBody));

        httpAdapter.reply(ctx.getResponse(), responseBody, httpStatus);
    }
}
