import { ArgumentsHost, Catch, ExceptionFilter, HttpException } from "@nestjs/common";
import {Request, Response} from 'express';
import {LogService} from "../../modules/log/log.service";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    constructor(private logService: LogService) {
    }

    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception.getStatus();

        const responseBody = {
            name: exception.name,
            message: exception.message,
            statusCode: status,
            timestamp: new Date().toISOString(),
            path: request.url,
        };

        if (responseBody.statusCode >= 500) {
            responseBody['trace'] = exception.stack;

            this.logService.error(JSON.stringify(responseBody));
        }

        response
            .status(status)
            .json(responseBody);
    }
}
